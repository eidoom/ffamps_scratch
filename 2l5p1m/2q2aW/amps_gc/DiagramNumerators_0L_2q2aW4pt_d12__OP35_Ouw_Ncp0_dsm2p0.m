DiagramTopologies = {topo[]};

DiagramNumerator["+-31",topo[]] = INT[(-2*ex[1]*(-2*ex[2]*ex[3]*ex[4] + ex[4]^2 + ex[3]*ex[4]^2 + ex[2]*ex[3]*ex[4]*ex[5] + ex[2]*ex[3]*ex[6] - ex[4]*ex[6] - ex[3]*ex[4]*ex[6]))/ex[4], {}, topo[]];
DiagramNumerator["+-32",topo[]] = 0;
DiagramNumerator["+-33",topo[]] = INT[-2*ex[1]*(ex[2] + ex[2]*ex[3] - ex[4] + ex[2]*ex[4] - ex[3]*ex[4] - ex[2]*ex[3]*ex[4] + ex[2]*ex[3]*ex[4]*ex[5] - ex[2]*ex[6]), {}, topo[]];
DiagramNumerator["+-3X",topo[]] = INT[2*ex[1]*(1 + ex[3])*(ex[2] - ex[4])*(1 + ex[4] - ex[6]), {}, topo[]];
DiagramNumerator["+-41",topo[]] = 0;
DiagramNumerator["+-42",topo[]] = 0;
DiagramNumerator["+-43",topo[]] = INT[1 + ex[4] - ex[6], {}, topo[]];
DiagramNumerator["+-4X",topo[]] = INT[-1 - ex[4] + ex[6], {}, topo[]];
DiagramNumerator["+-+1",topo[]] = INT[(-2*ex[1]*(-2*ex[2]*ex[3]*ex[4] + ex[4]^2 + ex[3]*ex[4]^2 + ex[2]*ex[3]*ex[4]*ex[5] + ex[2]*ex[3]*ex[6] - ex[4]*ex[6] - ex[3]*ex[4]*ex[6]))/ex[4], {}, topo[]];
DiagramNumerator["+-+2",topo[]] = 0;
DiagramNumerator["+-+3",topo[]] = INT[-2*ex[1]*(ex[2] + ex[2]*ex[3] - ex[4] + ex[2]*ex[4] - ex[3]*ex[4] - ex[2]*ex[3]*ex[4] + ex[2]*ex[3]*ex[4]*ex[5] - ex[2]*ex[6]), {}, topo[]];
DiagramNumerator["+-+X",topo[]] = INT[2*ex[1]*(1 + ex[3])*(ex[2] - ex[4])*(1 + ex[4] - ex[6]), {}, topo[]];
DiagramNumerator["+--1",topo[]] = 0;
DiagramNumerator["+--2",topo[]] = 0;
DiagramNumerator["+--3",topo[]] = INT[(-2*ex[4]*(1 + ex[4] - ex[6])^2)/(ex[1]^2*(ex[2]*ex[3] - ex[4] - ex[3]*ex[4] - ex[2]*ex[3]*ex[4] + ex[2]*ex[3]*ex[4]*ex[5])*(-2*ex[2]*ex[3]*ex[4] + ex[4]^2 + ex[3]*ex[4]^2 + ex[2]*ex[3]*ex[4]*ex[5] + ex[2]*ex[3]*ex[6] - ex[4]*ex[6] - ex[3]*ex[4]*ex[6])), {}, topo[]];
DiagramNumerator["+--X",topo[]] = INT[(2*ex[4]*(1 + ex[4] - ex[6])^2)/(ex[1]^2*(ex[2]*ex[3] - ex[4] - ex[3]*ex[4] - ex[2]*ex[3]*ex[4] + ex[2]*ex[3]*ex[4]*ex[5])*(-2*ex[2]*ex[3]*ex[4] + ex[4]^2 + ex[3]*ex[4]^2 + ex[2]*ex[3]*ex[4]*ex[5] + ex[2]*ex[3]*ex[6] - ex[4]*ex[6] - ex[3]*ex[4]*ex[6])), {}, topo[]];
