(* Created with the Wolfram Language : www.wolfram.com *)
{{-(Ql*((2*ex[2]*ex[3]*ex[4]*(ex[5]*(ex[6] - ex[7]) - ex[6]*ex[8])*
        (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
         ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
       (ex[1]*ex[5]^2*(ex[2]*ex[3]*ex[6] + 
         ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))*
        (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])) + 
      (InvCMWsq*(1 + ex[3])*ex[4]*((1 + ex[3])*ex[5]^2 + 
         ex[2]*ex[3]*ex[6]*ex[8] - ex[5]*(-1 + ex[2]*ex[3]*
            (1 + ex[6] - ex[7]) + ex[8] + ex[3]*ex[8]))*
        (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
         ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
       (ex[5]^2*(ex[2]*ex[3]*ex[6] + 
         ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))))*Wprop[s[3, 5, 6]]) - 
   Qw*(-((InvCMWsq*ex[1]*ex[3]*(1 + ex[3])*ex[4]*
        (-(ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
         ex[2]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8]))*
        (-((1 + ex[3])*ex[5]^2*(ex[5]*(-1 + ex[7]) + ex[8])*
           (1 + (1 + ex[3])*ex[5] - (1 + ex[3])*ex[8])) + 
         ex[2]^2*ex[3]^2*(-(ex[5]^2*(1 + ex[6] - ex[7])*(-1 + ex[7])) - 
           ex[5]*ex[6]*(2 + ex[6] - 2*ex[7])*ex[8] + ex[6]^2*ex[8]^2) + 
         ex[2]*ex[3]*ex[5]*((1 + ex[3])*ex[5]^2*(2 + ex[6] - ex[7])*
            (-1 + ex[7]) + ex[6]*ex[8]*(1 - 2*(1 + ex[3])*ex[8]) + 
           ex[5]*(-1 + ex[7] + (1 + ex[3])*(2 + 3*ex[6])*ex[8] - 
             (1 + ex[3])*(2 + ex[6])*ex[7]*ex[8]))))/
       (ex[5]^3*(ex[2]*ex[3]*ex[6] + 
         ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7])))))) - 
     (ex[4]*(ex[2]^2*ex[3]^2*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])*
         (ex[5]*(1 + 3*ex[6] + ex[3]*(1 + ex[6] - ex[7]) - 3*ex[7]) - 
          (3 + ex[3])*ex[6]*ex[8]) + (1 + ex[3])^2*ex[5]^2*
         ((1 + ex[3])*ex[5]^2*(-1 + ex[7]) - ex[8]*(-1 + 2*ex[6] + 
            (1 + ex[3])*ex[8]) + ex[5]*(-1 + 2*ex[6] + 2*ex[8] + 
            2*ex[3]*ex[8] - ex[7]*(1 + ex[8] + ex[3]*ex[8]))) - 
        ex[2]*ex[3]*(1 + ex[3])*ex[5]*(ex[5]^2*(2 + 3*ex[6] + 
            ex[3]*(2 + ex[6] - ex[7]) - 3*ex[7])*(-1 + ex[7]) - 
          ex[6]*ex[8]*(-1 + 2*ex[6] + 2*(2 + ex[3])*ex[8]) + 
          ex[5]*(-1 + 2*ex[6]^2 + ex[7] + 2*ex[8] + 2*ex[3]*ex[8] - 
            2*(2 + ex[3])*ex[7]*ex[8] + ex[6]*((7 + 3*ex[3])*ex[8] - 
              ex[7]*(2 + (3 + ex[3])*ex[8]))))))/
      (ex[5]^2*(ex[2]*ex[3]*ex[6] + 
        ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))))*Wprop[s[5, 6]]*
    Wprop[s[3, 5, 6]], 
  -(Ql*((2*(1 + (1 + ex[2])*ex[3])*ex[4]*(-1 + ex[6])*
        ((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8]) - 
         ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
       (ex[1]*ex[5]*(ex[2]*ex[3]*ex[6] + 
         ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))*
        (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])) + 
      (InvCMWsq*(1 + ex[3])*ex[4]*(-((1 + ex[3])*ex[5]) + 
         ex[2]^2*ex[3]*ex[6] + ex[2]*(1 - (1 + ex[3])*ex[5] + ex[3]*ex[6]))*
        (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
         ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
       (ex[2]*ex[5]*(ex[2]*ex[3]*ex[6] + 
         ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))))*Wprop[s[3, 5, 6]]) - 
   Qw*(-((InvCMWsq*ex[1]*ex[3]*(1 + ex[3])*ex[4]*(-((1 + ex[3])*ex[5]) + 
         ex[2]^2*ex[3]*ex[6] + ex[2]*(1 - (1 + ex[3])*ex[5] + ex[3]*ex[6]))*
        (-(ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
         ex[2]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8]))*
        (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
         ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
       (ex[2]*ex[5]^2*(ex[2]*ex[3]*ex[6] + 
         ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7])))))) + 
     (ex[4]*((1 + ex[3])^3*ex[5]^2*(ex[5]*(-1 + ex[7]) + ex[8]) + 
        ex[2]^3*ex[3]^2*(-2 + (3 + ex[3])*ex[6])*(ex[5]*(-1 + ex[7]) + 
          ex[6]*ex[8]) + ex[2]*(1 + ex[3])^2*ex[5]*
         ((1 + ex[3])*ex[5]^2*(-1 + ex[7]) + (1 - 2*(1 + ex[3])*ex[6])*
           ex[8] + ex[5]*(-1 + 2*ex[6] - ex[7] + ex[8] + 
            ex[3]*(1 + ex[6] - ex[7] - ex[6]*ex[7] + ex[8]))) - 
        ex[2]^2*ex[3]*(1 + ex[3])*(ex[5]^2*(-1 + 3*ex[6] + ex[3]*(1 + ex[6]))*
           (-1 + ex[7]) - ex[6]*(-1 + (2 + ex[3])*ex[6])*ex[8] + 
          ex[5]*(-1 + 2*ex[6]^2 + ex[7] - 2*ex[6]*ex[7] - 2*ex[8] + 
            4*ex[6]*ex[8] + ex[3]*ex[6]*(1 - ex[7] + 2*ex[8])))))/
      (ex[2]*ex[5]*(ex[2]*ex[3]*ex[6] + 
        ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))))*Wprop[s[5, 6]]*
    Wprop[s[3, 5, 6]], 
  -(Ql*((2*(1 + ex[3])*ex[4]*(-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + 
            ex[8])) + ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
       (ex[1]*ex[5]*(ex[2]*ex[3]*ex[6] + 
         ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))) - 
      (InvCMWsq*(1 + ex[3])*ex[4]*(-((1 + ex[3])*ex[5]) + 
         ex[2]*(1 + ex[3]*(ex[6] + ex[5]*(-1 + ex[7])) - ex[8]))*
        (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
         ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
       (ex[2]*ex[5]*(ex[2]*ex[3]*ex[6] + 
         ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))))*Wprop[s[3, 5, 6]]) - 
   Qw*((InvCMWsq*ex[1]*ex[3]*(1 + ex[3])*ex[4]*
       (-(ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
        ex[2]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8]))*
       ((1 + ex[3])^2*ex[5]^2*(ex[5]*(-1 + ex[7]) + ex[8]) + 
        ex[2]^2*ex[3]*(1 + ex[3]*(ex[6] + ex[5]*(-1 + ex[7])) - ex[8])*
         (ex[5]*(-1 + ex[7]) + ex[6]*ex[8]) - ex[2]*(1 + ex[3])*ex[5]*
         (ex[3]*ex[5]^2*(-1 + ex[7])^2 + (1 + 2*ex[3]*ex[6] - ex[8])*ex[8] + 
          ex[5]*(-1 + ex[7])*(1 - ex[8] + ex[3]*(1 + ex[6] + ex[8])))))/
      (ex[2]*ex[5]^2*(ex[2]*ex[3]*ex[6] + 
        ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))) - 
     ((1 + ex[3])*ex[4]*((1 + ex[3])^2*ex[5]^2*(ex[5]*(-1 + ex[7]) + ex[8]) + 
        ex[2]^2*ex[3]*(-1 + (2 + ex[3])*ex[6] + (2 + ex[3])*ex[5]*
           (-1 + ex[7]) + ex[8])*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8]) - 
        ex[2]*(1 + ex[3])*ex[5]*(ex[3]*ex[5]^2*(-1 + ex[7])^2 + 
          ex[8]*(-1 + 2*(1 + ex[3])*ex[6] + ex[8]) + ex[5]*(-1 + ex[7])*
           (1 + ex[8] + ex[3]*(1 + ex[6] + ex[8])))))/
      (ex[2]*ex[5]*(ex[2]*ex[3]*ex[6] + 
        ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))))*Wprop[s[5, 6]]*
    Wprop[s[3, 5, 6]], 
  -1/2*(Ql*((-2*InvCMWsq*ex[1]*(1 + ex[3])*ex[4]*((1 + ex[3])*ex[5] - 
          ex[2]*ex[3]*ex[6])*((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + 
            ex[8]) - ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
        (ex[5]*(ex[2]*ex[3]*ex[6] + ex[5]*(-1 + 
            ex[3]*(-1 + ex[2]*(-1 + ex[7]))))) + 
       (4*ex[2]*ex[3]*ex[4]*(-1 + ex[6])*
         (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
          ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
        (ex[5]*(ex[2]*ex[3]*ex[6] + ex[5]*(-1 + 
            ex[3]*(-1 + ex[2]*(-1 + ex[7]))))*(-1 + ex[6] + 
          ex[5]*(-1 + ex[7]) + ex[8])) + 
       ex[5]*((-2*InvCMWsq*ex[1]*(1 + ex[3])*ex[4]*((1 + ex[3])*ex[5] - 
            ex[2]*ex[3]*ex[6])*((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + 
              ex[8]) - ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
          (ex[5]*(ex[2]*ex[3]*ex[6] + ex[5]*(-1 + ex[3]*(-1 + 
                ex[2]*(-1 + ex[7]))))) + (4*ex[2]*ex[3]*ex[4]*(-1 + ex[6])*
           (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
            ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
          (ex[5]*(ex[2]*ex[3]*ex[6] + ex[5]*(-1 + ex[3]*(-1 + 
                ex[2]*(-1 + ex[7]))))*(-1 + ex[6] + ex[5]*(-1 + ex[7]) + 
            ex[8]))) - ex[8]*((-2*InvCMWsq*ex[1]*(1 + ex[3])*ex[4]*
           ((1 + ex[3])*ex[5] - ex[2]*ex[3]*ex[6])*
           ((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8]) - 
            ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
          (ex[5]*(ex[2]*ex[3]*ex[6] + ex[5]*(-1 + ex[3]*(-1 + 
                ex[2]*(-1 + ex[7]))))) + (4*ex[2]*ex[3]*ex[4]*(-1 + ex[6])*
           (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
            ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
          (ex[5]*(ex[2]*ex[3]*ex[6] + ex[5]*(-1 + ex[3]*(-1 + 
                ex[2]*(-1 + ex[7]))))*(-1 + ex[6] + ex[5]*(-1 + ex[7]) + 
            ex[8]))) + ex[1]^2*ex[5]*((-4*(1 + (1 + ex[2])*ex[3])*ex[4]*
           (ex[5]*(ex[6] - ex[7]) - ex[6]*ex[8])*
           (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
            ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
          (ex[1]^2*ex[5]^2*(ex[2]*ex[3]*ex[6] + 
            ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))*
           (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])) + 
         (2*InvCMWsq*(1 + ex[2])*(1 + ex[3])*ex[4]*
           (-((1 + ex[3])*ex[5]*(ex[5] - ex[8])) + ex[2]*ex[3]*
             (ex[5]*(1 + ex[6] - ex[7]) - ex[6]*ex[8]))*
           (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
            ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
          (ex[1]*ex[2]*ex[5]^2*(ex[2]*ex[3]*ex[6] + 
            ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7])))))))*
      Wprop[s[3, 5, 6]])/ex[1] - 
   (Qw*((-2*InvCMWsq*ex[1]^2*ex[3]*(1 + ex[3])*ex[4]*((1 + ex[3])*ex[5] - 
         ex[2]*ex[3]*ex[6])*(-(ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
         ex[2]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8]))*
        (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
         ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
       (ex[5]^2*(ex[2]*ex[3]*ex[6] + 
         ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))) - 
      (2*ex[1]*ex[4]*((1 + ex[3])^2*ex[5]^2*(-2 + 2*ex[6] + 
           (1 + ex[3])*ex[5]*(-1 + ex[7]) + ex[8] + ex[3]*ex[8]) + 
         ex[2]^2*ex[3]^2*(-2 + (3 + ex[3])*ex[6])*(ex[5]*(-1 + ex[7]) + 
           ex[6]*ex[8]) - ex[2]*ex[3]*(1 + ex[3])*ex[5]*
          (ex[5]*(-1 + 3*ex[6] + ex[3]*(1 + ex[6]))*(-1 + ex[7]) + 
           2*(ex[6]^2 - ex[8] + ex[6]*(-1 + (2 + ex[3])*ex[8])))))/
       (ex[5]*(ex[2]*ex[3]*ex[6] + ex[5]*
          (-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))) + 
      ex[5]*((-2*InvCMWsq*ex[1]^2*ex[3]*(1 + ex[3])*ex[4]*
          ((1 + ex[3])*ex[5] - ex[2]*ex[3]*ex[6])*
          (-(ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
           ex[2]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8]))*
          (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
           ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
         (ex[5]^2*(ex[2]*ex[3]*ex[6] + ex[5]*(-1 + ex[3]*(-1 + ex[2]*
                (-1 + ex[7]))))) - (2*ex[1]*ex[4]*
          ((1 + ex[3])^2*ex[5]^2*(-2 + 2*ex[6] + (1 + ex[3])*ex[5]*
              (-1 + ex[7]) + ex[8] + ex[3]*ex[8]) + ex[2]^2*ex[3]^2*
            (-2 + (3 + ex[3])*ex[6])*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8]) - 
           ex[2]*ex[3]*(1 + ex[3])*ex[5]*(ex[5]*(-1 + 3*ex[6] + ex[3]*
                (1 + ex[6]))*(-1 + ex[7]) + 2*(ex[6]^2 - ex[8] + ex[6]*
                (-1 + (2 + ex[3])*ex[8])))))/(ex[5]*(ex[2]*ex[3]*ex[6] + 
           ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7])))))) - 
      ex[8]*((-2*InvCMWsq*ex[1]^2*ex[3]*(1 + ex[3])*ex[4]*
          ((1 + ex[3])*ex[5] - ex[2]*ex[3]*ex[6])*
          (-(ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
           ex[2]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8]))*
          (-((1 + ex[3])*ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
           ex[2]*ex[3]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/
         (ex[5]^2*(ex[2]*ex[3]*ex[6] + ex[5]*(-1 + ex[3]*(-1 + ex[2]*
                (-1 + ex[7]))))) - (2*ex[1]*ex[4]*
          ((1 + ex[3])^2*ex[5]^2*(-2 + 2*ex[6] + (1 + ex[3])*ex[5]*
              (-1 + ex[7]) + ex[8] + ex[3]*ex[8]) + ex[2]^2*ex[3]^2*
            (-2 + (3 + ex[3])*ex[6])*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8]) - 
           ex[2]*ex[3]*(1 + ex[3])*ex[5]*(ex[5]*(-1 + 3*ex[6] + ex[3]*
                (1 + ex[6]))*(-1 + ex[7]) + 2*(ex[6]^2 - ex[8] + ex[6]*
                (-1 + (2 + ex[3])*ex[8])))))/(ex[5]*(ex[2]*ex[3]*ex[6] + 
           ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7])))))) + 
      ex[1]^2*ex[5]*((-2*InvCMWsq*(1 + ex[2])*ex[3]*(1 + ex[3])*ex[4]*
          (-(ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
           ex[2]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8]))*
          ((1 + ex[3])^2*ex[5]^2*(ex[5] - ex[8])*(ex[5]*(-1 + ex[7]) + 
             ex[8]) - ex[2]*ex[3]*(1 + ex[3])*ex[5]*
            (ex[5]^2*(2 + ex[6] - ex[7])*(-1 + ex[7]) + 
             ex[5]*(2 - ex[6]*(-3 + ex[7]) - 2*ex[7])*ex[8] - 
             2*ex[6]*ex[8]^2) + ex[2]^2*ex[3]^2*(ex[5]^2*(1 + ex[6] - ex[7])*
              (-1 + ex[7]) + ex[5]*ex[6]*(2 + ex[6] - 2*ex[7])*ex[8] - 
             ex[6]^2*ex[8]^2)))/(ex[2]*ex[5]^3*(ex[2]*ex[3]*ex[6] + 
           ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7]))))) + 
        (2*ex[4]*((1 + ex[3])^3*ex[5]^2*(ex[5] - ex[8])*(ex[5]*(-1 + ex[7]) + 
             ex[8]) + ex[2]^3*ex[3]^2*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])*
            (ex[5]*(1 + 3*ex[6] + ex[3]*(1 + ex[6] - ex[7]) - 3*ex[7]) - 
             (3 + ex[3])*ex[6]*ex[8]) + ex[2]*(1 + ex[3])^2*ex[5]*
            ((ex[5] - ex[8])*(ex[5]^2*(-1 + ex[7]) - 2*ex[6]*ex[8] + ex[5]*
                (2*ex[6] - 2*ex[7] + ex[8])) + ex[3]*(ex[5]^3*(-1 + ex[7]) + 
               ex[5]^2*(-(ex[6]*(-1 + ex[7])) + (-2 + ex[7])*(-1 + ex[7] - 
                   ex[8])) + 2*ex[6]*ex[8]^2 - ex[5]*ex[8]*
                (2 - ex[6]*(-3 + ex[7]) - 2*ex[7] + ex[8]))) - 
           ex[2]^2*ex[3]*(1 + ex[3])*(ex[5]^3*(2 + 3*ex[6] - 3*ex[7])*
              (-1 + ex[7]) + 2*ex[6]^2*ex[8]^2 - 2*ex[5]*ex[6]*ex[8]*
              (1 + 2*ex[6] - 2*ex[7] + 2*ex[8]) + ex[3]*(ex[5]^3*
                (2 + ex[6] - ex[7])*(-1 + ex[7]) + ex[6]^2*ex[8]^2 - ex[5]*
                ex[6]*ex[8]*(2 + ex[6] - 2*ex[7] + 2*ex[8]) + ex[5]^2*
                (ex[6] + (-1 + ex[7])*(-1 + ex[7] - 2*ex[8]) + 3*ex[6]*
                  ex[8] - ex[6]*ex[7]*(1 + ex[8]))) + 
             ex[5]^2*(2*ex[6]^2 + 2*(ex[7]^2 + ex[8] - ex[7]*(1 + 2*ex[8])) + 
               ex[6]*(2 + 7*ex[8] - ex[7]*(4 + 3*ex[8]))))))/
         (ex[1]*ex[2]*ex[5]^2*(ex[2]*ex[3]*ex[6] + 
           ex[5]*(-1 + ex[3]*(-1 + ex[2]*(-1 + ex[7])))))))*Wprop[s[5, 6]]*
     Wprop[s[3, 5, 6]])/(2*ex[1])}, 
 {-(Ql*(-((ex[1]*ex[2]*ex[3]*ex[4]*(ex[5]*(ex[6] - ex[7]) - ex[6]*ex[8]))/
        ex[5]) - (InvCMWsq*ex[1]^2*(1 + ex[3])*ex[4]*
        (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])*((1 + ex[3])*ex[5]^2 + 
         ex[2]*ex[3]*ex[6]*ex[8] - ex[5]*(-1 + ex[2]*ex[3]*
            (1 + ex[6] - ex[7]) + ex[8] + ex[3]*ex[8])))/(2*ex[5]))*
     Wprop[s[3, 5, 6]]) - 
   Qw*((InvCMWsq*ex[1]^3*ex[3]*(1 + ex[3])*ex[4]*(-1 + ex[6] + 
        ex[5]*(-1 + ex[7]) + ex[8])*((1 + ex[3])*ex[5]^2 + 
        ex[2]*ex[3]*ex[6]*ex[8] - ex[5]*(-1 + ex[2]*ex[3]*
           (1 + ex[6] - ex[7]) + ex[8] + ex[3]*ex[8]))*
       (-(ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
        ex[2]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/(2*ex[5]^2) - 
     (ex[1]^2*ex[4]*((1 + ex[3])^2*ex[5]^3*(-1 + ex[7]) + 
        ex[2]*ex[3]*ex[6]*ex[8]*(-3 + ex[6] + 3*ex[8] + 
          ex[3]*(-1 - ex[6] + ex[8])) + ex[5]^2*
         (ex[6] - (-2 + ex[7])*(-1 + ex[8]) + 
          ex[3]*(-3 - ex[2]*(1 + ex[6] - ex[7])*(-1 + ex[7]) + 
            ex[7]*(3 - 2*ex[8]) + 4*ex[8]) + ex[3]^2*(-1 - ex[6] + 
            ex[2]*(-1 + ex[6] - ex[7])*(-1 + ex[7]) + 2*ex[7] + 2*ex[8] - 
            ex[7]*ex[8])) + ex[5]*(-((-1 + ex[8])*(-1 + ex[6] + ex[8])) + 
          ex[3]^2*((1 + ex[6] - ex[8])*ex[8] + ex[2]*(ex[6]^2 + 
              (-1 + ex[7])*(-1 + ex[8]) - ex[6]*ex[7]*(1 + ex[8]))) + 
          ex[3]*(-1 + ex[6] + 3*ex[8] - 2*ex[8]^2 + 
            ex[2]*(-ex[6]^2 + (-1 + 3*ex[7])*(-1 + ex[8]) + ex[6]*(2 + 
                ex[7] - 4*ex[8] + ex[7]*ex[8]))))))/(2*ex[5]))*Wprop[s[5, 6]]*
    Wprop[s[3, 5, 6]], 
  -(Ql*(ex[1]*(1 + (1 + ex[2])*ex[3])*ex[4]*(-1 + ex[6]) - 
      (InvCMWsq*ex[1]^2*(1 + ex[3])*ex[4]*(-((1 + ex[3])*ex[5]) + 
         ex[2]^2*ex[3]*ex[6] + ex[2]*(1 - (1 + ex[3])*ex[5] + ex[3]*ex[6]))*
        (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8]))/(2*ex[2]))*
     Wprop[s[3, 5, 6]]) - 
   Qw*((InvCMWsq*ex[1]^3*ex[3]*(1 + ex[3])*ex[4]*(-((1 + ex[3])*ex[5]) + 
        ex[2]^2*ex[3]*ex[6] + ex[2]*(1 - (1 + ex[3])*ex[5] + ex[3]*ex[6]))*
       (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])*
       (-(ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
        ex[2]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/(2*ex[2]*ex[5]) + 
     (ex[1]^2*ex[4]*(ex[2]^2*ex[3]*(-ex[6]^2 + 
          ex[3]*(ex[5]*(-2 + ex[6])*(-1 + ex[7]) + ex[6]*(-1 + ex[6] - 
              ex[8])) + ex[6]*(3 + ex[5] - ex[5]*ex[7] - 3*ex[8]) + 
          2*(-1 + ex[8])) + (1 + ex[3])^2*ex[5]*(1 - ex[6] + 
          ex[5]*(-1 + ex[7]) + ex[8]) + ex[2]*(1 + ex[3])*
         (-1 + ex[6] + ex[5]^2*(-1 + ex[7]) + ex[8] - 2*ex[6]*ex[8] + 
          ex[5]*(ex[6] - ex[7] + ex[8]) + ex[3]*(ex[5]^2*(-1 + ex[7]) + 
            ex[6]*(-1 + ex[6] - ex[8]) + ex[5]*(3 + ex[6]*(-2 + ex[7]) - 
              2*ex[7] + ex[8])))))/(2*ex[2]))*Wprop[s[5, 6]]*
    Wprop[s[3, 5, 6]], 
  -(Ql*(-(ex[1]*(1 + ex[3])*ex[4]*(-1 + ex[6] + ex[5]*(-1 + ex[7]) + 
         ex[8])) + (InvCMWsq*ex[1]^2*(1 + ex[3])*ex[4]*
        (-((1 + ex[3])*ex[5]) + ex[2]*(1 + ex[3]*(ex[6] + 
             ex[5]*(-1 + ex[7])) - ex[8]))*(-1 + ex[6] + ex[5]*(-1 + ex[7]) + 
         ex[8]))/(2*ex[2]))*Wprop[s[3, 5, 6]]) - 
   Qw*(-1/2*(ex[1]^2*(1 + ex[3])*ex[4]*(-((1 + ex[3])*ex[5]) + 
         ex[2]*(1 + ex[3]*(ex[6] + ex[5]*(-1 + ex[7])) - ex[8]))*
        (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8]))/ex[2] + 
     (InvCMWsq*ex[1]^3*ex[3]*(1 + ex[3])*ex[4]*(-((1 + ex[3])*ex[5]) + 
        ex[2]*(1 + ex[3]*(ex[6] + ex[5]*(-1 + ex[7])) - ex[8]))*
       (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])*
       (ex[5]*(ex[5]*(-1 + ex[7]) + ex[8]) + ex[2]*(ex[5] - ex[5]*ex[7] - 
          ex[6]*ex[8])))/(2*ex[2]*ex[5]))*Wprop[s[5, 6]]*Wprop[s[3, 5, 6]], 
  -1/2*(Ql*(-2*ex[1]^2*ex[2]*ex[3]*ex[4]*(-1 + ex[6]) - 
       InvCMWsq*ex[1]^3*(1 + ex[3])*ex[4]*((1 + ex[3])*ex[5] - 
         ex[2]*ex[3]*ex[6])*(-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8]) + 
       ex[5]*(-2*ex[1]^2*ex[2]*ex[3]*ex[4]*(-1 + ex[6]) - 
         InvCMWsq*ex[1]^3*(1 + ex[3])*ex[4]*((1 + ex[3])*ex[5] - 
           ex[2]*ex[3]*ex[6])*(-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])) - 
       ex[8]*(-2*ex[1]^2*ex[2]*ex[3]*ex[4]*(-1 + ex[6]) - 
         InvCMWsq*ex[1]^3*(1 + ex[3])*ex[4]*((1 + ex[3])*ex[5] - 
           ex[2]*ex[3]*ex[6])*(-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])) + 
       ex[1]^2*ex[5]*((2*(1 + (1 + ex[2])*ex[3])*ex[4]*
           (ex[5]*(ex[6] - ex[7]) - ex[6]*ex[8]))/ex[5] - 
         (InvCMWsq*ex[1]*(1 + ex[2])*(1 + ex[3])*ex[4]*(-1 + ex[6] + 
            ex[5]*(-1 + ex[7]) + ex[8])*(-((1 + ex[3])*ex[5]*
              (ex[5] - ex[8])) + ex[2]*ex[3]*(ex[5]*(1 + ex[6] - ex[7]) - 
              ex[6]*ex[8])))/(ex[2]*ex[5])))*Wprop[s[3, 5, 6]])/ex[1] - 
   (Qw*(-((InvCMWsq*ex[1]^4*ex[3]*(1 + ex[3])*ex[4]*((1 + ex[3])*ex[5] - 
          ex[2]*ex[3]*ex[6])*(-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])*
         (ex[5]*(ex[5]*(-1 + ex[7]) + ex[8]) + ex[2]*(ex[5] - ex[5]*ex[7] - 
            ex[6]*ex[8])))/ex[5]) - ex[1]^3*ex[4]*
       (ex[2]*ex[3]*(-ex[6]^2 + ex[3]*(ex[5]*(-2 + ex[6])*(-1 + ex[7]) + 
            ex[6]*(-1 + ex[6] - ex[8])) + ex[6]*(3 + ex[5] - ex[5]*ex[7] - 
            3*ex[8]) + 2*(-1 + ex[8])) + (1 + ex[3])*ex[5]*
         (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8] + 
          ex[3]*(1 - ex[6] + ex[5]*(-1 + ex[7]) + ex[8]))) + 
      ex[5]*(-((InvCMWsq*ex[1]^4*ex[3]*(1 + ex[3])*ex[4]*((1 + ex[3])*ex[5] - 
            ex[2]*ex[3]*ex[6])*(-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])*
           (ex[5]*(ex[5]*(-1 + ex[7]) + ex[8]) + ex[2]*(ex[5] - ex[5]*ex[7] - 
              ex[6]*ex[8])))/ex[5]) - ex[1]^3*ex[4]*
         (ex[2]*ex[3]*(-ex[6]^2 + ex[3]*(ex[5]*(-2 + ex[6])*(-1 + ex[7]) + 
              ex[6]*(-1 + ex[6] - ex[8])) + ex[6]*(3 + ex[5] - ex[5]*ex[7] - 
              3*ex[8]) + 2*(-1 + ex[8])) + (1 + ex[3])*ex[5]*
           (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8] + 
            ex[3]*(1 - ex[6] + ex[5]*(-1 + ex[7]) + ex[8])))) - 
      ex[8]*(-((InvCMWsq*ex[1]^4*ex[3]*(1 + ex[3])*ex[4]*((1 + ex[3])*ex[5] - 
            ex[2]*ex[3]*ex[6])*(-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])*
           (ex[5]*(ex[5]*(-1 + ex[7]) + ex[8]) + ex[2]*(ex[5] - ex[5]*ex[7] - 
              ex[6]*ex[8])))/ex[5]) - ex[1]^3*ex[4]*
         (ex[2]*ex[3]*(-ex[6]^2 + ex[3]*(ex[5]*(-2 + ex[6])*(-1 + ex[7]) + 
              ex[6]*(-1 + ex[6] - ex[8])) + ex[6]*(3 + ex[5] - ex[5]*ex[7] - 
              3*ex[8]) + 2*(-1 + ex[8])) + (1 + ex[3])*ex[5]*
           (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8] + 
            ex[3]*(1 - ex[6] + ex[5]*(-1 + ex[7]) + ex[8])))) + 
      ex[1]^2*ex[5]*((InvCMWsq*ex[1]^2*(1 + ex[2])*ex[3]*(1 + ex[3])*ex[4]*
          (-1 + ex[6] + ex[5]*(-1 + ex[7]) + ex[8])*
          (-((1 + ex[3])*ex[5]*(ex[5] - ex[8])) + ex[2]*ex[3]*
            (ex[5]*(1 + ex[6] - ex[7]) - ex[6]*ex[8]))*
          (-(ex[5]*(ex[5]*(-1 + ex[7]) + ex[8])) + 
           ex[2]*(ex[5]*(-1 + ex[7]) + ex[6]*ex[8])))/(ex[2]*ex[5]^2) + 
        (ex[1]*ex[4]*((1 + ex[3])^2*ex[5]*(ex[5]^2*(-1 + ex[7]) - 
             ex[5]*(1 + ex[6] + ex[7]*(-2 + ex[8]) - 2*ex[8]) + 
             (1 + ex[6] - ex[8])*ex[8]) + ex[2]^2*ex[3]*
            (-(ex[5]^2*(-1 + ex[7])*(1 + ex[6] - ex[7] + ex[3]*(1 - ex[6] + 
                  ex[7]))) + ex[6]*ex[8]*(-3 + ex[6] + 3*ex[8] + ex[3]*
                (-1 - ex[6] + ex[8])) + ex[5]*(-ex[6]^2 + (-1 + 3*ex[7])*
                (-1 + ex[8]) + ex[6]*(2 + ex[7] - 4*ex[8] + ex[7]*ex[8]) + 
               ex[3]*(ex[6]^2 + (-1 + ex[7])*(-1 + ex[8]) - ex[6]*ex[7]*
                  (1 + ex[8])))) + ex[2]*(1 + ex[3])*(ex[5]^3*(-1 + ex[7]) + 
             ex[5]*(ex[6]*(2 - 3*ex[8]) + (2*ex[7] - ex[8])*(-1 + ex[8])) + 
             2*ex[6]*(-1 + ex[8])*ex[8] + ex[5]^2*(-1 + ex[6] - (-2 + ex[7])*
                ex[8]) + ex[3]*(ex[5]^3*(-1 + ex[7]) + ex[6]*ex[8]*
                (-1 - ex[6] + ex[8]) - ex[5]^2*(-2 + ex[7])*(-ex[6] + ex[7] + 
                 ex[8]) + ex[5]*(ex[6]^2 + (-1 + ex[7] - ex[8])*
                  (-1 + ex[8]) + ex[6]*(ex[8] - ex[7]*(1 + ex[8])))))))/
         (ex[2]*ex[5])))*Wprop[s[5, 6]]*Wprop[s[3, 5, 6]])/(2*ex[1])}}
