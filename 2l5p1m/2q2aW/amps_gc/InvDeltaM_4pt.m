(* Created with the Wolfram Language : www.wolfram.com *)
{{-(s[2, 3]/(s[1, 2]*s[1, 3])), s[1, 2]^(-1), s[1, 3]^(-1), 0}, 
 {s[1, 2]^(-1), -(s[1, 3]/(s[1, 2]*s[2, 3])), s[2, 3]^(-1), 0}, 
 {s[1, 3]^(-1), s[2, 3]^(-1), -(s[1, 2]/(s[1, 3]*s[2, 3])), 0}, 
 {0, 0, 0, s[1, 2]/(s[1, 3]*s[2, 3])}}
