{{(-2*cF*Nc*gcusp[0] + 4*eps*Nc*gquark[0] + eps*TR*gcusp[0]*
     ((-1 + Nc^2)*F[1, 2] - F[1, 5] - F[1, 6] - F[1, 7] + Nc^2*F[1, 7] + 
      F[1, 8] + F[1, 9] + 2*im[1, 1]))/(2*eps^2*Nc), 
  (TR*gcusp[0]*(F[1, 5] + F[1, 6] - F[1, 8] - F[1, 9] - 2*im[1, 1]))/
   (2*eps)}, {(TR*(F[1, 2] + F[1, 7] - F[1, 8] - F[1, 9])*gcusp[0])/(2*eps), 
  -1/2*(2*cF*Nc*gcusp[0] - 4*eps*Nc*gquark[0] + eps*TR*gcusp[0]*
      (F[1, 2] - (-1 + Nc^2)*F[1, 5] + F[1, 6] - Nc^2*F[1, 6] + F[1, 7] - 
       F[1, 8] - F[1, 9] - 2*im[1, 1] + 2*Nc^2*im[1, 1]))/(eps^2*Nc)}}
