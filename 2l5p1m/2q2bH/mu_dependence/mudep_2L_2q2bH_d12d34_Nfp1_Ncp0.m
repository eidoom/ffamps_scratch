(-4*F1L["d12d34", "1"]*Log[mu2])/3 + F1L["d14d23", "Nf"]*
  (F[1, 5] + F[1, 6] - F[1, 8] - F[1, 9] - 2*im[1, 1])*Log[mu2] + 
 F1L["d12d34", "Nf/Nc"]*((35/6 + F[1, 5] + F[1, 6] - 2*im[1, 1])*Log[mu2] - 
   Log[mu2]^2) + A0L["d14d23", "1"]*
  ((-10*(F[1, 5] + F[1, 6] - F[1, 8] - F[1, 9] - 2*im[1, 1])*Log[mu2])/9 + 
   (-F[1, 5] - F[1, 6] + F[1, 8] + F[1, 9] + 2*im[1, 1])*Log[mu2]^2) + 
 A0L["d12d34", "1/Nc"]*
  (((-149 - 60*F[1, 5] - 60*F[1, 6] + 120*im[1, 1] - 18*im[1, 1]^2)*Log[mu2])/
    54 + (-41/18 - F[1, 5] - F[1, 6] + 2*im[1, 1])*Log[mu2]^2 + 
   (8*Log[mu2]^3)/9)
