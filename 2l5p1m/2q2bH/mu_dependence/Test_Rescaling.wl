(* ::Package:: *)

Needs["PentagonFunctions`"];
<< "InitTwoLoopToolsFF.m"
<< "InitDiagramsFF.m"
process = "2q2bH";
psmode = "PSanalytic";
Get["setupfiles/Setup_2g2bH.m"];


MomMode = ToExpression[psmode];


RestoreMonoDimensions[x_]:=Module[{norm,in},
in=x /. mono[xx_]:>xx;
norm=in  /. { 
  F[__]:>1,
  re[__]:>1,
  im[__]:>1,
  sqrtDelta5->1/ex[1]^2,
  sqrtG3[y_]:>1/ex[1],
  sqrtSigma5[y_]:>1/ex[1]^2
};
Return[norm*mono[in]];
];


GetMomSquared[mom_List]:=mom[[1]]^2-mom[[2]]^2-mom[[3]]^2-mom[[4]]^2;
Dot4[mom_,i_,j_]:=mom[[i,1]]*mom[[j,1]]-mom[[i,2]]*mom[[j,2]]-mom[[i,3]]*mom[[j,3]]-mom[[i,4]]*mom[[j,4]];
EvalTr[mom_,i1_,i2_,i3_,i4_]:=4*(Dot4[mom,i1,i2]*Dot4[mom,i3,i4]-Dot4[mom,i1,i3]*Dot4[mom,i2,i4]+Dot4[mom,i1,i4]*Dot4[mom,i2,i3]);
EvalTr5[q_,i1_,i2_,i3_,i4_]:=4*I*Sum[Signature[{mu,nu,ro,si}]*q[[i1]][[mu]]*q[[i2]][[nu]]*q[[i3]][[ro]]*q[[i4]][[si]],{mu,1,4},{nu,1,4},{ro,1,4},{si,1,4}];


sijList[i1_,i2_,i3_,i4_,i5_] := {s[i1,i2],s[i2,i3],s[i3,i4],s[i4,i5],s[i1,i5],s[i5]};


EvaluateSijTr[pmom_] := {
  s[i_]:>GetMomSquared[pmom[[i]]],
  s[i_,j_]:>GetMomSquared[pmom[[i]]+pmom[[j]]],
  s[i_,j_,k_]:>GetMomSquared[pmom[[i]]+pmom[[j]]+pmom[[k]]],
  trp[i_,j_,k_,l_]:>(1/2)*EvalTr[pmom,i,j,k,l]+(1/2)*EvalTr5[pmom,i,j,k,l],
  trm[i_,j_,k_,l_]:>(1/2)*EvalTr[pmom,i,j,k,l]-(1/2)*EvalTr5[pmom,i,j,k,l]
};


FromTwistorVariables5pt[ex_,i1_,i2_,i3_,i4_,i5_]:={
ex[1] -> s[i1, i2],
ex[2] -> -(trp[i1, i2, i3, i4]/(s[i1, i2]*s[i3, i4])),
ex[3] -> (s[i4,i1]*trp[i1,i3,i5,i2]-s[i1,i3]*trp[i1, i4, i5, i2])/(s[i1, i3]*trp[i1, i4, i5, i2]),
ex[4] -> s[i2, i3]/s[i1, i2],
ex[5] -> -((s[i1,i2]*trm[i1, i5, i2, i3]+s[i1,i3]*trm[i1, i5, i2, i3]+s[5]*trm[i1, i3, i2, i3])/(s[i2, i3]*trm[i1, i5, i2, i3])),
ex[6] -> s[i1, i2, i3]/s[i1, i2]
};


GetInvariants[mom_List]:=Module[{res},
res={
  s12->GetMomSquared[mom[[1]]+mom[[2]]],
  s23->GetMomSquared[mom[[2]]+mom[[3]]],
  s34->GetMomSquared[mom[[3]]+mom[[4]]],
  s45->GetMomSquared[mom[[4]]+mom[[5]]],
  s15->GetMomSquared[mom[[1]]+mom[[5]]],
  s5->GetMomSquared[mom[[5]]],
  tr5[p[1],p[2],p[3],p[4]]->EvalTr5[mom,1,2,3,4]
};
Return[res];
];


Options[IncreaseMomPrecision]={"NewPrecision"->64,"ModifyPSpoint"->True};
IncreaseMomPrecision[momin_,OptionsPattern[]]:=Module[{momsq2,q,newmsqcheck,newmomentumconservation,psq,momentumconservation,
NewPrecision,qnew,t,t5,t4,tsol,ttsol,tmp,qnew2,qnew3,diff2,diff3,qnew4,diff4,psqlist,msqlist,msqcheck},
momsq2[p_]:=(p[[1]])^2-(p[[2]])^2-(p[[3]])^2-(p[[4]])^2;
(* flip incoming momenta to outgoing *)
q[1]=-momin[[1]];
q[2]=-momin[[2]];
q[3]=momin[[3]];
q[4]=momin[[4]];
q[5]=momin[[5]];
If[!OptionValue["ModifyPSpoint"],
   Return[q/@Range[5]];
];
(* check mom conservation and onshellness *)
psqlist=Abs[momsq2[q[#]]]& /@ Range[1,5];
msqlist=Rationalize[If[#<10^(-5),0,#,#]]& /@ psqlist;
msqcheck=Max[(Abs[momsq2[q[#]]]-msqlist[[#]])& /@ Range[1,5]];
momentumconservation=Max[Abs/@(Plus@@(q/@Range[5]))];
(*Print[msqcheck];
Print[momentumconservation];
Return[];*)
(* *)
NewPrecision = OptionValue["NewPrecision"];
qnew = Join[
Table[SetPrecision[#,NewPrecision]& /@ q[i] /. List[x__,y_]:>List[x,y+t[i]],{i,1,3}] /. {t[1]->0},
{MapIndexed[(#1+t4[#2[[1]]])&,SetPrecision[#,NewPrecision]& /@ q[4]]},
{MapIndexed[(#1+t5[#2[[1]]])&,SetPrecision[#,NewPrecision]& /@ q[5]]}
] /. {t4[1]->0,t4[2]->0};
tsol = Join[
Solve[momsq2[qnew[[2]]]==msqlist[[2]]] /. Rule[a_,b_]:>Nothing /; Abs[b]>1.0,
Solve[momsq2[qnew[[3]]]==msqlist[[3]]] /. Rule[a_,b_]:>Nothing /; Abs[b]>1.0
] // Flatten;
ttsol=Join[
Equal[#,0]& /@ Plus@@(qnew /. tsol),
{momsq2[qnew[[4]]]==msqlist[[4]]},
{momsq2[qnew[[5]]]==msqlist[[5]]}
];
ttsol = Quiet[Solve[ttsol]];
qnew2 = qnew /. tsol /. ttsol[[1]];
qnew3 = qnew /. tsol /. ttsol[[2]];
diff2=Max[Abs/@(Flatten[q/@Range[5]]-Flatten[qnew2])];
diff3=Max[Abs/@(Flatten[q/@Range[5]]-Flatten[qnew3])];
If[diff2<diff3,qnew4=qnew2,qnew4=qnew3];
newmomentumconservation=Max[Plus@@qnew4];
newmsqcheck=Max[(Abs[momsq2[qnew4[[#]]]]-msqlist[[#]])& /@ Range[1,5]];
diff4=Max[Abs/@(Flatten[q/@Range[5]]-Flatten[qnew4])];
If[diff4>10^(-10),
   Print["Problem!!!"];
   Return[$Failed];
];
Print[{{msqcheck,momentumconservation}->{newmsqcheck,newmomentumconservation},diff4}];
Return[qnew4];
];


rootdefs = {
    sqrtDelta5->Sqrt[s12^2 (s15-s23)^2+((s15-s34) s45+s23 (s34-s5))^2+2 s12 (-s15^2 s45+s15 s34 s45+s23 s34 (s45-2 s5)-s23^2 (s34+s5)+s15 s23 (s34+s45+s5))],
    sqrtG3[1]->Sqrt[s12^2+(s34-s5)^2-2 s12 (s34+s5)],
    sqrtG3[2]->Sqrt[(s12+s15-s34-s45)^2+2 (s12-s15+2 s23+s34-s45) s5+s5^2],
    sqrtG3[3]->Sqrt[(s15+s45)^2-4 s23 s5],
    sqrtSigma5[1]->Sqrt[s12^2 (s15-s23)^2+(s23 s34+(s15-s34) s45)^2+2 s12 (s15 s23 s34-s15^2 s45+s15 (s23+s34) s45+s23 s34 (-s23+s45))],
    sqrtSigma5[2]->Sqrt[s12^2 (-s15+s23+s5)^2+(s23 s34+s15 s45-s45 (s34+s5))^2-2 s12 (s23^2 s34+s15^2 s45-s23 s34 s45+s23 (s34+s45) s5+s45 s5 (s34+s5)-s15 (s23 (s34+s45)+s45 (s34+2 s5)))],
    sqrtSigma5[3]->Sqrt[(s15-s34)^2 s45^2+2 s23 (s15-s34) s45 (s34-s5)+(s12 (s15-s23)+s23 s34-(s12+s23) s5)^2-2 s12 s45 (s15^2-s23 s34+2 s23 s5+s34 s5-s15 (s23+s34+s5))],
    sqrtSigma5[4]->Sqrt[s12^2 (s15-s23)^2+(s23 s34+(s15-s34) (s45-s5))^2-2 s12 (-s15 (s23 s34+(s23+s34) s45)+s15^2 (s45-s5)+s15 (s23+s34) s5+s23 s34 (s23-s45+s5))],
    sqrtSigma5[5]->Sqrt[s12^2 (s15-s23)^2+(s23 s34+(s15-s34) s45-(s15+s45) s5+s5^2)^2+2 s12 (s15 s23 s34-s23^2 s34-s15^2 s45+s15 s23 s45+s15 s34 s45+s23 s34 s45+(s15-s23-2 s34) (s15+s45) s5+(-s15+s23+2 s34) s5^2)],
    sqrtSigma5[6]->Sqrt[s12^2 (s15-s23)^2+(s23 s34+(s15-s34) s45)^2-2 (-s15 s34 s45+s34 (s23+s34) (-s23+s45)+s15 s23 (2 s34+s45)) s5+(s23+s34)^2 s5^2-2 s12 (s23 s34 (s23-s45)+s15^2 s45+s23 (s23+s34) s5-s15 (s34 (s45-s5)+s23 (s34+s45+s5)))]
};


listOfFunctions = Get["/home/hbhartanto/gitrepos/myfiniteflowexamples/projector_reduction/2q2bH/generate_F_permutations/2l5p1m_allFs.m"];


momrescale = 173.2`100;


(* OpenLoops g(1) g(2) -> b(3) bbar(4) H(5) *)
pmomin0 = {
 {   500.00000000000000      ,   0.0000000000000000      ,   0.0000000000000000      ,   500.00000000000000      },
 {   500.00000000000000      ,   0.0000000000000000      ,   0.0000000000000000      ,  -500.00000000000000      },
 {   403.00827018817222      ,   228.43885455220016      ,   143.93926034002465      ,   299.18697315175382      },
 {   457.49038692063482      ,  -279.84438223496670      ,  -140.45505731823158      ,  -333.55202402332145      },
 {   139.50134289119288      ,   51.405527682766404      ,  -3.4842030217930358      ,   34.365050871567682      }};


pmomin=IncreaseMomPrecision[pmomin0];


(* our definition: 0 -> bbar(p1) b(p2) g(p3) g(p4) H(p5) *)
(* g(-p3) g(-p4) -> b(p2) bbar(p1) H(p5) *)
pmom=pmomin[[{4,3,1,2,5}]];
pmomRescaled=pmom/momrescale;


(* amplitude ordering *)
ALLpermutations = {
{1,2,3,4,5},
{2,1,3,4,5},
{1,2,4,3,5},
{2,1,4,3,5}
};


ALLpermutationsPFuncOrdering = Join[Reverse/@(ALLpermutations /. {1->5,5->1,2->4,4->2}),{{1,4,5,2,3}}];


Sij2Tij = {p1s->s[5],t12->s[4,5],t15->s[1,5],t23->s[3,4],t34->s[2,3],t45->s[1,2]};


(* we permute the phase space point to the t45 channel to use the pentagon function library *)
permTij["p14523"] = {p1s->p1s,t12->p1s-t15+t23-t45,t15->p1s-t12-t23+t45,t23->t45,t34->p1s-t12-t15+t34,t45->t23};


precision = "quadruple"; (* double, quadruple, octuple *)
evaluator = StartEvaluatorProcess["m1",listOfFunctions,"Precision"->precision];


vars={p1s,t12,t23,t34,t45,t15};


transcendentalconstants = {im[1,1]->I*Pi, re[3,1]->Zeta[3]};


permrulesDIR = "/home/hbhartanto/gitrepos/ffamps_scratch/2l5p1m/F_permutations";


(* read F permutation files *)
tick=AbsoluteTime[];
Do[
  Print[Position[ALLpermutationsPFuncOrdering,ii]," ",ii," ",AbsoluteTiming[permrules[ii]=Get[permrulesDIR<>"/F_perm_2l5p1m_"<>StringJoin[ToString/@ii]<>".m"];]];
,{ii,ALLpermutationsPFuncOrdering}];
Print["reading all F permutation files done in ",(AbsoluteTime[]-tick)/60.," minutes."];


Print[MatchQ[First/@(permrules[{1,4,5,2,3}]),listOfFunctions]];


(* ::Subsection:: *)
(*evaluate pfuncs for original mom*)


Do[
   tick=AbsoluteTime[];
   ExNum[Sequence@@iperm]=FromTwistorVariables5pt[ex,Sequence@@iperm] /. EvaluateSijTr[pmom];
   SijNum[Sequence@@iperm]=Rule[#,GetMomentumTwistorExpression[#,MomMode] /. ExNum[Sequence@@iperm]]& /@ sijList[1,2,3,4,5];
   SqrtNum[Sequence@@iperm] = rootdefs /. Sqrt[x_]:>Sqrt[x+I*10^(-64)] /. (SijNum[Sequence@@iperm] /. {s[i_,j_]:>ToExpression["s"<>ToString[i]<>ToString[j]],s[i_]:>ToExpression["s"<>ToString[i]]});
   Print[iperm," done in ",(AbsoluteTime[]-tick)/1.0," seconds."];
,{iperm,ALLpermutations}];


TijNum = Sij2Tij /. SijNum[1,2,3,4,5] // Chop;
permTijNum["p14523"] = Thread[permTij["p14523"][[All,1]]->(permTij["p14523"][[All,2]]/.TijNum)];


(* evaluate pentagon functions *)
{timing,Fnum14523} = AbsoluteTiming[EvaluateFunctions[evaluator,N[vars/.permTijNum["p14523"],80]] /. Rule[a_,b_]:>Rule[a,SetPrecision[b,32]] /; MatchQ[precision,"double"] ];
Print["pentagon functions evaluated in ", timing, " s (", precision, ")"];


Fnum14523 = permrules[{1,4,5,2,3}] /. Rule[a_,b_]:>Rule[a,b/. Dispatch[Fnum14523] /. transcendentalconstants];


tick=AbsoluteTime[];
Do[
  Print[ALLpermutations[[ii]]," ",ALLpermutationsPFuncOrdering[[ii]]];
  Print[AbsoluteTiming[Fnum[Sequence@@ALLpermutations[[ii]]] = permrules[ALLpermutationsPFuncOrdering[[ii]]] /. Rule[a_,b_]:>Rule[a,b /. Dispatch[Fnum14523] /. transcendentalconstants];]];
,{ii,Length[ALLpermutations]}];
Print["Fnum for all permutations done in ",(AbsoluteTime[]-tick)/60.," minutes."];


(* ::Subsection:: *)
(*evaluate pfuncs for rescaled mom*)


Do[
   tick=AbsoluteTime[];
   ExNumRescaled[Sequence@@iperm]=FromTwistorVariables5pt[ex,Sequence@@iperm] /. EvaluateSijTr[pmomRescaled];
   SijNumRescaled[Sequence@@iperm]=Rule[#,GetMomentumTwistorExpression[#,MomMode] /. ExNumRescaled[Sequence@@iperm]]& /@ sijList[1,2,3,4,5];
   SqrtNumRescaled[Sequence@@iperm] = rootdefs /. Sqrt[x_]:>Sqrt[x+I*10^(-64)] /. (SijNumRescaled[Sequence@@iperm] /. {
   s[i_,j_]:>ToExpression["s"<>ToString[i]<>ToString[j]],s[i_]:>ToExpression["s"<>ToString[i]]});
   Print[iperm," done in ",(AbsoluteTime[]-tick)/1.0," seconds."];
,{iperm,ALLpermutations}];


TijNumRescaled = Sij2Tij /. SijNumRescaled[1,2,3,4,5] // Chop;
permTijNumRescaled["p14523"] = Thread[permTij["p14523"][[All,1]]->(permTij["p14523"][[All,2]]/.TijNumRescaled)];


(* evaluate pentagon functions *)
{timing,Fnum14523Rescaled} = AbsoluteTiming[EvaluateFunctions[evaluator,N[vars/.permTijNumRescaled["p14523"],80]] /. Rule[a_,b_]:>Rule[a,SetPrecision[b,32]] /; MatchQ[precision,"double"] ];
Print["pentagon functions evaluated in ", timing, " s (", precision, ")"];


Fnum14523Rescaled = permrules[{1,4,5,2,3}] /. Rule[a_,b_]:>Rule[a,b/. Dispatch[Fnum14523Rescaled] /. transcendentalconstants];


tick=AbsoluteTime[];
Do[
  Print[ALLpermutations[[ii]]," ",ALLpermutationsPFuncOrdering[[ii]]];
  Print[AbsoluteTiming[FnumRescaled[Sequence@@ALLpermutations[[ii]]] = permrules[ALLpermutationsPFuncOrdering[[ii]]] /. Rule[a_,b_]:>Rule[a,b /. Dispatch[Fnum14523Rescaled] /. transcendentalconstants];]];
,{ii,Length[ALLpermutations]}];
Print["Fnum for all permutations done in ",(AbsoluteTime[]-tick)/60.," minutes."];


(* ::Subsection:: *)
(*check mu-dependence*)


ampfiles = {
(* *)
"1L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncp0",
"1L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncpm2",
"1L_2q2bH_d12d34_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncpm1",
"1L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncp1",
"1L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncpm1",
"1L_2q2bH_d14d23_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncp0",
(* *)
"2L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncp1",
"2L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncpm1",
"2L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncpm3",
"2L_2q2bH_d12d34_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncp0",
"2L_2q2bH_d12d34_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncpm2",
"2L_2q2bH_d12d34_qkp2_Oqkm8qkm6_OHHqbm4qbm2_Ncpm1",
"2L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncp0",
"2L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncp2",
"2L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncpm2",
"2L_2q2bH_d14d23_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncp1",
"2L_2q2bH_d14d23_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncpm1",
"2L_2q2bH_d14d23_qkp2_Oqkm8qkm6_OHHqbm4qbm2_Ncp0"
(* *)
};


ncfactor2ncpower = Association[{"Ncp0"->0, "Ncp1"->1, "Ncp2"->2, "Ncpm1"->-1, "Ncpm2"->-2, "Ncpm3"->-3}];
nffactor2nfpower = Association[{""->0, "qk"->1, "qkp2"->2}];

MakeHelicity[x_]:=StringReplace[x,{"+"->"p","-"->"m"}];

toNcNfLabel=Association[{
  {2,0}->"Nc^2",
  {1,0}->"Nc",
  {0,0}->"1",
  {-1,0}->"1/Nc",
  {-2,0}->"1/Nc^2",
  {-3,0}->"1/Nc^3",
  {1,1}->"Nc*Nf",
  {0,1}->"Nf",
  {-1,1}->"Nf/Nc",
  {-2,1}->"Nf/Nc^2",
  {0,2}->"Nf^2",
  {-1,2}->"Nf^2/Nc",
  Nothing
}];

colourfactor2number = Association[{
  "d14d23"->1,
  "d12d34"->2
}];
ToNfNcNotation[x_]:=StringReplace[x,{"Oqkm8qkm6_OHHqbm4qbm2"->"Nfp"<>ToString[nfpower],"__"->"_","_qk_"->"_","_qkp2_"->""}];


Get["../amps_2q2bH_analytic/bare/DiagramNumerators_0L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncp0_dsm2p0.m"];
Amp["0L","d14d23","1"] = DiagramNumerator["+++-0",topo[]] /. INT[n_,p_,tp_]:>n/2;


Get["../amps_2q2bH_analytic/bare/DiagramNumerators_0L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncpm1_dsm2p0.m"];
Amp["0L","d12d34","1/Nc"] = DiagramNumerator["+++-0",topo[]] /. INT[n_,p_,tp_]:>n/2;


AmpNum["0L","d14d23","1"] = Amp["0L","d14d23","1"] /. ExNum[1,2,3,4,5];
AmpNum["0L","d12d34","1/Nc"] = Amp["0L","d12d34","1/Nc"] /. ExNum[1,2,3,4,5];


repl = Join[ExNum[1,2,3,4,5],SqrtNum[1,2,3,4,5],Fnum[1,2,3,4,5]];


GetFinRemNum[x_]:=Normal[Series[( x[[2,1]] /. (x[[1]] /. repl)) . ( (RestoreMonoDimensions/@x[[2,2]]) /. repl /. transcendentalconstants /. mono->Identity),{eps,0,0}]]


F1Lmu1["d14d23","Nc"]=GetFinRemNum[Get["../finrem_2q2bH/finrem_MT_spfn_1L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncp1_PSanalyticX1_pppm0.m"]];
F1Lmu1["d14d23","1/Nc"]=GetFinRemNum[Get["../finrem_2q2bH/finrem_MT_spfn_1L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncpm1_PSanalyticX1_pppm0.m"]];
F1Lmu1["d14d23","Nf"]=GetFinRemNum[Get["../finrem_2q2bH/finrem_MT_spfn_1L_2q2bH_d14d23_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncp0_PSanalyticX1_pppm0.m"]];
F1Lmu1["d12d34","1"]=GetFinRemNum[Get["../finrem_2q2bH/finrem_MT_spfn_1L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncp0_PSanalyticX1_pppm0.m"]];
F1Lmu1["d12d34","1/Nc^2"]=GetFinRemNum[Get["../finrem_2q2bH/finrem_MT_spfn_1L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncpm2_PSanalyticX1_pppm0.m"]];
F1Lmu1["d12d34","Nf/Nc"]=GetFinRemNum[Get["../finrem_2q2bH/finrem_MT_spfn_1L_2q2bH_d12d34_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncpm1_PSanalyticX1_pppm0.m"]];


translate = {
  A0L["d14d23","1"]->AmpNum["0L","d14d23","1"],
  A0L["d12d34","1/Nc"]->AmpNum["0L","d12d34","1/Nc"],
  F1L["d14d23","Nc"]->F1Lmu1["d14d23","Nc"],
  F1L["d14d23","1/Nc"]->F1Lmu1["d14d23","1/Nc"],
  F1L["d12d34","1"]->F1Lmu1["d12d34","1"],
  F1L["d12d34","1/Nc^2"]->F1Lmu1["d12d34","1/Nc^2"],
  F1L["d14d23","Nf"]->F1Lmu1["d14d23","Nf"],
  F1L["d12d34","Nf/Nc"]->F1Lmu1["d12d34","Nf/Nc"],
  Nothing
};


Do[
  loopord = StringSplit[ampfile,"_"][[1]];
  colourfactor = StringSplit[ampfile,"_"][[3]];
  nfpower = nffactor2nfpower[StringSplit[ampfile,"_"][[4]]];
  ncpower = ncfactor2ncpower[StringSplit[ampfile,"_"][[7]]];
  ncnflabel = toNcNfLabel[{ncpower,nfpower}];
  colourcomponent = colourfactor2number[colourfactor];
  (*Print[loopord," ",colourfactor," ",ncnflabel];*)
  mudepfile = ToNfNcNotation["mudep_"<>ampfile<>".m"];
  finrem = Get["../finrem_2q2bH/finrem_MT_spfn_"<>ampfile<>"_PSanalyticX1_pppm0.m"];
  mudep = Get[mudepfile];
  finremNUMmu1 = Normal[Series[( finrem[[2,1]] /. (finrem[[1]] /. ExNum[1,2,3,4,5])) . (RestoreMonoDimensions/@finrem[[2,2]] /. mono[x_]:>x
                 /. Fnum[1,2,3,4,5] /. ExNum[1,2,3,4,5] /. SqrtNum[1,2,3,4,5] /. transcendentalconstants),{eps,0,0}]];
  finremNUMrescaled = Normal[Series[( finrem[[2,1]] /. (finrem[[1]] /. ExNumRescaled[1,2,3,4,5])) . (RestoreMonoDimensions/@finrem[[2,2]] /. mono[x_]:>x
                      /. FnumRescaled[1,2,3,4,5] /. ExNumRescaled[1,2,3,4,5] /. SqrtNumRescaled[1,2,3,4,5] /. transcendentalconstants),{eps,0,0}]];
  mudepNUM = (mudep /. translate /. Fnum[1,2,3,4,5] /. mu2->momrescale^2 /. transcendentalconstants);
  finremNUMmu = finremNUMmu1 + mudepNUM;
  (*Print[{finremNUMrescaled,finremNUMmu,finremNUMrescaled-finremNUMmu}];*)
  (*Print[Variables[mudepNUM]];*)
  Print[Position[ampfiles,ampfile]," ",loopord," ",colourfactor," ",ncnflabel," ",{finremNUMrescaled-finremNUMmu}];
,{ampfile,ampfiles[[1;;-1]]}];




