(F[1, 2] + F[1, 7] - F[1, 8] - F[1, 9])*F1L["d12d34", "Nf/Nc"]*Log[mu2] - 
 (4*F1L["d14d23", "1/Nc"]*Log[mu2])/3 + F1L["d14d23", "Nf"]*
  ((3/2 - F[1, 2] - F[1, 5] - F[1, 6] - F[1, 7] + F[1, 8] + F[1, 9] + 
     2*im[1, 1])*Log[mu2] + Log[mu2]^2) + A0L["d12d34", "1/Nc"]*
  ((-10*(F[1, 2] + F[1, 7] - F[1, 8] - F[1, 9])*Log[mu2])/9 + 
   (-F[1, 2] - F[1, 7] + F[1, 8] + F[1, 9])*Log[mu2]^2) + 
 A0L["d14d23", "1"]*(((-31 + 60*F[1, 2] + 60*F[1, 5] + 60*F[1, 6] + 
      60*F[1, 7] - 60*F[1, 8] - 60*F[1, 9] - 120*im[1, 1] + 18*im[1, 1]^2)*
     Log[mu2])/54 + (-47/18 + F[1, 2] + F[1, 5] + F[1, 6] + F[1, 7] - 
     F[1, 8] - F[1, 9] - 2*im[1, 1])*Log[mu2]^2 - (8*Log[mu2]^3)/9)
