(* ::Package:: *)

<<FiniteFlow`


CollectFactorsX[EXPR_] := Module[{tmp,depth,coeff},
  depth = 1;
  If[MatchQ[EXPR[[0]],Plus], tmp = List@@EXPR; depth+=1;, tmp = EXPR;];
  coeff = Replace[EXPR,{Power[expr_,i_Integer]:>FFF[expr]^i /; i>0, Power[expr_,i_Integer]:>FFF[expr]^i /; i<0, Plus[expr_]:>FFF[Plus[expr]]},{depth}];
  Return[coeff];
];


FindIndependentFactorsX[Coefficients_List] := Module[{vars,rules,coeffs},
  coeffs = CollectFactorsX /@ Coefficients;
  vars = Join@@(Select[Variables[#],!MatchQ[#,FFF[_?NumericQ]]&]& /@ coeffs);
  vars = DeleteDuplicates[vars];
  rules =  MapIndexed[Rule[#1,y@@#2]&,vars];
  coeffs = coeffs //. Dispatch[rules] /. FFF[a_]:>a;
  Return[{coeffs,rules /. Rule[a_,b_]:>Rule[b,a /. FFF[x_]:>x]}];
];


RestoreMonoDimensions[x_]:=Module[{norm,in},
in=x /. mono[xx_]:>xx;
norm=in  /. { 
  F[__]:>1,
  re[__]:>1,
  im[__]:>1,
  sqrtDelta5->1/ex[1]^2,
  sqrtG3[y_]:>1/ex[1],
  sqrtSigma5[y_]:>1/ex[1]^2
};
Return[norm*mono[in]];
];


INdir = "finrem_2q2bH";
OUTdir = "finrem_2q2bH_sm";


(* ::Section:: *)
(*1L*)


fnames = FileNames[INdir<>"/finrem*1L*.m"];
(*fnames = fnames[[Flatten[Position[StringFreeQ[fnames,"eps"],True]]]];*)


Nfiles = Length[fnames];


Print[Nfiles];


fnames


Clear[amp,count,allfbasis,allfsubs,collectedfs,ys,collectedfsrules,ampfbasis];


count=1;
Do[
  fnamein[count]=ifile;
  ampIN[count]=Get[ifile];
  If[MatchQ[ampIN[count],0],
    ampfbasis[count]={mono[1]};
    fsubs[count]={g[1,count]->0};
    ampCC[count]={g[1,count]};
    ampPF[count]={mono[1]};
    ,
    {fsubs[count],{ampCC[count],ampPF[count]}} = ampIN[count] /. f[i_]:>g[i,count];
    ampPF[count]=mono/@ampPF[count];
    ampfbasis[count]=Cases[Variables[ampCC[count] . ampPF[count]],_mono];
  ];
  count=count+1;
,{ifile,fnames[[1;;-1]]}];


allfbasis = Sort[DeleteDuplicates[Flatten[ampfbasis[#]& /@ Range[Nfiles]]]];


allfsubs = Flatten[fsubs /@ Range[Nfiles]];


Print[Length[allfbasis]];


Print[AbsoluteTiming[{collectedfs,ys} = FindIndependentFactorsX[Values[allfsubs]];]];


collectedfsrules = Thread[(First/@allfsubs)->collectedfs];


Print[Length[ys]];


(*Print[AbsoluteTiming[ysV2 = ys /. Rule[a_,b_]:>Rule[a,FullSimplify[b]];]];*)


file=OpenWrite[OUTdir<>"/ys_1L.m"];
WriteString[file,InputForm[ys]];
Close[file];


file=OpenWrite[OUTdir<>"/FunctionBasis_1L.m"];
WriteString[file,InputForm[(RestoreMonoDimensions/@allfbasis) /. mono->Identity]];
Close[file];


Do[
  newfsubs1=Rule[# /. g[i1_,i2_]:>f[i1],# /. fsubs[icount]]& /@ (First/@fsubs[icount]);
  fvars = First/@newfsubs1;
  newfsubs2=Rule[# /. g[i1_,i2_]:>f[i1],# /. collectedfsrules]& /@ (First/@fsubs[icount]);
  myamp = ampCC[icount] . ampPF[icount] /. g[i1_,i2_]:>f[i1];
  Print[icount," ",fnamein[icount]," ",AbsoluteTiming[myccs = FFLinearCoefficients[#,fvars]& /@ FFLinearCoefficients[myamp,allfbasis] // Transpose;]];
  If[StringContainsQ[fnamein[icount],"1L"],
    sparsearray = SparseArray[Table[Coefficient[myccs,eps,ieps],{ieps,{0,1,2}}]];
  ,
    sparsearray = SparseArray[myccs];
  ];
  fnameout1 = StringReplace[fnamein[icount],{"finrem_2q2bH"->OUTdir,"finrem_"->"FiniteRemainder_sm_",
              "MT_spfn_"->"","Oqkm8qkm6_OHHqbm4qbm2_"->"","qkp2_"->"Nfp2_","qk_"->"Nfp1_","__"->"_Nfp0_","0.m"->".m","PSanalyticX1_"->""}];
  fnameout2 = StringReplace[fnamein[icount],{"finrem_2q2bH"->OUTdir,"finrem_"->"FiniteRemainder_coeffs_",
              "MT_spfn_"->"","Oqkm8qkm6_OHHqbm4qbm2_"->"","qkp2_"->"Nfp2_","qk_"->"Nfp1_","__"->"_Nfp0_","0.m"->".m","PSanalyticX1_"->""}];
  fnameout3 = StringReplace[fnamein[icount],{"finrem_2q2bH"->OUTdir,"finrem_"->"FiniteRemainder_coeffs_y_",
              "MT_spfn_"->"","Oqkm8qkm6_OHHqbm4qbm2_"->"","qkp2_"->"Nfp2_","qk_"->"Nfp1_","__"->"_Nfp0_","0.m"->".m","PSanalyticX1_"->""}];
  file=OpenWrite[fnameout1];
  WriteString[file,InputForm[sparsearray]];
  Close[file];
  file=OpenWrite[fnameout2];
  WriteString[file,InputForm[newfsubs1]];
  Close[file];
  file=OpenWrite[fnameout3];
  WriteString[file,InputForm[newfsubs2]];
  Close[file];
,{icount,1,Nfiles}];


(* ::Section:: *)
(*2L*)


fnames = FileNames[INdir<>"/finrem*2L*.m"];


Nfiles = Length[fnames];


Print[Nfiles];


fnames


Clear[amp,count,allfbasis,allfsubs,collectedfs,ys,collectedfsrules,ampfbasis];


count=1;
Do[
  fnamein[count]=ifile;
  ampIN[count]=Get[ifile];
  If[MatchQ[ampIN[count],0],
    ampfbasis[count]={mono[1]};
    fsubs[count]={g[1,count]->0};
    ampCC[count]={g[1,count]};
    ampPF[count]={mono[1]};
    ,
    {fsubs[count],{ampCC[count],ampPF[count]}} = ampIN[count] /. f[i_]:>g[i,count];
    ampPF[count]=mono/@ampPF[count];
    ampfbasis[count]=Cases[Variables[ampCC[count] . ampPF[count]],_mono];
  ];
  count=count+1;
,{ifile,fnames[[1;;-1]]}];


allfbasis = Sort[DeleteDuplicates[Flatten[ampfbasis[#]& /@ Range[Nfiles]]]];


allfsubs = Flatten[fsubs /@ Range[Nfiles]];


Print[Length[allfbasis]];


Print[AbsoluteTiming[{collectedfs,ys} = FindIndependentFactorsX[Values[allfsubs]];]];


collectedfsrules = Thread[(First/@allfsubs)->collectedfs];


Print[Length[ys]];


(*Print[AbsoluteTiming[ysV2 = ys /. Rule[a_,b_]:>Rule[a,FullSimplify[b]];]];*)


file=OpenWrite[OUTdir<>"/ys_2L.m"];
WriteString[file,InputForm[ys]];
Close[file];


file=OpenWrite[OUTdir<>"/FunctionBasis_2L.m"];
WriteString[file,InputForm[(RestoreMonoDimensions/@allfbasis) /. mono->Identity]];
Close[file];


Do[
  newfsubs1=Rule[# /. g[i1_,i2_]:>f[i1],# /. fsubs[icount]]& /@ (First/@fsubs[icount]);
  fvars = First/@newfsubs1;
  newfsubs2=Rule[# /. g[i1_,i2_]:>f[i1],# /. collectedfsrules]& /@ (First/@fsubs[icount]);
  myamp = ampCC[icount] . ampPF[icount] /. g[i1_,i2_]:>f[i1];
  Print[icount," ",fnamein[icount]," ",AbsoluteTiming[myccs = FFLinearCoefficients[#,fvars]& /@ FFLinearCoefficients[myamp,allfbasis] // Transpose;]];
  If[StringContainsQ[fnamein[icount],"1L"],
    sparsearray = SparseArray[Table[Coefficient[myccs,eps,ieps],{ieps,{0,1,2}}]];
  ,
    sparsearray = SparseArray[myccs];
  ];
  fnameout1 = StringReplace[fnamein[icount],{"finrem_2q2bH"->OUTdir,"finrem_"->"FiniteRemainder_sm_",
              "MT_spfn_"->"","Oqkm8qkm6_OHHqbm4qbm2_"->"","qkp2_"->"Nfp2_","qk_"->"Nfp1_","__"->"_Nfp0_","0.m"->".m","PSanalyticX1_"->""}];
  fnameout2 = StringReplace[fnamein[icount],{"finrem_2q2bH"->OUTdir,"finrem_"->"FiniteRemainder_coeffs_",
              "MT_spfn_"->"","Oqkm8qkm6_OHHqbm4qbm2_"->"","qkp2_"->"Nfp2_","qk_"->"Nfp1_","__"->"_Nfp0_","0.m"->".m","PSanalyticX1_"->""}];
  fnameout3 = StringReplace[fnamein[icount],{"finrem_2q2bH"->OUTdir,"finrem_"->"FiniteRemainder_coeffs_y_",
              "MT_spfn_"->"","Oqkm8qkm6_OHHqbm4qbm2_"->"","qkp2_"->"Nfp2_","qk_"->"Nfp1_","__"->"_Nfp0_","0.m"->".m","PSanalyticX1_"->""}];
  file=OpenWrite[fnameout1];
  WriteString[file,InputForm[sparsearray]];
  Close[file];
  file=OpenWrite[fnameout2];
  WriteString[file,InputForm[newfsubs1]];
  Close[file];
  file=OpenWrite[fnameout3];
  WriteString[file,InputForm[newfsubs2]];
  Close[file];
,{icount,1,Nfiles}];


