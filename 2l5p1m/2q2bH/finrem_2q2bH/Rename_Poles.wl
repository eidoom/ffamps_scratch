(* ::Package:: *)

fnametag = {
(* *)
"1L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncp0",
"1L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncpm2",
"1L_2q2bH_d12d34_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncpm1",
"1L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncp1",
"1L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncpm1",
"1L_2q2bH_d14d23_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncp0",
(* *)
"2L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncp1",
"2L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncpm1",
"2L_2q2bH_d12d34__Oqkm8qkm6_OHHqbm4qbm2_Ncpm3",
"2L_2q2bH_d12d34_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncp0",
"2L_2q2bH_d12d34_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncpm2",
"2L_2q2bH_d12d34_qkp2_Oqkm8qkm6_OHHqbm4qbm2_Ncpm1",
"2L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncp0",
"2L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncp2",
"2L_2q2bH_d14d23__Oqkm8qkm6_OHHqbm4qbm2_Ncpm2",
"2L_2q2bH_d14d23_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncp1",
"2L_2q2bH_d14d23_qk_Oqkm8qkm6_OHHqbm4qbm2_Ncpm1",
"2L_2q2bH_d14d23_qkp2_Oqkm8qkm6_OHHqbm4qbm2_Ncp0",
(* *)
Nothing
};


fnamesIN = ("poles_MT_spfn_"<>#<>"_PSanalyticX1_pppm0.m")& /@ fnametag;


ModifyFileName[x_]:=StringReplace[x,
{"MT_spfn_"->"","Oqkm8qkm6_OHHqbm4qbm2_"->"","qkp2_"->"Nfp2_","qk_"->"Nfp1_","__"->"_Nfp0_","0.m"->".m","PSanalyticX1_"->""}];


Do[
  Run["cp "<>ifile<>" "<>ModifyFileName[ifile]];
,{ifile,fnamesIN}];


fnametag
