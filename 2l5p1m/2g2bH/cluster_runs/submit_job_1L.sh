#!/bin/sh

#SBATCH -J f1L
#SBATCH --array=1-12%4
####SBATCH --partition=debug
#SBATCH --partition=debug3
#SBATCH --account=group3
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=48
#SBATCH --mem=240G
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load mathematica/12.2

module list

###cat .Mathematica/Licensing/mathpass

echo $SLURM_NTASKS
echo $(hostname)

ii=$SLURM_ARRAY_TASK_ID


cd /home/bayu/gitrepos/ffamps_scratch/2l5p1m/2g2bH 
math -script make_finrem_1L.wl -fileno $ii


