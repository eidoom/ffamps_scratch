(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


<< FiniteFlow`;


<< "InitTwoLoopToolsFF.m";
<< "InitDiagramsFF.m";
Get["setupfiles/Setup_2g2bH.m"];


(* external routines *)
(*Get["/home/bayu/gitrepos/ffamps_scratch/FFTools.wl"];*)
(*Get["/home/hbhartanto/gitrepos/ffamps_scratch/FFTools.wl"];*)

process = "2g2bH";
psmode = "PSnumeric";


ampfiles = {
"1L_2g2bH_T2341_Nfp0_Ncp1",
"1L_2g2bH_T2341_Nfp0_Ncpm1",
"1L_2g2bH_T2341_Nfp1_Ncp0",
"1L_2g2bH_d12d34_Nfp0_Ncp0",
(* *)
"2L_2g2bH_T2341_Nfp0_Ncp2",
"2L_2g2bH_T2341_Nfp0_Ncp0",
"2L_2g2bH_T2341_Nfp0_Ncpm2",
"2L_2g2bH_T2341_Nfp1_Ncp1",
"2L_2g2bH_T2341_Nfp1_Ncpm1",
"2L_2g2bH_T2341_Nfp2_Ncp0",
(* *)
"2L_2g2bH_d12d34_Nfp0_Ncp1",
"2L_2g2bH_d12d34_Nfp0_Ncpm1",
"2L_2g2bH_d12d34_Nfp1_Ncp0",
"2L_2g2bH_d12d34_Nfp1_Ncpm2"
};


CmdOptions = {
  "-fileno"
};


Do[
  pos = Flatten[Position[$CommandLine,CmdOptions[[oo]]]];
  If[Length[pos]==1,

    Switch[oo,
      1,
      fileno = ToExpression[$CommandLine[[pos[[1]]+1]]];,
      _,
      Print["unknown argument ",oo,CmdOptions[[oo]]]; Quit[];
    ];
  ];
,{oo,1,Length[CmdOptions]}];


(*fileno=1;*)


If[Not[ValueQ[fileno]] || Not[IntegerQ[fileno]],
  Print["\n"];
  Print["use the option '-fileno' to specify an integer corresponding to a file out of the following list:\n"];
  Do[
    Print[ii];
  ,{ii,MapIndexed[Rule[#2[[1]],#1] &,ampfiles]}];
  Print[Length[ampfiles]];
  Exit[];
];


ampfile = ampfiles[[fileno]];


Print[ampfile];


ncfactor2ncpower = Association[{"Ncp0"->0, "Ncp1"->1, "Ncp2"->2, "Ncpm1"->-1, "Ncpm2"->-2, "Ncpm3"->-3}];
nffactor2nfpower = Association[{"Nfp0"->0, "Nfp1"->1, "Nfp2"->2}];

MakeHelicity[x_]:=StringReplace[x,{"+"->"p","-"->"m"}];

toNcNfLabel=Association[{
  {0,0}->"1",
  {1,0}->"Nc",
  {2,0}->"Nc^2",
  {0,1}->"Nf",
  {-2,0}->"1/Nc^2",
  {-1,1}->"Nf/Nc",
  {1,1}->"Nc*Nf",
  {-1,0}->"1/Nc",
  {0,2}->"Nf^2",
  {-1,2}->"Nf^2/Nc",
  {-2,1}->"Nf/Nc^2"}];

colourfactor2number = Association[{
  "T2341"->1,
  "T2431"->2,
  "d12d34"->3
}];


(*If[!FileExistsQ[ampdir<>"/"<>ampfile], Print[ampdir<>"/"<>ampfile, " not found"]; Exit[]];*)
Print["processing ", ampfile];


loopord = StringSplit[ampfile,"_"][[1]];
colourfactor = StringSplit[ampfile,"_"][[3]];
nfpower = nffactor2nfpower[StringSplit[ampfile,"_"][[4]]];
ncpower = ncfactor2ncpower[StringSplit[ampfile,"_"][[5]]];

ncnflabel = toNcNfLabel[{ncpower,nfpower}];
colourcomponent = colourfactor2number[colourfactor];


Print["loopord = ", loopord];
Print["colourfactor = ", colourfactor];
Print["nfpower = ", nfpower];
Print["ncpower = ", ncpower];
Print["ncnflabel = ", ncnflabel];


(*(* *)
norm["0L","T2341"]=1/2;
norm["1L","T2341"]=norm["0L","T2341"]*(-1/2);
norm["2L","T2341"]=norm["0L","T2341"]*(1/4);
(* *)
norm["0L","T2431"]=1/2;
norm["1L","T2431"]=norm["0L","T2431"]*(-1/2);
norm["2L","T2431"]=norm["0L","T2431"]*(1/4);
(* *)
norm["1L","d12d34"]=norm["1L","T2341"]/2;
norm["2L","d12d34"]=norm["2L","T2341"]/2;
(* *)*)


mudepfile = "mudep_"<>ampfile<>".m";


Print[mudepfile];


(* ::Section:: *)
(*prepare symbolic subtraction*)


(* ::Subsection:: *)
(*one-mass pentagon functions*)


(* NB: here p1^2 =!= 0 *)
(* w.r.t. the process, momenta are relabelled as 12345->54321 *)
PF2log = 
 {F[1,1]->Log[p1sq],
  F[1,2]->Log[-s[3,4]],
  F[1,3]->Log[s[1,2]],
  F[1,4]->Log[-s[1,5]],
  F[1,5]->Log[s[2,3]],
  F[1,6]->Log[s[4,5]],
  F[1,7]->Log[-s[2,5]],
  F[1,8]->Log[-s[2,4]],
  F[1,9]->Log[-s[3,5]],
  F[1,10]->Log[s[1,3]],
  F[1,11]->Log[-s[1,4]]};

log2PF = PF2log /. Rule[a_,b_]:>Rule[b,a];


toPFmonomials = {F[a__]:>mono[F[a]],im[a__]:>mono[im[a]], re[a__]:>mono[re[a]]};
monorules = {mono[a_]*mono[b_]:>mono[a*b], mono[a_]^k_:>mono[a^k]};


(* ::Subsection:: *)
(*anomalous dimensions (from https://arxiv.org/pdf/2304.06682.pdf)*)


(* values of all anomalous dimensions and flags *)
ExpectedValues = {
  gcusp[0]->4,
  gcusp[1]->(268/9-4*Pi^2/3)*cA-80/9*Tf*Nf,
  gcusp[2]->cA^2*(490/3-536/27*Pi^2+44/45*Pi^4+88/3*Zeta[3])+
    cA*Tf*Nf*(-1672/27+160/27*Pi^2-224/3*Zeta[3])+
    cF*Tf*Nf*(-220/3+64*Zeta[3])-64/27*Tf^2*Nf^2,
    
  gquark[0]->-3*cF,
  gquark[1]->cF^2*(-3/2+2*Pi^2-24*Zeta[3])+cF*cA*(-961/54-11/6*Pi^2+26*Zeta[3])+
    cF*Tf*Nf*(130/27+2*Pi^2/3),
(*  gquark[2]->cF^3*gqflag[1]+cF^2*cA*gqflag[2]+cF*cA^2*gqflag[3]+cF^2*Tf*Nf*gqflag[4]+
    cF*cA*Tf*Nf*gqflag[5]+cF*Tf^2*Nf^2*gqflag[6],*)
    
  ggluon[0]->-beta[0],
  ggluon[1]->cA^2*(-692/27+11/18*Pi^2+2*Zeta[3])+cA*Tf*Nf*(256/27-2*Pi^2/9)+4*cF*Tf*Nf,
  
  beta[0]->11/3*cA-4/3*Tf*Nf,
  beta[1]->2/3*(17*cA^2-10*cA*TR*Nf-6*cF*TR*Nf),
  
  cA -> Nc*(2*TR), 
  cF -> (Nc^2-1)/2/Nc*(2*TR),
  TR -> 1/2,
  Tf -> TR
} /.  {Pi->-I*im[1, 1], Zeta[3]->re[3,1]};


ExpectedValues = Expand[ExpectedValues  /. toPFmonomials]//.monorules;


(* ::Subsection:: *)
(*subtraction*)


fullamp0Lsymb = {
  A0L["T2341"],
  A0L["T2431"],
  0  (* A0L["d12d34"] vanishes *)
};

(*
colourfactors={"T2341","T2431","d12d34"};
nfpowers = {0,1};
ncpowers={-1,0,1};

fullF1Lsymb = Table[Sum[
  F1L[cc, "Nfp"<>ToString[nfp],StringReplace["Ncp"<>ToString[ncp],"-"->"m"]]*Nc^ncp*Nf^nfp,
  {ncp,ncpowers},{nfp,nfpowers}],
  {cc,colourfactors}];
*)

fullF1Lsymb = {
  F1L["T2341","Nc"]*Nc + F1L["T2341","1/Nc"]/Nc + F1L["T2341","Nf"]*Nf,
  F1L["T2431","Nc"]*Nc + F1L["T2431","1/Nc"]/Nc + F1L["T2431","Nf"]*Nf,
  F1L["d12d34","1"]*1
};

id = IdentityMatrix[3];
Z1 = Get["../Z1.m"];
Z2 = Get["../Z2.m"];

Z1=Expand[Z1 /. toPFmonomials] //. monorules;
Z2=Expand[Z2 /. toPFmonomials] //. monorules;


(* renormalisation of Yukawa coupling - eqs. (A.3-4) of https://arxiv.org/pdf/1904.08961.pdf *)
(* NB they expand in aS/(2*Pi) whereas we expand in aS/(4*Pi), hence a factor of 2 in s1 and of 4 in s2 *)
YukawaS1 = 2*(-3/2*cF/eps);
YukawaS2 = 4*(3/8/eps^2*(3*cF^2+beta[0]*cF)-(1/8/eps)*(3/2*cF^2+97/6*cF*cA-10/3*cF*Tf*Nf));


(* ::Subsection:: *)
(*work out mu-restoring terms (for illustration purposes)*)


(*
Z1mu = Normal[Series[Z1 /. PF2log /. Log[a_]:>Log[a]-Log[mu2] /. log2PF,{eps,0,2}]];
Z2mu = Normal[Series[Z2 /. PF2log /. Log[a_]:>Log[a]-Log[mu2] /. log2PF,{eps,0,0}]];


(* hard-coded finite remainders with mu-dependence *)
Clear[F1,F2];
F1[mu2_] := A1LbareFull - mu2^-eps*(Z1full[mu2] + beta[0]/eps - YukawaS1)*A0L;

F2[mu2_] := A2LbareFull - mu2^(-eps)*(Z1full[mu2]+ 2 beta[0]/eps - YukawaS1)*A1LbareFull + 
  mu2^(-2 eps) (Z1full[mu2]^2-Z2full[mu2]+beta[0]^2/eps^2+(Z1full[mu2]*beta[0])/eps-beta[1]/(2 eps) 
  + YukawaS2 - YukawaS1*Z1full[mu2] - YukawaS1*beta[0]/eps)*A0L;


epsexp = {A1LbareFull -> Sum[A1Lbare[k]*eps^k,{k,-2,2}],
  A2LbareFull -> Sum[A2Lbare[k]*eps^k,{k,-4,0}],
  Z1full[mu2_] :> Z1$[-2][1]/eps^2 + Z1$[-1][mu2]/eps ,
  Z2full[mu2_] :> Z2$[-4][1]/eps^4 + Z2$[-3][mu2]/eps^3 +
    Z2$[-2][mu2]/eps^2 + Z2$[-1][mu2]/eps
     };


A1rules=Flatten[Solve[CoefficientList[eps^2*Normal[Series[F1[1]/.epsexp,{eps,0,-1}]],eps]==0,
  {A1Lbare[-2],A1Lbare[-1]}]];


Z1rules=Flatten[Solve[Normal[Series[F1[mu2]/.epsexp/.A1rules,{eps,0,-1}]]==0, Z1$[-1][mu2]]];


toF1=Flatten[Solve[F1$[1]==Normal[Series[F1[1]/.epsexp/.A1rules/.Z1rules,{eps,0,0}]],A1Lbare[0]]]


Join[A1rules,Z1rules]//TableForm


A2rules=Flatten[Solve[CoefficientList[eps^4*Normal[Series[F2[1]/.epsexp/.A1rules/.toF1/.Z1rules,{eps,0,-1}]],eps]==0,
  {A2Lbare[-4],A2Lbare[-3],A2Lbare[-2],A2Lbare[-1]}]];


(*A2rules[[All,2]]//Variables*)


zero2=Normal[Series[F2[mu2]/.epsexp/.A1rules/.toF1/.A2rules/.Z1rules,{eps,0,-1}]];

Z2rules=Flatten[Solve[CoefficientList[zero2*eps^3,eps]==0, 
  {Z2$[-3][mu2],Z2$[-2][mu2],Z2$[-1][mu2]}]];


(* check Z2rules - watchout for matrix products! *)
Do[ check=Simplify[Total[List@@Expand[zz/.Rule[a_,b_]:>a-b] /. 
  {Z1$[k_][1]:>Coefficient[Z1,eps,k],
  Z1$[k_][1]^2:>Coefficient[Z1,eps,k] . Coefficient[Z1,eps,k],
  Z1$[k_][1]*Z1$[h_][1]:>Coefficient[Z1,eps,k] . Coefficient[Z1,eps,h],
  Z2$[k_][1]:>Coefficient[Z2,eps,k],
  Z2$[k_][mu2]:>Coefficient[Z2mu,eps,k]}]//.ExpectedValues /. mono[x_]:>x];
  Print[Union[Flatten[check]]==={0}];
  Clear[check];,
  {zz,Z2rules}]


tmpmuRestore1L=Normal[Series[F1[mu2]-F1[1]/.epsexp/.A1rules/.Z1rules,{eps,0,0}]];


Collect[tmpmuRestore1L,A0L,Collect[#,Log[mu2],Simplify]&]  /. {
Z1$[i_][1]:>Coefficient$[Z1$,eps,i],
Z2$[i_][1]:>Coefficient$[Z2$,eps,i]
} // InputForm


tmpmuRestore2L=Normal[Series[F2[mu2]-F2[1]/.epsexp/.A2rules/.A1rules/.toF1/.Z1rules/.Z2rules,{eps,0,0}]];


Collect[tmpmuRestore2L,A0L|F1$[1],Collect[#,Log[mu2],Simplify]&] /. {
Z1$[i_][1]:>Coefficient$[Z1$,eps,i],
Z2$[i_][1]:>Coefficient$[Z2$,eps,i]
} // InputForm
*)


(* ::Subsection:: *)
(*mu restoring terms*)


muRestore["1L"] = fullamp0Lsymb . ((Log[mu2]^2*Coefficient[Z1,eps,-2])/2 
                    + Log[mu2]*(3*cF*id + beta[0]*id + Coefficient[Z1,eps,-1])) //. ExpectedValues;


muRestore["2L"] = ( fullF1Lsymb . ((3*cF*id + 2*beta[0]*id + Coefficient[Z1, eps, -1])*Log[mu2] + (Coefficient[Z1, eps, -2]*Log[mu2]^2)/2) + 
 fullamp0Lsymb . (((97*cA*cF)/6*id + (3*cF^2)/2*id - (10*cF*Nf*Tf)/3*id + beta[1]*id + 2*Coefficient[Z2, eps, -1])*Log[mu2] + 
   ((9*cF^2*id + 9*cF*beta[0]*id + 2*beta[0]^2*id + 6*cF*Coefficient[Z1, eps, -1] + 5*beta[0]*Coefficient[Z1, eps, -1] - 
      Coefficient[Z1, eps, -1] . Coefficient[Z1, eps, -1] + 4*Coefficient[Z2, eps, -2])*Log[mu2]^2)/2 + 
   ((9*cF*Coefficient[Z1, eps, -2] + 10*beta[0]*Coefficient[Z1, eps, -2] - 5*Coefficient[Z1, eps, -2] . Coefficient[Z1, eps, -1] + 
      8*Coefficient[Z2, eps, -3])*Log[mu2]^3)/6 + ((-5*Coefficient[Z1, eps, -2] . Coefficient[Z1, eps, -2])/24 + (2*Coefficient[Z2, eps, -4])/3)*Log[mu2]^4) ) //. ExpectedValues;
      
muRestore["2L"] = Collect[muRestore["2L"],_F1L|_A0L,Collect[#,Log[mu2],Simplify]&];


neededMuDep=Coefficient[Coefficient[muRestore[loopord][[colourcomponent]],Nf,nfpower],Nc,ncpower] /. mono[x_]:>x;
neededMuDep=Collect[neededMuDep,_A0L|_F1L,Collect[#,Log[mu2],Simplify]&];


neededamps0L=Union[Cases[neededMuDep,A0L[__],Infinity]];
neededfin1L=Union[Cases[neededMuDep,F1L[__],Infinity]];
Print["needed ingredients: ", Join[neededamps0L,neededfin1L]];


missedpieces=Complement[DeleteCases[Variables[neededMuDep],eps|_F|_re|_im|Log[mu2]],Join[neededamps0L,neededfin1L]];
If[missedpieces=!={}, Print["careful! something is left unfixed and may cause trouble: ", missedpieces]];


Print["writing ", mudepfile];
Put[neededMuDep,mudepfile];
