F1L["T2341", "Nc"]*((11/3 + F[1, 2] + F[1, 5] + F[1, 7] - im[1, 1])*
    Log[mu2] - (3*Log[mu2]^2)/2) + 
 A0L["T2341"]*(((-67 + 3*F[1, 2]^2 + 3*F[1, 5]^2 + 11*F[1, 7] + 3*F[1, 7]^2 + 
      F[1, 5]*(11 + 6*F[1, 7] - 6*im[1, 1]) + 
      F[1, 2]*(11 + 6*F[1, 5] + 6*F[1, 7] - 6*im[1, 1]) - 11*im[1, 1] - 
      6*F[1, 7]*im[1, 1])*Log[mu2]^2)/6 + 
   ((-11 - 9*F[1, 2] - 9*F[1, 5] - 9*F[1, 7] + 9*im[1, 1])*Log[mu2]^3)/6 + 
   (9*Log[mu2]^4)/8 + (Log[mu2]*(-544 + 268*F[1, 7] - 268*im[1, 1] - 
      7*im[1, 1]^2 + 12*F[1, 7]*im[1, 1]^2 - 12*im[1, 1]^3 + 
      4*F[1, 2]*(67 + 3*im[1, 1]^2) + 4*F[1, 5]*(67 + 3*im[1, 1]^2) + 
      324*re[3, 1]))/36)
