{{(4*(cA + cF)^2*gcusp[0]^2 - 4*(cA + cF)*eps^2*gcusp[1] - 
    (4*(cA + cF)*eps*gcusp[0]*(-3*Nc*beta[0] + 4*Nc*(ggluon[0] + gquark[0]) + 
       2*Nc^2*TR*gcusp[0]*(F[1, 2] + F[1, 5] + F[1, 7] - im[1, 1]) + 
       2*TR*gcusp[0]*(-F[1, 6] + im[1, 1])))/Nc + 
    (8*eps^3*(2*Nc*(ggluon[1] + gquark[1]) + Nc^2*TR*gcusp[1]*
        (F[1, 2] + F[1, 5] + F[1, 7] - im[1, 1]) + 
       TR*gcusp[1]*(-F[1, 6] + im[1, 1])))/Nc + 
    4*eps^2*(-(TR^2*(F[1, 2] + F[1, 7] - F[1, 8] - F[1, 9])*gcusp[0]^2*
        (-F[1, 5] - F[1, 6] + F[1, 8] + F[1, 9] + 2*im[1, 1])) + 
      ((2*Nc*(ggluon[0] + gquark[0]) + Nc^2*TR*gcusp[0]*(F[1, 2] + F[1, 5] + 
           F[1, 7] - im[1, 1]) + TR*gcusp[0]*(-F[1, 6] + im[1, 1]))*
        (2*Nc*(-beta[0] + ggluon[0] + gquark[0]) + Nc^2*TR*gcusp[0]*
          (F[1, 2] + F[1, 5] + F[1, 7] - im[1, 1]) + 
         TR*gcusp[0]*(-F[1, 6] + im[1, 1])))/Nc^2))/(32*eps^4), 
  (TR^2*(F[1, 2] + F[1, 7] - F[1, 8] - F[1, 9])*gcusp[0]^2*
    (-F[1, 5] - F[1, 6] + F[1, 8] + F[1, 9] + 2*im[1, 1]))/(8*eps^2), 
  (TR^2*(F[1, 5] + F[1, 6] - F[1, 8] - F[1, 9] - 2*im[1, 1])*
    (-2*(cA + cF)*Nc*gcusp[0]^2 + 2*eps^2*Nc*gcusp[1] + 
     eps*gcusp[0]*(-2*Nc*beta[0] + 4*Nc*(ggluon[0] + gquark[0]) + 
       Nc^2*TR*gcusp[0]*(F[1, 2] + 3*F[1, 5] + F[1, 6] + F[1, 7] - 
         4*im[1, 1]) + 2*TR*gcusp[0]*(-F[1, 6] + im[1, 1]))))/(8*eps^3*Nc)}, 
 {-1/8*(TR^2*(F[1, 2] + F[1, 7] - F[1, 8] - F[1, 9])*gcusp[0]^2*
     (F[1, 2] - F[1, 5] - F[1, 6] + F[1, 7] + 2*im[1, 1]))/eps^2, 
  (4*(cA + cF)^2*gcusp[0]^2 - 4*(cA + cF)*eps^2*gcusp[1] - 
    (4*(cA + cF)*eps*gcusp[0]*(-3*Nc*beta[0] + 4*Nc*(ggluon[0] + gquark[0]) + 
       2*Nc^2*TR*gcusp[0]*(F[1, 5] + F[1, 8] + F[1, 9] - im[1, 1]) + 
       2*TR*gcusp[0]*(-F[1, 6] + im[1, 1])))/Nc + 
    (8*eps^3*(2*Nc*(ggluon[1] + gquark[1]) + Nc^2*TR*gcusp[1]*
        (F[1, 5] + F[1, 8] + F[1, 9] - im[1, 1]) + 
       TR*gcusp[1]*(-F[1, 6] + im[1, 1])))/Nc + 
    4*eps^2*(TR^2*(F[1, 2] + F[1, 7] - F[1, 8] - F[1, 9])*gcusp[0]^2*
       (F[1, 2] - F[1, 5] - F[1, 6] + F[1, 7] + 2*im[1, 1]) + 
      ((2*Nc*(ggluon[0] + gquark[0]) + Nc^2*TR*gcusp[0]*(F[1, 5] + F[1, 8] + 
           F[1, 9] - im[1, 1]) + TR*gcusp[0]*(-F[1, 6] + im[1, 1]))*
        (2*Nc*(-beta[0] + ggluon[0] + gquark[0]) + Nc^2*TR*gcusp[0]*
          (F[1, 5] + F[1, 8] + F[1, 9] - im[1, 1]) + 
         TR*gcusp[0]*(-F[1, 6] + im[1, 1])))/Nc^2))/(32*eps^4), 
  -1/8*(TR^2*(F[1, 2] - F[1, 5] - F[1, 6] + F[1, 7] + 2*im[1, 1])*
     (-2*(cA + cF)*Nc*gcusp[0]^2 + 2*eps^2*Nc*gcusp[1] + 
      eps*gcusp[0]*(-2*Nc*beta[0] + 4*Nc*(ggluon[0] + gquark[0]) + 
        Nc^2*TR*gcusp[0]*(3*F[1, 5] + F[1, 6] + F[1, 8] + F[1, 9] - 
          4*im[1, 1]) + 2*TR*gcusp[0]*(-F[1, 6] + im[1, 1]))))/(eps^3*Nc)}, 
 {((F[1, 2] + F[1, 7] - F[1, 8] - F[1, 9])*(-2*(cA + cF)*Nc*gcusp[0]^2 + 
     2*eps^2*Nc*gcusp[1] + eps*gcusp[0]*(-2*Nc*beta[0] + 
       4*Nc*(ggluon[0] + gquark[0]) + Nc^2*TR*gcusp[0]*(F[1, 2] + 3*F[1, 5] + 
         F[1, 6] + F[1, 7] - 4*im[1, 1]) + 2*TR*gcusp[0]*
        (-F[1, 6] + im[1, 1]))))/(8*eps^3*Nc), 
  -1/8*((F[1, 2] + F[1, 7] - F[1, 8] - F[1, 9])*(-2*(cA + cF)*Nc*gcusp[0]^2 + 
      2*eps^2*Nc*gcusp[1] + eps*gcusp[0]*(-2*Nc*beta[0] + 
        4*Nc*(ggluon[0] + gquark[0]) + Nc^2*TR*gcusp[0]*
         (3*F[1, 5] + F[1, 6] + F[1, 8] + F[1, 9] - 4*im[1, 1]) + 
        2*TR*gcusp[0]*(-F[1, 6] + im[1, 1]))))/(eps^3*Nc), 
  (cA^2*Nc^2*gcusp[0]^2 + cF^2*Nc^2*gcusp[0]^2 - 
    cF*eps*Nc*(Nc*(-3*beta[0]*gcusp[0] + eps*gcusp[1] + 
        4*gcusp[0]*(ggluon[0] + gquark[0])) + 2*Nc^2*TR*gcusp[0]^2*
       (2*F[1, 5] + F[1, 6] - 3*im[1, 1]) + 2*TR*gcusp[0]^2*
       (-F[1, 6] + im[1, 1])) - cA*Nc*(-2*cF*Nc*gcusp[0]^2 + 
      eps^2*Nc*gcusp[1] + eps*gcusp[0]*(-3*Nc*beta[0] + 
        4*Nc*(ggluon[0] + gquark[0]) + 2*Nc^2*TR*gcusp[0]*
         (2*F[1, 5] + F[1, 6] - 3*im[1, 1]) + 2*TR*gcusp[0]*
         (-F[1, 6] + im[1, 1]))) + 
    eps^2*(-2*Nc^3*TR*(beta[0]*gcusp[0] - eps*gcusp[1] - 
        2*gcusp[0]*(ggluon[0] + gquark[0]))*(2*F[1, 5] + F[1, 6] - 
        3*im[1, 1]) + Nc^4*TR^2*gcusp[0]^2*(2*F[1, 5] + F[1, 6] - 3*im[1, 1])^
        2 + 2*Nc*TR*(beta[0]*gcusp[0] - eps*gcusp[1] - 
        2*gcusp[0]*(ggluon[0] + gquark[0]))*(F[1, 6] - im[1, 1]) + 
      TR^2*gcusp[0]^2*(F[1, 6] - im[1, 1])^2 + 
      Nc^2*(4*(ggluon[0]^2 + eps*ggluon[1] + 2*ggluon[0]*gquark[0] + 
          gquark[0]^2 - beta[0]*(ggluon[0] + gquark[0]) + eps*gquark[1]) + 
        TR^2*gcusp[0]^2*(F[1, 2]^2 - 2*F[1, 6]^2 + F[1, 7]^2 - 
          2*F[1, 7]*F[1, 8] + F[1, 8]^2 + 2*F[1, 2]*(F[1, 7] - F[1, 8] - 
            F[1, 9]) - 2*F[1, 7]*F[1, 9] + 2*F[1, 8]*F[1, 9] + F[1, 9]^2 - 
          4*F[1, 5]*(F[1, 6] - im[1, 1]) + 8*F[1, 6]*im[1, 1] - 
          6*im[1, 1]^2))))/(8*eps^4*Nc^2)}}
