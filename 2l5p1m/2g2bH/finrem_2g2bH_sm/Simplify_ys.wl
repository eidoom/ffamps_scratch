(* ::Package:: *)

(*
filein="ys_1L.m";
fileout="ys_1L_simp.m";
*)

(* *)
filein="ys_2L.m";
fileout="ys_2L_simp.m";
(* *)

ysin=Get[filein];
Print[AbsoluteTiming[ysnew=ysin /. Rule[a_,b_]:>Rule[a,Simplify[b]];]];

file=OpenWrite[fileout];
WriteString[file,InputForm[ysnew]];
Close[file];

Quit[];
