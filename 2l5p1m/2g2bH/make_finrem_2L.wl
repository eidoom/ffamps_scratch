(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


<< FiniteFlow`;


<< "InitTwoLoopToolsFF.m";
<< "InitDiagramsFF.m";
Get["setupfiles/Setup_2g2bH.m"];


(* external routines *)
Get["/home/bayu/gitrepos/ffamps_scratch/FFTools.wl"];
(*Get["/home/hbhartanto/gitrepos/ffamps_scratch/FFTools.wl"];*)

process = "2g2bH";
psmode = "PSnumeric";
nthreads = 48;
batchsize = 2000;

ampdir = "amps_2g2bH_analytic/bare_helamp";


ampfiles = StringSplit[#,"/"][[-1]]&/@FileNames[ampdir<>"/*2L*"];


If[ampfiles==={},
  Print["no amplitudes found in ", ampdir];
  Exit[],

  Print["found amplitudes:\n", ampfiles," \n ",Length[ampfiles]];
];


CmdOptions = {
  "-fileno"
};


Do[
  pos = Flatten[Position[$CommandLine,CmdOptions[[oo]]]];
  If[Length[pos]==1,

    Switch[oo,
      1,
      fileno = ToExpression[$CommandLine[[pos[[1]]+1]]];,
      _,
      Print["unknown argument ",oo,CmdOptions[[oo]]]; Quit[];
    ];
  ];
,{oo,1,Length[CmdOptions]}];


(*fileno=18;*)


If[Not[ValueQ[fileno]] || Not[IntegerQ[fileno]],
  Print["\n"];
  Print["use the option '-fileno' to specify an integer corresponding to a file out of the following list:\n"];
  Do[
    Print[ii];
  ,{ii,MapIndexed[Rule[#2[[1]],#1] &,ampfiles]}];
  Print[Length[ampfiles]];
  Exit[];
];


ampfile = ampfiles[[fileno]];


Print[ampfile];


ncfactor2ncpower = Association[{"Ncp0"->0, "Ncp1"->1, "Ncp2"->2, "Ncpm1"->-1, "Ncpm2"->-2, "Ncpm3"->-3}];
nffactor2nfpower = Association[{"Nfp0"->0, "Nfp1"->1, "Nfp2"->2}];

MakeHelicity[x_]:=StringReplace[x,{"+"->"p","-"->"m"}];

toNcNfLabel=Association[{
  {0,0}->"1",
  {1,0}->"Nc",
  {2,0}->"Nc^2",
  {0,1}->"Nf",
  {-2,0}->"1/Nc^2",
  {-1,1}->"Nf/Nc",
  {1,1}->"Nc*Nf",
  {-1,0}->"1/Nc",
  {0,2}->"Nf^2",
  {-1,2}->"Nf^2/Nc",
  {-2,1}->"Nf/Nc^2"}];

colourfactor2number = Association[{
  "T2341"->1,
  "T2431"->2,
  "d12d34"->3
}];


If[!FileExistsQ[ampdir<>"/"<>ampfile], Print[ampdir<>"/"<>ampfile, " not found"]; Exit[]];
Print["processing ", ampfile];


loopord = "2L";
colourfactor = StringSplit[ampfile,"_"][[4]];
nfpower = nffactor2nfpower[StringSplit[ampfile,"_"][[5]]];
ncpower = ncfactor2ncpower[StringSplit[ampfile,"_"][[6]]];
helicity = StringReplace[StringSplit[ampfile,"_"][[-1]],{".m"->"","p"->"+","m"->"-"}];

ncnflabel = toNcNfLabel[{ncpower,nfpower}];
colourcomponent = colourfactor2number[colourfactor];


Print["loopord = ", loopord];
Print["colourfactor = ", colourfactor];
Print["nfpower = ", nfpower];
Print["ncpower = ", ncpower];
Print["helicity = ", helicity];
Print["ncnflabel = ", ncnflabel];


Which[
  loopord==="1L",
  {epsmin,epsmax} = {-2,2},
  loopord==="2L",
  {epsmin,epsmax} = {-4,0},
  True, Print["unknown loop order"]; Exit[]];


(* *)
norm["0L","T2341"]=1/2;
norm["1L","T2341"]=norm["0L","T2341"]*(-1/2);
norm["2L","T2341"]=norm["0L","T2341"]*(1/4);
(* *)
norm["0L","T2431"]=1/2;
norm["1L","T2431"]=norm["0L","T2431"]*(-1/2);
norm["2L","T2431"]=norm["0L","T2431"]*(1/4);
(* *)
norm["1L","d12d34"]=norm["1L","T2341"]/2;
norm["2L","d12d34"]=norm["2L","T2341"]/2;
(* *)


fileoutsub = "finrem_2g2bH/"<>StringReplace[ampfile,"bare"->"poles"];
fileoutfin = "finrem_2g2bH/"<>StringReplace[ampfile,"bare"->"finrem"];


Print[fileoutsub];
Print[fileoutfin];


(* ::Section:: *)
(*read amplitudes*)


Clear[Amp,AmpRules];
AllAmpRules = {};


(* ::Subsection:: *)
(*tree level*)


Get[ampdir<>"/DiagramNumerators_0L_2g2bH_T2341__OHHqbm4qbm2_Ncp0_dsm2p0.m"];
Amp["0L","T2341","1"]=norm["0L","T2341"]*f["0L","T2341","1"]*mono[1];
AmpRules["0L","T2341","1"] = {f["0L","T2341","1"]->DiagramNumerator[helicity,topo[]] /. INT[n_,p_,tp_]:>n} /. ex[1]->1;
AllAmpRules = Join[AllAmpRules, AmpRules["0L","T2341","1"]];
Clear[DiagramNumerator];


Get[ampdir<>"/DiagramNumerators_0L_2g2bH_T2431__OHHqbm4qbm2_Ncp0_dsm2p0.m"];
Amp["0L","T2431","1"]=norm["0L","T2431"]*f["0L","T2431","1"]*mono[1];
AmpRules["0L","T2431","1"] = {f["0L","T2431","1"]->DiagramNumerator[helicity,topo[]] /. INT[n_,p_,tp_]:>n} /. ex[1]->1;
AllAmpRules = Join[AllAmpRules, AmpRules["0L","T2431","1"]];
Clear[DiagramNumerator];


(* ::Subsection:: *)
(*one loop*)


(* hard-coded 1-loop amplitude files *)
files1L={
  (* *)
  "bare_1L_2g2bH_T2341_Nfp0_Ncp1",
  "bare_1L_2g2bH_T2341_Nfp0_Ncpm1",
  "bare_1L_2g2bH_T2341_Nfp1_Ncp0",
  "bare_1L_2g2bH_d12d34_Nfp0_Ncp0",
  "bare_1L_2g2bH_T2431_Nfp0_Ncp1",
  "bare_1L_2g2bH_T2431_Nfp0_Ncpm1",
  "bare_1L_2g2bH_T2431_Nfp1_Ncp0",
  (* *)
  Nothing
};
files1L = StringJoin[#,"_",MakeHelicity[helicity],".m"]& /@ files1L;


Do[
  If[FileExistsQ[ampdir<>"/"<>file],
    Print["reading ", ampdir<>"/"<>file];
    label = toNcNfLabel[{ncfactor2ncpower[StringSplit[file,"_"][[6]]],
      nffactor2nfpower[StringSplit[file,"_"][[5]]]}];
    cf = StringSplit[file,"_"][[4]];
    
    {AmpRules["1L",cf,label], Amp["1L",cf,label]}=Get[ampdir<>"/"<>file] /.
       f[i_]:>f["1L",cf,label][i];
    
    Amp["1L",cf,label] = norm["1L",cf]*Dot@@Amp["1L",cf,label];
    AllAmpRules = Join[AllAmpRules, AmpRules["1L",cf,label]];
    
    Clear[cf,label];    
    ,
    Print[ampdir<>"/"<>file, " not found!"]; Abort[];
  ];
,{file,files1L}];


(* ::Subsection:: *)
(*two loops*)


{AmpRules[loopord,colourfactor,ncnflabel], 
 Amp[loopord,colourfactor,ncnflabel]} = Get[ampdir<>"/"<>ampfile] /. 
    f[i_]:>f[loopord,colourfactor,ncnflabel][i];
    
Amp[loopord,colourfactor,ncnflabel] = norm[loopord,colourfactor]*Dot@@Amp[loopord,colourfactor,ncnflabel];

AllAmpRules = Join[AllAmpRules, AmpRules[loopord,colourfactor,ncnflabel]];


(* ::Subsection:: *)
(*prepare ansatz for factors*)


sum2list[expr_]:=Which[
  NumberQ[expr], expr,
  Head[expr]===Plus, List@@expr,
  True, expr];


denfactors=Denominator[Together/@Flatten[sum2list/@Values[AllAmpRules]]];
denfactors=Flatten[DeleteCases[FactorList[#][[All,1]],a_/;NumberQ[a]]&/@denfactors];
denfactors=Flatten[DeleteCases[FactorList[#][[All,1]],a_/;NumberQ[a]]&/@denfactors];
denfactors=Flatten[DeleteCases[FactorList[#][[All,1]],a_/;NumberQ[a]]&/@denfactors];

Print["found ", Length[denfactors], " denominator factors"];


(* ::Section:: *)
(*prepare symbolic subtraction*)


(* ::Subsection:: *)
(*one-mass pentagon functions*)


(* NB: here p1^2 =!= 0 *)
(* w.r.t. the process, momenta are relabelled as 12345->54321 *)
PF2log = 
 {F[1,1]->Log[p1sq],
  F[1,2]->Log[-s[3,4]],
  F[1,3]->Log[s[1,2]],
  F[1,4]->Log[-s[1,5]],
  F[1,5]->Log[s[2,3]],
  F[1,6]->Log[s[4,5]],
  F[1,7]->Log[-s[2,5]],
  F[1,8]->Log[-s[2,4]],
  F[1,9]->Log[-s[3,5]],
  F[1,10]->Log[s[1,3]],
  F[1,11]->Log[-s[1,4]]};

log2PF = PF2log /. Rule[a_,b_]:>Rule[b,a];


toPFmonomials = {F[a__]:>mono[F[a]],im[a__]:>mono[im[a]], re[a__]:>mono[re[a]]};
monorules = {mono[a_]*mono[b_]:>mono[a*b], mono[a_]^k_:>mono[a^k]};


(* ::Subsection:: *)
(*anomalous dimensions (from https://arxiv.org/pdf/2304.06682.pdf)*)


(* values of all anomalous dimensions and flags *)
ExpectedValues = {
  gcusp[0]->4,
  gcusp[1]->(268/9-4*Pi^2/3)*cA-80/9*Tf*Nf,
  gcusp[2]->cA^2*(490/3-536/27*Pi^2+44/45*Pi^4+88/3*Zeta[3])+
    cA*Tf*Nf*(-1672/27+160/27*Pi^2-224/3*Zeta[3])+
    cF*Tf*Nf*(-220/3+64*Zeta[3])-64/27*Tf^2*Nf^2,
    
  gquark[0]->-3*cF,
  gquark[1]->cF^2*(-3/2+2*Pi^2-24*Zeta[3])+cF*cA*(-961/54-11/6*Pi^2+26*Zeta[3])+
    cF*Tf*Nf*(130/27+2*Pi^2/3),
(*  gquark[2]->cF^3*gqflag[1]+cF^2*cA*gqflag[2]+cF*cA^2*gqflag[3]+cF^2*Tf*Nf*gqflag[4]+
    cF*cA*Tf*Nf*gqflag[5]+cF*Tf^2*Nf^2*gqflag[6],*)
    
  ggluon[0]->-beta[0],
  ggluon[1]->cA^2*(-692/27+11/18*Pi^2+2*Zeta[3])+cA*Tf*Nf*(256/27-2*Pi^2/9)+4*cF*Tf*Nf,
  
  beta[0]->11/3*cA-4/3*Tf*Nf,
  beta[1]->2/3*(17*cA^2-10*cA*TR*Nf-6*cF*TR*Nf),
  
  cA -> Nc*(2*TR), 
  cF -> (Nc^2-1)/2/Nc*(2*TR),
  TR -> 1/2,
  Tf -> TR
} /.  {Pi->-I*im[1, 1], Zeta[3]->re[3,1]};


ExpectedValues = Expand[ExpectedValues  /. toPFmonomials]//.monorules;


(* ::Subsection:: *)
(*subtraction*)


fullamp0L = {
  Amp["0L","T2341","1"],
  Amp["0L","T2431","1"],
  0
};


fullamp1L = {
  Amp["1L","T2341","Nc"]*Nc + Amp["1L","T2341","1/Nc"]*Nc^(-1) + Amp["1L","T2341","Nf"]*Nf,
  Amp["1L","T2431","Nc"]*Nc + Amp["1L","T2431","1/Nc"]*Nc^(-1) + Amp["1L","T2431","Nf"]*Nf,
  Amp["1L","d12d34","1"]*1
};


id = IdentityMatrix[3];
Z1 = Get["Z1.m"];
Z2 = Get["Z2.m"];

Z1=Expand[Z1 /. toPFmonomials] //. monorules;
Z2=Expand[Z2 /. toPFmonomials] //. monorules;


(* renormalisation of Yukawa coupling - eqs. (A.3-4) of https://arxiv.org/pdf/1904.08961.pdf *)
(* NB they expand in aS/(2*Pi) whereas we expand in aS/(4*Pi), hence a factor of 2 in s1 and of 4 in s2 *)
YukawaS1 = 2*(-3/2*cF/eps);
YukawaS2 = 4*(3/8/eps^2*(3*cF^2+beta[0]*cF)-(1/8/eps)*(3/2*cF^2+97/6*cF*cA-10/3*cF*Tf*Nf));


sub2Lsymb = fullamp0L . (Z2 - Z1 . Z1 - Z1*beta[0]/eps - beta[0]^2/eps^2*id + beta[1]/2/eps*id - YukawaS2*id +
  YukawaS1*Z1 + YukawaS1*beta[0]/eps*id) + 
  fullamp1L . (Z1 + 2*beta[0]/eps*id - YukawaS1*id);


subtractionsymb = Coefficient[Coefficient[sub2Lsymb[[colourcomponent]]//.ExpectedValues,Nc,ncpower],Nf,nfpower];
subtractionsymb = Expand[subtractionsymb]//.monorules;


Print[AbsoluteTiming[subtractionsymb = Collect[subtractionsymb,_mono|_f,Normal[Series[#,{eps,0,epsmax}]]&];]];


(* ::Section:: *)
(*subtraction*)


(* ::Subsection:: *)
(*check cancellation of poles numerically*)


finremsymb = Amp[loopord,colourfactor,ncnflabel]-subtractionsymb;


(* numerical evaluation of finite remainder for later checks *)
xs = {ex[2],ex[3],ex[4],ex[5],ex[6]};
rndxs = Thread[xs->RandomInteger[{10^2,10^8},Length[xs]]];

finremnum = finremsymb /. Dispatch[AllAmpRules/.rndxs];
finremnum = Collect[finremnum,_mono,Normal[Series[#,{eps,0,epsmax}]]&];


(* check cancellation of poles *)
Do[
  If[Coefficient[finremnum,eps,w]===0, Print["eps^"<>ToString[w]<>" = 0"],  Print["eps^"<>ToString[w]<>" =!= 0"]; (*Exit[]*)];
,{w,epsmin,-1}];


(* ::Subsection:: *)
(*reconstruct subtraction*)


If[MatchQ[subtractionsymb,0],
  Put[0,fileoutsub];
  subtraction = {{},{{},{}}};
,
  submonos = Union[Cases[subtractionsymb,mono[__],Infinity]];
  subcoeffs = Expand[Coefficient[subtractionsymb,submonos]];
  If[DeleteCases[Variables[subcoeffs],eps|f[__]|f[__][_]]=!={},
    Print["error"];
    Abort[]];
  allfs = Union[Cases[subcoeffs, f[__]|f[__][_], Infinity]];
  submatr = Flatten[Coefficient[#,allfs]&/@Flatten[Table[Coefficient[subcoeffs,eps,w],{w,epsmin,epsmax}]]];
  FFDeleteGraph["graph"];
  FFNewGraph["graph","in",xs];
  FFAlgRatNumEval["graph","matr",submatr]//Print;
  FFAlgRatExprEval["graph","fs",{"in"},xs,allfs/.AllAmpRules]//Print;
  FFAlgMatMul["graph","mult",{"matr","fs"},Length[submonos]*(epsmax-epsmin+1), Length[allfs], 1]//Print;
  FFGraphOutput["graph","mult"];
  {subcoeffs,subrules}=Reconstruct["graph",xs,denfactors,
  "NThreads"->nthreads,
  "BatchSize"->batchsize,
  "Apart"->ex[5]];
  subtraction = Table[eps^w,{w,epsmin,epsmax}] . ArrayReshape[subcoeffs, {epsmax-epsmin+1,Length[submonos]}];
  subtraction = {subrules,{subtraction,submonos}};
  Print["writing ", fileoutsub];
  Put[subtraction/.mono->Identity,fileoutsub];
  Clear[out,fileout,subcoeffs,submonos,subrules,submatr,subtractionsymb,finremsymb,allfs];
  FFDeleteGraph["graph"];
];


(* ::Subsection:: *)
(*reconstruct finite remainder*)


If[MatchQ[finremnum,0],
  Put[0,fileoutfin];
  Exit[];
,
  subtraction = subtraction /. f[i_]:>f["sub",i];
  finremsymb = Amp[loopord,colourfactor,ncnflabel]-(Dot@@subtraction[[2]]);
  allfs = Union[Cases[finremsymb, f[__]|f[__][_], Infinity]];
  monos = Union[Cases[finremsymb,mono[__],Infinity]];
  finremcoeffs = Expand[Coefficient[finremsymb,monos]];
  If[DeleteCases[Variables[finremcoeffs],eps|f[__]|f[__][_]]=!={},
  Print["error"];
  Abort[]];
  matr = Flatten[Coefficient[#,allfs]&/@Flatten[Table[Coefficient[finremcoeffs,eps,w],{w,0,epsmax}]]];
  FFDeleteGraph["graph"];
  FFNewGraph["graph","in",xs];
  FFAlgRatNumEval["graph","matr",matr]//Print;
  FFAlgRatExprEval["graph","fs",{"in"},xs,allfs/.AllAmpRules/.subtraction[[1]]]//Print;
  FFAlgMatMul["graph","mult",{"matr","fs"},Length[monos]*(epsmax+1), Length[allfs], 1]//Print;
  FFGraphOutput["graph","mult"];
  {fincoeffs,finrules}=Reconstruct["graph",xs,denfactors,
  "NThreads"->nthreads,
  "BatchSize"->batchsize,
  "Apart"->ex[5]];
  finrem = Table[eps^w,{w,0,epsmax}] . ArrayReshape[fincoeffs, {epsmax+1,Length[monos]}];
  Print["final check"];
  zero=(finrem/.(finrules/.rndxs)) . monos-finremnum;
  zero=Collect[zero,_mono|eps,Together];
  If[zero===0, Print["True"], Print["Failed"]; Abort[]];
  outfinrem = {finrules,{finrem,monos}}/.mono->Identity;
  Print["writing ", fileoutfin];
  Put[outfinrem/.mono->Identity,fileoutfin];
  Exit[];
];
