(* ::Package:: *)

<<FFUtils`;


Clear[GetSlice];
Options[GetSlice] = {"NThreads"->Automatic, "PrimeNo"->0, "PrintDebugInfo"->0};
GetSlice[graphin_,varsin_List,OptionsPattern[]]:=Module[{graphslice,nthreads,in,XXX,varsinXXX,slicedvars,slicecoeffs,res},

    nthreads = If[TrueQ[OptionValue["NThreads"]==Automatic],FFAutomaticNThreads[],OptionValue["NThreads"]];
    nthreads = If[EvenQ[nthreads],nthreads,nthreads-1];  (* round to even *)

    FFNewGraph[graphslice];
    FFGraphInputVars[graphslice,in,{XXX}];
    varsinXXX = Thread[RandomInteger[{2^4,2^60},Length[varsin]]+XXX*RandomInteger[{2^4,2^60},Length[varsin]]];
    FFAlgRatFunEval[graphslice,slicedvars,{in},{XXX},varsinXXX];
    FFAlgSimpleSubgraph[graphslice,slicecoeffs,{slicedvars},graphin];
    FFGraphOutput[graphslice,slicecoeffs];

Print["debug 1"];
    res = FFParallelReconstructUnivariateMod[graphslice,{XXX},{
       "StartingPrimeNo"->OptionValue["PrimeNo"],
       "NThreads"->nthreads, 
       "MaxPrimes"->200,
       "MaxDegree"->500,
       "PrintDebugInfo"->OptionValue["PrintDebugInfo"]}];
Print["debug 2"];

    FFDeleteGraph[graphslice];
Print["debug 3"];

    Return[{Rule@@@Transpose@({varsin, varsinXXX}),res} /. XXX->xxx];
];


Clear[GetSliceIn];
Options[GetSliceIn] = {"NThreads"->Automatic, "PrimeNo"->0, "PrintDebugInfo"->0, "MaxPrimes"->200};
GetSliceIn[graphin_,varsin_List,slicevars_List,OptionsPattern[]]:=Module[{graphslice,in,
  slice,othervars,slicedvars,slicecoeffs,
psvalues,res,nthreads},
    nthreads = If[TrueQ[OptionValue["NThreads"]==Automatic],FFAutomaticNThreads[],OptionValue["NThreads"]];
    nthreads = If[EvenQ[nthreads],nthreads,nthreads-1];  (* round to even *)
    FFNewGraph[graphslice,in,slicevars];
    othervars = Complement[varsin,slicevars];
    slice = varsin /. Thread[othervars->RandomInteger[{2^4,2^60},Length[othervars]]];
    
    FFAlgRatFunEval[graphslice,slicedvars,{in},slicevars,slice];
    FFAlgSimpleSubgraph[graphslice,slicecoeffs,{slicedvars},graphin];
    FFGraphOutput[graphslice,slicecoeffs];
    
    If[Length[slicevars]===1,
      res = FFParallelReconstructUnivariateMod[graphslice,slicevars,
         "StartingPrimeNo"->OptionValue["PrimeNo"], 
         "PrintDebugInfo"->OptionValue["PrintDebugInfo"],
         "NThreads"->nthreads, 
         "MaxPrimes"->200,
(*         "MinDegree"->nthreads/2-3,
         "DegreeStep"->nthreads/2,*)
         "MaxDegree"->500];,
       
       res = FFReconstructFunctionMod[graphslice,slicevars,
         "StartingPrimeNo"->OptionValue["PrimeNo"],
         "NThreads"->nthreads,
         "MaxDegree"->500,
         "PrintDebugInfo"->0,
         "PrintDebugInfo"->OptionValue["PrintDebugInfo"],
         "MaxPrimes"->OptionValue["MaxPrimes"]];
    ];
    
    FFDeleteGraph[graphslice];
    Return[{slice,res}];
];


Clear[GetLinearRelationsFromEvaluationsSimple];
Options[GetLinearRelationsFromEvaluationsSimple] = {"ExtraCoefficients"->{}};

GetLinearRelationsFromEvaluationsSimple[results_,inputvalues_, primeno_, complexity_,OptionsPattern[]]:=Module[
  {primevalue,allevaluations,allinputvalues,tmplearneps,tmpprimevalue,tmpfuncs,
   ccs,sys,subrules,resultsextra,resultsextramod,nextra,ccsextra,sysextra,fullsys,
   sortedccs,fullccs,ind1,subset,ind2,dseval,dslearn,dsrec,coeffsrels,linrels,
   independentsubset,cfdegs,amp,extrarules,neededextracoeffs,gg,g,cc,extravars},

  primevalue = FFPrimeNo[primeno];

  allevaluations = results;
  allinputvalues = inputvalues;

  ccs=cc/@Range[Length[allevaluations[[1]]]];
  sys = ccs . #&/@allevaluations;
  sortedccs = SortBy[ccs,complexity[[#[[1]]]]&];

  Print["found ",Length[allevaluations]," evaluations for ",Length[ccs]," coefficients"];

  If[!MatchQ[OptionValue["ExtraCoefficients"],{}],

    Print["using ",Length[OptionValue["ExtraCoefficients"]]," extra coefficients in linear fit"];
    extravars = Variables[OptionValue["ExtraCoefficients"]];
    If[Length[extravars]!=Length[allinputvalues[[1]]], Print["check extra coefficients variables"]; Abort[];];
subrules = Table[
      Rule@@@Transpose[{extravars,allinputvalues[[i]]}]
    ,{i,1,Length[allinputvalues]}];

    nextra = Length[OptionValue["ExtraCoefficients"]];

    FFNewGraph[gg,in,extravars];
    FFAlgRatExprEval[gg,extracoeffs,{in},extravars,OptionValue["ExtraCoefficients"]];
    FFGraphOutput[gg,extracoeffs];
    resultsextramod = Table[FFGraphEvaluate[gg,val[[;;,2]],{"PrimeNo"->primeno}],{val,subrules}];
    FFDeleteGraph[gg];

    ccsextra=cc/@(Length[allevaluations[[1]]]+Range[nextra]);
    sysextra = ccsextra . #&/@(resultsextramod);

    (* check for independence of extra coefficients *)
    nextralinrels = Length@FFDenseSolve[#==0&/@sysextra,ccsextra,{"IndepVarsOnly"->True, "StartingPrimeNo"->primeno}];
    Print["relations between extra coeffs: ",nextralinrels];
    GLOBALnextraindep = nextra-nextralinrels;

    fullsys = sys+sysextra;
    fullccs = Join[ccsextra,sortedccs];
    
    ,
    nextra = 0;
    fullsys = sys;
    fullccs = sortedccs;
  ];

  (* solve full system *)
  ind1 = FFDenseSolve[#==0&/@fullsys,fullccs,{"IndepVarsOnly"->True, "StartingPrimeNo"->primeno}];

  subset = Union[RandomSample[Range[Length[fullsys]],Length[fullsys]-3]];
  ind2 = FFDenseSolve[#==0&/@fullsys[[subset]],fullccs,{"IndepVarsOnly"->True, "StartingPrimeNo"->primeno}];

  If[Length[ind1]==Length[ind2],Print["system has closed"];,Print["system has not closed"]; Abort[];];
  Print["found ", Length[ind2]," linear relations"];

  (*Print[Length[allevaluations[[1]]]+nextra-Length[ind2]," independent coefficients"];*)

  FFNewGraph[g];
  FFAlgDenseSolver[g,ds,{},{},#==0&/@fullsys,fullccs];
  FFGraphOutput[g,ds];
  FFSetLearningOptions[g,ds,"PrimeNo"->primeno];
  dslearn = FFDenseSolverLearn[g,fullccs];
  dseval = FFGraphEvaluate[g,{},{"PrimeNo"->primeno}];
  dsrec = FFRatRec[dseval,primevalue];
  coeffsrels = FFDenseSolverSol[dsrec,dslearn];
  linrels = FFLinearRelationsFromFit[f@@#&/@fullccs,fullccs,coeffsrels];
  (* ignore linear relations amonsgt extra coefficients, we should probably strip these out of fullsys before solving *)

  independentsubset = Complement[f@@#&/@fullccs,linrels[[;;,1]]][[;;,1]];
  independentsubset = independentsubset /. i_Integer:>Nothing /; i>Length[complexity];
  Print["number of indep. coeffs in evaluations ", Length[independentsubset]];

  Print["maximal degree before linear relations: ",Max[complexity]];
  Print["maximal degree after linear relations: ",Max[complexity[[independentsubset]]]];

  linrels = linrels /. Rule[f[i_],_]:>Nothing /; i>Length[ccs];

  If[!MatchQ[OptionValue["ExtraCoefficients"],{}],
    neededextracoeffs = Variables[linrels[[;;,2]]] /. f[i_]:>Nothing /; i<=Length[allevaluations[[1]]];
    extrarules = Rule@@@Transpose[{ccsextra /. cc->f,OptionValue["ExtraCoefficients"]}];
    extrarules = extrarules /. Rule[a_,b_]:>Nothing /; !MemberQ[neededextracoeffs,a];
  ,
    extrarules = {};
  ];

  FFDeleteGraph[g];
  Return[{linrels,extrarules}];
]


Clear[GetLinearRelationsFromGraph];
Options[GetLinearRelationsFromGraph] = {"NThreads"->Automatic, 
  "BatchSize"->Automatic,"MaxPrimes"->15,"ExtraCoefficients"->{}};

GetLinearRelationsFromGraph[graphin_,varsin_List, complexity_List,OptionsPattern[]]:=Module[{
  batchsize,primeno,primecheck,primes,batchpts,batchvals,newlinrels,extra,
  depfs,indepfs,newdepfs,newindepfs,linrels,linrelsmatr,newlinrelsmatr,
  checkvals,extravals,rndpts},
  
  If[FFNParsOut[graphin]===1,
    Print["only one function"];
    Return[{}];
  ];

  If[OptionValue["ExtraCoefficients"]=!={}, 
    Print["using ", Length[OptionValue["ExtraCoefficients"]], " coefficients as ansatz"]];
    
  batchsize = If[OptionValue["BatchSize"]===Automatic, 
    FFNParsOut[graphin]+Length[OptionValue["ExtraCoefficients"]]+3, 
    OptionValue["BatchSize"]];
  primecheck = False;
  primeno=0;
  primes = {FFPrimeNo[primeno]};

  While[Not[primecheck]&&primeno<OptionValue["MaxPrimes"],
    Print["prime no. ", primeno];
    batchpts = RandomInteger[{10^2,10^8},{batchsize,Length[varsin]}];
    batchvals = FFGraphEvaluateMany[graphin,batchpts,"PrimeNo"->primeno,"NThreads"->OptionValue["NThreads"]];

    {newlinrels,extra}=GetLinearRelationsFromEvaluationsSimple[batchvals,batchpts,primeno,complexity,
      "ExtraCoefficients"->OptionValue["ExtraCoefficients"]];

    If[primeno>0,
      Print["chinese remainder"];

      depfs=Keys[linrels];
      indepfs=Variables[Values[linrels]];
      newdepfs=Keys[newlinrels];
      newindepfs=Variables[Values[newlinrels]];
      If[newdepfs=!=depfs || newindepfs=!=indepfs, Print["error: linear relations changed form"]; Abort[]];
      Clear[newdepfs,newindepfs];

      linrelsmatr=Flatten[Coefficient[#,indepfs]&/@Values[linrels]];
      newlinrelsmatr=Flatten[Coefficient[#,indepfs]&/@Values[newlinrels]];

      linrelsmatr=Table[FFRatRec[ChineseRemainder[{FFRatMod[linrelsmatr[[ii]],primes[[-1]]],FFRatMod[newlinrelsmatr[[ii]],FFPrimeNo[primeno]]},{primes[[-1]],FFPrimeNo[primeno]}],primes[[-1]]*FFPrimeNo[primeno]], {ii,Length[linrelsmatr]}];
      AppendTo[primes,primes[[-1]]*FFPrimeNo[primeno]];

      linrels=Thread[depfs->ArrayReshape[linrelsmatr,{Length[depfs],Length[indepfs]}] . indepfs];
      Clear[newlinrels,linrelsmatr,newlinrelsmatr,depfs,indepfs];

      ,

      linrels=newlinrels;
      Clear[newlinrels];
      
    ];
    
    If[linrels === {}, 
      primecheck=True; 
      ,
      
      rndpts=RandomInteger[{10^2,10^8},Length[varsin]];
      checkvals = FFGraphEvaluate[graphin,rndpts,"PrimeNo"->primeno+1];
      extravals = extra /. Thread[varsin->rndpts];
      primecheck=DeleteDuplicates[Flatten[FFRatMod[linrels/.extravals/.f[i_]:>checkvals[[i]]/.Rule[a_,b_]:>a-b,FFPrimeNo[primeno+1]]]] === {0};
      If[Not[primecheck], 
        Print["more primes needed"];
        batchsize = Length[Complement[Array[f,FFNParsOut[graphin]],Keys[linrels]]]+GLOBALnextraindep+3;
      ];
    ];

    primeno+=1;
    Print["========================================"];
  ];

  If[primeno<OptionValue["MaxPrimes"],
    If[OptionValue["ExtraCoefficients"]==={},
      Return[{linrels,{}}],
      Return[{linrels,extra}]
    ];
    Print["primes insufficient - increase MaxPrimes"];
    Return[$Failed]
  ]
];


Clear[GetDegree, GetDegreeMax];

GetDegree[expr_]:=deg[Exponent[Numerator[expr],xxx],Exponent[Denominator[expr],xxx]];

GetDegreeMax[expr_List] := Module[{degs,maxnum,maxden},
  degs = GetDegree/@expr;
  maxnum = Max@degs[[;;,1]];
  maxden = Max@degs[[;;,2]];
  Return[{maxnum,maxden}];
];


Clear[FindIndependentFactorsNumerical];

FindIndependentFactorsNumerical[expr_]:=Module[
  {exprfl,factors,varshere,unknowns,eqs,dummygraph,learn,indepfactors,
   indepfactorsasso,factoredexpr},

  exprfl = If[Head[expr]===List, FactorList/@expr, {FactorList[expr]}];

  factors = SortBy[Union[DeleteCases[Flatten[exprfl],a_/;Variables[a]==={}]],LeafCount]; 
  varshere = Variables[factors];
       
  unknowns = Array[ccc,Length[factors]];
  eqs = DeleteCases[Table[unknowns . D[Log[factors],vvv],{vvv,varshere}],0];
  eqs = Flatten[eqs /. 
      Table[Thread[varshere->RandomInteger[{10^3,10^6},Length[varshere]]],Length[factors]]];
  
  FFNewGraph[dummygraph];
  FFAlgDenseSolver[dummygraph,"solver",{},{},eqs==0//Thread,unknowns];
  FFGraphOutput[dummygraph,"solver"];
  learn = FFDenseSolverLearn[dummygraph,unknowns];
  FFDeleteGraph[dummygraph];
  indepfactors = Union@@({"DepVars","ZeroVars"}/.learn) /. ccc[k_]:>k;
  indepfactors = factors[[indepfactors]];
  indepfactors = Thread[Array[y,Length[indepfactors]]->indepfactors];
  
  indepfactorsasso = Association[Join[indepfactors /. Rule[a_,b_]:>Rule[b,a],
        Thread[Factor[-indepfactors[[All,2]]]->-indepfactors[[All,1]]] ]];
  
  factoredexpr = Table[ ff[[1,1]]^ff[[1,2]]*Times@@Map[Power[indepfactorsasso[#[[1]]],#[[2]]]&, Rest[ff]],{ff,exprfl}];
  
  Return[{factoredexpr,indepfactors}]
];


Clear[MatchCoefficientFactors2];

Options[MatchCoefficientFactors2]={"PrimeNo"->0};

MatchCoefficientFactors2[slicedcoeffs_List, slicedfactorguess_List,OptionsPattern[]]:=Module[
  {allfactors,factorguessclist,factorguessass,slicedcoeffsclist,slicedcoeffsclistass,
   eqs,sol,lhs,cs,rhss,slicedfactorguessnonconst},
  
  If[Variables[slicedcoeffs]=!={xxx}, 
    Print["Careful!!!! This should go through anyway, but there is a problem with FindIndependentFactorsNumerical"]
  ];
  
  slicedfactorguessnonconst = DeleteCases[slicedfactorguess, zz_/;Variables[zz[[1]]]==={}];

  factorguessclist = DeleteCases[FactorList[#,"Modulus"->FFPrimeNo[OptionValue["PrimeNo"]]], a_/;Variables[a]==={}]&/@slicedfactorguessnonconst[[All,2]];
  allfactors = Union[DeleteCases[Flatten[factorguessclist],a_/;Variables[a]==={}]];
  factorguessass = Table[Association[Rule@@#&/@cc],{cc,factorguessclist}];

  slicedcoeffsclist = Map[DeleteCases[FactorList[#,"Modulus"->FFPrimeNo[OptionValue["PrimeNo"]]], a_/;Variables[a]==={}]&,slicedcoeffs];

  slicedcoeffsclistass = Table[Association[(Rule@@#)&/@scf], {scf,slicedcoeffsclist}];
  cs = Array[tiz,Length[slicedfactorguessnonconst]];
  lhs = Table[cs . (#[fac]&/@factorguessass/._Missing->0), {fac,allfactors}];
  rhss = Table[Table[(scfa[fac]/._Missing->0), {fac,allfactors}],{scfa,slicedcoeffsclistass}];
  sol = Flatten[Quiet[Solve[lhs-#==0]]]&/@rhss;

  If[MemberQ[sol,{}], Print["No solution found"]; Return[{}],
    Return[Times@@(slicedfactorguessnonconst[[All,1]]^#) &/@(cs /. sol)] 
  ];
];


Clear[ReconstructFunctionFactors];

Options[ReconstructFunctionFactors] = {"PrintDebugInfo"->1, "NThreads"->Automatic,
  "SliceFile"->False, "Degrees"->False};
  
ReconstructFunctionFactors[graphin_,vars_List,factors_List,OptionsPattern[]] := 
  Module[{graph,slicerules,coeffs,factoransatz,coeffactorguess,coeffnorm,res,timings,rndpt},

    If[!SubsetQ[vars,Variables[factors]], 
      Print["wrong variables"];
      Abort[];
    ];
    
    FFNewGraph[graph];
    FFGraphInputVars[graph,"in",vars];
    FFAlgSimpleSubgraph[graph,"funcs",{"in"},graphin];
    FFGraphOutput[graph,"funcs"];

    If[OptionValue["SliceFile"]===False,
      Print["reconstructing univariate slice..."];
      {slicerules,coeffs}=GetSlice[graph,vars,"NThreads"->OptionValue["NThreads"]];
      ,
      Print["not yet implemented (needed slice of independent coefficients)"];
      Abort[];
 (*     Print["reading slice from ", OptionValue["SliceFile"]];
      {slicerules,coeffs} = Get[OptionValue["SliceFile"]];
 *)
    ];

    If[OptionValue["PrintDebugInfo"]>0,Print["degrees before normalisation ",GetDegreeMax[coeffs]];];

    factoransatz = FindIndependentFactorsNumerical[factors][[2,;;,2]];
    coeffactorguess = Rule[#,Factor[#/.slicerules,Modulus->FFPrimeNo[0]]]&/@factoransatz;
    coeffnorm = MatchCoefficientFactors2[coeffs,coeffactorguess];

    FFAlgRatFunEval[graph,"factornode",{"in"},vars,Together[1/coeffnorm]];
    FFAlgMul[graph,"funcnorm",{"funcs","factornode"}];
    FFGraphOutput[graph,"funcnorm"];
    
    If[OptionValue["PrintDebugInfo"]>0,
      Print["after normalisation {max num., max den. }",
        GetDegreeMax[Factor[coeffs/coeffnorm/. slicerules,Modulus->FFPrimeNo[0]]]];
    ];

    (* manual timing *)
    If[OptionValue["PrintDebugInfo"]>0,
      timings = {};
      Do[
        rndpt = RandomInteger[{10^4,10^8},Length[vars]];
        AppendTo[timings, AbsoluteTiming[FFGraphEvaluate[graph,rndpt]][[1]]];
      ,5];
      Print["timings (s): ", timings];
      Print["mean evaluation time (s) excluding the first: ", Mean[timings[[2;;-1]]]];
      Clear[rndpt,timings];
    ];
 
    If[OptionValue["Degrees"]===False,
      res = FFReconstructFunction[graph,vars,
        "PrintDebugInfo"->OptionValue["PrintDebugInfo"],"NThreads"->OptionValue["NThreads"]];
      ,
      Print["==================================="];
      Print["computing degrees manually"];
      GetAllDegrees[graph,vars,OptionValue["Degrees"],
        "NThreads"->OptionValue["NThreads"], "PrintDebugInfo"->OptionValue["PrintDebugInfo"]];
      Print["==================================="];
        
      res = FFReconstructFunction[graph,vars,
        "PrintDebugInfo"->OptionValue["PrintDebugInfo"],"NThreads"->OptionValue["NThreads"],
        "Degrees"->OptionValue["Degrees"]];
    ];

    FFDeleteGraph[graph];

    res = res*coeffnorm;
    Return[res];
];


Clear[ConstructAnsatzX];
ConstructAnsatzX[cnormfactored_,factors_List,numdeg_,x_] := Module[{tmp,den,factlist,facts,degfact,expfact,
   guessednumdeg,factorsnox,dendeg,denfactors,ansatzterms},
    tmp = Together[cnormfactored];
    
    den = Denominator[tmp];
    guessednumdeg = Exponent[Numerator[tmp] /. factors,x];
    
    (* We set to 1 all factors which do not depend on x *)
    factorsnox = Select[factors, FreeQ[Variables[#[[2]]],x]&];
    factorsnox = factorsnox[[All,1]]->1//Thread;

    dendeg = Exponent[den /. factors,x];
    den = den /. factorsnox;
    factlist = Select[FactorList[den],Variables[#]=!={}&];
    
    expfact = factlist[[All,2]];
    facts = factlist[[All,1]];
    degfact = Exponent[facts /. factors,x];

    ansatzterms = Table[ x^i0/facts[[i1]]^i2,{i1,1,Length[facts]},{i0,0,degfact[[i1]]-1},{i2,0,expfact[[i1]]}]//Flatten;
    
    If[guessednumdeg+numdeg>dendeg, 
       ansatzterms = Join[ansatzterms, Table[x^ii, {ii,1,guessednumdeg+numdeg-dendeg}]]; ];
    
    ansatzterms = Append[DeleteDuplicates@DeleteCases[ansatzterms,1],1]; (* 1 must be at the end in FFLinearFit *)

    ansatzterms = ansatzterms/cnormfactored;
    
    Return[ansatzterms]
]



SumToList[expr_] := If[Head[expr]===Plus, List@@expr, {expr}];


Clear[ReconstructFunctionApart];
Options[ReconstructFunctionApart]={"PrimeNo"->0,"NThreads"->Automatic,
   "ReconstructionMode"->"factors","PrintDebugInfo"->0,
   "CoefficientAnsatzResidues"->False,"Iterative"->0,
   "Degrees"->Automatic,"DegreesOnly"->False,"ExternalCoeffNorm"->{}};

ReconstructFunctionApart[graphin_, varsin_List, X_, coefficientansatz_List, OptionsPattern[]] := Module[{graph,slicerules,coeffactorguess,
  coeffansatztwist,slicedfactors,coeffnorm,dendegreesX,numdegreesX,slicerulesX,slicedX,coeffnormfactored,fitgraph,allmt,sliced,now,
  factors,ansaetze,lengthansaetze,outputelements,fitentries,fitcoefficients,allmtbutX,fitlearn,recfit,solfit,
  outcoeffs,allcoeffsout,sort,rndpoint,Print1,Print2,extrafactorsguess,tmpsol,newcoeffactorguess,
  totaldegrees,tmpslicerules,tmpsliced,maxmindegrees,alldegrees,zeovars,sliceout,
  ncoeffnorm,coeffnormfactorlist,coeffnormfactors,coeffnormfillers,coeffnormfilled,coeffnormfilledtransposed,coeffnormtick
  },

  If[!MemberQ[varsin,X], Print["cannot apart w.r.t. ", X]; Abort[];,
     allmt = Prepend[DeleteCases[varsin,X], X];];
            
  Print1[strings__] := If[OptionValue["PrintDebugInfo"]>0, Print[strings]]; 
  Print2[strings__] := If[OptionValue["PrintDebugInfo"]>1, Print[strings]];
  
  (* Needed for iterative stuff *)
  coeffansatztwist = Select[Factor@coefficientansatz, Complement[Variables[#],varsin]==={} &]; 

  FFDeleteGraph[graph];
  FFNewGraph[graph,in,allmt];

  (* Sort variables. The variable we partial-fraction with respect to must be first *)
  FFAlgRatFunEval[graph,sortedvars,{in},allmt,varsin];
  FFAlgSimpleSubgraph[graph,coefficients,{sortedvars},graphin];
  FFGraphOutput[graph, coefficients];
  
  Print1["computing univariate slice in ", allmt]; now = AbsoluteTime[];
  {slicerules, sliced} = GetSlice[graph, allmt,{"PrimeNo"->OptionValue["PrimeNo"], "NThreads"->OptionValue["NThreads"]}];
  Print1["-> ", AbsoluteTime[]-now, " s"];
  Print1["degrees before normalisation {max num., max den.}", GetDegreeMax@sliced];

  Print1["matching coefficient factors"];
  coeffactorguess = FindIndependentFactorsNumerical[coeffansatztwist][[2,;;,2]];
  coeffactorguess = Rule[#,Factor[#/.slicerules,Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]]&/@coeffactorguess;
  coeffnorm = MatchCoefficientFactors2[sliced,coeffactorguess,"PrimeNo"->OptionValue["PrimeNo"]]; 
  Print1["degrees after normalisation {max num., max den.}", GetDegreeMax@Factor[(sliced/coeffnorm/.slicerules),Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]];

  (*FFAlgRatFunEval[graph,norm,{in},allmt,Together[1/coeffnorm]]//Print;*)

  coeffnormtick=AbsoluteTime[];
  ncoeffnorm = Length[coeffnorm];
  Print["number of coeficients: ", ncoeffnorm];
  Print["getting factorlist ",AbsoluteTiming[coeffnormfactorlist = FactorList/@coeffnorm;]];
  coeffnormfactors = Table[coeffnormfactorlist[[ii,;;,1]]^coeffnormfactorlist[[ii,;;,2]], {ii, ncoeffnorm}];
  coeffnormfillers = Table[1,#]& /@ (Max[Length/@coeffnormfactors]-Length/@coeffnormfactors);
  coeffnormfilled = Join[coeffnormfactors[[#]],coeffnormfillers[[#]]]& /@ Range[ncoeffnorm];
  coeffnormfilledtransposed = Transpose[coeffnormfilled];
  Print[coeffnormfilledtransposed // Dimensions];
  Do[
    Print[ii," ",AbsoluteTiming[ FFAlgRatFunEval[graph,coeffnormnode[ii],{in},allmt,1/coeffnormfilledtransposed[[ii]]] ]];
  ,{ii,1,Length[coeffnormfilledtransposed]}];
  Print[FFAlgMul[graph,norm,Table[coeffnormnode[ii],{ii,Length[coeffnormfilledtransposed]}]]];
  Print["norm done in ",(AbsoluteTime[]-coeffnormtick)/1.0," seconds"];

  FFAlgMul[graph,normcoeffs,{coefficients,norm}]//Print;
  FFGraphOutput[graph,normcoeffs];
  
  Print1["computing univariate slice in ", X];  now = AbsoluteTime[];
  {slicerulesX, slicedX} = GetSliceIn[graph,allmt,{X},{"PrimeNo"->OptionValue["PrimeNo"], "NThreads"->OptionValue["NThreads"]}];
  slicerulesX = Thread[allmt->slicerulesX];
  Print1["-> ", AbsoluteTime[]-now, " s"];
  Print2["check denominators: ", Union[Denominator[slicedX]]==={1}];

  dendegreesX = Exponent[Denominator[Together[coeffnorm /. slicerulesX]], X];
  numdegreesX = Exponent[slicedX, X];
  

   If[OptionValue["ExternalCoeffNorm"]!={},
     coeffnorm=OptionValue["ExternalCoeffNorm"];
     ];

  
  Print1["finding independent factors"];
  {coeffnormfactored, factors} = FindIndependentFactorsNumerical[coeffnorm];
  
  Print1["constructing ansaetze for partial fractions"];
  ansaetze = Table[ConstructAnsatzX[coeffnormfactored[[Kk]],factors,numdegreesX[[Kk]],X],{Kk,Length[coeffnormfactored]}];

  lengthansaetze = Length/@ansaetze;
  Print["max lengthansaetze = ",Max[lengthansaetze]];

  (*Export["debug_recapart_ansaetze.m",{allmt,X,factors,ansaetze}];*)

  Print[AbsoluteTiming[FFAlgRatFunEval[graph,ys,{in},allmt, Append[factors[[All,2]],X]];]];

  Block[{},
    FFCoefficientRules[x_,y_]:=CoefficientRules[x,y];
    Print[AbsoluteTiming[ FFAlgRatFunEval[graph,ansatz,{ys},Append[factors[[All,1]],X],Flatten[ansaetze]] ]];
  ];

  FFAlgChain[graph,eqs,{ansatz,normcoeffs}];
  FFGraphOutput[graph,eqs];

  (* preparing pattern for multi-fit *)
  outputelements = Flatten[Join[Table[ans[i1,i2], {i1,1,Length[coeffnormfactored]},{i2,1,lengthansaetze[[i1]]}],
    Array[r,Length[coeffnormfactored]]]];
  Print[FFNParsOut[graph,eqs]-Length[outputelements]===0];

  fitentries = Table[ Append[Table[ans[i1,i2],{i2,lengthansaetze[[i1]]}], r[i1]], {i1,Length[coeffnormfactored]}];
  fitcoefficients = Table[c[i1,i2], {i1,Length[coeffnormfactored]},{i2,lengthansaetze[[i1]]}];

  (* Form of the equations for the fit: *)
  (*Append[fitcoefficients[[1]],0].fitentries[[1]] == fitentries[[1,-1]]*)
  
  Print1["preparing multi-fit graph"];
  allmtbutX = DeleteCases[allmt,X];
  FFDeleteGraph[fitgraph];
  FFNewGraph[fitgraph,in,allmtbutX];
  FFAlgSubgraphMultiFit[fitgraph,multifit,{in},graph,{X},outputelements->fitentries];
  FFGraphOutput[fitgraph,multifit];
  
  Print1["learning"]; now = AbsoluteTime[];
  fitlearn = FFMultiFitLearn[fitgraph,fitcoefficients];
  zerovars=Flatten["ZeroVars"/.fitlearn];
  Print1["-> ", AbsoluteTime[]-now, " s"];
  Print2["check learn output: ", And@@(#[[0]]==List&/@fitlearn)];
  Print2["check indep. variables: ", Union["IndepVars" /. fitlearn]==={{}}];
  
  Which[OptionValue["ReconstructionMode"]==="standard",
    Print["reconstructing... this may take some time"]; now = AbsoluteTime[];
    recfit = FFReconstructFunction[fitgraph,allmtbutX,{"NThreads"->OptionValue["NThreads"],"PrintDebugInfo"->OptionValue["PrintDebugInfo"]}];   
    Print["-> ", AbsoluteTime[]-now, " s"];,
 
       
    OptionValue["ReconstructionMode"]==="iterative",
    Print["the 'iterative' option doesn't work yet, sorry"]; Abort[];,
(*    Print["partial fractioning w.r.t. ", OptionValue["Iterative"]]; now = AbsoluteTime[];
    Print["----------------------------"];
    Print1["enhancing factor guess"]; now=AbsoluteTime[];    
    extrafactorsguess = {};
    For[ii=1,ii<=Length[coeffactorguess],ii++,
      If[Exponent[coeffactorguess[[ii,1]],X]===1,
        tmpsol = Flatten@Solve[coeffactorguess[[ii,1]]==0,X];         
        extrafactorsguess = Union[extrafactorsguess,Factor[coeffactorguess[[All,1]]/.tmpsol]];
        Clear[tmpsol];
      ];
    ];
    newcoeffactorguess = Select[Union[coeffactorguess[[All,1]],extrafactorsguess],FreeQ[Variables[#], X]&];
    newcoeffactorguess = FindIndependentFactors[newcoeffactorguess][[2,All,2]];    
    recfit = ReconstructFunctionApart[fitgraph, allmtbutX, OptionValue["Iterative"],newcoeffactorguess, 
       "PrintDebugInfo"\[Rule]OptionValue["PrintDebugInfo"]];
    recfit = recfit[[1]] /. recfit[[2]];
    Print["-> ", AbsoluteTime[]-now, " s"];
    Print["----------------------------"];,
*)    
        
   
    OptionValue["ReconstructionMode"]==="factors",
  
    recfit = Module[{slicerules2,sliced2,coeffactorguess2,slicedfactors2,coeffnorm2,recfit2},
      Print1["computing univariate slice in ", allmtbutX]; now=AbsoluteTime[];
      {slicerules2, sliced2} = GetSlice[fitgraph, allmtbutX,{"PrimeNo"->OptionValue["PrimeNo"], "NThreads"->OptionValue["NThreads"]}];
      Print1["->", AbsoluteTime[]-now, " s"];

      Print1["degrees before normalisation {max num., max den.}", GetDegreeMax@sliced2];

      If[OptionValue["CoefficientAnsatzResidues"],
        (* Guess spurious factors in the denominators *)
        Print1["enhancing factor guess"]; now=AbsoluteTime[];    
        extrafactorsguess = {};
        For[ii=1,ii<=Length[coeffactorguess],ii++,
          If[Exponent[coeffactorguess[[ii,1]],X]===1,
            tmpsol = Flatten@Solve[coeffactorguess[[ii,1]]==0,X];
            extrafactorsguess = Union[extrafactorsguess,Factor[coeffactorguess[[All,1]]/.tmpsol]];
            Clear[tmpsol];
          ];
        ];
        coeffactorguess2 = Select[Union[coeffactorguess[[All,1]],extrafactorsguess],FreeQ[Variables[#], X]&];
        coeffactorguess2 = FindIndependentFactorsNumerical[coeffactorguess2][[2,All,2]];
        Print1[Length[coeffactorguess2], " independent factors"];,
        
        coeffactorguess2 = Select[coeffactorguess[[All,1]], FreeQ[Variables[#], X]&];
      ];
        
      Print1["guessing factors"];      
      coeffactorguess2=Rule[#,Factor[#/.slicerules2,Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]]&/@coeffactorguess2;
      coeffnorm2 = MatchCoefficientFactors2[sliced2,coeffactorguess2,"PrimeNo"->OptionValue["PrimeNo"]];  
      Print1["degrees after normalisation {max num., max den.}", GetDegreeMax@Factor[(sliced2/coeffnorm2/.slicerules2),Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]];

     (*If[OptionValue["DegreesOnly"],
        Return[GetDegree/@Factor[(sliced2/coeffnorm2/.slicerules2),Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]];
       ];*)
    (*If[OptionValue["DegreesOnly"],
       Print["SUBS=",Thread[Rule[zerovars,ConstantArray[0,Length@zerovars]]]];
       subListLengths=(DeleteCases[#,0]&/@(fitcoefficients /.Thread[Rule[zerovars,ConstantArray[0,Length@zerovars]]]))[[All,-1]];
        
       Print["SUBLISTLENGTHS=",subListLengths];
       (*subListLengths=subListLengths /.c[i_,j_]:>j;*)
       subListLengths= (fitcoefficients[[All, -1]] /. c[i_, j_] :> j) - toSubtract;
       Print["SUBLISTLENGTHS=",subListLengths];
       degList=(GetDegree/@Factor[(sliced2/coeffnorm2/.slicerules2),Modulus->FFPrimeNo[OptionValue["PrimeNo"]]])/.deg[i_,j_]:>Max@{i,j};
       Print["DEGLIST=",degList];
       degList2={};
       term=0;
       incremental={};
       Do[
          term=term+length;
          AppendTo[incremental,term];
          ,{length,subListLengths}];
       incremental=Prepend[incremental,1];
       Do[
          If[i==1,n=1;,n=incremental[[i]]+1];
          AppendTo[degList2,degList[[n;;incremental[[i+1]]]]];
          ,{i,Length@incremental-1}];

      Return[degList2];
     ];*)
     If[OptionValue["DegreesOnly"],
       zeroList=zerovars /.c[i_,j_]:>i;
       toSubtract =ConstantArray[0, fitcoefficients[[-1, -1]] /. c[i_, j_] :> i];

       Do[
          toSubtract[[i]] = toSubtract[[i]] + Length@Position[zeroList, i];
          ,{i, (fitcoefficients[[-1, -1]] /. c[i_, j_] :> i) }];
       subListLengths= (fitcoefficients[[All, -1]] /. c[i_, j_] :> j) - toSubtract;
       degList=(GetDegree/@Factor[(sliced2/coeffnorm2/.slicerules2),Modulus->FFPrimeNo[OptionValue["PrimeNo"]]])/.deg[i_,j_]:>Max@{i,j};
       degList2={};
       term=0;
       incremental={};
       Do[
          term=term+length;
          AppendTo[incremental,term];
          ,{length,subListLengths}];
        incremental=Prepend[incremental,1];
       Do[
          If[i==1,n=1;,n=incremental[[i]]+1];
          AppendTo[degList2,degList[[n;;incremental[[i+1]]]]];
          ,{i,Length@incremental-1}];

      Return[degList2];
     ];

      
      FFAlgRatFunEval[fitgraph,norm,{in},allmtbutX,Together[1/coeffnorm2]]//Print;
      FFAlgMul[fitgraph,normcoeffs,{multifit,norm}]//Print;
      FFGraphOutput[fitgraph,normcoeffs];
      
      If[OptionValue["Degrees"]=!=Automatic,

        (* compute degrees manually, dump to file and read them in *)
        Print1["computing degrees"];
        totaldegrees = GetDegree/@Factor[(sliced2/coeffnorm2/.slicerules2),Modulus->FFPrimeNo[OptionValue["PrimeNo"]]];
        Print2[totaldegrees];
      
        Do[ 
          Print1["degrees in ", vv]; now = AbsoluteTime[];
          {tmpslicerules, tmpsliced} = GetSliceIn[fitgraph,allmtbutX,{vv},{"PrimeNo"->OptionValue["PrimeNo"],"NThreads"->OptionValue["NThreads"]}];
          maxmindegrees[vv] =  GetMaxMinDegrees[#,vv]&/@tmpsliced;
          Clear[tmpslicerules,tmpsliced];
          Print1["->", AbsoluteTime[]-now, " s"]; Print2[maxmindegrees[vv]];
        ,{vv,allmtbutX}];
        
        alldegrees = Flatten[{FFNParsOut[fitgraph,in],
           FFNParsOut[fitgraph],
           Table[{totaldegrees[[oo]], Table[maxmindegrees[vv][[oo]],{vv,allmtbutX}]},
             {oo,FFNParsOut[fitgraph]}]}/.deg[HH__]:>HH];
             
        Print["dumping degrees to ", OptionValue["Degrees"]];
        Export[OptionValue["Degrees"],alldegrees,"Integer64"];

        Print["MaxMemoryUsed = ", N[MaxMemoryUsed[]/10^9], " GB"];
(*        
        {nparsin,nparsout} = {"NParsIn","NParsOut"}/.FFNParsFromDegreesFile[OptionValue["Degrees"]];
        Print["nparsin = ", nparsin];
        Print["nparsout = ", nparsout];

        FFNewDummyGraph[spgraph,nparsin,nparsout];
        FFLoadDegrees[spgraph,OptionValue["Degrees"]];

        Print@FFDumpSamplePoints[spgraph, "samplepoints.m"];
        Print["number of sample points: ", FFSamplesFileSize["samplepoints.m"]];
        DeleteFile["samplepoints.m"];

    Print["Continuing in RecFunApart"];
    Continue[];
*)

        Print["reconstructing... this may take some time"]; now = AbsoluteTime[];
        recfit2 = FFReconstructFunction[fitgraph,allmtbutX,{"PrintDebugInfo"->OptionValue["PrintDebugInfo"],
          "NThreads"->OptionValue["NThreads"],"Degrees"->OptionValue["Degrees"]}]; 
        Print["-> ", AbsoluteTime[]-now, " s"];
        ,
        (* let FiniteFlow compute the degrees *)
         Print["reconstructing... this may take some time"]; now = AbsoluteTime[];
         recfit2 = FFReconstructFunction[fitgraph,allmtbutX,{"PrintDebugInfo"->OptionValue["PrintDebugInfo"],"NThreads"->OptionValue["NThreads"]}]; 
         Print["-> ", AbsoluteTime[]-now, " s"];
      ];
      
      recfit2 = coeffnorm2*recfit2;
      recfit2
    ];,
    
    True,
    Print["unknown reconstruction mode", OptionValue["ReconstructionMode"]]; Abort[];
   
   ];
      
  solfit = Join@@FFMultiFitSol[recfit,fitlearn];
  allcoeffsout = Table[ Factor[fitcoefficients[[Kk]] /. solfit] . (coeffnormfactored[[Kk]]*ansaetze[[Kk]] /. factors),
      {Kk,1,Length[coeffnormfactored]}];
  Print2["check variables: ", Variables[allcoeffsout]===Union[allmt]];
  
  FFDeleteGraph[fitgraph];
  FFDeleteGraph[graph];
  
  rndpoint = Thread[varsin->RandomInteger[{99,99999999999},Length[varsin]]];

  Print["final check (brace yourself): ", Union[FFGraphEvaluate[graphin, varsin /. rndpoint,"PrimeNo"->OptionValue["PrimeNo"]+1]-
       FFRatMod[allcoeffsout /. rndpoint, FFPrimeNo[OptionValue["PrimeNo"]+1]]]==={0}];    

  Print["MaxMemoryUsed = ", N[MaxMemoryUsed[]/10^9], " GB"];
  
  Return[allcoeffsout]
]


Clear[Reconstruct];
Options[Reconstruct]={"NThreads"->Automatic, "BatchSize"->100,
  "Apart"->False, "PrintDebugInfo"->1, "Degrees"->Automatic, "ExtraCoefficients"->{}};
  
Reconstruct[graphin_,vs_List,factors_List,OptionsPattern[]]:=Module[
  {subgraph,nzlearn,unislice,complexity,linrels,allfuncs,indepfuncs,
   rules,funcs,extrarules,ncoeffs},

  FFNewGraph[subgraph,"in",vs];
  FFAlgSimpleSubgraph[subgraph,"funcs",{"in"},graphin];
  FFAlgNonZeroes[subgraph,"nz",{"funcs"}];
  FFGraphOutput[subgraph,"nz"];
  
  nzlearn = FFNonZeroesLearn[subgraph];
  
  Print["computing univariate slice..."];
  unislice = GetSlice[subgraph,vs,"NThreads"->OptionValue["NThreads"]];
  complexity = Max@@GetDegree[#]&/@unislice[[2]];

  ncoeffs=FFNParsOut[subgraph];

  {linrels,extrarules} = GetLinearRelationsFromGraph[subgraph,vs,complexity,
    "NThreads"->OptionValue["NThreads"],
    "BatchSize"->OptionValue["BatchSize"],
    "ExtraCoefficients"->OptionValue["ExtraCoefficients"]];
  
  allfuncs = Array[f,FFNParsOut[subgraph]] /. f[i_]:>Nothing /; i>ncoeffs;
  indepfuncs = Variables[allfuncs/.linrels] /. f[i_]:>Nothing /; i>ncoeffs;
  If[Length[indepfuncs]==0,
    funcs = Normal[FFNonZeroesSol[allfuncs/.linrels,nzlearn]];
    rules = extrarules;
    Return[{funcs,rules}];
  ];

  Print[FFAlgTake[subgraph,"indep",{"nz"},{allfuncs}->indepfuncs]];
  FFGraphOutput[subgraph,"indep"];

  If[OptionValue["Apart"]=!=False && MemberQ[vs,OptionValue["Apart"]],
      Print["partial fractioning w.r.t. ", OptionValue["Apart"]];
      rules = ReconstructFunctionApart[subgraph,vs,OptionValue["Apart"],factors,
        "NThreads"->OptionValue["NThreads"],
        "PrintDebugInfo"->OptionValue["PrintDebugInfo"],
        "Degrees"->OptionValue["Degrees"] ];
      ,
      rules = ReconstructFunctionFactors[subgraph,vs,factors,
        "NThreads"->OptionValue["NThreads"],
        "PrintDebugInfo"->OptionValue["PrintDebugInfo"]];  
  ]; 

  rules = Thread[indepfuncs->rules];
  
  funcs = Normal[FFNonZeroesSol[allfuncs/.linrels,nzlearn]];

  FFDeleteGraph[subgraph];
 
  Return[{funcs,rules}];
];


GetMaxMinDegrees[expr_,y_]:=Module[{num,den},
  {num,den} = NumeratorDenominator[Together[expr]];
  {DeleteCases[Flatten[Position[CoefficientList[num,y],HH_/;HH=!=0,1]],0][[{-1,1}]]-{1,1},
   DeleteCases[Flatten[Position[CoefficientList[den,y],HH_/;HH=!=0,1]],0][[{-1,1}]]-{1,1}}
]


Clear[GetAllDegrees];

Options[GetAllDegrees]={"PrimeNo"->0, "PrintDebugInfo"->0, "NThreads"->Automatic};

GetAllDegrees[graphin_,variables_List,degreefilename_,OptionsPattern[]]:=Module[
  {options,totaldegrees,maxmindegrees,alldegrees},
  Print["computing degrees in parallel"];
  options = {"PrimeNo"->OptionValue["PrimeNo"],"NThreads"->OptionValue["NThreads"],
    "PrintDebugInfo"->OptionValue["PrintDebugInfo"]};
   
  totaldegrees = GetDegree/@GetSlice[graphin,variables,options][[2]];
  Print["max total degrees: ", {Max[totaldegrees/.deg[a_,_]:>a], Max[totaldegrees/.deg[_,a_]:>a]}];
  
  Do[ 
    maxmindegrees[vv] = GetMaxMinDegrees[#,vv]&/@GetSliceIn[graphin,variables,{vv},options][[2]];
    Print["max degrees in ", vv, ": ", {Max[maxmindegrees[vv][[All,1,1]]],Max[maxmindegrees[vv][[All,2,1]]]}];
  ,{vv,variables}];

  alldegrees = Flatten[{Length[variables],
           FFNParsOut[graphin],
           Table[{totaldegrees[[oo]], Table[maxmindegrees[vv][[oo]],{vv,variables}]},
             {oo,FFNParsOut[graphin]}]}/.deg[HH__]:>HH];
  
  Print["dumping degrees to ", degreefilename];
  Export[degreefilename,alldegrees,"Integer64"];
];
