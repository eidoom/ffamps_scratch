(* ::Package:: *)

(* example of how to construct finite remainders *)


SetDirectory[NotebookDirectory[]];


(* one-loop *)


(* the special function monomials are defined globally at each loop level *)
sfm1=Get["sfm_1L_2q2gA.m"];


(* rational coefficient functions *)
rcf1=Get["rcf_1L_2q2gA_d12d34__OGAqkm4qkm2Qqk_Ncp0_m+m++_PSanalyticX1.m"];


(* rational sparse matrix *)
rcsm1=Get["rcsm_1L_2q2gA_d12d34__OGAqkm4qkm2Qqk_Ncp0_m+m++_PSanalyticX1.m"];


(* ex[1] dependence must be restored, factor is always the same for 2q2gA *)
fin = Plus @@ ( ex[1]^2 * (rcf1[[1]] /. rcf1[[2]]) . rcsm1[[#]] . sfm1 * eps^(#-1)& /@ Range[3] )


(* two-loop *)


sfm2=Get["sfm_2L_2q2gA.m"];
rcf2=Get["rcf_2L_2q2gA_T2341_qkqkGA_Oqkm4qkm2Qqk_Ncp0_m++++_PSanalyticX1.m"];
rcsm2=Get["rcsm_2L_2q2gA_T2341_qkqkGA_Oqkm4qkm2Qqk_Ncp0_m++++_PSanalyticX1.m"];


(* two-loop sparse matrix does not have a dimension for powers of epsilon *)
fin = ex[1]^2 * (rcf2[[1]] /. rcf2[[2]]) . rcsm2 . sfm2
