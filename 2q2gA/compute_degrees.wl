(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


<<FiniteFlow`
<<FFUtils`
<< "InitTwoLoopToolsFF.m"
<< "InitDiagramsFF.m"

process = "2q2gA"; (* "2q2gA", "2q2QA" *)
Print["process: ", process];
directory = "/home/zoia/ffamps_scratch/"<>process<>"/eps_exp/";
(*directory = "/Users/zoia/Work/ffamps_scratch/"<>process<>"/eps_exp/";*)
If[process==="2q2QA",
  directory = StringReplace[directory,"2q2QA"->"2q2QA_alt"];
];
files = FileNames[directory<>"amp_2L*"];
Print[Length[files], " 2-loop amplitudes found"];

nthreads = 20;


psmode="PSanalyticX1";
Get["amplitudes/GlobalMapProcessSetup.m"];
Get["TwoLoopTools/BCFWreconstructionTools/BCFWReconstructFunction.wl"];
Get["TwoLoopTools/BCFWreconstructionTools/ReconstructFunctionApart.wl"];


(* ::Section::Closed:: *)
(*ReconstructFunctionApart without reconstruction*)


Clear[ReconstructFunctionApartDegrees];
Options[ReconstructFunctionApartDegrees]={"PrimeNo"->0,"NThreads"->Automatic,
   "ReconstructionMode"->"factors","PrintDebugInfo"->0,
   "CoefficientAnsatzResidues"->False,"Iterative"->0,
   "Degrees"->Automatic,"DegreesOnly"->False,"ExternalCoeffNorm"->{}};

ReconstructFunctionApartDegrees[graphin_, varsin_List, X_, coefficientansatz_List, OptionsPattern[]] := Module[{graph,slicerules,coeffactorguess,
  coeffansatztwist,slicedfactors,coeffnorm,dendegreesX,numdegreesX,slicerulesX,slicedX,coeffnormfactored,fitgraph,allmt,sliced,now,
  factors,ansaetze,lengthansaetze,outputelements,fitentries,fitcoefficients,allmtbutX,fitlearn,recfit,solfit,
  outcoeffs,allcoeffsout,sort,rndpoint,Print1,Print2,extrafactorsguess,tmpsol,newcoeffactorguess,
  totaldegrees,tmpslicerules,tmpsliced,maxmindegrees,alldegrees,zeovars},

  If[!MemberQ[varsin,X], Print["cannot apart w.r.t. ", X]; Abort[];,
     allmt = Prepend[DeleteCases[varsin,X], X];];
            
  Print1[strings__] := If[OptionValue["PrintDebugInfo"]>0, Print[strings]]; 
  Print2[strings__] := If[OptionValue["PrintDebugInfo"]>1, Print[strings]];
  
  (* Needed for iterative stuff *)
  coeffansatztwist = Select[Factor@coefficientansatz, Complement[Variables[#],varsin]==={} &]; 

  FFDeleteGraph[graph];
  FFNewGraph[graph,in,allmt];

  (* Sort variables. The variable we partial-fraction with respect to must be first *)
  FFAlgRatFunEval[graph,sortedvars,{in},allmt,varsin];
  FFAlgSimpleSubgraph[graph,coefficients,{sortedvars},graphin];
  FFGraphOutput[graph, coefficients];
  
  Print1["computing univariate slice in ", allmt]; now = AbsoluteTime[];
  {slicerules, sliced} = GetSlice[graph, allmt,{"PrimeNo"->OptionValue["PrimeNo"], "NThreads"->OptionValue["NThreads"]}];
  Print1["-> ", AbsoluteTime[]-now, " s"];
  Print1["degrees before normalisation {max num., max den.}", GetDegreeMax@sliced];

  Print1["matching coefficient factors"];
  coeffactorguess = FindIndependentFactorsNumerical[coeffansatztwist][[2,;;,2]];
  coeffactorguess = Rule[#,Factor[#/.slicerules,Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]]&/@coeffactorguess;
  coeffnorm = MatchCoefficientFactors2[sliced,coeffactorguess,"PrimeNo"->OptionValue["PrimeNo"]]; 
  Print1["degrees after normalisation {max num., max den.}", GetDegreeMax@Factor[(sliced/coeffnorm/.slicerules),Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]];
  FFAlgRatFunEval[graph,norm,{in},allmt,Together[1/coeffnorm]]//Print;
  FFAlgMul[graph,normcoeffs,{coefficients,norm}]//Print;
  FFGraphOutput[graph,normcoeffs];
  
  Print1["computing univariate slice in ", X];  now = AbsoluteTime[];
  {slicerulesX, slicedX} = GetSliceIn[graph,allmt,X,{"PrimeNo"->OptionValue["PrimeNo"], "NThreads"->OptionValue["NThreads"]}];
  Print1["-> ", AbsoluteTime[]-now, " s"];
  Print2["check denominators: ", Union[Denominator[slicedX]]==={1}];

  dendegreesX = Exponent[Denominator[Together[coeffnorm /. slicerulesX]], X];
  numdegreesX = Exponent[slicedX, X];
  

   If[OptionValue["ExternalCoeffNorm"]!={},
     coeffnorm=OptionValue["ExternalCoeffNorm"];
     ];

  
  Print1["finding independent factors"];
  {coeffnormfactored, factors} = FindIndependentFactorsNumerical[coeffnorm];
  
  Print1["constructing ansaetze for partial fractions"];
  ansaetze = Table[ConstructAnsatzX[coeffnormfactored[[Kk]],factors,numdegreesX[[Kk]],X],{Kk,Length[coeffnormfactored]}];
  lengthansaetze = Length/@ansaetze;

  FFAlgRatFunEval[graph,ys,{in},allmt, Append[factors[[All,2]],X]];
  FFAlgRatFunEval[graph,ansatz,{ys},Append[factors[[All,1]],X],Flatten[ansaetze]];
  FFAlgChain[graph,eqs,{ansatz,normcoeffs}];
  FFGraphOutput[graph,eqs];
  
  (* preparing pattern for multi-fit *)
  outputelements = Flatten[Join[Table[ans[i1,i2], {i1,1,Length[coeffnormfactored]},{i2,1,lengthansaetze[[i1]]}],
    Array[r,Length[coeffnormfactored]]]];
  Print[FFNParsOut[graph,eqs]-Length[outputelements]===0];

  fitentries = Table[ Append[Table[ans[i1,i2],{i2,lengthansaetze[[i1]]}], r[i1]], {i1,Length[coeffnormfactored]}];
  fitcoefficients = Table[c[i1,i2], {i1,Length[coeffnormfactored]},{i2,lengthansaetze[[i1]]}];

  (* Form of the equations for the fit: *)
  (*Append[fitcoefficients[[1]],0].fitentries[[1]] == fitentries[[1,-1]]*)
  
  Print1["preparing multi-fit graph"];
  allmtbutX = DeleteCases[allmt,X];
  FFDeleteGraph[fitgraph];
  FFNewGraph[fitgraph,in,allmtbutX];
  FFAlgSubgraphMultiFit[fitgraph,multifit,{in},graph,{X},outputelements->fitentries];
  FFGraphOutput[fitgraph,multifit];
  
  Print1["learning"]; now = AbsoluteTime[];
  fitlearn = FFMultiFitLearn[fitgraph,fitcoefficients];
  zerovars=Flatten["ZeroVars"/.fitlearn];
  Print1["-> ", AbsoluteTime[]-now, " s"];
  Print2["check learn output: ", And@@(#[[0]]==List&/@fitlearn)];
  Print2["check indep. variables: ", Union["IndepVars" /. fitlearn]==={{}}];
  
  If[
    OptionValue["ReconstructionMode"]==="factors",
  
    recfit = Module[{slicerules2,sliced2,coeffactorguess2,slicedfactors2,coeffnorm2,recfit2},
      Print1["computing univariate slice in ", allmtbutX]; now=AbsoluteTime[];
      {slicerules2, sliced2} = GetSlice[fitgraph, allmtbutX,{"PrimeNo"->OptionValue["PrimeNo"], "NThreads"->OptionValue["NThreads"]}];
      Print1["->", AbsoluteTime[]-now, " s"];

      Print1["degrees before normalisation {max num., max den.}", GetDegreeMax@sliced2];

      If[OptionValue["CoefficientAnsatzResidues"],
        (* Guess spurious factors in the denominators *)
        Print1["enhancing factor guess"]; now=AbsoluteTime[];    
        extrafactorsguess = {};
        For[ii=1,ii<=Length[coeffactorguess],ii++,
          If[Exponent[coeffactorguess[[ii,1]],X]===1,
            tmpsol = Flatten@Solve[coeffactorguess[[ii,1]]==0,X];
            extrafactorsguess = Union[extrafactorsguess,Factor[coeffactorguess[[All,1]]/.tmpsol]];
            Clear[tmpsol];
          ];
        ];
        coeffactorguess2 = Select[Union[coeffactorguess[[All,1]],extrafactorsguess],FreeQ[Variables[#], X]&];
        coeffactorguess2 = FindIndependentFactorsNumerical[coeffactorguess2][[2,All,2]];
        Print1[Length[coeffactorguess2], " independent factors"];,
        
        coeffactorguess2 = Select[coeffactorguess[[All,1]], FreeQ[Variables[#], X]&];
      ];
        
      Print1["guessing factors"];      
      coeffactorguess2=Rule[#,Factor[#/.slicerules2,Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]]&/@coeffactorguess2;
      coeffnorm2 = MatchCoefficientFactors2[sliced2,coeffactorguess2,"PrimeNo"->OptionValue["PrimeNo"]];  
      Print1["degrees after normalisation {max num., max den.}", GetDegreeMax@Factor[(sliced2/coeffnorm2/.slicerules2),Modulus->FFPrimeNo[OptionValue["PrimeNo"]]]];
      ];,
      
      Print["no other reconstruction mode available"];
   ];

  
  FFDeleteGraph[fitgraph];
  FFDeleteGraph[graph];
   
]


(* ::Section:: *)
(*main*)


Print["===================================================="];

Do[
  Module[{xs,allcoeffs,fs,matr,degs,complexity,functions,coefficients,sortedcoefficients,sortedfunctions,fitlearn,fitsol,
    fitrec,linrels,independentfuncs,res,coeffrules,coeffs,spfuncs,graph,slicerules,slicedcoeffs,graphfit},

    Print["processing ", StringSplit[file,"/"][[-1]]];

    {coeffrules,{coeffs,spfuncs}}=Get[file];
    xs = Variables[Values[coeffrules]];

    (* check that spfuncs contains monomials only *)
    Print["check monomials: ", SubsetQ[{F,Integer,Power,tci,tcr,Times}, Union[Head/@Expand[spfuncs]]]];

    allcoeffs = DeleteCases[DeleteDuplicates@Together@Flatten@CoefficientList[eps^4*coeffs,eps],0];
    fs=Keys[coeffrules];
    matr = Coefficient[#,fs]&/@allcoeffs;

    FFNewGraph[graph,in,xs];
    FFAlgRatExprEval[graph,"fs",{in},xs,Values[coeffrules]]//Print;
    FFAlgRatNumEval[graph,"matr",Flatten[matr]]//Print;
    FFAlgMatMul[graph,"matr.fs",{"matr","fs"},Length[matr], Length[fs],1]//Print;
    FFGraphOutput[graph,"matr.fs"];

    {slicerules,slicedcoeffs} = GetSlice[graph,xs,"NThreads"->nthreads];
    degs = deg@@Exponent[NumeratorDenominator[#],xx]&/@Together[slicedcoeffs];
(*    degs=FFTotalDegrees[graph, "MaxDegree"->101];*)
    Print["original max degree: ", MaximalBy[MaximalBy[degs,#[[1]]&],#[[2]]&]//First];
    Print["fitting linear relations..."];
    complexity = Max@@#&/@degs;
    functions = Array[g,Length[matr]];
    sortedfunctions = SortBy[functions,complexity[[#[[1]]]]&];
    sortedcoefficients = c@@#&/@sortedfunctions;

    FFAlgTake[graph,sorted,{"matr.fs"},{functions}->sortedfunctions]//Print;
    FFAlgRatNumEval[graph,zero,{0}];
    FFAlgChain[graph,fiteq,{sorted,zero}];
    FFGraphOutput[graph,fiteq];

    FFNewGraph[graphfit];
    FFAlgSubgraphFit[graphfit,fit,{},graph,xs,sortedcoefficients];
    FFGraphOutput[graphfit,fit];
    fitlearn=FFDenseSolverLearn[graphfit,sortedcoefficients];
    fitrec = FFReconstructNumeric[graphfit];
    fitsol = FFDenseSolverSol[fitrec,fitlearn];
    linrels = FFLinearRelationsFromFit[sortedfunctions,sortedcoefficients,fitsol];
    FFDeleteGraph[graphfit];

    independentfuncs = Complement[functions,Keys[linrels]];

    FFGraphOutput[graph,"matr.fs"];
    FFGraphPrune[graph];
    FFAlgTake[graph,indep,{"matr.fs"},{functions}->independentfuncs];
    FFGraphOutput[graph,indep];
    
    Print["partial fraction decomposition..."];
    res = ReconstructFunctionApartDegrees[
    	graph,
    	xs,
    	ex[4],
    	GetMomentumTwistorExpression[coeffansatz,ToExpression[psmode]],
  		{"NThreads"->nthreads, "PrintDebugInfo"->1, "CoefficientAnsatzResidues"->True}
   	];

    FFDeleteGraph[graph];
    Print["===================================================="];
  ];
,{file,files}];


Print["all done! bye"];
Quit[];
