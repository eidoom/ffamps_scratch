(* ::Package:: *)

(* normalisation of the colour generators: Tr[T^a T^b] = delta[a,b] , i.e. TR=1 *)


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


(*PathToAmps = "~/Work/ffamps_scratch/2q2gA/eps_exp/";
PathToPoles = "~/Work/ffamps_scratch/2q2gA/irpoles/";
PathToFin = "~/Work/ffamps_scratch/2q2gA/fin/";*)

PathToAmps = "/scratch/public/ffamps_scratch/2q2gA/eps_exp/";
PathToPoles = "~/ffamps_scratch/2q2gA/irpoles/";
PathToFin = "~/ffamps_scratch/2q2gA/fin/";


<<FiniteFlow`;
<< "InitTwoLoopToolsFF.m";
<< "InitDiagramsFF.m";
<< FFUtils`;


process = "2q2gA";


(*(* example *)
looporder="2L";
helicity="m++++";
ncfactor = "Ncp0";
colourfactor = "T2341";
psmode = "PSanalyticX1";
closedloopfactor="_OGAqkm4qkm2Qqk"*)


CmdOptions = {
  "-helicity",
  "-colourfactor",
  "-ncfactor",
  "-closedloopfactor",
  "-psmode",
  "-looporder"
};

psmode = "PSanalyticX1";

Print[$CommandLine];

Do[
pos = Flatten[Position[$CommandLine,CmdOptions[[oo]]]];
If[Length[pos]==1,

  Switch[oo,
    1,  
    helicity = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    2,
    colourfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    3,
    ncfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    4,
    closedloopfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    5,
    psmode = $CommandLine[[pos[[1]]+1]];,
    6,
    looporder = $CommandLine[[pos[[1]]+1]];,
    _,
    Print["unknown argument ",oo,CmdOptions[[oo]]];
  ];

];
,{oo,1,Length[CmdOptions]}];


Get["amplitudes/GlobalMapProcessSetup.m"];
Get["TwoLoopTools/LinearRelationsTools/GetLinearRelations.wl"];
Get["TwoLoopTools/LinearRelationsTools/MergeLinearRelations.wl"];
Get["TwoLoopTools/BCFWreconstructionTools/BCFWReconstructFunction.wl"];
Get["TwoLoopTools/BCFWreconstructionTools/ReconstructFunctionApart.wl"];


Normalisation["1L"] = -1;
Normalisation["2L"] = +1;


(* ::Title:: *)
(*load files*)


AmpFileIn = "amp_"<>looporder<>"_"<>process<>"_"<>colourfactor<>"_"<>closedloopfactor<>"_"<>
  ncfactor<>"_"<>helicity<>"_"<>psmode<>".m";


Print["getting ", AmpFileIn];
amp=Get[PathToAmps<>AmpFileIn] /. f[i_]:>f["amp",i];

(* normalisation *)
amp[[2,1]] = Normalisation[looporder]*amp[[2,1]];


PolesFileIn=StringReplace[AmpFileIn,"amp"->"poles"];
Print["getting ", PolesFileIn];
poles=Get[PathToPoles<>PolesFileIn] /. f[i_]:>f["poles",i];


amp[[2,2]]=mono/@amp[[2,2]];
poles[[2,2]]=mono/@poles[[2,2]];


(* ::Section:: *)
(*subtraction*)


allpfmonomials = Union[Join[amp[[2,2]],poles[[2,2]]]];


Which[ looporder==="1L", {epsmin,epsmax} = {-2,+2};,
       looporder==="2L", {epsmin,epsmax} = {-4,0};,
       True, Print["unknown loop order ", looporder]; Quit[]];


finsymb = Dot@@amp[[2]]-Dot@@poles[[2]];


{rem,finsymb} = Normal[CoefficientArrays[finsymb,allpfmonomials]];
If[rem=!=0, Print["error in CoefficientArrays"]; Quit[], Clear[rem]];


allfincoeffs=Table[f["fin",e,i],{e,epsmin,epsmax},{i,Length[allpfmonomials]}];
allfincoeffs=Flatten[Table[Thread[allfincoeffs[[e]]->Coefficient[finsymb,eps,epsmin+e-1]],{e,1,epsmax-epsmin+1}]];
zerofincoeffs=Select[allfincoeffs,#[[2]]===0&];
allfincoeffs=Complement[allfincoeffs,zerofincoeffs];


xs = Variables[Flatten[ToExpression[psmode]/.SPN4->List]];


(* ::Subsection:: *)
(*check that poles cancel*)


rndX = Thread[xs->RandomInteger[{10^4,10^10},Length[xs]]];


check=Union[Flatten[Table[Coefficient[finsymb,eps,e]/.Dispatch[Join[amp[[1]],poles[[1]]]/.rndX],{e,epsmin,-1}]]];


If[check=!={0}, Print["poles do not cancel out!"]; Quit[];,
  Print["poles cancel out"];]


(* ::Subsection:: *)
(*prepare graph*)


FFDeleteGraph[graph];
FFNewGraph[graph,in,xs];

FFAlgRatExprEval[graph,coeffsamp,{in},xs,amp[[1,All,2]]]//Print;

(* compute degrees of amplitude coefficients *)
FFGraphOutput[graph,coeffsamp];
degreesamp = FFTotalDegrees[graph,"MaxDegree"->200];
degreesamp = {Max[degreesamp[[All,1]]],Max[degreesamp[[All,2]]]};
Print["max degrees in amplitude: ", degreesamp];


FFAlgRatExprEval[graph,coeffpoles,{in},xs,poles[[1,All,2]]]//Print;
FFAlgChain[graph,coeffall,{coeffsamp,coeffpoles}]//Print;

allcoeffs = Join[amp[[1,All,1]],poles[[1,All,1]]];
matr = Normal[Last[CoefficientArrays[allfincoeffs[[All,2]],allcoeffs]]];
Union[Expand[matr . allcoeffs - allfincoeffs[[All,2]]]]==={0}//Print;

FFAlgRatNumEval[graph,coeffmatr,Flatten[matr]]//Print;

FFAlgMatMul[graph,coefffin,{coeffmatr,coeffall},Dimensions[matr][[1]],Dimensions[matr][[2]],1]//Print;

FFGraphOutput[graph,coefffin]//Print;


(* ::Subsection:: *)
(*linear relations*)


degrees = FFTotalDegrees[graph,"MaxDegree"->200];
complexity = Max@@#&/@degrees;
complexity=Association[Thread[allfincoeffs[[All,1]]->complexity]];
sortedfunctions = SortBy[allfincoeffs[[All,1]],complexity[#]&];
sortedcoefficients=sortedfunctions/.f->c;


FFAlgTake[graph,sorted,{coefffin},{allfincoeffs[[All,1]]}->sortedfunctions]//Print;
FFAlgRatNumEval[graph,zero,{0}];
FFAlgChain[graph,fiteq,{sorted,zero}];
FFGraphOutput[graph,fiteq];


FFDeleteGraph[graphfit];
FFNewGraph[graphfit];
FFAlgSubgraphFit[graphfit,fit,{},graph,xs,sortedcoefficients]//Print;
FFGraphOutput[graphfit,fit];
fitlearn=FFDenseSolverLearn[graphfit,sortedcoefficients];
fitrec = FFReconstructNumeric[graphfit];
fitsol = FFDenseSolverSol[fitrec,fitlearn];


linrels=FFLinearRelationsFromFit[sortedfunctions,sortedcoefficients,fitsol];
independentfuncs = Complement[allfincoeffs[[All,1]],First/@linrels];


newdegrees = (independentfuncs/.Thread[allfincoeffs[[All,1]]->degrees]);
newdegrees = {Max[newdegrees[[All,1]]],Max[newdegrees[[All,2]]]};
Print["max degrees in finite remainder: ", newdegrees];
Clear[degrees,complexity];


FFDeleteGraph[graphfit];


FFAlgTake[graph,indepfs,{coefffin},{allfincoeffs[[All,1]]}->independentfuncs]//Print;
FFGraphOutput[graph,indepfs];


(* ::Subsection:: *)
(*reconstruct*)


indepfincoeffs=ReconstructFunctionApart2[graph,xs,ex[4],
  GetMomentumTwistorExpression[coeffansatz,ToExpression[psmode]],"PrintDebugInfo"->1,
 "CoefficientAnsatzResidues"->True];
 
indepfincoeffs=Thread[independentfuncs->indepfincoeffs];


(* ::Subsection:: *)
(*format*)


fin = {indepfincoeffs,
  {Table[Sum[(f["fin",e,i]/.zerofincoeffs/.linrels)*eps^e,{e,epsmin,epsmax}],{i,Length[allpfmonomials]}],
  allpfmonomials}} /. mono[a_]:>a /. 
    Thread[independentfuncs->Array[f,Length[independentfuncs]]];


FFDeleteGraph[graph];


If[Position[fin[[2,1]],0,1]=!={}, "warning: some pfuncs monomials dropped out, modify script to remove them"];


(* ::Subsection:: *)
(*final check*)


rndX = Thread[xs->RandomInteger[{10^4,10^10},Length[xs]]];


check=Collect[(((Dot@@amp[[2]])/.(amp[[1]]/.rndX))-((Dot@@poles[[2]])/.(poles[[1]]/.rndX))/.mono[a_]:>a)-
  ((Dot@@fin[[2]])/.(fin[[1]]/.rndX)),eps|_F|_tci|_tcr,Simplify];
  
If[check=!=0, Print["final check failed"]; Quit[]];


(* ::Subsection:: *)
(*save*)


FinFileOut = StringReplace[AmpFileIn,"amp"->"fin"];
Print["writing ", FinFileOut];
Put[fin, PathToFin<>FinFileOut];


(* ::Subsection:: *)
(*degree info*)


slice=Thread[xs->RandomInteger[{10^4,10^10},Length[xs]]+xx*RandomInteger[{10^4,10^10},Length[xs]]];


Print["\n -- degree info on ", AmpFileIn, " --"];

Print["amplitude degrees (common denominator): ", degreesamp];

partialfractions=Flatten[List@@#&/@amp[[All,1,2]]];
partialfractions=Together[partialfractions/.slice,Modulus->FFPrimeNo[0]];
Print["amplitude degrees (pfd ex[4]): ", 
  {Max[Exponent[Numerator[partialfractions],xx]],Max[Exponent[Denominator[partialfractions],xx]]}];
Clear[partialfractions];

Print["finite remainder degrees (common denominator): ", newdegrees];

partialfractions=Flatten[List@@#&/@fin[[All,1,2]]];
partialfractions=Together[partialfractions/.slice,Modulus->FFPrimeNo[0]];
Print["finite remainder degrees (pfd ex[4]): ", 
  {Max[Exponent[Numerator[partialfractions],xx]],Max[Exponent[Denominator[partialfractions],xx]]}];


Quit[];
