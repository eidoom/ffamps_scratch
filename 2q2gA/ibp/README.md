# IBPs

| Family | Planar | Syzygy |
| ------ | ------ | ------ |
| t332   | N      | Y      |
| t422   | N      | Y      |
| t431   | Y      | Y      |
| t521   | Y      | Y      |
