(* Created with the Wolfram Language : www.wolfram.com *)
{{f[0] -> (-8*ex[1]^2*ex[2]*(1 + ex[2])*ex[3])/(1 + ex[3] + ex[2]*ex[3])}, 
 {{0, 0, 0, 0, 0, 0, 0, 0, -(eps*f[0])/24, (eps^2*f[0])/6, -f[0]/(2*eps), 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
  {mono[F[1, 1, 5]^4], mono[F[1, 1, 5]^3], mono[F[1, 1, 5]^2*tci[1, 1]^2], 
   mono[F[1, 1, 5]^2], mono[F[1, 1, 5]*tci[1, 1]^2], 
   mono[F[1, 1, 5]*tcr[3, 3]], mono[F[1, 1, 5]], mono[tci[1, 1]^4], 
   mono[tci[1, 1]^2], mono[tcr[3, 3]], mono[1], mono[F[1, 1, 2]^4], 
   mono[F[1, 1, 2]^3], mono[F[1, 1, 2]^2*tci[1, 1]^2], mono[F[1, 1, 2]^2], 
   mono[F[1, 1, 2]*tci[1, 1]^2], mono[F[1, 1, 2]*tcr[3, 3]], 
   mono[F[1, 1, 2]], mono[F[1, 1, 3]^4], mono[F[1, 1, 3]^3*tci[1, 2]], 
   mono[F[1, 1, 3]^3], mono[F[1, 1, 3]^2*tci[1, 1]^2], 
   mono[F[1, 1, 3]^2*tci[1, 2]], mono[F[1, 1, 3]^2], 
   mono[F[1, 1, 3]*tci[1, 1]^2*tci[1, 2]], mono[F[1, 1, 3]*tci[1, 1]^2], 
   mono[F[1, 1, 3]*tci[1, 2]], mono[F[1, 1, 3]*tcr[3, 3]], mono[F[1, 1, 3]], 
   mono[tci[1, 1]^2*tci[1, 2]], mono[tci[1, 2]*tcr[3, 3]], mono[tci[1, 2]], 
   mono[F[1, 1, 9]^4], mono[F[1, 1, 9]^3], mono[F[1, 1, 9]^2*tci[1, 1]^2], 
   mono[F[1, 1, 9]^2], mono[F[1, 1, 9]*tci[1, 1]^2], 
   mono[F[1, 1, 9]*tcr[3, 3]], mono[F[1, 1, 9]], mono[F[1, 1, 10]^4], 
   mono[F[1, 1, 10]^3], mono[F[1, 1, 10]^2*tci[1, 1]^2], mono[F[1, 1, 10]^2], 
   mono[F[1, 1, 10]*tci[1, 1]^2], mono[F[1, 1, 10]*tcr[3, 3]], 
   mono[F[1, 1, 10]], mono[F[3, 56]], mono[F[4, 120]], 
   mono[F[1, 1, 3]*F[1, 1, 9]*tci[1, 1]^2], mono[F[1, 1, 3]*F[1, 1, 9]], 
   mono[F[1, 1, 9]*tci[1, 1]^2*tci[1, 2]], mono[F[1, 1, 9]*tci[1, 2]], 
   mono[F[1, 2, 2]*tci[1, 1]^2*tci[1, 2]], mono[F[1, 2, 2]*tci[1, 2]], 
   mono[F[2, 1, 13]*tci[1, 1]^2], mono[F[2, 1, 13]], 
   mono[F[2, 1, 15]*tci[1, 1]^2], mono[F[2, 1, 15]], 
   mono[tci[1, 1]^2*tci[1, 2]*tcr[1, 2]], mono[tci[1, 1]^2*tcr[1, 2]], 
   mono[F[3, 3]], mono[F[3, 5]], mono[F[3, 7]], mono[F[3, 8]], 
   mono[F[3, 11]], mono[F[3, 13]], mono[F[3, 17]], mono[F[3, 24]], 
   mono[F[3, 52]], mono[F[3, 55]], mono[F[3, 57]], mono[F[3, 59]], 
   mono[F[4, 162]], mono[F[1, 1, 1]^2*F[1, 1, 2]], 
   mono[F[1, 1, 1]^2*F[1, 1, 9]], mono[F[1, 1, 1]*F[1, 1, 2]^2], 
   mono[F[1, 1, 1]*F[1, 1, 2]*F[1, 1, 4]], 
   mono[F[1, 1, 1]*F[1, 1, 2]*F[1, 1, 8]], 
   mono[F[1, 1, 1]*F[1, 1, 2]*F[1, 1, 9]], 
   mono[F[1, 1, 1]*F[1, 1, 2]*tci[1, 2]], 
   mono[F[1, 1, 1]*F[1, 1, 4]*F[1, 1, 9]], 
   mono[F[1, 1, 1]*F[1, 1, 8]*F[1, 1, 9]], mono[F[1, 1, 1]*F[1, 1, 9]^2], 
   mono[F[1, 1, 1]*F[1, 1, 9]*tci[1, 2]], mono[F[1, 1, 1]*F[2, 1, 11]], 
   mono[F[1, 1, 2]^2*F[1, 1, 5]], mono[F[1, 1, 2]^2*F[1, 1, 8]], 
   mono[F[1, 1, 2]^2*F[1, 1, 9]], mono[F[1, 1, 2]^2*F[1, 1, 10]], 
   mono[F[1, 1, 2]^2*tci[1, 2]], mono[F[1, 1, 2]*F[1, 1, 4]^2], 
   mono[F[1, 1, 2]*F[1, 1, 4]*F[1, 1, 5]], 
   mono[F[1, 1, 2]*F[1, 1, 4]*F[1, 1, 8]], 
   mono[F[1, 1, 2]*F[1, 1, 4]*F[1, 1, 9]], 
   mono[F[1, 1, 2]*F[1, 1, 4]*tci[1, 2]], mono[F[1, 1, 2]*F[1, 1, 5]^2], 
   mono[F[1, 1, 2]*F[1, 1, 5]*tci[1, 2]], mono[F[1, 1, 2]*F[1, 1, 8]^2], 
   mono[F[1, 1, 2]*F[1, 1, 8]*F[1, 1, 9]], 
   mono[F[1, 1, 2]*F[1, 1, 8]*F[1, 1, 10]], 
   mono[F[1, 1, 2]*F[1, 1, 8]*tci[1, 2]], mono[F[1, 1, 2]*F[1, 1, 9]^2], 
   mono[F[1, 1, 2]*F[1, 1, 9]*tci[1, 2]], mono[F[1, 1, 2]*F[1, 1, 10]^2], 
   mono[F[1, 1, 2]*F[1, 1, 10]*tci[1, 1]^2], 
   mono[F[1, 1, 2]*F[1, 1, 10]*tci[1, 2]], mono[F[1, 1, 2]*F[1, 1, 10]], 
   mono[F[1, 1, 2]*F[1, 2, 6]*tci[1, 2]], mono[F[1, 1, 2]*F[2, 1, 3]], 
   mono[F[1, 1, 2]*F[2, 1, 4]], mono[F[1, 1, 2]*F[2, 1, 11]], 
   mono[F[1, 1, 3]^2*F[1, 1, 6]], mono[F[1, 1, 3]^2*F[1, 1, 9]], 
   mono[F[1, 1, 3]^2*F[1, 1, 10]], mono[F[1, 1, 3]*F[1, 1, 6]^2], 
   mono[F[1, 1, 3]*F[1, 1, 6]*F[1, 1, 9]], 
   mono[F[1, 1, 3]*F[1, 1, 6]*F[1, 1, 10]], 
   mono[F[1, 1, 3]*F[1, 1, 6]*tci[1, 2]], mono[F[1, 1, 3]*F[1, 1, 9]^2], 
   mono[F[1, 1, 3]*F[1, 1, 9]*tci[1, 2]], 
   mono[F[1, 1, 3]*F[1, 2, 2]*tci[1, 2]], mono[F[1, 1, 3]*F[2, 1, 13]], 
   mono[F[1, 1, 4]^2*F[1, 1, 5]], mono[F[1, 1, 4]^2*F[1, 1, 9]], 
   mono[F[1, 1, 4]*F[1, 1, 5]^2], mono[F[1, 1, 4]*F[1, 1, 5]*F[1, 1, 9]], 
   mono[F[1, 1, 4]*F[1, 1, 5]*tci[1, 2]], 
   mono[F[1, 1, 4]*F[1, 1, 8]*F[1, 1, 9]], mono[F[1, 1, 4]*F[1, 1, 9]^2], 
   mono[F[1, 1, 4]*F[1, 1, 9]*tci[1, 2]], 
   mono[F[1, 1, 4]*F[1, 2, 6]*tci[1, 2]], mono[F[1, 1, 4]*F[2, 1, 3]], 
   mono[F[1, 1, 4]*F[2, 1, 11]], mono[F[1, 1, 4]*tci[1, 1]^2], 
   mono[F[1, 1, 5]^2*F[1, 1, 9]], mono[F[1, 1, 5]^2*tci[1, 2]], 
   mono[F[1, 1, 5]*F[1, 1, 9]^2], mono[F[1, 1, 5]*F[1, 1, 9]*tci[1, 2]], 
   mono[F[1, 1, 5]*F[2, 1, 4]], mono[F[1, 1, 6]^3], 
   mono[F[1, 1, 6]^2*F[1, 1, 9]], mono[F[1, 1, 6]^2*F[1, 1, 10]], 
   mono[F[1, 1, 6]^2*tci[1, 2]], mono[F[1, 1, 6]*F[1, 1, 9]^2], 
   mono[F[1, 1, 6]*F[1, 1, 9]*tci[1, 2]], 
   mono[F[1, 1, 6]*F[1, 1, 10]*tci[1, 2]], mono[F[1, 1, 6]*F[2, 1, 14]], 
   mono[F[1, 1, 6]*tci[1, 1]^2], mono[F[1, 1, 8]^3], 
   mono[F[1, 1, 8]^2*F[1, 1, 9]], mono[F[1, 1, 8]^2*F[1, 1, 10]], 
   mono[F[1, 1, 8]^2*tci[1, 2]], mono[F[1, 1, 8]*F[1, 1, 9]^2], 
   mono[F[1, 1, 8]*F[1, 1, 9]*tci[1, 2]], mono[F[1, 1, 8]*F[1, 1, 10]^2], 
   mono[F[1, 1, 8]*F[1, 1, 10]*tci[1, 2]], 
   mono[F[1, 1, 8]*F[1, 2, 9]*tci[1, 2]], mono[F[1, 1, 8]*F[2, 1, 11]], 
   mono[F[1, 1, 8]*F[2, 1, 12]], mono[F[1, 1, 8]*tci[1, 1]^2], 
   mono[F[1, 1, 9]^2*tci[1, 2]], mono[F[1, 1, 9]*F[1, 1, 10]^2], 
   mono[F[1, 1, 9]*F[1, 1, 10]*tci[1, 1]^2], mono[F[1, 1, 9]*F[1, 1, 10]], 
   mono[F[1, 1, 9]*F[1, 2, 9]*tci[1, 2]], mono[F[1, 1, 9]*F[2, 1, 11]], 
   mono[F[1, 1, 9]*F[2, 1, 12]], mono[F[1, 1, 9]*F[2, 1, 15]], 
   mono[F[1, 1, 10]^2*tci[1, 2]], mono[F[1, 1, 10]*F[1, 2, 2]*tci[1, 2]], 
   mono[F[1, 1, 10]*F[2, 1, 13]], mono[F[1, 1, 10]*F[2, 1, 14]], 
   mono[F[1, 1, 10]*F[2, 1, 15]], mono[F[1, 2, 2]*tci[1, 1]^2], 
   mono[F[1, 2, 6]*tci[1, 1]^2], mono[F[1, 2, 9]*tci[1, 1]^2], 
   mono[F[2, 1, 3]*tci[1, 2]], mono[F[2, 1, 11]*tci[1, 1]^2], 
   mono[F[2, 1, 11]*tci[1, 2]], mono[F[2, 1, 11]], 
   mono[F[2, 1, 12]*tci[1, 2]], mono[F[2, 1, 13]*tci[1, 2]], mono[F[3, 1]], 
   mono[F[3, 2]], mono[F[3, 4]], mono[F[3, 6]], mono[F[3, 12]], 
   mono[F[3, 18]], mono[F[3, 19]], mono[F[3, 51]], mono[F[3, 61]], 
   mono[F[4, 206]], mono[F[1, 1, 1]^3], mono[F[1, 1, 1]^2*F[1, 1, 4]], 
   mono[F[1, 1, 1]^2*F[1, 1, 5]], mono[F[1, 1, 1]^2*F[1, 1, 8]], 
   mono[F[1, 1, 1]^2*F[1, 1, 10]], mono[F[1, 1, 1]^2*tci[1, 2]], 
   mono[F[1, 1, 1]*F[1, 1, 3]^2], mono[F[1, 1, 1]*F[1, 1, 3]*tci[1, 2]], 
   mono[F[1, 1, 1]*F[1, 1, 4]^2], mono[F[1, 1, 1]*F[1, 1, 4]*F[1, 1, 8]], 
   mono[F[1, 1, 1]*F[1, 1, 4]*tci[1, 2]], mono[F[1, 1, 1]*F[1, 1, 5]^2], 
   mono[F[1, 1, 1]*F[1, 1, 5]*F[1, 1, 10]], 
   mono[F[1, 1, 1]*F[1, 1, 5]*tci[1, 2]], mono[F[1, 1, 1]*F[1, 1, 8]^2], 
   mono[F[1, 1, 1]*F[1, 1, 8]*tci[1, 2]], mono[F[1, 1, 1]*F[1, 1, 10]^2], 
   mono[F[1, 1, 1]*F[1, 1, 10]*tci[1, 2]], mono[F[1, 1, 1]*F[2, 1, 1]], 
   mono[F[1, 1, 1]*F[2, 1, 2]], mono[F[1, 1, 1]*F[2, 1, 9]], 
   mono[F[1, 1, 1]*tci[1, 1]^2], mono[F[1, 1, 2]^2*F[1, 1, 4]], 
   mono[F[1, 1, 3]^2*F[1, 1, 4]], mono[F[1, 1, 3]^2*F[1, 1, 8]], 
   mono[F[1, 1, 3]*F[1, 1, 4]^2], mono[F[1, 1, 3]*F[1, 1, 4]*F[1, 1, 8]], 
   mono[F[1, 1, 3]*F[1, 1, 4]*tci[1, 2]], 
   mono[F[1, 1, 3]*F[1, 1, 5]*tci[1, 1]^2], mono[F[1, 1, 3]*F[1, 1, 5]], 
   mono[F[1, 1, 3]*F[1, 1, 8]^2], mono[F[1, 1, 3]*F[1, 1, 8]*tci[1, 2]], 
   mono[F[1, 1, 3]*F[1, 1, 9]*F[1, 1, 10]], mono[F[1, 1, 3]*F[1, 1, 10]^2], 
   mono[F[1, 1, 3]*F[1, 1, 10]*tci[1, 1]^2], 
   mono[F[1, 1, 3]*F[1, 1, 10]*tci[1, 2]], mono[F[1, 1, 3]*F[1, 1, 10]], 
   mono[F[1, 1, 3]*F[2, 1, 1]], mono[F[1, 1, 4]^2*F[1, 1, 8]], 
   mono[F[1, 1, 4]*F[1, 1, 8]^2], mono[F[1, 1, 4]*F[1, 1, 8]*tci[1, 2]], 
   mono[F[1, 1, 4]*F[2, 1, 2]], mono[F[1, 1, 5]^2*F[1, 1, 10]], 
   mono[F[1, 1, 5]*F[1, 1, 10]^2], mono[F[1, 1, 5]*F[1, 1, 10]*tci[1, 1]^2], 
   mono[F[1, 1, 5]*F[1, 1, 10]*tci[1, 2]], mono[F[1, 1, 5]*F[1, 1, 10]], 
   mono[F[1, 1, 5]*tci[1, 1]^2*tci[1, 2]], mono[F[1, 1, 5]*tci[1, 2]], 
   mono[F[1, 1, 8]*F[2, 1, 9]], mono[F[1, 1, 9]^2*F[1, 1, 10]], 
   mono[F[1, 1, 9]*F[1, 1, 10]*tci[1, 2]], 
   mono[F[1, 1, 10]*tci[1, 1]^2*tci[1, 2]], mono[F[1, 1, 10]*tci[1, 2]], 
   mono[F[1, 2, 7]*tci[1, 1]^2*tci[1, 2]], mono[F[1, 2, 7]*tci[1, 2]], 
   mono[F[2, 1, 1]*tci[1, 2]], mono[F[2, 1, 2]*tci[1, 2]], 
   mono[F[2, 1, 5]*tci[1, 1]^2], mono[F[2, 1, 5]], 
   mono[F[2, 1, 9]*tci[1, 2]], mono[tci[1, 1]^2*tci[1, 2]*tcr[1, 1]], 
   mono[tci[1, 1]^2*tcr[1, 1]^2], mono[tci[1, 1]^2*tcr[1, 1]*tcr[1, 2]], 
   mono[tci[1, 1]^2*tcr[1, 1]], mono[tci[1, 1]^2*tcr[1, 2]^2], 
   mono[tci[1, 1]^2*tcr[2, 1]], mono[tci[1, 2]*tcr[1, 1]^3], 
   mono[tci[1, 2]*tcr[1, 1]^2*tcr[1, 2]], mono[tci[1, 2]*tcr[1, 1]^2], 
   mono[tci[1, 2]*tcr[1, 1]*tcr[1, 2]], mono[tci[1, 2]*tcr[1, 2]^3], 
   mono[tci[1, 2]*tcr[2, 1]], mono[tci[1, 2]*tcr[3, 1]], 
   mono[tci[1, 2]*tcr[3, 2]], mono[tcr[1, 1]^4], mono[tcr[1, 1]^3*tcr[1, 2]], 
   mono[tcr[1, 1]^3], mono[tcr[1, 1]^2*tcr[1, 2]^2], 
   mono[tcr[1, 1]*tcr[1, 2]^3], mono[tcr[1, 1]*tcr[2, 1]], mono[tcr[1, 2]^4], 
   mono[tcr[4, 1]], mono[tcr[4, 2]], mono[tcr[4, 3]], mono[tcr[4, 4]], 
   mono[tcr[4, 5]], mono[F[4, 17]], mono[F[1, 1, 2]*F[1, 1, 5]*tci[1, 1]^2], 
   mono[F[1, 1, 2]*F[1, 1, 5]], mono[F[1, 1, 2]*F[1, 1, 9]*tci[1, 1]^2], 
   mono[F[1, 1, 2]*F[1, 1, 9]], mono[F[1, 1, 5]*F[1, 1, 9]*tci[1, 1]^2], 
   mono[F[1, 1, 5]*F[1, 1, 9]], mono[F[2, 1, 4]*tci[1, 1]^2], 
   mono[F[2, 1, 4]], mono[F[3, 15]], mono[F[4, 15]], 
   mono[F[1, 1, 2]*F[1, 1, 3]*tci[1, 1]^2], mono[F[1, 1, 2]*F[1, 1, 3]], 
   mono[F[1, 1, 2]*tci[1, 1]^2*tci[1, 2]], mono[F[1, 1, 2]*tci[1, 2]], 
   mono[F[3, 23]*F[1, 1, 1]], mono[F[3, 23]*F[1, 1, 2]], 
   mono[F[3, 23]*F[1, 1, 7]], mono[F[3, 23]*F[1, 1, 8]], 
   mono[F[3, 23]*F[1, 1, 9]], mono[F[3, 23]*F[1, 1, 10]], 
   mono[F[3, 25]*F[1, 1, 1]], mono[F[3, 25]*F[1, 1, 2]], 
   mono[F[3, 25]*F[1, 1, 3]], mono[F[3, 25]*F[1, 1, 5]], 
   mono[F[3, 25]*F[1, 1, 6]], mono[F[3, 25]*F[1, 1, 7]], 
   mono[F[3, 25]*F[1, 1, 10]], mono[F[3, 43]*F[1, 1, 1]], 
   mono[F[3, 43]*F[1, 1, 2]], mono[F[3, 43]*F[1, 1, 4]], 
   mono[F[3, 43]*F[1, 1, 6]], mono[F[3, 43]*F[1, 1, 7]], 
   mono[F[3, 43]*F[1, 1, 9]], mono[F[3, 43]*F[1, 1, 10]], 
   mono[F[3, 45]*F[1, 1, 1]], mono[F[3, 45]*F[1, 1, 2]], 
   mono[F[3, 45]*F[1, 1, 3]], mono[F[3, 45]*F[1, 1, 6]], 
   mono[F[3, 45]*F[1, 1, 7]], mono[F[3, 45]*F[1, 1, 9]], 
   mono[F[3, 62]*F[1, 1, 1]], mono[F[3, 62]*F[1, 1, 2]], 
   mono[F[3, 62]*F[1, 1, 4]], mono[F[3, 62]*F[1, 1, 5]], 
   mono[F[3, 62]*F[1, 1, 6]], mono[F[3, 62]*F[1, 1, 7]], 
   mono[F[3, 62]*F[1, 1, 10]], mono[F[3, 63]*F[1, 1, 1]], 
   mono[F[3, 63]*F[1, 1, 2]], mono[F[3, 63]*F[1, 1, 3]], 
   mono[F[3, 63]*F[1, 1, 5]], mono[F[3, 63]*F[1, 1, 6]], 
   mono[F[3, 63]*F[1, 1, 7]], mono[F[3, 63]*F[1, 1, 8]], 
   mono[F[3, 63]*F[1, 1, 9]], mono[F[3, 63]*F[1, 1, 10]], 
   mono[F[3, 70]*F[1, 1, 1]], mono[F[3, 70]*F[1, 1, 2]], 
   mono[F[3, 70]*F[1, 1, 3]], mono[F[3, 70]*F[1, 1, 9]], 
   mono[F[3, 71]*F[1, 1, 1]], mono[F[3, 71]*F[1, 1, 2]], 
   mono[F[3, 71]*F[1, 1, 3]], mono[F[3, 71]*F[1, 1, 5]], 
   mono[F[3, 71]*F[1, 1, 6]], mono[F[3, 71]*F[1, 1, 7]], 
   mono[F[3, 71]*F[1, 1, 8]], mono[F[3, 78]*F[1, 1, 1]], 
   mono[F[3, 78]*F[1, 1, 2]], mono[F[3, 78]*F[1, 1, 3]], 
   mono[F[3, 78]*F[1, 1, 9]], mono[F[3, 80]*F[1, 1, 2]], 
   mono[F[3, 80]*F[1, 1, 6]], mono[F[3, 80]*F[1, 1, 7]], 
   mono[F[3, 80]*F[1, 1, 8]], mono[F[3, 80]*F[1, 1, 9]], 
   mono[F[3, 80]*tci[1, 2]], mono[F[3, 81]*F[1, 1, 1]], 
   mono[F[3, 81]*F[1, 1, 3]], mono[F[3, 81]*F[1, 1, 4]], 
   mono[F[3, 81]*F[1, 1, 6]], mono[F[3, 81]*F[1, 1, 7]], 
   mono[F[3, 81]*F[1, 1, 9]], mono[F[3, 81]*F[1, 1, 10]], 
   mono[F[3, 82]*F[1, 1, 1]], mono[F[3, 82]*F[1, 1, 2]], 
   mono[F[3, 82]*F[1, 1, 3]], mono[F[3, 82]*F[1, 1, 4]], 
   mono[F[3, 82]*F[1, 1, 5]], mono[F[3, 82]*F[1, 1, 6]], 
   mono[F[3, 82]*F[1, 1, 7]], mono[F[3, 82]*F[1, 1, 8]], 
   mono[F[3, 82]*F[1, 1, 9]], mono[F[3, 82]*F[1, 1, 10]], 
   mono[F[3, 82]*tci[1, 2]], mono[F[3, 82]], mono[F[4, 25]], mono[F[4, 28]], 
   mono[F[4, 39]], mono[F[4, 41]], mono[F[4, 51]], mono[F[4, 80]], 
   mono[F[4, 83]], mono[F[4, 91]], mono[F[4, 93]], mono[F[4, 100]], 
   mono[F[4, 102]], mono[F[4, 111]], mono[F[4, 113]], mono[F[4, 114]], 
   mono[F[4, 129]], mono[F[4, 132]], mono[F[4, 139]], mono[F[4, 141]], 
   mono[F[4, 146]], mono[F[4, 148]], mono[F[4, 171]], mono[F[4, 174]], 
   mono[F[4, 182]], mono[F[4, 184]], mono[F[4, 190]], mono[F[4, 192]], 
   mono[F[4, 199]], mono[F[4, 201]], mono[F[4, 202]], mono[F[4, 213]], 
   mono[F[4, 215]], mono[F[4, 219]], mono[F[4, 221]], mono[F[4, 225]], 
   mono[F[4, 233]], mono[F[4, 234]], mono[F[4, 235]], mono[F[4, 244]], 
   mono[F[4, 246]], mono[F[4, 252]], mono[F[4, 255]], mono[F[4, 271]], 
   mono[F[4, 273]], mono[F[4, 277]], mono[F[4, 279]], mono[F[4, 282]], 
   mono[F[4, 289]], mono[F[4, 290]], mono[F[4, 291]], mono[F[4, 293]], 
   mono[F[4, 295]], mono[F[4, 330]], mono[F[1, 1, 1]*tci[1, 1]^3], 
   mono[F[1, 1, 1]*tci[1, 1]*tcr[1, 1]^2], 
   mono[F[1, 1, 1]*tci[1, 2]*tci[2, 1]], 
   mono[F[1, 1, 1]*tci[2, 1]*tcr[1, 1]], mono[F[1, 1, 1]*tci[3, 1]], 
   mono[F[1, 1, 2]*tci[1, 1]^3], mono[F[1, 1, 2]*tci[1, 1]*tcr[1, 1]^2], 
   mono[F[1, 1, 2]*tci[1, 2]*tci[2, 1]], 
   mono[F[1, 1, 2]*tci[2, 1]*tcr[1, 1]], mono[F[1, 1, 2]*tci[3, 1]], 
   mono[F[1, 1, 3]*tci[1, 1]^3], mono[F[1, 1, 3]*tci[1, 1]*tcr[1, 1]^2], 
   mono[F[1, 1, 3]*tci[1, 2]*tci[2, 1]], 
   mono[F[1, 1, 3]*tci[2, 1]*tcr[1, 1]], mono[F[1, 1, 3]*tci[3, 1]], 
   mono[F[1, 1, 5]*tci[1, 1]^3], mono[F[1, 1, 5]*tci[1, 1]*tcr[1, 1]^2], 
   mono[F[1, 1, 5]*tci[1, 2]*tci[2, 1]], 
   mono[F[1, 1, 5]*tci[2, 1]*tcr[1, 1]], mono[F[1, 1, 5]*tci[3, 1]], 
   mono[F[1, 1, 6]*tci[1, 1]^3], mono[F[1, 1, 6]*tci[1, 1]*tcr[1, 1]^2], 
   mono[F[1, 1, 6]*tci[1, 2]*tci[2, 1]], 
   mono[F[1, 1, 6]*tci[2, 1]*tcr[1, 1]], mono[F[1, 1, 6]*tci[3, 1]], 
   mono[F[1, 1, 7]*tci[1, 1]^3], mono[F[1, 1, 7]*tci[1, 1]*tcr[1, 1]^2], 
   mono[F[1, 1, 7]*tci[1, 2]*tci[2, 1]], 
   mono[F[1, 1, 7]*tci[2, 1]*tcr[1, 1]], mono[F[1, 1, 7]*tci[3, 1]], 
   mono[F[1, 1, 8]*tci[1, 1]^3], mono[F[1, 1, 8]*tci[1, 2]*tci[2, 1]], 
   mono[F[1, 1, 9]*tci[1, 1]^3], mono[F[1, 1, 9]*tci[1, 1]*tcr[1, 1]^2], 
   mono[F[1, 1, 9]*tci[1, 2]*tci[2, 1]], 
   mono[F[1, 1, 9]*tci[2, 1]*tcr[1, 1]], mono[F[1, 1, 9]*tci[3, 1]], 
   mono[F[1, 1, 10]*tci[1, 1]^3], mono[F[1, 1, 10]*tci[1, 1]*tcr[1, 1]^2], 
   mono[F[1, 1, 10]*tci[1, 2]*tci[2, 1]], 
   mono[F[1, 1, 10]*tci[2, 1]*tcr[1, 1]], mono[F[1, 1, 10]*tci[3, 1]], 
   mono[tci[1, 1]^3*tci[1, 2]], mono[tci[1, 1]^3*tcr[1, 1]], 
   mono[tci[1, 1]^3], mono[tci[1, 1]^2*tci[2, 1]], 
   mono[tci[1, 1]*tci[1, 2]*tcr[1, 1]^2], 
   mono[tci[1, 1]*tci[1, 2]*tcr[1, 2]^2], mono[tci[1, 1]*tcr[1, 1]^3], 
   mono[tci[1, 2]*tci[2, 1]*tcr[1, 1]], mono[tci[1, 2]*tci[2, 1]*tcr[1, 2]], 
   mono[tci[1, 2]*tci[2, 1]], mono[tci[1, 2]*tci[3, 1]], 
   mono[tci[1, 2]*tci[3, 2]], mono[tci[2, 1]*tcr[1, 1]^2], 
   mono[tci[3, 1]*tcr[1, 1]], mono[tci[4, 1]]}}}
