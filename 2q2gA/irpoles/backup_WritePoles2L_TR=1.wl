(* ::Package:: *)

(* normalisation of the colour generators: Tr[T^a T^b] = delta[a,b] , i.e. TR=1 *)


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


process = "2q2gA";
looporder = "2L";


<<FiniteFlow`;
<<FFUtils`;
<< "InitTwoLoopToolsFF.m";
<< "InitDiagramsFF.m";


PathToAmps0L = Directory[]<>"/../numerators/";
(*PathToAmps1L = "/scratch/public/ffamps_scratch/2q2gA/eps_exp/";
PathToAmps2L = "/scratch/public/ffamps_scratch/2q2gA/eps_exp/";*)
PathToAmps1L = Directory[]<>"/../eps_exp/";
PathToAmps2L = Directory[]<>"/../eps_exp/";


(*structures=Import["~/Work/ffamps_scratch/2q2gA/structures.json"];
allclosedloopfactors=Join[("1L"/.("factors"/.structures[[2]]))[[All,1]],("2L"/.("factors"/.structures[[2]]))[[All,1]]]//Union*)


CmdOptions = {
  "-helicity",
  "-colourfactor",
  "-ncfactor",
  "-closedloopfactor",
  "-psmode",
  "-checkpoles",
  "-checkpolesonly"
};

psmode = "PSanalytic";
checkpoles = False;
checkpolesonly = False;

Print[$CommandLine];

Do[
pos = Flatten[Position[$CommandLine,CmdOptions[[oo]]]];
If[Length[pos]==1,

  Switch[oo,
    1,  
    helicity = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    2,
    colourfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    3,
    ncfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    4,
    closedloopfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    5,
    psmode = $CommandLine[[pos[[1]]+1]];,
    6,
    checkpoles = True;,
    7,
    checkpolesonly = True;,
    _,
    Print["unknown argument ",oo,CmdOptions[[oo]]];
  ];

];
,{oo,1,Length[CmdOptions]}];

If[checkpolesonly, checkpoles=True];


(* EXAMPLE: "amp_2L_2q2gA_T2341__OGAqkm4qkm2Qqk_Ncp0_m+m++_PSanalyticX1.m";
colourfactor="T2341";
closedloopfactor="_OGAqkm4qkm2Qqk";
ncfactor="Ncp0";
helicity="m+m++";
psmode="PSanalyticX1";
checkpoles=True;
checkpolesonly=True;*)


closedloopfactortype=StringSplit[closedloopfactor,"_"][[-1]];


(* ::Chapter:: *)
(*Prepare IR subtraction*)


(* ::Subtitle:: *)
(*from arXiv:0903.1126*)


id = IdentityMatrix[3];


(* ::Section:: *)
(*write Z1 and Z2 operators (only run once to save Z1, Z2, then comment out)*)


Print["preparing symbolic subtraction"];


Get["ColorMath1.0.m"]; (* conflicts with "InitTwoLoopToolsFF.m" and "InitDiagramsFF.m" *)


(* Casimirs *)
CA=CSimplify[Superscript[f,{a1,b,c}]Superscript[f,{a2,b,c}]]/ Superscript[\[CapitalDelta],{a1,a2}];
CF=CSimplify[Subscript[Superscript[Superscript[t,{a}],i],ii]*Subscript[Superscript[Superscript[t,{a}],ii],j] ]/Subscript[Superscript[\[Delta],i], j];

If[Simplify[{CA - Nc (2*TR), CF - (Nc^2-1)/2/Nc (2*TR)}]=!={0,0}, Print["error in colour"]; Abort[]];


(* ::Subsection:: *)
(*colour-insertion operators*)


(* colour structures relevant for 2q2gA *)
colourfactors = {T[i2,a3,a4,i1],T[i2,a4,a3,i1],delta$[i1,i2] Delta$[a3,a4]};


toExplColour[expr_]:=CSimplify[expr/.
  {T[b1_,aaa__,b2_]:>Subscript[Superscript[Superscript[t,{aaa}],b1],b2],
    delta$[y1_,y2_]:>delta[y2,y1], 
    Delta$[y1_,y2_]:>Delta[y1,y2]}]/.
  {Subscript[Superscript[Superscript[t,{aa__}],j2_],j1_]:>T[j2,aa,j1],
   delta[i2,i1]->delta$[i1,i2],
   Delta[a3,a4]->Delta$[a3,a4]};


toColourBasis[expr_,basis_]:=Module[{cc,eqs,sol},
  cc=Array[c,Length[basis]];
  eqs=Union[Cases[Collect[expr-cc . basis,_delta$|_Delta$|_T|_trT,eq],eq[hh__]:>hh,Infinity]];
  sol=cc/.Flatten[Solve[eqs==0,cc]];
  If[Intersection[Variables[sol],cc]=!={}, 
    Print["failed 1"]; Return[$Failed], 
    
    If[Expand[sol . basis-expr]===0, Return[sol], Print["failed 2"]; Return[$Failed]]]
];


Clear[T];

T[i_,j_][ee_]:=T[j,i][ee]/;j<i;

T[1,2][ T[i2,a3,a4,i1] ] = s1 T[i2,a,a3,a4,a,i1];
T[1,2][ T[i2,a4,a3,i1] ] = s2 T[i2,a,a4,a3,a,i1];
T[1,2][ delta$[i1,i2] Delta$[a3,a4] ] = s3 Delta$[a3,a4] T[i2,a,a,i1];

T[1,3][ T[i2,a3,a4,i1] ] = s4 T[i2,b,a4,a,i1] I f[a,a3,b];
T[1,3][ T[i2,a4,a3,i1] ] = s5 T[i2,a4,b,a,i1] I f[a,b,a3];
T[1,3][ delta$[i1,i2] Delta$[a3,a4] ] = s6 T[i2,a,i1] I f[a,a3,a4];

T[1,4][ T[i2,a3,a4,i1] ] = s7 T[i2,a3,b,a,i1] I f[a,a4,b];
T[1,4][ T[i2,a4,a3,i1] ] = s8 T[i2,b,a3,a,i1] I f[a,a4,b];
T[1,4][ delta$[i1,i2] Delta$[a3,a4] ] = s9 T[i2,a,i1] I f[a,a3,a4];

T[2,3][ T[i2,a3,a4,i1] ] = s10 T[i2,a,b,a4,i1] I f[a,a3,b];
T[2,3][ T[i2,a4,a3,i1] ] = s11 T[i2,a,a4,b,i1] I f[a,a3,b];
T[2,3][ delta$[i1,i2] Delta$[a3,a4] ] = s12 T[i2,a,i1] I f[a,a3,a4];

T[2,4][ T[i2,a3,a4,i1] ] = s13 T[i2,a,a3,b,i1] I f[a,a4,b];
T[2,4][ T[i2,a4,a3,i1] ] = s14 T[i2,a,b,a3,i1] I f[a,a4,b];
T[2,4][ delta$[i1,i2] Delta$[a3,a4] ] = s15 T[i2,a,i1] I f[a,a3,a4];

T[3,4][ T[i2,a3,a4,i1] ] = s16 T[i2,b,c,i1] I f[a,a3,b] I f[a,a4,c];
T[3,4][ T[i2,a4,a3,i1] ] = s17 T[i2,b,c,i1] I f[a,a4,b] I f[a,a3,c];
T[3,4][ delta$[i1,i2] Delta$[a3,a4] ] = s18 delta$[i1,i2] I f[a3,b,a] I f[b,a4,a];

fixedsigns = {s1->-1,s2->-1,s3->-1,s4->1,s5->-1,s6->unfixedsign,s7->1,s8->1,s9->-unfixedsign,s10->-1,
  s11->-1,s12->-unfixedsign,s13->-1,s14->-1,s15->unfixedsign,s16->1,s17->1,s18->-1};


(* check colour conservation *)
colourids = {
  toColourBasis[Sum[ toExplColour[T[1,j][ T[i2,a3,a4,i1] ]],{j,{2,3,4}}],colourfactors]+{CF,0,0},
  toColourBasis[Sum[ toExplColour[T[2,j][ T[i2,a3,a4,i1] ]],{j,{1,3,4}}],colourfactors]+{CF,0,0},
  toColourBasis[Sum[ toExplColour[T[3,j][ T[i2,a3,a4,i1] ]],{j,{1,2,4}}],colourfactors]+{CA,0,0},
  toColourBasis[Sum[ toExplColour[T[4,j][ T[i2,a3,a4,i1] ]],{j,{1,2,3}}],colourfactors]+{CA,0,0},

  toColourBasis[Sum[ toExplColour[T[1,j][ T[i2,a4,a3,i1] ]],{j,{2,3,4}}],colourfactors]+{0,CF,0},
  toColourBasis[Sum[ toExplColour[T[2,j][ T[i2,a4,a3,i1] ]],{j,{1,3,4}}],colourfactors]+{0,CF,0},
  toColourBasis[Sum[ toExplColour[T[3,j][ T[i2,a4,a3,i1] ]],{j,{1,2,4}}],colourfactors]+{0,CA,0},
  toColourBasis[Sum[ toExplColour[T[4,j][ T[i2,a4,a3,i1] ]],{j,{1,2,3}}],colourfactors]+{0,CA,0},
  
  toColourBasis[Sum[ toExplColour[T[1,j][ delta$[i1,i2] Delta$[a3,a4] ]],{j,{2,3,4}}],colourfactors]+{0,0,CF},
  toColourBasis[Sum[ toExplColour[T[2,j][ delta$[i1,i2] Delta$[a3,a4] ]],{j,{1,3,4}}],colourfactors]+{0,0,CF},
  toColourBasis[Sum[ toExplColour[T[3,j][ delta$[i1,i2] Delta$[a3,a4] ]],{j,{1,2,4}}],colourfactors]+{0,0,CA},
  toColourBasis[Sum[ toExplColour[T[4,j][ delta$[i1,i2] Delta$[a3,a4] ]],{j,{1,2,3}}],colourfactors]+{0,0,CA}
}/.fixedsigns//Simplify;

If[Union[Flatten[colourids]]=!={0}, Print["error in colour"]; Abort[]];


Clear[TTmatrix];
Do[
  TTmatrix[i,j]=toColourBasis[toExplColour[T[i,j][#]]/.fixedsigns,colourfactors]&/@colourfactors;,
  {i,1,4},{j,Complement[Range[4],{i}]}];


(* check colour conservation *)
checks = Simplify[{Sum[TTmatrix[1,i],{i,{2,3,4}}]+CF id,
  Sum[TTmatrix[2,i],{i,{1,3,4}}]+CF id,
  Sum[TTmatrix[3,i],{i,{1,2,4}}]+CA id,
  Sum[TTmatrix[4,i],{i,{1,2,3}}]+CA id,
  2*Sum[TTmatrix[i1,i2],{i1,1,4},{i2,i1+1,4}]+(2*CA+2*CF)id
  }];

If[Union[Flatten[checks]]=!={0}, Print["error in colour"]; Abort[]];


(* ::Subsection:: *)
(*subtraction terms*)


 a -> alpha_S/(4 Pi) 


GammaCusp = Sum[gcusp[n]*a^(n+1), {n,0,3}];
GammaGluon = Sum[ggluon[n]*a^(n+1), {n,0,3}];
GammaQuark = Sum[gquark[n]*a^(n+1), {n,0,3}];


BoldGammaFull=Sum[(1/2)*normTT*TTmatrix[i,j]*(-Log[-s[i,j]]),{i,1,4},{j,i+1,4}]*GammaCusp + 
  (2*GammaGluon+2*GammaQuark)*id //. log2PF;
  
Do[ BoldGamma[n]=Coefficient[BoldGammaFull,a,n+1];, {n,0,2}];


GammaPrimeFull=-GammaCusp*(2*cA+2*cF);
Do[ GammaPrime[n]=Coefficient[GammaPrimeFull,a,n+1];, {n,0,2}];


Z1 = GammaPrime[0]/4/eps^2*id + BoldGamma[0]/2/eps;

Z2 = GammaPrime[0]^2/32/eps^4*id + GammaPrime[0]/8/eps^3*(BoldGamma[0]-3/2*beta[0]*id) + 
  1/8/eps^2*BoldGamma[0] . (BoldGamma[0]-2*beta[0]*id)+ GammaPrime[1]/16/eps^2*id + BoldGamma[1]/4/eps;


Put[Z1,"Z1.m"];
Put[Z2,"Z2.m"];


(* ::Section:: *)
(*prepare symbolic form of subtraction terms*)


(* ::Subsection::Closed:: *)
(*UV renormalization & finite remainders (just for illustration purposes, not needed)*)


(*(* a = alphaS/(4 Pi) *)
couplingrenormalisation = a0->a*(1-a*beta[0]/eps+a^2*(beta[0]^2/eps^2-beta[1]/2/eps)+ALARM a^3);

renormalisation = Thread[{Aren[0],Aren[1],Aren[2]}->CoefficientList[Normal[Series[g0^2*(A[0] + g0^2*A[1]+ g0^4*A[2]) /. g0->Sqrt[a0] /. couplingrenormalisation,{a,0,3}]],a][[2;;-1]]];*)


(*Fdef = (Aren[0]+a*Aren[1]+a^2*Aren[2]+ALARM*a^3) - (1+z1*a+z2*a^2+ALARM*a^3)*(F[0]+a*F[1]+a^2*F[2]+ALARM*a^3);

finiteremainders=Simplify[Flatten[Solve[Table[Coefficient[Fdef/.renormalisation,a,k],{k,0,2}]==0,{F[0],F[1],F[2]}]]];*)


(* ::Subsection:: *)
(*pentagon functions*)


log2PF = {
 Log[-s[1,2]] -> F[1,1,1]-tci[1,2],
 Log[-s[1,3]] -> F[1,1,6],
 Log[-s[1,4]] -> F[1,1,9],
 Log[-s[1,5]] -> F[1,1,5],
 Log[-s[2,3]] -> F[1,1,2],
 Log[-s[2,4]] -> F[1,1,7],
 Log[-s[2,5]] -> F[1,1,10],
 Log[-s[3,4]] -> F[1,1,3]-tci[1,2],
 Log[-s[3,5]] -> F[1,1,8]-tci[1,2],
 Log[-s[4,5]] -> F[1,1,4]-tci[1,2],
 Pi->-I*tci[1, 2],
 Zeta[3] -> tcr[3,3]
};

pisqrule = tci[1,2]^a_:>tci[1,1]^2*tci[1,2]^(a-2) /; a>=2;

PF2log=Flatten[Solve[log2PF[[1;;10]]/.Rule->Equal]];


(* ::Subsection:: *)
(*construct general subtraction terms*)


Z1=Get["Z1.m"];
Z2=Get["Z2.m"];


(* values of all anomalous dimensions and flags *)
ExpectedValues = {
  gcusp[0]->4,
  gcusp[1]->(268/9-4*Pi^2/3)*cA-80/9*Tf*nf,
  gcusp[2]->cA^2*(490/3-536/27*Pi^2+44/45*Pi^4+88/3*Zeta[3])+
    cA*Tf*nf*(-1672/27+160/27*Pi^2-224/3*Zeta[3])+
    cF*Tf*nf*(-220/3+64*Zeta[3])-64/27*Tf^2*nf^2,
    
  gquark[0]->-3*cF,
  gquark[1]->cF^2*(-3/2+2*Pi^2-24*Zeta[3])+cF*cA*(-961/54-11/6*Pi^2+26*Zeta[3])+
    cF*Tf*nf*(130/27+2*Pi^2/3),
  gquark[2]->cF^3*gqflag[1]+cF^2*cA*gqflag[2]+cF*cA^2*gqflag[3]+cF^2*Tf*nf*gqflag[4]+
    cF*cA*Tf*nf*gqflag[5]+cF*Tf^2*nf^2*gqflag[6],
    
  ggluon[0]->-beta[0],
  ggluon[1]->cA^2*(-692/27+11/18*Pi^2+2*Zeta[3])+cA*Tf*nf*(256/27-2*Pi^2/9)+4*cF*Tf*nf,
  
  beta[0]->11/3*cA-4/3*Tf*nf,
  beta[1]->2/3*(17*cA^2-10*cA*TR*nf-6*cF*TR*nf),
  
  cA->2*Nc*flag["cA"], 
  cF->(Nc^2-1)/Nc*flag["cF"],
  
  TR->1,
  Tf->1,
  flag["cA"]->1,
  flag["cF"]->1,
  flag["d12d34"]->1,
  unfixedsign->1
  
} /.  {Pi->-I*tci[1, 1],Zeta[3] -> tcr[3,3]};


fullamp0Lsymb={A0L["T2341"],A0L["T2431"],0};  (* A0L["d12d34"] vanishes *)


colourfactors={"T2341","T2431","d12d34"};
nfpowers = {0,1};
ncpowers={-1,0,1};

fullamp1Lsymb = Table[Sum[
  A1L[cc,StringReplace["Ncp"<>ToString[ncp],"-"->"m"], "nfp"<>ToString[nfp]]*Nc^ncp*nf^nfp,
  {ncp,ncpowers},{nfp,nfpowers}],
  {cc,colourfactors}];


sub1L = fullamp0Lsymb . (Z1 + beta[0]/eps*id);
sub1L = Collect[sub1L //. ExpectedValues /. log2PF,eps,Simplify];


sub2L = fullamp0Lsymb . (-Z1 . Z1+Z2-Z1*beta[0]/eps+(-(beta[0]^2/eps^2)+beta[1]/(2 eps))*id) + 
  fullamp1Lsymb . (Z1+2*beta[0]/eps*id);
sub2L = Collect[Expand[sub2L //. ExpectedValues //. log2PF]//.pisqrule,eps,Simplify];


(* ::Chapter::Closed:: *)
(*auxiliary functions*)


GetFunctionVector[expr_]:=Module[{tmpexpr,funcs},
  If[MatchQ[expr,0],Return[{{0},{mono[1]}}];];
  tmpexpr = Collect[expr,tci[1,2]] //. tci[1,2]^a_:>tci[1,1]^2*tci[1,2]^(a-2) /; a>=2;
  funcs= Select[Variables[tmpexpr],Or[MatchQ[#,F[__]],MatchQ[#,tci[__]],MatchQ[#,tcr[__]]]&];
  If[Length[funcs]==0,funcs={dummy}];
  Return[Transpose[CoefficientRules[tmpexpr,funcs] /. Rule[a_,b_]:>{b,mono[Times@@(funcs^a)]}]];
];


GetFunctionVectorEps[expr_]:=Module[{tmpexpr,funcs},
  If[MatchQ[expr,0],Return[{{0},{mono[1]}}];];
  tmpexpr = Collect[expr,tci[1,2]] //. tci[1,2]^a_:>tci[1,1]^2*tci[1,2]^(a-2) /; a>=2;
  funcs= Select[Variables[tmpexpr],Or[MatchQ[#,F[__]],MatchQ[#,tci[__]],MatchQ[#,tcr[__]],MatchQ[#,Power[eps,_]],MatchQ[#,eps]]&];
  If[Length[funcs]==0,funcs={dummy}];
  Return[Transpose[CoefficientRules[eps^6*tmpexpr,funcs] /. Rule[a_,b_]:>{b,mono[(Times@@(funcs^a))/eps^6]}]];
];


MyAmpMult[{c1_List,f1_List},{c2_List,f2_List},outmonolist_]:=Module[{cfrules,newc1,newc2,newf1,newf2,fout,cout,cvec,ccc},
  If[DeleteDuplicates[c1]=={0}||DeleteDuplicates[c2]=={0},Return[Table[0,{ii,outmonolist}]]];
  cfrules = Rule@@@Transpose[{(ccc/@Range[Length[c1]]),Factor[c1]}];
  {newc1,newf1} = GetFunctionVector[(ccc/@Range[Length[c1]]) . f1 /. mono[x_]:>x];
  {newc2,newf2} = GetFunctionVector[c2 . f2 /. mono[x_]:>x];
  fout = Outer[mono[Times[#1 /. mono[x_]:>x,#2 /. mono[x_]:>x]]&,newf1,newf2];
  fout = fout //. tci[1,2]^a_:>tci[1,1]^2*tci[1,2]^(a-2) /; a>=2;
  cout = Outer[Times,newc1,newc2];
  cvec = Table[Plus@@Extract[cout,Position[fout,mmm]],{mmm,(mono/@outmonolist)}];
  
  (* add check! *)
  
  Return[cvec /. cfrules];
];


Clear[GetLinearRelations];
Options[GetLinearRelations] = {"PrintDebugInfo"->0};
GetLinearRelations[funcs_List, OptionsPattern[]]:=Module[{vs,lrgraph,in,out,
  fs,degrees,complexity,sortedcs,sortedfs,sorted,zero,fiteq,graphfit,fit,
  fitlearn,fitrec,fitsol,linrels},

  If[Length[funcs]===1, Return[{}]];
  vs=Variables[funcs];
  If[OptionValue["PrintDebugInfo"]>0, Print[Length[funcs], " coefficients"]; Print["variables: ", vs]];
  FFNewGraph[lrgraph,in,vs];
  FFAlgRatExprEval[lrgraph,out,{in},vs,funcs]//Print;
  FFGraphOutput[lrgraph,out];
  
  fs = f/@Range[Length[funcs]];
  
  degrees = FFTotalDegrees[lrgraph];
  complexity = Max@@#&/@degrees;
  
  sortedfs = SortBy[fs,complexity[[#[[1]]]]&];
  sortedcs = c@@#&/@sortedfs;
  
  FFAlgTake[lrgraph,sorted,{out},{fs}->sortedfs];
  FFAlgRatNumEval[lrgraph,zero,{0}];
  FFAlgChain[lrgraph,fiteq,{sorted,zero}];
  FFGraphOutput[lrgraph,fiteq];
  
  FFNewGraph[graphfit];
  FFAlgSubgraphFit[graphfit,fit,{},lrgraph,vs,sortedcs]//Print;
  FFGraphOutput[graphfit,fit];

  fitlearn=FFDenseSolverLearn[graphfit,sortedcs];
  fitrec = FFReconstructNumeric[graphfit];
  fitsol = FFDenseSolverSol[fitrec,fitlearn];
  
  linrels=FFLinearRelationsFromFit[sortedfs,sortedcs,fitsol];
  
  If[OptionValue["PrintDebugInfo"]>0, 
    Print[Length[Variables[Array[f,Length[funcs]]/.linrels]]," linearly independent coefficients"]];

  FFDeleteGraph[graphfit];
  FFDeleteGraph[lrgraph];

  Return[linrels]; 
];


(* ::Chapter:: *)
(*poles of 2-loop amplitudes*)


(* ::Section:: *)
(*normalisations*)


Normalisation0L = 1;
Normalisation1L = -1;
Normalisation2L = 1;


(* ::Section:: *)
(*symbolic subtraction*)


dictColourFactors = Association[{"T2341"->1,"T2431"->2,"d12d34"->3}];
dictNcFactors = Association[Table[StringReplace["Ncp"<>ToString[i],"-"->"m"]->i,{i,-2,2}]];

dictNf2qk["OGAqkm4qkm2Qqk"] = Association[{"nfp0"->"","nfp1"->"qk","nfp2"->"qkp2"}];
dictNf2qk["Oqkm4qkm2Qqk"] = Association[{"nfp0"->"qkGA","nfp1"->"qkqkGA"}];
dictNf2int = Association[{"nfp0"->0,"nfp1"->1,"nfp2"->2}];

dictClosedLoopFactor2nf = Association[{"qkp2_OGAqkm4qkm2Qqk"->2,
  "qk_OGAqkm4qkm2Qqk"->1,
  "_OGAqkm4qkm2Qqk"->0,
  "qkGA_Oqkm4qkm2Qqk"->0,
  "qkqkGA_Oqkm4qkm2Qqk"->1}];


neededsub2L=Coefficient[Coefficient[sub2L[[dictColourFactors[colourfactor]]],nf,dictClosedLoopFactor2nf[closedloopfactor]],Nc,dictNcFactors[ncfactor]];


ampname2L = StringJoin["amp_",looporder,"_",process,"_",colourfactor,"_",closedloopfactor,"_",
  ncfactor,"_",helicity,"_",psmode,".m"];


neededamps0L=Union[Cases[neededsub2L,A0L[__],Infinity]];
neededamps1L=Union[Cases[neededsub2L,A1L[__],Infinity]];
Print["needed amplitudes: ", Join[neededamps0L,neededamps1L]];


missedpieces=Complement[DeleteCases[Variables[neededsub2L],eps|_F|_tci|_tcr],Join[neededamps0L,neededamps1L]];
If[missedpieces=!={}, Print["careful! something is left unfixed and may cause trouble: ", missedpieces]];


(* ::Section:: *)
(*load lower-loop info*)


(* ::Subsection:: *)
(*tree-level*)


Do[
  file = "DiagramNumerators_0L_2q2gA_"<>col<>"__"<>closedloopfactortype<>"_Ncp0_dsm2p0.m";
  If[!FileExistsQ[PathToAmps0L<>file],
    Print[file, " not found - setting to 0"];
    amp0L[col]=0;,
    
    Print["loading ", PathToAmps0L<>file];
    Get[PathToAmps0L<>file];
    amp0L[col]=Normalisation0L*DiagramNumerator[StringReplace[helicity,"m"->"-"],topo[]]/.INT[nn_,__]:>nn;
    Clear[DiagramNumerator,file];
  ];
,{col, Cases[neededamps0L,A0L[cc_]:>cc]}];


(* ::Subsection:: *)
(*1-loop*)


coeffrules1L = {};

Do[
  {col,ncf,nff}=List@@a;
  
  file="amp_1L_"<>process<>"_"<>col<>"_"<>dictNf2qk[closedloopfactortype][nff]<>"_"<>
    closedloopfactortype<>"_"<>ncf<>"_"<>helicity<>"_"<>psmode<>".m";

  If[!FileExistsQ[PathToAmps1L<>file],
    Print[file, " not found - setting to 0"];
    tmpcoeffrules={};
    amp1Ldata[col,ncf,nff]={{0},{1}};,
    
    Print[file, " found"];
    {tmpcoeffrules,amp1Ldata[col,ncf,nff]}=Get[PathToAmps1L<>file]/.f[i_]:>f1L[col,ncf,nff,i];
    ];
  
  coeffrules1L=Join[coeffrules1L,tmpcoeffrules];

  amp1L[col,ncf,nff] = Normalisation1L*(Dot@@amp1Ldata[col,ncf,nff]);

  Clear[file,col,ncf,nff,tmpcoeffrules,ampdata1L];
,{a,neededamps1L}];


(* ::Section:: *)
(*2-loop poles*)


rndX = Which[psmode==="PSanalyticX1",
  Prepend[Thread[Table[ex[i],{i,2,5}]->RandomInteger[{99,999999999},4]],ex[1]->1],
  psmode==="PSanalytic",
  Thread[Table[ex[i],{i,1,5}]->RandomInteger[{99,999999999},5]],
  True,
  Print["unknown psmode"]; Quit[]];


Print["constructing explicit expression of the subtraction term - this may take some time"];
neededsub2Lexpl = Normal@Series[neededsub2L /. A0L->amp0L/. A1L->amp1L, {eps,0,0}];
neededsub2Lexpl = neededsub2Lexpl/.coeffrules1L;
If[psmode==="PSanalyticX1", neededsub2Lexpl = neededsub2Lexpl /. ex[1]->1];


ampname2L = StringJoin["amp_",looporder,"_",process,"_",colourfactor,"_",closedloopfactor,"_",
  ncfactor,"_",helicity,"_",psmode,".m"];

If[checkpoles,
  If[!FileExistsQ[PathToAmps2L<>ampname2L],Print[ampname2L, " not found!"]; Quit[];];
  Print["getting ", ampname2L]; now=AbsoluteTime[];
  ampdata2L = Get[PathToAmps2L<>ampname2L];
  Print["-> ",AbsoluteTime[]-now, " s"];

  coeffrules2L=ampdata2L[[1]];
  amp2L=ampdata2L[[2]];
  Clear[ampdata2L];
  
  (* check if the 2-loop amplitude is finite *)
  If[Union[Flatten[Table[Coefficient[amp2L[[1]],eps,k],{k,-4,-1}]]]==={0},
    Print["2-loop amplitude is finite"]];
  
  checkzero = (Dot@@amp2L /. Dispatch[coeffrules2L/.rndX]) - (neededsub2Lexpl /. rndX);
  checkzero = Table[Coefficient[checkzero,eps,k],{k,-4,-1}];
  checkzero = Expand[checkzero]/.pisqrule;
  If[Union[checkzero]==={0}, Print["the two-loop poles cancel out"], 
    Print["something wrong in the two-loop poles!"]; Quit[]];
  Clear[checkzero];
  
];


If[checkpolesonly, Quit[]];


{subcoeffs,submonos}=GetFunctionVectorEps[neededsub2Lexpl] /. mono->Identity;

(* rewrite subtraction term in terms of linearly independent f[i]'s *)
rels=GetLinearRelations[subcoeffs, "PrintDebugInfo"->1];
subcoeffsfs=Array[f,Length[subcoeffs]]/.rels;

(* definition of independent f[i]'s *)
indepfs=Variables[subcoeffsfs];
indepfs=Thread[indepfs->subcoeffs[[indepfs/.f->Identity]]];

(* redefine f[i]'s so that i=1,2,3,... *)
subcoeffsfs=subcoeffsfs/.Thread[indepfs[[All,1]]->Array[f,Length[indepfs]]];
indepfs=indepfs/.Thread[indepfs[[All,1]]->Array[f,Length[indepfs]]];

(* common denominator form *)
Print["together... this may take a while"];
indepfs=Thread[indepfs[[All,1]]->Together[indepfs[[All,2]]]];

(* format *)
{subcoeffsfs,submonos}=GetFunctionVector[subcoeffsfs . submonos] /. mono->Identity;
subcoeffsfs = Collect[subcoeffsfs,eps,Factor];

sub2Lfinal={indepfs,{subcoeffsfs,submonos}};

Clear[subcoeffsfs,indepfs,rels,submonos,subcoeffs];


now=AbsoluteTime[];
check=Expand[Expand[(Dot@@sub2Lfinal[[2]]/.Dispatch[sub2Lfinal[[1]]/.rndX])-(neededsub2Lexpl /. rndX)] /. pisqrule]===0;
Print[AbsoluteTime[]-now];
If[!check,Print["something went wrong"]; Quit[]];


subname2L=StringReplace[ampname2L,"amp"->"poles"];
Put[sub2Lfinal,subname2L];


Quit[];
