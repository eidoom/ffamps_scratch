#!/usr/bin/env python3

import glob
import subprocess

amp2list = glob.glob('/scratch/public/ffamps_scratch/2q2gA/eps_exp/amp_2L*');
# amp2list = glob.glob('/Users/zoia/Work/ffamps_scratch/2q2gA/eps_exp/amp_2L*');

print(len(amp2list), 'amplitudes found');

amp2list = [a.split('_') for a in amp2list];

inputs = [{"psmode": a[-1].split('.')[0],
  "helicity": a[-2], 
  "ncfactor": a[-3], 
  "closedloopfactor": a[-5]+'_'+a[-4], 
  "colourfactor": a[-6]} for a in amp2list];

print('--------------------------------\n',flush=True);

for j in range(len(amp2list)):
  print('running ', j, '/', len(amp2list)-1,flush=True);
  subprocess.run(['math','-script', 'WritePoles2L.wl', 
    '-helicity', inputs[j]["helicity"], 
    '-ncfactor', inputs[j]["ncfactor"],
    '-closedloopfactor', inputs[j]["closedloopfactor"],
    '-colourfactor', inputs[j]["colourfactor"],
    '-psmode', inputs[j]["psmode"],
    '-checkpolesonly']);
  print('--------------------------------\n',flush=True);

