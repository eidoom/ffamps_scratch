{{((2*cA + 2*cF)^2*gcusp[0]^2)/(32*eps^4) - ((2*cA + 2*cF)*gcusp[1])/
    (16*eps^2) - ((2*cA + 2*cF)*gcusp[0]*((-3*beta[0])/2 - 
      (TR*F[1, 1, 1]*gcusp[0])/Nc + Nc*TR*F[1, 1, 2]*gcusp[0] + 
      Nc*TR*F[1, 1, 3]*gcusp[0] + Nc*TR*F[1, 1, 9]*gcusp[0] + 2*ggluon[0] + 
      2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2]))/
    (8*eps^3) + (-((TR*F[1, 1, 1]*gcusp[1])/Nc) + Nc*TR*F[1, 1, 2]*gcusp[1] + 
     Nc*TR*F[1, 1, 3]*gcusp[1] + Nc*TR*F[1, 1, 9]*gcusp[1] + 2*ggluon[1] + 
     2*gquark[1] + (TR*gcusp[1]*tci[1, 2])/Nc - Nc*TR*gcusp[1]*tci[1, 2])/
    (4*eps) + ((unfixedsign*F[1, 1, 2] - unfixedsign*F[1, 1, 6] - 
       unfixedsign*F[1, 1, 7] + unfixedsign*F[1, 1, 9])*gcusp[0]^2*
      (-(TR^2*F[1, 1, 6]) - TR^2*F[1, 1, 7] + TR^2*(F[1, 1, 1] - tci[1, 2]) + 
       TR^2*(F[1, 1, 3] - tci[1, 2])) + (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + 
       Nc*TR*F[1, 1, 2]*gcusp[0] + Nc*TR*F[1, 1, 3]*gcusp[0] + 
       Nc*TR*F[1, 1, 9]*gcusp[0] + 2*ggluon[0] + 2*gquark[0] + 
       (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2])*
      (-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/Nc + Nc*TR*F[1, 1, 2]*gcusp[0] + 
       Nc*TR*F[1, 1, 3]*gcusp[0] + Nc*TR*F[1, 1, 9]*gcusp[0] + 2*ggluon[0] + 
       2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2]))/
    (8*eps^2), ((-(unfixedsign*F[1, 1, 2]) + unfixedsign*F[1, 1, 6] + 
     unfixedsign*F[1, 1, 7] - unfixedsign*F[1, 1, 9])*gcusp[0]^2*
    (-(TR^2*F[1, 1, 6]) - TR^2*F[1, 1, 7] + TR^2*(F[1, 1, 1] - tci[1, 2]) + 
     TR^2*(F[1, 1, 3] - tci[1, 2])))/(8*eps^2), 
  -1/8*((2*cA + 2*cF)*gcusp[0]^2*(-(TR^2*F[1, 1, 6]) - TR^2*F[1, 1, 7] + 
       TR^2*(F[1, 1, 1] - tci[1, 2]) + TR^2*(F[1, 1, 3] - tci[1, 2])))/
     eps^3 + (gcusp[1]*(-(TR^2*F[1, 1, 6]) - TR^2*F[1, 1, 7] + 
      TR^2*(F[1, 1, 1] - tci[1, 2]) + TR^2*(F[1, 1, 3] - tci[1, 2])))/
    (4*eps) + (gcusp[0]*(-(TR^2*F[1, 1, 6]) - TR^2*F[1, 1, 7] + 
       TR^2*(F[1, 1, 1] - tci[1, 2]) + TR^2*(F[1, 1, 3] - tci[1, 2]))*
      (-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/Nc + Nc*TR*F[1, 1, 1]*gcusp[0] + 
       2*Nc*TR*F[1, 1, 3]*gcusp[0] + 2*ggluon[0] + 2*gquark[0] + 
       (TR*gcusp[0]*tci[1, 2])/Nc - 3*Nc*TR*gcusp[0]*tci[1, 2]) + 
     gcusp[0]*(-(TR^2*F[1, 1, 6]) - TR^2*F[1, 1, 7] + 
       TR^2*(F[1, 1, 1] - tci[1, 2]) + TR^2*(F[1, 1, 3] - tci[1, 2]))*
      (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + Nc*TR*F[1, 1, 2]*gcusp[0] + 
       Nc*TR*F[1, 1, 3]*gcusp[0] + Nc*TR*F[1, 1, 9]*gcusp[0] + 2*ggluon[0] + 
       2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2]))/
    (8*eps^2)}, {((unfixedsign*F[1, 1, 2] - unfixedsign*F[1, 1, 6] - 
     unfixedsign*F[1, 1, 7] + unfixedsign*F[1, 1, 9])*gcusp[0]^2*
    (-(TR^2*F[1, 1, 2]) - TR^2*F[1, 1, 9] + TR^2*(F[1, 1, 1] - tci[1, 2]) + 
     TR^2*(F[1, 1, 3] - tci[1, 2])))/(8*eps^2), 
  ((2*cA + 2*cF)^2*gcusp[0]^2)/(32*eps^4) - ((2*cA + 2*cF)*gcusp[1])/
    (16*eps^2) - ((2*cA + 2*cF)*gcusp[0]*((-3*beta[0])/2 - 
      (TR*F[1, 1, 1]*gcusp[0])/Nc + Nc*TR*F[1, 1, 3]*gcusp[0] + 
      Nc*TR*F[1, 1, 6]*gcusp[0] + Nc*TR*F[1, 1, 7]*gcusp[0] + 2*ggluon[0] + 
      2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2]))/
    (8*eps^3) + (-((TR*F[1, 1, 1]*gcusp[1])/Nc) + Nc*TR*F[1, 1, 3]*gcusp[1] + 
     Nc*TR*F[1, 1, 6]*gcusp[1] + Nc*TR*F[1, 1, 7]*gcusp[1] + 2*ggluon[1] + 
     2*gquark[1] + (TR*gcusp[1]*tci[1, 2])/Nc - Nc*TR*gcusp[1]*tci[1, 2])/
    (4*eps) + ((-(unfixedsign*F[1, 1, 2]) + unfixedsign*F[1, 1, 6] + 
       unfixedsign*F[1, 1, 7] - unfixedsign*F[1, 1, 9])*gcusp[0]^2*
      (-(TR^2*F[1, 1, 2]) - TR^2*F[1, 1, 9] + TR^2*(F[1, 1, 1] - tci[1, 2]) + 
       TR^2*(F[1, 1, 3] - tci[1, 2])) + (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + 
       Nc*TR*F[1, 1, 3]*gcusp[0] + Nc*TR*F[1, 1, 6]*gcusp[0] + 
       Nc*TR*F[1, 1, 7]*gcusp[0] + 2*ggluon[0] + 2*gquark[0] + 
       (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2])*
      (-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/Nc + Nc*TR*F[1, 1, 3]*gcusp[0] + 
       Nc*TR*F[1, 1, 6]*gcusp[0] + Nc*TR*F[1, 1, 7]*gcusp[0] + 2*ggluon[0] + 
       2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2]))/
    (8*eps^2), -1/8*((2*cA + 2*cF)*gcusp[0]^2*(-(TR^2*F[1, 1, 2]) - 
       TR^2*F[1, 1, 9] + TR^2*(F[1, 1, 1] - tci[1, 2]) + 
       TR^2*(F[1, 1, 3] - tci[1, 2])))/eps^3 + 
   (gcusp[1]*(-(TR^2*F[1, 1, 2]) - TR^2*F[1, 1, 9] + 
      TR^2*(F[1, 1, 1] - tci[1, 2]) + TR^2*(F[1, 1, 3] - tci[1, 2])))/
    (4*eps) + (gcusp[0]*(-(TR^2*F[1, 1, 2]) - TR^2*F[1, 1, 9] + 
       TR^2*(F[1, 1, 1] - tci[1, 2]) + TR^2*(F[1, 1, 3] - tci[1, 2]))*
      (-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/Nc + Nc*TR*F[1, 1, 1]*gcusp[0] + 
       2*Nc*TR*F[1, 1, 3]*gcusp[0] + 2*ggluon[0] + 2*gquark[0] + 
       (TR*gcusp[0]*tci[1, 2])/Nc - 3*Nc*TR*gcusp[0]*tci[1, 2]) + 
     gcusp[0]*(-(TR^2*F[1, 1, 2]) - TR^2*F[1, 1, 9] + 
       TR^2*(F[1, 1, 1] - tci[1, 2]) + TR^2*(F[1, 1, 3] - tci[1, 2]))*
      (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + Nc*TR*F[1, 1, 3]*gcusp[0] + 
       Nc*TR*F[1, 1, 6]*gcusp[0] + Nc*TR*F[1, 1, 7]*gcusp[0] + 2*ggluon[0] + 
       2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2]))/
    (8*eps^2)}, 
 {-1/8*((2*cA + 2*cF)*(unfixedsign*F[1, 1, 2] - unfixedsign*F[1, 1, 6] - 
       unfixedsign*F[1, 1, 7] + unfixedsign*F[1, 1, 9])*gcusp[0]^2)/eps^3 + 
   ((unfixedsign*F[1, 1, 2] - unfixedsign*F[1, 1, 6] - 
      unfixedsign*F[1, 1, 7] + unfixedsign*F[1, 1, 9])*gcusp[1])/(4*eps) + 
   ((unfixedsign*F[1, 1, 2] - unfixedsign*F[1, 1, 6] - 
       unfixedsign*F[1, 1, 7] + unfixedsign*F[1, 1, 9])*gcusp[0]*
      (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + Nc*TR*F[1, 1, 1]*gcusp[0] + 
       2*Nc*TR*F[1, 1, 3]*gcusp[0] + 2*ggluon[0] + 2*gquark[0] + 
       (TR*gcusp[0]*tci[1, 2])/Nc - 3*Nc*TR*gcusp[0]*tci[1, 2]) + 
     (unfixedsign*F[1, 1, 2] - unfixedsign*F[1, 1, 6] - 
       unfixedsign*F[1, 1, 7] + unfixedsign*F[1, 1, 9])*gcusp[0]*
      (-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/Nc + Nc*TR*F[1, 1, 2]*gcusp[0] + 
       Nc*TR*F[1, 1, 3]*gcusp[0] + Nc*TR*F[1, 1, 9]*gcusp[0] + 2*ggluon[0] + 
       2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2]))/
    (8*eps^2), 
  -1/8*((2*cA + 2*cF)*(-(unfixedsign*F[1, 1, 2]) + unfixedsign*F[1, 1, 6] + 
       unfixedsign*F[1, 1, 7] - unfixedsign*F[1, 1, 9])*gcusp[0]^2)/eps^3 + 
   ((-(unfixedsign*F[1, 1, 2]) + unfixedsign*F[1, 1, 6] + 
      unfixedsign*F[1, 1, 7] - unfixedsign*F[1, 1, 9])*gcusp[1])/(4*eps) + 
   ((-(unfixedsign*F[1, 1, 2]) + unfixedsign*F[1, 1, 6] + 
       unfixedsign*F[1, 1, 7] - unfixedsign*F[1, 1, 9])*gcusp[0]*
      (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + Nc*TR*F[1, 1, 1]*gcusp[0] + 
       2*Nc*TR*F[1, 1, 3]*gcusp[0] + 2*ggluon[0] + 2*gquark[0] + 
       (TR*gcusp[0]*tci[1, 2])/Nc - 3*Nc*TR*gcusp[0]*tci[1, 2]) + 
     (-(unfixedsign*F[1, 1, 2]) + unfixedsign*F[1, 1, 6] + 
       unfixedsign*F[1, 1, 7] - unfixedsign*F[1, 1, 9])*gcusp[0]*
      (-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/Nc + Nc*TR*F[1, 1, 3]*gcusp[0] + 
       Nc*TR*F[1, 1, 6]*gcusp[0] + Nc*TR*F[1, 1, 7]*gcusp[0] + 2*ggluon[0] + 
       2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2]))/
    (8*eps^2), ((2*cA + 2*cF)^2*gcusp[0]^2)/(32*eps^4) - 
   ((2*cA + 2*cF)*gcusp[1])/(16*eps^2) - 
   ((2*cA + 2*cF)*gcusp[0]*((-3*beta[0])/2 - (TR*F[1, 1, 1]*gcusp[0])/Nc + 
      Nc*TR*F[1, 1, 1]*gcusp[0] + 2*Nc*TR*F[1, 1, 3]*gcusp[0] + 2*ggluon[0] + 
      2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - 3*Nc*TR*gcusp[0]*tci[1, 2]))/
    (8*eps^3) + (-((TR*F[1, 1, 1]*gcusp[1])/Nc) + Nc*TR*F[1, 1, 1]*gcusp[1] + 
     2*Nc*TR*F[1, 1, 3]*gcusp[1] + 2*ggluon[1] + 2*gquark[1] + 
     (TR*gcusp[1]*tci[1, 2])/Nc - 3*Nc*TR*gcusp[1]*tci[1, 2])/(4*eps) + 
   ((unfixedsign*F[1, 1, 2] - unfixedsign*F[1, 1, 6] - 
       unfixedsign*F[1, 1, 7] + unfixedsign*F[1, 1, 9])*gcusp[0]^2*
      (-(TR^2*F[1, 1, 6]) - TR^2*F[1, 1, 7] + TR^2*(F[1, 1, 1] - tci[1, 2]) + 
       TR^2*(F[1, 1, 3] - tci[1, 2])) + (-(unfixedsign*F[1, 1, 2]) + 
       unfixedsign*F[1, 1, 6] + unfixedsign*F[1, 1, 7] - 
       unfixedsign*F[1, 1, 9])*gcusp[0]^2*(-(TR^2*F[1, 1, 2]) - 
       TR^2*F[1, 1, 9] + TR^2*(F[1, 1, 1] - tci[1, 2]) + 
       TR^2*(F[1, 1, 3] - tci[1, 2])) + (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + 
       Nc*TR*F[1, 1, 1]*gcusp[0] + 2*Nc*TR*F[1, 1, 3]*gcusp[0] + 
       2*ggluon[0] + 2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - 
       3*Nc*TR*gcusp[0]*tci[1, 2])*(-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/
        Nc + Nc*TR*F[1, 1, 1]*gcusp[0] + 2*Nc*TR*F[1, 1, 3]*gcusp[0] + 
       2*ggluon[0] + 2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - 
       3*Nc*TR*gcusp[0]*tci[1, 2]))/(8*eps^2)}}
