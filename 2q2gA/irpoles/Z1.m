{{-1/4*((2*cA + 2*cF)*gcusp[0])/eps^2 + 
   (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + Nc*TR*F[1, 1, 2]*gcusp[0] + 
     Nc*TR*F[1, 1, 3]*gcusp[0] + Nc*TR*F[1, 1, 9]*gcusp[0] + 2*ggluon[0] + 
     2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2])/
    (2*eps), 0, (gcusp[0]*(-(TR^2*F[1, 1, 6]) - TR^2*F[1, 1, 7] + 
     TR^2*(F[1, 1, 1] - tci[1, 2]) + TR^2*(F[1, 1, 3] - tci[1, 2])))/
   (2*eps)}, {0, -1/4*((2*cA + 2*cF)*gcusp[0])/eps^2 + 
   (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + Nc*TR*F[1, 1, 3]*gcusp[0] + 
     Nc*TR*F[1, 1, 6]*gcusp[0] + Nc*TR*F[1, 1, 7]*gcusp[0] + 2*ggluon[0] + 
     2*gquark[0] + (TR*gcusp[0]*tci[1, 2])/Nc - Nc*TR*gcusp[0]*tci[1, 2])/
    (2*eps), (gcusp[0]*(-(TR^2*F[1, 1, 2]) - TR^2*F[1, 1, 9] + 
     TR^2*(F[1, 1, 1] - tci[1, 2]) + TR^2*(F[1, 1, 3] - tci[1, 2])))/
   (2*eps)}, {((unfixedsign*F[1, 1, 2] - unfixedsign*F[1, 1, 6] - 
     unfixedsign*F[1, 1, 7] + unfixedsign*F[1, 1, 9])*gcusp[0])/(2*eps), 
  ((-(unfixedsign*F[1, 1, 2]) + unfixedsign*F[1, 1, 6] + 
     unfixedsign*F[1, 1, 7] - unfixedsign*F[1, 1, 9])*gcusp[0])/(2*eps), 
  -1/4*((2*cA + 2*cF)*gcusp[0])/eps^2 + 
   (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + Nc*TR*F[1, 1, 1]*gcusp[0] + 
     2*Nc*TR*F[1, 1, 3]*gcusp[0] + 2*ggluon[0] + 2*gquark[0] + 
     (TR*gcusp[0]*tci[1, 2])/Nc - 3*Nc*TR*gcusp[0]*tci[1, 2])/(2*eps)}}
