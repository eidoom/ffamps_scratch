#!/usr/bin/env python3

import glob
import subprocess

amplist = glob.glob('/scratch/public/ffamps_scratch/2q2gA/eps_exp/amp_1L*');
# amplist = glob.glob('/Users/zoia/Work/ffamps_scratch/2q2gA/eps_exp/amp_1L*');

print(len(amplist), 'amplitudes found');

amplist = [a.split('_') for a in amplist];

inputs = [{"psmode": a[-1].split('.')[0],
  "helicity": a[-2], 
  "ncfactor": a[-3], 
  "closedloopfactor": a[-5]+'_'+a[-4], 
  "colourfactor": a[-6]} for a in amplist];

print('--------------------------------\n',flush=True);

for j in range(len(amplist)):
  print('running ', j, '/', len(amplist)-1,flush=True);
  subprocess.run(['math','-script', 'WritePoles1L.wl', 
    '-helicity', inputs[j]["helicity"], 
    '-ncfactor', inputs[j]["ncfactor"],
    '-closedloopfactor', inputs[j]["closedloopfactor"],
    '-colourfactor', inputs[j]["colourfactor"],
    '-psmode', inputs[j]["psmode"],
    '-checkpoles']);
  print('--------------------------------\n',flush=True);

