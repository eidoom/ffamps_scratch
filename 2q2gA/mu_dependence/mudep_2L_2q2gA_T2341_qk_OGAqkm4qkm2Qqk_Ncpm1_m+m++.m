2*(F[1, 1, 2] - F[1, 1, 6] - F[1, 1, 7] + F[1, 1, 9])*
  F1L["d12d34", "Ncpm1", "nfp1"]*Log[mu2] - 
 (2*F1L["T2341", "Ncpm1", "nfp0"]*Log[mu2])/3 + 
 A0L["T2341"]*(-1/9*Log[mu2]^3 + 
   (Log[mu2]*(-65 + 60*F[1, 1, 1] + 9*tci[1, 1]^2 - 60*tci[1, 2]))/54 + 
   (Log[mu2]^2*(-19 + 6*F[1, 1, 1] - 6*tci[1, 2]))/18) + 
 F1L["T2341", "Ncp0", "nfp1"]*(Log[mu2]^2/2 + 
   Log[mu2]*(3/2 - F[1, 1, 1] + tci[1, 2]))
