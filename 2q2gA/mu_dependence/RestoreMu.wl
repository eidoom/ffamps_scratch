(* ::Package:: *)

(* normalisation of the colour generators: Tr[T^a T^b] = delta[a,b]/2 , i.e. TR=1/2 *)


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


process = "2q2gA";


<<FiniteFlow`;
<<FFUtils`;
<< "InitTwoLoopToolsFF.m";
<< "InitDiagramsFF.m";
Get["setupfiles/Setup_"<>process<>".m"];


PathToAmps0L = Directory[]<>"/../numerators/";
PathToFin = Directory[]<>"/../fin/";
PathToPoles = Directory[]<>"/../irpoles/";


(* EXAMPLE *)
(*1L_2q2gA_T2341__OGAqkm4qkm2Qqk_Ncpm1_m++m+_PSanalyticX1.m*)
colourfactor="T2341";
closedloopfactor="_OGAqkm4qkm2Qqk";
ncfactor="Ncpm1";
helicity="m++m+";
psmode="PSanalyticX1";
looporder = "1L";


CmdOptions = {
  "-helicity",
  "-colourfactor",
  "-ncfactor",
  "-closedloopfactor",
  "-psmode",
  "-looporder"
};

psmode = "PSanalytic";

Print[$CommandLine];

Do[
pos = Flatten[Position[$CommandLine,CmdOptions[[oo]]]];
If[Length[pos]==1,

  Switch[oo,
    1,  
    helicity = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    2,
    colourfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    3,
    ncfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    4,
    closedloopfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    5,
    psmode = $CommandLine[[pos[[1]]+1]];,
    6,
    looporder = $CommandLine[[pos[[1]]+1]];,
    _,
    Print["unknown argument ",oo,CmdOptions[[oo]]];
  ];

];
,{oo,1,Length[CmdOptions]}];



closedloopfactortype=StringSplit[closedloopfactor,"_"][[-1]];


(* ::Title:: *)
(*Prepare IR/UV subtraction*)


(* ::Subtitle:: *)
(*from arXiv:0903.1126*)


id = IdentityMatrix[3];


(* ::Section:: *)
(*prepare symbolic form of subtraction terms*)


(* ::Subsection::Closed:: *)
(*UV renormalization & finite remainders (just for illustration purposes, not needed)*)


(*(* bare coupling: a0
renormalised coupling: aR 
renormalisation of the coupling: a0 -> aR[mu2]*Zuv[mu2]*mu2^eps
mu2 = mu^2
*)

Zuv[mu2] = (1-aR[mu2]*beta[0]/eps+aR[mu2]^2*(beta[0]^2/eps^2-beta[1]/2/eps)+ALARM aR[mu2]^3);*)


(*bareamplitude = g0^2*(A[0] + g0^2*A[1]+ g0^4*A[2]+ALARM g0^6) /. g0->Sqrt[a0];*)


(*renormamplitude = bareamplitude /. a0->aR[mu2]*Zuv[mu2]*mu2^eps /. aR[mu2]->aRnorm/mu2^eps;
renormamplitude=Normal[Series[renormamplitude,{aRnorm,0,3}]];
renormamplitude=Collect[renormamplitude,aRnorm,Collect[#,eps,Expand]&];

renormalisation = Thread[{Aren[0],Aren[1],Aren[2]}->CoefficientList[renormamplitude,aRnorm][[2;;-1]]]*)


(*Fdef = (Aren[0]+aR*Aren[1]+aR^2*Aren[2]+ALARM*aR^3) - (1+Z1$*aR+Z2$*aR^2+ALARM*aR^3)*(F[0]+aR*F[1]+aR^2*F[2]+ALARM*aR^3);

finiteremainders=Simplify[Flatten[Solve[Table[Coefficient[Fdef/.renormalisation,aR,k],{k,0,2}]==0,{F[0],F[1],F[2]}]]]*)


(*finremexp = F[0] + aR[mu2] mu2^eps F[1] + (aR[mu2] mu2^eps)^2 F[2] + ALARM aR[mu2]^3;
Zexp = 1 + aR[mu2] Z1$ + aR[mu2]^2 Z2$ + ALARM aR[mu2]^3;
Arenexp = A[0] + aR[mu2] mu2^eps Aren[1] + (aR[mu2] mu2^eps)^2 Aren[2] + ALARM aR[mu2]^3;*)


(*Fdef=Simplify[Flatten[Solve[CoefficientList[Normal[Series[Zexp*finremexp - Arenexp,{aR[mu2],0,2}]],aR[mu2]]==0,{F[0],F[1],F[2]}]]];
Fdef=Simplify[Fdef/.renormalisation]*)


(* ::Subsection:: *)
(*pentagon functions*)


log2PF = {
 Log[-s[1,2]] -> F[1,1,1]-tci[1,2],
 Log[-s[1,3]] -> F[1,1,6],
 Log[-s[1,4]] -> F[1,1,9],
 Log[-s[1,5]] -> F[1,1,5],
 Log[-s[2,3]] -> F[1,1,2],
 Log[-s[2,4]] -> F[1,1,7],
 Log[-s[2,5]] -> F[1,1,10],
 Log[-s[3,4]] -> F[1,1,3]-tci[1,2],
 Log[-s[3,5]] -> F[1,1,8]-tci[1,2],
 Log[-s[4,5]] -> F[1,1,4]-tci[1,2],
 Pi->-I*tci[1, 2],
 Zeta[3] -> tcr[3,3]
};

pisqrule = tci[1,2]^a_:>tci[1,1]^2*tci[1,2]^(a-2) /; a>=2;

PF2log=Flatten[Solve[log2PF[[1;;10]]/.Rule->Equal]];


(* ::Subsection:: *)
(*construct general subtraction terms*)


Z1=Get[PathToPoles<>"Z1.m"];
Z2=Get[PathToPoles<>"Z2.m"];


(* values of all anomalous dimensions and flags *)
ExpectedValues = {
  gcusp[0]->4,
  gcusp[1]->(268/9-4*Pi^2/3)*cA-80/9*Tf*nf,
  gcusp[2]->cA^2*(490/3-536/27*Pi^2+44/45*Pi^4+88/3*Zeta[3])+
    cA*Tf*nf*(-1672/27+160/27*Pi^2-224/3*Zeta[3])+
    cF*Tf*nf*(-220/3+64*Zeta[3])-64/27*Tf^2*nf^2,
    
  gquark[0]->-3*cF,
  gquark[1]->cF^2*(-3/2+2*Pi^2-24*Zeta[3])+cF*cA*(-961/54-11/6*Pi^2+26*Zeta[3])+
    cF*Tf*nf*(130/27+2*Pi^2/3),
  gquark[2]->cF^3*gqflag[1]+cF^2*cA*gqflag[2]+cF*cA^2*gqflag[3]+cF^2*Tf*nf*gqflag[4]+
    cF*cA*Tf*nf*gqflag[5]+cF*Tf^2*nf^2*gqflag[6],
    
  ggluon[0]->-beta[0],
  ggluon[1]->cA^2*(-692/27+11/18*Pi^2+2*Zeta[3])+cA*Tf*nf*(256/27-2*Pi^2/9)+4*cF*Tf*nf,
  
  beta[0]->11/3*cA-4/3*Tf*nf,
  beta[1]->2/3*(17*cA^2-10*cA*TR*nf-6*cF*TR*nf),

  unfixedsign->1,

  cA -> Nc*(2*TR), 
  cF -> (Nc^2-1)/2/Nc*(2*TR),
  TR -> 1/2,
  Tf -> TR 
} /.  {Pi->-I*tci[1, 1],Zeta[3] -> tcr[3,3]};


fullamp0Lsymb = If[closedloopfactortype==="Oqkm4qkm2Qqk", 
   {0,0,0},
   {A0L["T2341"],A0L["T2431"],0}  (* A0L["d12d34"] vanishes *)
];

colourfactors={"T2341","T2431","d12d34"};
nfpowers = {0,1};
ncpowers={-1,0,1};

fullF1Lsymb = Table[Sum[
  F1L[cc,StringReplace["Ncp"<>ToString[ncp],"-"->"m"], "nfp"<>ToString[nfp]]*Nc^ncp*nf^nfp,
  {ncp,ncpowers},{nfp,nfpowers}],
  {cc,colourfactors}];


(* ::Subsection:: *)
(*work out mu-restoring terms (for illustration purposes)*)


(*Z1mu = Normal[Series[Z1 /. PF2log /. Log[a_]:>Log[a]-Log[mu2] /. log2PF,{eps,0,2}]];
Z2mu = Normal[Series[Z2 /. PF2log /. Log[a_]:>Log[a]-Log[mu2] /. log2PF,{eps,0,0}]];*)


(*(* hard-coded finite remainders with mu-dependence *)
Clear[F1,F2];
F1[mu2_] := A1LbareFull - mu2^-eps*(Z1full[mu2]+beta[0]/eps)*A0L;

F2[mu2_] := A2LbareFull - mu2^(-eps)*(Z1full[mu2]+ 2 beta[0]/eps)*A1LbareFull + 
  mu2^(-2 eps) (Z1full[mu2]^2-Z2full[mu2]+beta[0]^2/eps^2+(Z1full[mu2]*beta[0])/eps-beta[1]/(2 eps)) A0L;*)


(*epsexp = {A1LbareFull -> Sum[A1Lbare[k]*eps^k,{k,-2,2}],
  A2LbareFull -> Sum[A2Lbare[k]*eps^k,{k,-4,0}],
  Z1full[mu2_] :> Z1$[-2][1]/eps^2 + Z1$[-1][mu2]/eps ,
  Z2full[mu2_] :> Z2$[-4][1]/eps^4 + Z2$[-3][mu2]/eps^3 +
    Z2$[-2][mu2]/eps^2 + Z2$[-1][mu2]/eps
     };*)


(*A1rules=Flatten[Solve[CoefficientList[eps^2*Normal[Series[F1[1]/.epsexp,{eps,0,-1}]],eps]==0,
  {A1Lbare[-2],A1Lbare[-1]}]];
  
Z1rules=Flatten[Solve[Normal[Series[F1[mu2]/.epsexp/.A1rules,{eps,0,-1}]]==0, Z1$[-1][mu2]]];

toF1=Flatten[Solve[F1$[1]==Normal[Series[F1[1]/.epsexp/.A1rules/.Z1rules,{eps,0,0}]],A1Lbare[0]]]

Join[A1rules,Z1rules]//TableForm*)


(*A2rules=Flatten[Solve[CoefficientList[eps^4*Normal[Series[F2[1]/.epsexp/.A1rules/.toF1/.Z1rules,{eps,0,-1}]],eps]==0,
  {A2Lbare[-4],A2Lbare[-3],A2Lbare[-2],A2Lbare[-1]}]];*)


(*A2rules[[All,2]]//Variables*)


(*zero2=Normal[Series[F2[mu2]/.epsexp/.A1rules/.toF1/.A2rules/.Z1rules,{eps,0,-1}]];

Z2rules=Flatten[Solve[CoefficientList[zero2*eps^3,eps]==0, 
  {Z2$[-3][mu2],Z2$[-2][mu2],Z2$[-1][mu2]}]];*)


(*(* check Z2rules - watchout for matrix products! *)
Do[ check=Simplify[Total[List@@Expand[zz/.Rule[a_,b_]:>a-b] /. 
  {Z1$[k_][1]:>Coefficient[Z1,eps,k],
  Z1$[k_][1]^2:>Coefficient[Z1,eps,k].Coefficient[Z1,eps,k],
  Z1$[k_][1]*Z1$[h_][1]:>Coefficient[Z1,eps,k].Coefficient[Z1,eps,h],
  Z2$[k_][1]:>Coefficient[Z2,eps,k],
  Z2$[k_][mu2]:>Coefficient[Z2mu,eps,k]}]//.ExpectedValues];
  Print[Union[Flatten[check]]==={0}];
  Clear[check];,
  {zz,Z2rules}]*)


(*tmpmuRestore2L=Normal[Series[F2[mu2]-F2[1]/.epsexp/.A2rules/.A1rules/.toF1/.Z1rules/.Z2rules,{eps,0,0}]]*)


(*Collect[tmpmuRestore2L,A0L|F1$[1],Collect[#,Log[mu2],Simplify]&]*)


(* ::Subsection:: *)
(*mu-restoring terms*)


muRestore["1L"] = fullamp0Lsymb . (1/2 Log[mu2]^2 Coefficient[Z1,eps,-2]+
  Log[mu2] (beta[0]*id+Coefficient[Z1,eps,-1]) ) //. ExpectedValues;


muRestore["2L"] = fullF1Lsymb . (Log[mu2]^2*Coefficient[Z1,eps,-2]/2+Log[mu2]*(2*beta[0]*id+Coefficient[Z1,eps,-1])) +
  fullamp0Lsymb . (Log[mu2]^4*(-(5/24)*Coefficient[Z1,eps,-2] . Coefficient[Z1,eps,-2]+2/3*Coefficient[Z2,eps,-4])+
    1/6*Log[mu2]^3*(10*beta[0]*Coefficient[Z1,eps,-2]-5*Coefficient[Z1,eps,-2] . Coefficient[Z1,eps,-1]+8*Coefficient[Z2,eps,-3]) +
    Log[mu2]^2 *(beta[0]^2*id+5/2*beta[0]*Coefficient[Z1,eps,-1]-1/2*Coefficient[Z1,eps,-1] . Coefficient[Z1,eps,-1]+2*Coefficient[Z2,eps,-2])+
    Log[mu2] *(beta[1]*id+2*Coefficient[Z2,eps,-1])
  ) //. ExpectedValues;
  
muRestore["2L"] = Collect[muRestore["2L"],_F1L|_A0L,Collect[#,Log[mu2],Simplify]&];


(* ::Title:: *)
(*mu dependence*)


(* ::Section:: *)
(*symbolic*)


dictColourFactors = Association[{"T2341"->1,"T2431"->2,"d12d34"->3}];
dictNcFactors = Association[Table[StringReplace["Ncp"<>ToString[i],"-"->"m"]->i,{i,-2,2}]];

dictNf2qk["OGAqkm4qkm2Qqk"] = Association[{"nfp0"->"","nfp1"->"qk","nfp2"->"qkp2"}];
dictNf2qk["Oqkm4qkm2Qqk"] = Association[{"nfp0"->"qkGA","nfp1"->"qkqkGA"}];
dictNf2int = Association[{"nfp0"->0,"nfp1"->1,"nfp2"->2}];

dictClosedLoopFactor2nf = Association[{"qkp2_OGAqkm4qkm2Qqk"->2,
  "qk_OGAqkm4qkm2Qqk"->1,
  "_OGAqkm4qkm2Qqk"->0,
  "qkGA_Oqkm4qkm2Qqk"->0,
  "qkqkGA_Oqkm4qkm2Qqk"->1}];


neededMuDep=Coefficient[Coefficient[muRestore[looporder][[dictColourFactors[colourfactor]]],nf,dictClosedLoopFactor2nf[closedloopfactor]],Nc,dictNcFactors[ncfactor]];


neededamps0L=Union[Cases[neededMuDep,A0L[__],Infinity]];
neededfin1L=Union[Cases[neededMuDep,F1L[__],Infinity]];
Print["needed ingredients: ", Join[neededamps0L,neededfin1L]];


missedpieces=Complement[DeleteCases[Variables[neededMuDep],eps|_F|_tci|_tcr|Log[mu2]],Join[neededamps0L,neededfin1L]];
If[missedpieces=!={}, Print["careful! something is left unfixed and may cause trouble: ", missedpieces]];


(* ::Section:: *)
(*normalisations*)


(* absolute normalisation of the amplitudes *)
Normalisation0L = 1/8;
Normalisation1L = -1/16;
Normalisation2L = 1/32;


(* absolute normalisation of the colour factors *)
Normalisation["T2341"]=2;
Normalisation["T2431"]=2;
Normalisation["d12d34"]=1;


(* ::Section:: *)
(*load lower-loop ingredients*)


(* ::Subsection:: *)
(*tree level*)


Do[
  file = "DiagramNumerators_0L_2q2gA_"<>col<>"__"<>closedloopfactortype<>"_Ncp0_dsm2p0.m";
  If[!FileExistsQ[PathToAmps0L<>file],
    Print[file, " not found - setting to 0"];
    amp0L[col]=0;,
    
    Print["loading ", PathToAmps0L<>file];
    Get[PathToAmps0L<>file];
    amp0L[col]=Normalisation[col]*Normalisation0L*DiagramNumerator[StringReplace[helicity,"m"->"-"],topo[]]/.INT[nn_,__]:>nn;
    Clear[DiagramNumerator,file];
  ];
,{col, Cases[neededamps0L,A0L[cc_]:>cc]}];


ex1factor=Which[psmode==="PSanalyticX1",
  ex[1]^2,
  psmode==="PSanalytic",
  1]; (* hard-coded... CAREFUL! *)
  
Print["note: restoring ex[1] dependence (hard-coded)"];


(* ::Subsection:: *)
(*one loop - only needed for 2-loop finite remainder*)


If[looporder==="2L",

  coeffrules1L = {};

  Do[
    {col,ncf,nff}=List@@a;
  
    file="fin_1L_"<>process<>"_"<>col<>"_"<>dictNf2qk[closedloopfactortype][nff]<>"_"<>
      closedloopfactortype<>"_"<>ncf<>"_"<>helicity<>"_"<>psmode<>".m";

    If[!FileExistsQ[PathToFin<>file],
      Print[file, " not found - setting to 0"];
      tmpcoeffrules={};
      fin1Ldata[col,ncf,nff]={{0},{1}};,
    
      Print[file, " found"];
      {tmpcoeffrules,fin1Ldata[col,ncf,nff]}=Get[PathToFin<>file]/.f[i_]:>f1L[col,ncf,nff,i];
      ];
  
    coeffrules1L=Join[coeffrules1L,tmpcoeffrules];

    fin1L[col,ncf,nff] = ex1factor*Dot@@fin1Ldata[col,ncf,nff];

    Clear[file,col,ncf,nff,tmpcoeffrules,findata1L];
  ,{a,neededfin1L}];
];


(* ::Section:: *)
(*check scaling*)


(* ::Subsection:: *)
(*benchmark values*)


{X,pfuncs}=Get["pfuncseval_benchmarkpoint_B1.m"];
{X10,pfuncs10}=Get["pfuncseval_benchmarkpoint_B1_scaled10.m"];


sijs={s[1,2],s[2,3],s[3,4],s[4,5],s[1,5]};


tr5expl=GetMomentumTwistorExpression[spB[1,2]spA[2,3]spB[3,4]spA[4,1]-spA[1,2]spB[2,3]spA[3,4]spB[4,1],ToExpression[psmode]];


XMT=Solve[GetMomentumTwistorExpression[sijs,PSanalytic]==X];
XMT=Select[XMT,Im[tr5expl/.#]>0&][[1]]; (* select MT such that tr5 has positive imaginary part *)

XMT10=Solve[GetMomentumTwistorExpression[sijs,PSanalytic]==X10];
XMT10=Select[XMT10,Im[tr5expl/.#]>0&][[1]];


$MaxExtraPrecision=300;


(* ::Subsection:: *)
(*check*)


finname = StringJoin["fin_",looporder,"_",process,"_",colourfactor,"_",closedloopfactor,"_",
  ncfactor,"_",helicity,"_",psmode,".m"];
Print["loading ", finname];
fin=Get[PathToFin<>finname];

mudepfile=StringReplace[finname,{"fin"->"mudep","_PSanalyticX1"->""}];


If[Union[fin[[2,1]]/.eps->0]==={0},
  Print["finite remainder is zero at O(eps^0)"];
  Print["writing ", mudepfile];
  Put[0,mudepfile];
  Quit[];
  ];


finval=N[((ex1factor/.XMT)*fin[[2,1]]/.eps->0/.(fin[[1]]/.XMT)) . (fin[[2,2]]/.pfuncs),20];
Print["F(1,s) = ", finval];

finval10=N[((ex1factor/.XMT10)*fin[[2,1]]/.eps->0/.(fin[[1]]/.XMT10)) . (fin[[2,2]]/.pfuncs10),20];
Print["F(1,10*s) = ", finval10];


Rval = Which[
  looporder==="1L",
  N[neededMuDep /. pfuncs /. A0L->amp0L /. XMT /. eps->0 /. mu2->1/10,20],
  looporder==="2L",
  N[neededMuDep/.A0L->amp0L/.F1L[y__]->fin1L[y]/.XMT/.eps->0/.pfuncs/.(coeffrules1L/.XMT)/.mu2->1/10,20]
  ];
  
Print["R(mu2,s) = ", Rval];


scaling=Rationalize[(finval+Rval)/finval10];
If[scaling===1/100, 
  Print["scaling check: True"],
  Print["scaling check failed"];
  Print[scaling]; Quit[]];


(* ::Section:: *)
(*saving*)


neededMuDep=Collect[neededMuDep,_A0L|_F1L,Collect[#,Log[mu2],Simplify]&];


If[closedloopfactortype === "Oqkm4qkm2Qqk",
  neededMuDep = neededMuDep /. F1L[a__]:>F1L[a,"qkGA"];
];


Print["writing ", mudepfile];
Put[neededMuDep,mudepfile];
