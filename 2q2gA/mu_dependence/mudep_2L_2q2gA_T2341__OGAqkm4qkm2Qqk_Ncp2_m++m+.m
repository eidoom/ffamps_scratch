F1L["T2341", "Ncp1", "nfp0"]*((-3*Log[mu2]^2)/2 + 
   Log[mu2]*(13/6 + F[1, 1, 2] + F[1, 1, 3] + F[1, 1, 9] - tci[1, 2])) + 
 A0L["T2341"]*((9*Log[mu2]^4)/8 + 
   (Log[mu2]^3*(5 - 18*F[1, 1, 2] - 18*F[1, 1, 3] - 18*F[1, 1, 9] + 
      18*tci[1, 2]))/12 + (Log[mu2]^2*(-307 + 12*F[1, 1, 2]^2 + 
      12*F[1, 1, 3]^2 + 8*F[1, 1, 9] + 12*F[1, 1, 9]^2 - 12*tci[1, 1]^2 + 
      8*F[1, 1, 3]*(1 + 3*F[1, 1, 9] - 3*tci[1, 2]) + 
      8*F[1, 1, 2]*(1 + 3*F[1, 1, 3] + 3*F[1, 1, 9] - 3*tci[1, 2]) - 
      8*tci[1, 2] - 24*F[1, 1, 9]*tci[1, 2] + 12*tci[1, 2]^2))/24 + 
   (Log[mu2]*(-1697 + 536*F[1, 1, 9] - 14*tci[1, 1]^2 + 
      24*F[1, 1, 9]*tci[1, 1]^2 + 8*F[1, 1, 2]*(67 + 3*tci[1, 1]^2) + 
      8*F[1, 1, 3]*(67 + 3*tci[1, 1]^2) - 536*tci[1, 2] - 
      24*tci[1, 1]^2*tci[1, 2] + 648*tcr[3, 3]))/72)
