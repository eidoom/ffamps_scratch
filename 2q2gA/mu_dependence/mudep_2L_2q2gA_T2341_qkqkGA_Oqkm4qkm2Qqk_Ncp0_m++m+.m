2*(F[1, 1, 2] - F[1, 1, 6] - F[1, 1, 7] + F[1, 1, 9])*
  F1L["d12d34", "Ncp0", "nfp1", "qkGA"]*Log[mu2] - 
 (2*F1L["T2341", "Ncp0", "nfp0", "qkGA"]*Log[mu2])/3 + 
 F1L["T2341", "Ncpm1", "nfp1", "qkGA"]*((-3*Log[mu2]^2)/2 + 
   Log[mu2]*(13/6 + F[1, 1, 2] + F[1, 1, 3] + F[1, 1, 9] - tci[1, 2])) + 
 F1L["T2341", "Ncp1", "nfp1", "qkGA"]*(Log[mu2]^2/2 + 
   Log[mu2]*(3/2 - F[1, 1, 1] + tci[1, 2]))
