#!/usr/bin/env python3

import glob
import subprocess

finlist = glob.glob('../fin/fin_*');

print(len(finlist), 'finite remainders found');

finlist = [a.split('_') for a in finlist];

inputs = [{"psmode": a[-1].split('.')[0],
  "helicity": a[-2], 
  "ncfactor": a[-3], 
  "closedloopfactor": a[-5]+'_'+a[-4], 
  "colourfactor": a[-6],
  "looporder": a[-8]} for a in finlist];


print('--------------------------------\n',flush=True);

for j in range(len(finlist)):
  print('running ', j, '/', len(finlist)-1,flush=True);
  subprocess.run(['wolfram','-script', 'RestoreMu.wl', 
    '-looporder', inputs[j]["looporder"],
    '-helicity', inputs[j]["helicity"], 
    '-ncfactor', inputs[j]["ncfactor"],
    '-closedloopfactor', inputs[j]["closedloopfactor"],
    '-colourfactor', inputs[j]["colourfactor"],
    '-psmode', inputs[j]["psmode"]]);
  print('--------------------------------\n',flush=True);

