DiagramTopologies={topo[]};

DiagramNumerator["-+3++",topo[]]=0;
DiagramNumerator["-++++",topo[]]=0;
DiagramNumerator["-+++-",topo[]]=INT[-8*ex[1]^2*ex[2],{},topo[]];
DiagramNumerator["-+-++",topo[]]=INT[(-8*ex[1]^2*ex[2]^2*ex[3])/(1+ex[3]+ex[2]*ex[3]),{},topo[]];
DiagramNumerator["-++-+",topo[]]=INT[(-8*ex[1]^2*ex[2]*(1+ex[2])*ex[3])/(1+ex[3]+ex[2]*ex[3]),{},topo[]];
