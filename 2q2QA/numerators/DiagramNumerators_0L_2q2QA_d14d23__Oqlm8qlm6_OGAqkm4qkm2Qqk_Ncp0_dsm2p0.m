DiagramTopologies = {topo[]};

DiagramNumerator["-+-+5",topo[]] = 0;
DiagramNumerator["-+-++",topo[]] = INT[(4*ex[1]^2*ex[2]^2*ex[3])/(1 + ex[3] + ex[2]*ex[3]), {}, topo[]];
DiagramNumerator["-++-+",topo[]] = INT[(-4*ex[1]^2*ex[2]^2*ex[3])/(1 + ex[3] + ex[2]*ex[3]), {}, topo[]];
DiagramNumerator["+-+-+",topo[]] = INT[(4*(1 + ex[2])^2*ex[3])/(1 + ex[3] + ex[2]*ex[3]), {}, topo[]];
DiagramNumerator["+--++",topo[]] = INT[(-4*ex[2]^2*ex[3])/(1 + ex[3] + ex[2]*ex[3]), {}, topo[]];
