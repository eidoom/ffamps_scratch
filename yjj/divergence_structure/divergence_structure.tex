\documentclass[a4paper,11pt]{extarticle}
\pdfoutput=1 

\usepackage[colorlinks=true, allcolors=blue,linkcolor=blue, urlcolor=blue,citecolor=blue]{hyperref}
\usepackage{amsmath, amssymb, amsfonts}
\usepackage[T1]{fontenc}

%%% 

\renewcommand{\d}{\mathrm{d}}
\newcommand{\dlog}{\mathrm{dlog}}
\newcommand{\im}{\mathrm{im}}
\newcommand{\eps}{\epsilon}
\newcommand{\alphas}{\alpha_{\mathrm{s}}}
\renewcommand{\i}{\mathrm{i}}

\newcommand{\SZ}[1]{\textcolor{red}{\textbf{SZ: #1}}}

%%% 


\title{Renormalisation and infrared subtraction for the two-loop $2q2g\gamma$ amplitudes}
\author{}
\date{\today}

\begin{document} 

\maketitle

\begin{abstract}
In these notes I discuss the structure of the infrared and ultraviolet poles of the $2q2g\gamma$ amplitudes up to two-loop order in QCD, keeping track of the dependence on the renormalisation scale $\mu$. I follow the notation of ref.~\cite{Becher:2009qa} (see ref.~\cite{Gardi:2009qi} for an equivalent formulation).
\end{abstract}

\section{Renormalisation}

We start from the perturbative QCD expansion of the bare amplitude,
\begin{align}
\mathcal{A}_{\text{bare}} = g_{\mathrm{s},0}^2 \sum_{\ell\ge 0} \left(\frac{\alpha_{\mathrm{s},0}}{4 \pi}\right)^{\ell} A^{(\ell)}_{\text{bare}} \,,
\end{align}
where $g_{\mathrm{s},0}$ is the bare QCD coupling, and $\alpha_{\mathrm{s},0} = g_{\mathrm{s},0}^2/(4 \pi)$. We work in dimensional regularisation, with $d=4-2\eps$. 

Note that there is a mismatch between the loop-integration measure in the Feynman rules ($\mathrm{d}^d k/(2 \pi)^d$) and the one used in the computation of the Feynman integrals ($\mathrm{e}^{\eps \gamma_{\mathrm{E}}} \mathrm{d}^d k/(\i \pi^{d/2})$). Our results for the bare amplitudes should thus be multiplied by a factor of $m_{\eps} = \i \, \mathrm{e}^{-\eps \gamma_{\mathrm{E}}} (4 \pi)^{\eps}$ per loop order. However, we adopt the $\overline{\text{MS}}$ scheme, and absorb this mismatch in the arbitrary renormalisation scale $\mu$. 


The renormalisation of the UV divergences is achieved by replacing the bare coupling with the running coupling,
\begin{align}
g_{\mathrm{s},0}^2 \longrightarrow g_{\mathrm{s}}^2(\mu) \, \mu^{2 \eps} \, Z_{\mathrm{UV}}(\mu) \,,
\end{align}
with
\begin{align}
Z_{\mathrm{UV}}(\mu) = 1 - \frac{\alphas(\mu)}{4\pi} \frac{\beta_0}{\eps} + \left(\frac{\alphas(\mu)}{4\pi}\right)^2 \left(\frac{\beta_0^2}{\eps^2} - \frac{\beta_1}{2 \eps} \right) + \mathcal{O}\left(\alphas^3\right) \,.
\end{align}
I spell out all needed constants in section~\ref{sec:constants}. 

The renormalised amplitude then is given by
\begin{align} \label{eq:ArenExp}
\mathcal{A}_{\text{ren}} = g_{\mathrm{s}}^2(\mu) \mu^{2\eps} \sum_{\ell \ge 0} \left( \frac{\alphas(\mu)}{4 \pi} \mu^{2\eps} \right)^{\ell} A^{(\ell)}_{\text{ren}} \,,
\end{align}
where
\begin{align}
& A^{(0)}_{\text{ren}} = A^{(0)}_{\text{bare}} \equiv A^{(0)} \,, \\
& A^{(1)}_{\text{ren}} = A^{(1)}_{\text{bare}} - \mu^{-2\eps} \frac{\beta_0}{\eps} A^{(0)}\,,  \\
& A^{(2)}_{\text{ren}} = A^{(2)}_{\text{bare}} - \mu^{-2\eps} \frac{ 2 \beta_0}{\eps} A^{(1)}_{\text{bare}} +
   \mu^{-4 \eps} \left( \frac{\beta_0^2}{\eps^2} -\frac{\beta_1}{2 \eps} \right) A^{(0)} \,.
\end{align}


\section{Infrared factorisation}
In order to discuss the structure of the infrared poles we need to first consider the colour structure of the amplitudes.
We study the process
\begin{align}
0 \to \bar{q}(p_1, \bar{i}_1) + q(p_2, i_2) + g(p_3, a_3) + g(p_4, a_4) + \gamma(p_5) \,,
\end{align}
where the second argument for the partons denotes the colour index. 
There are three linearly independent colour factors:
\begin{align}
\mathcal{C}_1 =  \left(T^{a_3} T^{a_4} \right)^{\bar{i_1}}_{\ i_2 } \,, 
\qquad \mathcal{C}_2 =  \left(T^{a_4} T^{a_3} \right)^{\bar{i_1}}_{\ i_2 } \,,
\qquad \mathcal{C}_3 =  \delta^{a_3}_{\ a_4} \delta^{\bar{i_1}}_{\ i_2} \,.
\end{align}
We view the amplitudes as vectors in the colour space spanned by $\mathcal{C}_1$, $\mathcal{C}_2$ and $\mathcal{C}_3$.

The infrared divergences of the renormalised amplitude factorise according to
\begin{align} \label{eq:IRfactorisation}
\mathcal{A}_{\text{ren}} = Z_{\text{IR}} \cdot \mathcal{F} \,,
\end{align}
where $Z_{\text{IR}}$ is an operator which acts on the colour space and captures all infrared poles, and $\mathcal{F}$ is an infrared and ultraviolet finite quantity called \emph{finite remainder}.
We represent $\mathcal{A}_{\text{ren}}$ and $\mathcal{F}$ as $3$-dimensional vectors, and $Z_{\text{IR}}$ as a $3\times3$ matrix.
We expand $Z_{\text{IR}}$ in $\alphas$ as
\begin{align} \label{eq:Zexp}
Z_{\text{IR}} = \mathbb{I} + \frac{\alphas(\mu)}{4 \pi} Z_{\text{IR}}^{(1)} + \left(\frac{\alphas(\mu)}{4 \pi}\right)^2 Z_{\text{IR}}^{(2)} + \mathcal{O}\left(\alphas^3\right) \,.
\end{align}
Explicit expressions for $Z_{\text{IR}}^{(1)}$ and $Z_{\text{IR}}^{(2)}$ can be read off eq.~(A.2) of ref.~\cite{Becher:2009qa}. The finite remainder $\mathcal{F}$ inherits the perturbative expansion from the amplitudes,
\begin{align} \label{eq:Fexp}
\mathcal{F} = g_{\mathrm{s}}^2(\mu) \mu^{2\eps} \sum_{\ell \ge 0} \left( \frac{\alphas(\mu)}{4 \pi} \mu^{2\eps} \right)^{\ell} F^{(\ell)} \,.
\end{align}
 Plugging eqs.~\eqref{eq:ArenExp}, \eqref{eq:Zexp} and~\eqref{eq:Fexp} into the IR factorisation formula~\eqref{eq:IRfactorisation}, expanding in $\alphas$, and solving for the terms of the finite remainder gives
 \begin{align}
 & F^{(0)} = A^{(0)} \,, \\
 & F^{(1)}(\mu) = A^{(1)}_{\text{bare}} - \mu^{-2 \eps} \left(Z^{(1)}_{\text{IR}}(\mu) + \frac{\beta_0}{\eps} \mathbb{I}  \right) \cdot A^{(0)} \,, \\
 & F^{(2)}(\mu) = A^{(2)}_{\text{bare}} - \mu^{-2 \eps} \left(Z^{(1)}_{\text{IR}}(\mu)+ \frac{2 \beta_0}{\eps} \mathbb{I} \right) \cdot A^{(1)}_{\text{bare}} \nonumber \\
 & \phantom{F^{(2)} =}  -  \mu^{-4 \eps} \left[Z^{(2)}_{\text{IR}}(\mu) - \left(Z^{(1)}_{\text{IR}}(\mu)\right)^2 - \frac{\beta_0}{\eps} Z^{(1)}_{\text{IR}}(\mu) +\frac{\beta_1}{2 \eps} \mathbb{I}-\frac{\beta_0^2}{\eps^2} \mathbb{I} \right] \cdot A^{(0)} \,,
 \end{align}
 where I spelled out the dependence on $\mu$ in view of the next section.
 
 
\section{$\mu$ dependence}
 
 We computed the finite remainders at $\mu=1$. In this section we discuss how to restore the dependence on $\mu$,
 \begin{align}
 F^{(\ell)}(\mu) =  F^{(\ell)}(1) + R^{(\ell)}(\mu) \,.
 \end{align}
Note that the bare amplitudes do not depend on $\mu$. The cancellation of their poles can therefore be carried out at $\mu=1$ without any loss of generality. The dependence on $\mu$ kicks in in the finite parts only.

 In order to write down the $\mu$-restoring function $R^{(\ell)}(\mu)$ we need to expand all ingredients in $\eps$:
 \begin{align}
 & A^{(1)}_{\text{bare}} = \sum_{k\ge-2}  A^{(1;k)}_{\text{bare}} \eps^k \,, \\
 & A^{(2)}_{\text{bare}} = \sum_{k\ge-4}  A^{(2;k)}_{\text{bare}} \eps^k \,, \\
 & Z_{\text{IR}}^{(1)}(\mu) =  \frac{Z_{\text{IR}}^{(1;-2)}}{\eps^2} + \frac{Z_{\text{IR}}^{(1;-1)}(\mu)}{\eps} \,, \\
 & Z_{\text{IR}}^{(2)}(\mu) =  \frac{Z_{\text{IR}}^{(1;-4)}}{\eps^4} + \frac{Z_{\text{IR}}^{(1;-3)}(\mu)}{\eps^3} + \frac{Z_{\text{IR}}^{(1;-2)}(\mu)}{\eps^2} + \frac{Z_{\text{IR}}^{(1;-1)}(\mu)}{\eps} \,.
 \end{align}
 Note that in this scheme $ Z_{\text{IR}}$ contains poles only. The deepest poles of $Z_{\text{IR}}^{(1)}$ and $Z_{\text{IR}}^{(2)}$ do not depend on $\mu$, and take a particularly simple form:
 \begin{align}
 & Z_{\text{IR}}^{(1;-2)}  = - \frac{C_A+C_F}{2} \gamma^{\text{cusp}}_0 \, \mathbb{I} \,, \\
 & Z_{\text{IR}}^{(2;-4)}  = \frac{(C_A+C_F)^2}{8} \left(\gamma^{\text{cusp}}_0\right)^2 \, \mathbb{I} \,,
 \end{align}
 with $ \gamma^{\text{cusp}}_0 = 4$. 
 
 We now work out explicitly the $\mu$-dependence of the one-loop finite remainder.
 The subtraction of the poles at $\mu=1$ tells us that
 \begin{align}
& A^{(1;-2)}_{\text{bare}} = Z_{\text{IR}}^{(1;-2)} \cdot A^{(0)} \,, \\
& A^{(1;-1)}_{\text{bare}} = \left(Z_{\text{IR}}^{(1;-1)}(1) + \beta_0 \mathbb{I} \right) \cdot A^{(0)} \,.
 \end{align} 
 These ensure the cancellation of the $1/\eps^2$ pole for generic $\mu$. The $1/\eps$ pole requires also the $\mu$-dependence of $Z^{(1;-1}_{\text{IR}}(\mu) $, which follows from its explicit expression:
 \begin{align}
 Z_{\text{IR}}^{(1;-1)}(\mu) =  Z_{\text{IR}}^{(1;-1)}(1) + \log\left(\mu^2\right)  Z_{\text{IR}}^{(1;-2)}(1) \,.
 \end{align}
 Subtracting $F^{(1)}(1)$ from $F^{(1)}(\mu)$ and using these relations gives the $\mu$-restoring function at one loop:
 \begin{align} \label{eq:R1}
 R^{(1)}(\mu) = \left[ \log\left(\mu^2\right) \left(\beta_0 \, \mathbb{I} + Z^{(1;-1)}_{\text{IR}}(1)\right) + 
   \frac{1}{2} \log^2\left(\mu^2\right) Z^{(1;-2)}_{\text{IR}}  \right] \cdot A^{(0)} \,.
 \end{align}
 
  We proceed similarly at two-loop order. The result is
 \begin{align} \label{eq:R2}
 & R^{(2)}(\mu) = \left[ \frac{1}{2} \log^2\left(\mu^2\right) Z^{(1;-2)}_{\text{IR}}(1) + 
    \log\left(\mu^2\right) \left(2 \beta_0 +  Z^{(1;-1)}_{\text{IR}}(1)  \right) \right] \cdot F^{(1)}(1) \nonumber \\
 &  \quad + \biggl[ 
     \log^4\left(\mu^2\right) \left(-\frac{5}{24} \left(Z^{(1;-2)}_{\text{IR}}(1)\right)^2 + \frac{2}{3} Z^{(2;-4)}_{\text{IR}}(1) \right) + \nonumber \\
  & \quad +  \frac{1}{6}  \log^3\left(\mu^2\right) \left(10 \beta_0 Z^{(1;-2)}_{\text{IR}}(1) - 5 Z^{(1;-2)}_{\text{IR}}(1) \cdot Z^{(1;-1)}_{\text{IR}}(1) + 8 Z^{(2;-3)}_{\text{IR}}(1) \right) \nonumber \\
 & \quad + \log^2\left(\mu^2\right) \left(\beta^2_0 \, \mathbb{I} + \frac{5}{2} \beta_0 Z^{(1;-1)}_{\text{IR}}(1) - \frac{1}{2} \left(Z^{(1;-1)}_{\text{IR}}(1)\right)^2 + 2 Z^{(2;-2)}_{\text{IR}}(1) \right) \nonumber \\
 & \quad +\log\left(\mu^2\right) \left(\beta_1 \, \mathbb{I} + 2 Z^{(2;-1)}_{\text{IR}}(1) \right) \biggr] \cdot A^{(0)} \,.
 \end{align}
 
 We emphasise that the dependence on $\mu$ in the $\mu$-restoring functions enters through logarithms of $\mu^2$ only. The one-loop finite remainder and the IR pole operator on the RHS of eqs.~\eqref{eq:R1} and~\eqref{eq:R2} are in fact evaluated at $\mu=1$. Furthermore, note that $R^{(\ell)}(\mu)$ contains logarithms of $\mu^2$ up to power $2 \ell$ multiplying the tree-level amplitude.
 
  
\section{Scale check}

We validate the $\mu$ dependence as follows. We denote cumulatively by $\vec{s} = (s_{12},s_{23},s_{34},s_{45},s_{51})$ the independent kinematic invariants. The finite remainder is a homogeneous function of $\mu^2$ and $\vec{s}$,
\begin{align} \label{eq:scaling}
F^{(\ell)}\left( \lambda \mu^2 , \lambda \vec{s} \right) = \lambda^k F^{(\ell)}\left(\mu^2 , \vec{s} \right)  \,,
\end{align}
for some integer $k$ which can be determined through dimensional analysis or by rescaling the tree-level amplitude. In this case I find $k=2$. 
By choosing $\lambda = 1/\mu^2$ we can turn eq.~\eqref{eq:scaling} into
\begin{align}
\frac{ F^{(\ell)}\left(1, \vec{s} \right) + R^{(\ell)}\left(\mu^2 , \vec{s} \right) }{F^{(\ell)}\left( 1 , \frac{\vec{s}}{\mu^2} \right)} = \mu^{2 k} \,,
\end{align}
which we use to validate $R^{(\ell)}$ through numerical evaluations of $F^{(\ell)}\left(1, \vec{s} \right)$ at rescaled phase-space points.

 
\section{Constants}
\label{sec:constants}

The QCD $\beta$ function is defined as
\begin{align}
\frac{\mathrm{d} \alphas(\mu)}{\mathrm{d} \mu} = \beta(\alphas) - 2 \eps \, \alphas \,.
\end{align}
We expand it in $\alphas$ as
\begin{align}
\beta = - 2 \alphas \sum_{k \ge 0} \beta_{k} \left(\frac{\alphas}{4\pi}\right)^{k+1}  \,,
\end{align}
with
\begin{align}
\begin{aligned}   
& \beta_0 = \frac{11}{3} C_A - \frac{4}{3} n_f \,, \\
& \beta_1 = \frac{2}{3}\left(17 C_A^2-10 C_A n_f-6 C_F n_f \right)\,.
\end{aligned}
\end{align}
The eigenvalues of the quadratic Casimir operators are
\begin{align}
C_A = 2 N_c \,, \qquad \qquad C_F = \frac{N_c^2-1}{N_c} \,.
\end{align}

\begin{thebibliography}{9}

\bibitem{Becher:2009qa}
T.~Becher and M.~Neubert,
``On the Structure of Infrared Singularities of Gauge-Theory Amplitudes,''
JHEP \textbf{06} (2009), 081
[erratum: JHEP \textbf{11} (2013), 024]
doi:10.1088/1126-6708/2009/06/081
[\href{https://arxiv.org/pdf/0903.1126.pdf}{arXiv:0903.1126 [hep-ph]}].

\bibitem{Gardi:2009qi}
E.~Gardi and L.~Magnea,
``Factorization constraints for soft anomalous dimensions in QCD scattering amplitudes,''
JHEP \textbf{03} (2009), 079
doi:10.1088/1126-6708/2009/03/079
[\href{https://arxiv.org/pdf/0901.1091.pdf}{arXiv:0901.1091 [hep-ph]}].

\end{thebibliography}

\end{document}

