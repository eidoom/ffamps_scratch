\documentclass[10pt, a4paper]{article}

\usepackage[colorlinks,linkcolor=blue,urlcolor=blue,citecolor=blue]{hyperref}
\usepackage{amsmath}
\usepackage[capitalize]{cleveref}

\title{Notes on the double-virtual contributions to photon-plus-dijet production at hadron colliders}
\author{}

\begin{document}

\maketitle

\section{Channels}

There are two independent channels to consider at two loops for the NNLO double-virtual corrections, which we take as:

\begin{align*}
    q\bar{q}&\to gg\gamma,\\
    \bar{q_1}q_1&\to \bar{q_2}q_2\gamma.\\
\end{align*}

We do not need to compute $gg\to gg\gamma$ at two loops as the process is loop-induced and thus first appears in the $\alpha_s$ expansion with two loops at N$^3$LO.

In the following, all momenta are taken as outgoing.

\section{Channel: $2q2g\gamma$}

\subsection{Organisation of the finite remainder}

Particle ordering:
\begin{align*}
\bar{q} q g g \gamma.
\end{align*}

Independent helicity configurations:
\begin{align*}
    -++++,\\
    -+++-,\\
    -+-++,\\
    -++-+.\\
\end{align*}

Colour factors corresponding to independent partial amplitudes:
\begin{align*}
    &\left(t^3t^4\right)_2^{\phantom{2}\bar1},\\
    &\delta_{12}\delta_{34}.\\
\end{align*}

Finite remainder decomposition:
\begin{align}
    \label{eq:frd-2q2gA}
    F^{(\ell)} = \sum_{i=-\ell}^{\ell} \sum_{j=0}^{\ell} \sum_{k=0}^{1} N_c^i N_f^j F^{(\ell)}_{i,j,k} ,
\end{align}
\noindent
where subscript $k$ denotes how many photons are attached to quark loops.

Nonzero tree-level amplitudes:
\begin{align*}
    F^{(0)}
    =& F^{(0)}_{0,0,0}.
\end{align*}

Nonzero one-loop amplitudes:
\begin{align*}
    F^{(1)}
    =& N_c F^{(1)}_{1,0,0}
    + F^{(1)}_{0,0,0}
    + \frac{1}{N_c} F^{(1)}_{-1,0,0}
    \\
    &+ N_f \left( F^{(1)}_{0,1,0} + F^{(1)}_{0,1,1} \right)
    + \frac{N_f}{N_c} F^{(1)}_{-1,1,1}
    + \mathcal{O}(\epsilon^3).
\end{align*}

Nonzero two-loop amplitudes:
\begin{align*}
    F^{(2)}
    =& N_c^2 F^{(2)}_{2,0,0}
    + N_c F^{(2)}_{1,0,0}
    + F^{(2)}_{0,0,0}
    + \frac{1}{N_c} F^{(2)}_{-1,0,0}
    + \frac{1}{N_c^2} F^{(2)}_{-2,0,0}
    \\
    &+ N_c N_f \left(F^{(2)}_{1,1,1} + F^{(2)}_{1,1,0}\right)
    + N_f \left(F^{(2)}_{0,1,1} + F^{(2)}_{0,1,0}\right)
    \\
    &+ \frac{N_f}{N_c} \left(F^{(2)}_{-1,1,1} + F^{(2)}_{-1,1,0}\right)
    + \frac{N_f}{N_c^2} \left(F^{(2)}_{-2,1,1} + F^{(2)}_{-2,1,0}\right)
    \\
    &+ N_f^2  \left(F^{(2)}_{0,2,1} + F^{(2)}_{0,2,0}\right)
    + \frac{N_f^2}{N_c} F^{(2)}_{-1,2,1}
    + \mathcal{O}(\epsilon).
\end{align*}

\subsection{Polynomial degrees}
\label{sec:degrees-2q2gA}

In previous work \cite{Badger:2021imn}, it was found that directly reconstructing the finite remainder decreased the reconstruction time as the finite remainder had lower degrees than the full amplitude.
However, for photon-plus-dijet production, we find that the drop in degree from amplitude to finite remainder is insignificant with respect to reconstruction times.
Therefore, we reconstruct the full amplitude and subsequently analytically compute the finite remainders.

The amplitude is decomposed in the same way as the finite remainder, shown in \cref{eq:frd-2q2gA}.
We reconstruct the coefficients of the $(d_s-2)$ expansion:
\begin{align*}
    A^{(\ell)}_{i,j,k} = \sum_{a=0}^{\ell-j} A^{(\ell)}_{i,j,k;a} (d_s-2)^a ,
\end{align*}
\noindent
where $d_s=4-2\epsilon$.

Information on the degrees at each stage of the reconstruction for the most complicated reconstructions is shown in \cref{tab:degrees-2q2gA}.

\begin{table}
    \centering
    \begin{tabular}{llllll}
        \hline
        amplitude & original & stage 1 & stage 2 & stage 3 & stage 4 \\
        \hline
        $A^{(2)}_{          -2,0,0;1}(-+-++)$ & 113/109 & 106/102 & 106/0 & 24/16 & 23/8 \\
        $A^{(2)}_{\phantom{-}0,0,0;1}(-+-++)$ & 109/105 & 102/98 & 102/0 & 23/16 & 23/8 \\
        $A^{(2)}_{          -1,1,1;0}(-+-++)$ & 106/99 & 89/82 & 88/0 & 23/14 & 22/8 \\
        $A^{(2)}_{          -2,0,0;0}(-+-++)$ & 99/95 & 90/86 & 88/0 & 21/16 & 19/6 \\
        $A^{(2)}_{\phantom{-}0,0,0;1}(-++-+)$ & 98/95 & 73/70 & 72/0 & 22/18 & 21/8 \\
        $A^{(2)}_{\phantom{-}0,0,0;0}(-+-++)$ & 98/94 & 85/81 & 83/0 & 20/15 & 18/6 \\
        \hline
        $A^{(2)}_{\phantom{-}2,0,0;1}(-+-++)$ & 63/59 & 57/52 & 55/0 & 21/12 & 19/3 \\
        \hline
    \end{tabular}
    \caption{
        Maximal numerator/denominator polynomial degrees of the rational coefficients for the six most complicated amplitudes ordered by the maximal numerator degree prior to any optimisation.
        The most complicated leading colour contribution is also included in the last row of the table for comparison.
        Degrees are shown for each stage of the reconstruction as in Table 1 of Ref.~\cite{Badger:2021imn}.
    }
    \label{tab:degrees-2q2gA}
\end{table}

\subsection{IBP relations}

Syzygies improvement: $\times6$ speedup in reconstruction (approximate single point evaluation time).

\section{Channel: $2q_12q_2\gamma$}

\subsection{Organisation of the finite remainder}

Particle ordering:
\begin{align*}
    q_1 \bar{q_1} q_2 \bar{q_2} \gamma .
\end{align*}

Independent helicity configurations:
\begin{align*}
    +-+-+,\\
    +--++,\\
    -++-+,\\
    -+-++.\\
\end{align*}

Colour factors corresponding to independent partial amplitudes:
\begin{align*}
    \delta_{12}\delta_{34},\\
    \delta_{14}\delta_{23}.\\
\end{align*}

Finite remainder decomposition:
\begin{align*}
    F^{(\ell)} = \sum_{i=-\ell-1}^{\ell} \sum_{j=0}^{\ell} \sum_{k=0}^{1} N_c^i N_f^j F^{(\ell)}_{i,j,k} ,
\end{align*}
\noindent
where subscript $k$ denotes how many photons are attached to the quark loop.

Nonzero tree-level amplitudes:
\begin{align*}
    F^{(0)}
    =& F^{(0)}_{0,0,0}
    + \frac{1}{N_c} F^{(0)}_{-1,0,0}.
\end{align*}

Nonzero one-loop amplitudes:
\begin{align*}
    F^{(1)}
    =& N_c F^{(1)}_{1,0,0}
    + F^{(1)}_{0,0,0}
    + \frac{1}{N_c} F^{(1)}_{-1,0,0}
    + \frac{1}{N_c^2} F^{(1)}_{-2,0,0}
    \\
    &+ N_f F^{(1)}_{0,1,0}
    + \frac{N_f}{N_c} F^{(1)}_{-1,1,0}
    + \mathcal{O}(\epsilon^3).
\end{align*}

Nonzero two-loop amplitudes:
\begin{align*}
    F^{(2)}
    =& N_c^2 F^{(2)}_{2,0,0}
    + N_c F^{(2)}_{1,0,0}
    + F^{(2)}_{0,0,0}
    + \frac{1}{N_c} F^{(2)}_{-1,0,0}
    + \frac{1}{N_c^2} F^{(2)}_{-2,0,0}
    + \frac{1}{N_c^3} F^{(2)}_{-3,0,0}
    \\
    &+ N_c N_f \left(F^{(2)}_{1,1,1} + F^{(2)}_{1,1,0}\right)
    + N_f \left(F^{(2)}_{0,1,1} + F^{(2)}_{0,1,0}\right)
    \\
    &+ \frac{N_f}{N_c} \left(F^{(2)}_{-1,1,1} + F^{(2)}_{-1,1,0}\right)
    + \frac{N_f}{N_c^2} \left(F^{(2)}_{-2,1,1} + F^{(2)}_{-2,1,0}\right)
    \\
    &+ N_f^2  F^{(2)}_{0,2,0}
    + \frac{N_f^2}{N_c} F^{(2)}_{-1,2,0}
    + \mathcal{O}(\epsilon).
\end{align*}

\subsection{Polynomial degrees}

Similarly as for $2q2g\gamma$ in \cref{sec:degrees-2q2gA}, information on the degrees for $2q_12q_2\gamma$ is shown in \cref{tab:degrees-2q2QA}.

\begin{table}
    \centering
    \begin{tabular}{llllll}
        \hline
        amplitude & original & stage 1 & stage 2 & stage 3 & stage 4 \\
        \hline
        $A^{(2)}_{-2,0,0;1}(-+-++)$ & 94/91 & 63/60 & 63/0 & 22/13 & 17/0 \\
        $A^{(2)}_{-3,0,0;1}(-+-++)$ & 94/91 & 63/60 & 63/0 & 22/13 & 17/0 \\
        $A^{(2)}_{-2,0,0;1}(-++-+)$ & 93/90 & 63/60 & 63/0 & 22/14 & 17/0 \\
        $A^{(2)}_{-3,0,0;1}(-++-+)$ & 93/90 & 63/60 & 63/0 & 22/14 & 17/0 \\
        $A^{(2)}_{-1,0,0;1}(-+-++)$ & 86/83 & 53/50 & 53/0 & 19/13 & 17/0 \\
        $A^{(2)}_{-1,0,0;1}(-++-+)$ & 85/82 & 52/49 & 52/0 & 18/14 & 17/0 \\
        \hline
        $A^{(2)}_{\phantom{-}2,0,0;1}(-++-+)$ & 54/51 & 42/39 & 40/0 & 19/13 & 17/0 \\
        \hline
    \end{tabular}
    \caption{
        As in \cref{tab:degrees-2q2gA} but for $2q_12q_2\gamma$.
    }
    \label{tab:degrees-2q2QA}
\end{table}

\bibliographystyle{JHEP}
\bibliography{notes_yjj}

\end{document}
