{{-2*Log[s12] - 2*Log[s23] + 2*Log[s12 + s23 - s4], 
  2*Log[s12 - s4] - 2*Log[s12 + s23 - s4], 2*Log[s12 - s4] + 
   4*Log[s12 + s23 - s4] - 6*Log[s4], -Log[s12 + s23 - s4] + Log[s4], 
  -2*Log[s12 - s4] - 7*Log[s12 + s23 - s4] + 9*Log[s4], 
  -2*Log[s23] + (2*Log[s12 - s4])/3 + (16*Log[s12 + s23 - s4])/3 - 4*Log[s4], 
  6*Log[s12 - s4] + 18*Log[s12 + s23 - s4] - 24*Log[s4], 
  -6*Log[s12 + s23 - s4] + 6*Log[s4], 2*Log[s23 - s4] - 
   6*Log[s12 + s23 - s4] + 4*Log[s4], -4*Log[s12 - s4] - 
   2*Log[s12 + s23 - s4] + 6*Log[s4], -3*Log[s12 + s23 - s4] + 3*Log[s4], 
  -2*Log[s12 - s4] - 10*Log[s12 + s23 - s4] + 12*Log[s4], 
  16*Log[s12 - s4] + 47*Log[s12 + s23 - s4] - 63*Log[s4], 
  -2*Log[s12 - s4] + 2*Log[s12 + s23 - s4], -2*Log[s12 - s4] - 
   10*Log[s12 + s23 - s4] + 12*Log[s4], 2*Log[s12 - s4] - 
   2*Log[s12 + s23 - s4], -6*Log[s12 - s4] - 12*Log[s12 + s23 - s4] + 
   18*Log[s4], 3*Log[s12 - s4] + 6*Log[s12 + s23 - s4] - 9*Log[s4]}, 
 {-Log[s23] + Log[s12 + s23 - s4], -2*Log[s12] + Log[s23] - 
   Log[s12 + s23 - s4], 4*Log[s12] - 7*Log[s23] + 2*Log[s12 - s4] + 
   5*Log[s12 + s23 - s4] - 4*Log[s4], -Log[s12] + Log[s23]/2 - 
   Log[s12 + s23 - s4]/2 + Log[s4], -7*Log[s12] + (19*Log[s23])/2 + 
   2*Log[s12 - s4] - (19*Log[s12 + s23 - s4])/2 + 5*Log[s4], 
  (10*Log[s12])/3 - (17*Log[s23])/3 - (2*Log[s12 - s4])/3 + 
   (17*Log[s12 + s23 - s4])/3 - (8*Log[s4])/3, 19*Log[s12] - 21*Log[s23] - 
   3*Log[s12 - s4] + 21*Log[s12 + s23 - s4] - 16*Log[s4], 
  -3*Log[s12] + 6*Log[s23] - 6*Log[s12 + s23 - s4] + 3*Log[s4], 
  -2*Log[s12] + 6*Log[s23] - 6*Log[s12 + s23 - s4] + 2*Log[s4], 
  -5*Log[s12] + 7*Log[s23] + Log[s12 - s4] - 7*Log[s12 + s23 - s4] + 
   4*Log[s4], -3*Log[s12] - (3*Log[s23])/2 + (3*Log[s12 + s23 - s4])/2 + 
   3*Log[s4], -10*Log[s12] + 8*Log[s23] + 2*Log[s12 - s4] - 
   8*Log[s12 + s23 - s4] + 8*Log[s4], 50*Log[s12] - (113*Log[s23])/2 - 
   7*Log[s12 - s4] + (113*Log[s12 + s23 - s4])/2 - 43*Log[s4], 
  -Log[s12] - Log[s23] + 2*Log[s12 - s4] + Log[s12 + s23 - s4] - Log[s4], 
  -10*Log[s12] + 14*Log[s23] + 2*Log[s12 - s4] - 14*Log[s12 + s23 - s4] + 
   8*Log[s4], Log[s12] + Log[s23] - 2*Log[s12 - s4] - Log[s12 + s23 - s4] + 
   Log[s4], -15*Log[s12] + 15*Log[s23] + 3*Log[s12 - s4] - 
   15*Log[s12 + s23 - s4] + 12*Log[s4], (15*Log[s12])/2 - (15*Log[s23])/2 - 
   (3*Log[s12 - s4])/2 + (15*Log[s12 + s23 - s4])/2 - 6*Log[s4]}, 
 {0, 0, -Log[s12] - Log[s4], 0, 0, 0, 4*Log[s12] - 4*Log[s4], 0, 0, 0, 0, 0, 
  12*Log[s12] - 12*Log[s4], 0, 0, 0, -3*Log[s12] + 3*Log[s4], 0}, 
 {0, 0, 0, -2*Log[s12] - 2*Log[s23] + (3*Log[s12 + s23 - s4])/2 + Log[s4]/2, 
  3*Log[s12 - s4] - (3*Log[s12 + s23 - s4])/2 - (3*Log[s4])/2, 0, 0, 0, 0, 0, 
  (3*Log[s12 + s23 - s4])/2 - (3*Log[s4])/2, 0, 
  (3*Log[s12 + s23 - s4])/2 - (3*Log[s4])/2, 0, 
  3*Log[s12 - s4] - 3*Log[s12 + s23 - s4], 0, 0, 0}, 
 {0, 0, 0, -1/2*Log[s12 + s23 - s4] + Log[s4]/2, 
  2*Log[s12] - 2*Log[s23] - Log[s12 - s4] + Log[s12 + s23 - s4]/2 - 
   (3*Log[s4])/2, (-4*Log[s12])/3 + (2*Log[s12 - s4])/3 + 
   (2*Log[s12 + s23 - s4])/3, -4*Log[s12] + 4*Log[s12 - s4], 0, 0, 0, 
  2*Log[s12] - Log[s12 + s23 - s4]/2 - (3*Log[s4])/2, 
  4*Log[s12] - 2*Log[s12 - s4] - 2*Log[s12 + s23 - s4], 
  -8*Log[s12] - 2*Log[s23] + 10*Log[s12 - s4] + (3*Log[s12 + s23 - s4])/2 - 
   (3*Log[s4])/2, 0, 4*Log[s12] - 4*Log[s12 + s23] - Log[s12 - s4] + 
   Log[s12 + s23 - s4], 2*Log[s12 - s4] - 2*Log[s12 + s23 - s4], 
  4*Log[s12] - 4*Log[s12 - s4], -2*Log[s12] + 2*Log[s12 - s4]}, 
 {0, 0, 0, 0, 0, -2*Log[s12] - 2*Log[s23] + Log[s12 - s4] + 
   Log[s12 + s23 - s4], 0, 0, 0, 0, 3*Log[s23 - s4] - 3*Log[s4], 
  -3*Log[s12 - s4] + 3*Log[s12 + s23 - s4], -3*Log[s12 - s4] + 
   3*Log[s12 + s23 - s4], 0, 0, 3*Log[s12 - s4] + 3*Log[s23 - s4] - 
   3*Log[s12 + s23 - s4] - 3*Log[s4], 0, (-3*Log[s23 - s4])/2 + 
   (3*Log[s4])/2}, {0, 0, 0, 0, 0, 0, -2*Log[s12], 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0}, {0, 0, -Log[s12] + Log[s4], 0, 2*Log[s12] - 2*Log[s4], 
  (-2*Log[s12])/3 + (2*Log[s4])/3, -4*Log[s12] + 4*Log[s4], 
  -Log[s12] + Log[s23] + Log[s12 + s23 - s4] - 3*Log[s4], 
  2*Log[s23 - s4] - 2*Log[s4], 2*Log[s12] - 2*Log[s4], 
  -Log[s23 - s4] + Log[s4], 2*Log[s12] - 2*Log[s4], 
  -10*Log[s12] + 10*Log[s4], Log[s12] + Log[s23] - Log[s12 + s23 - s4] - 
   Log[s4], 4*Log[s12] - 2*Log[s12 + s23] - 2*Log[s4], 
  -Log[s12] - Log[s23] + 3*Log[s23 - s4] - Log[s12 + s23 - s4], 
  3*Log[s12] - 3*Log[s4], -2*Log[s12] + Log[s23 - s4]/2 + (3*Log[s4])/2}, 
 {0, 0, Log[s12] + 2*Log[s23] - Log[s12 + s23 - s4] - 2*Log[s4], 
  Log[s12]/2 - Log[s12 + s23 - s4]/2, (-5*Log[s12])/2 - 4*Log[s23] + 
   (5*Log[s12 + s23 - s4])/2 + 4*Log[s4], Log[s12]/3 + (4*Log[s23])/3 - 
   Log[s12 + s23 - s4]/3 - (4*Log[s4])/3, 4*Log[s12] + 8*Log[s23] - 
   4*Log[s12 + s23 - s4] - 8*Log[s4], -3*Log[s23] + 3*Log[s4], 
  -Log[s12] - 2*Log[s23] - 2*Log[s23 - s4] + Log[s12 + s23 - s4] + 2*Log[s4], 
  -2*Log[s12] - 2*Log[s23] + 2*Log[s12 + s23 - s4] + 2*Log[s4], 
  (3*Log[s12])/2 - (3*Log[s12 + s23 - s4])/2, -Log[s12] - 4*Log[s23] + 
   Log[s12 + s23 - s4] + 4*Log[s4], (19*Log[s12])/2 + 20*Log[s23] - 
   (19*Log[s12 + s23 - s4])/2 - 20*Log[s4], -Log[s12] - Log[s23] + 
   Log[s12 + s23 - s4] + Log[s4], -4*Log[s12] - 4*Log[s23] + 
   4*Log[s12 + s23 - s4] + 4*Log[s4], Log[s12] + Log[s23] - 
   Log[s12 + s23 - s4] - Log[s4], -3*Log[s12] - 6*Log[s23] + 
   3*Log[s12 + s23 - s4] + 6*Log[s4], (3*Log[s12])/2 + 3*Log[s23] - 
   (3*Log[s12 + s23 - s4])/2 - 3*Log[s4]}, 
 {0, 0, 0, 0, 0, 0, Log[s12] - Log[s12 - s4], 0, 0, 
  -Log[s12] - Log[s12 - s4], 0, 0, 3*Log[s12] - 3*Log[s12 - s4], 0, 0, 0, 
  -Log[s12] + Log[s12 - s4], Log[s12]/2 - Log[s12 - s4]/2}, 
 {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2*Log[s23], 0, 0, 0, 0, 0, 0, 0}, 
 {0, 0, 0, 0, 0, 0, 2*Log[s23] - 2*Log[s12 - s4], 0, 0, 0, 
  -2*Log[s12] + 2*Log[s23 - s4], -2*Log[s12] - 2*Log[s23] + 
   2*Log[s12 + s23 - s4], 6*Log[s23] - 6*Log[s12 - s4], 0, 0, 0, 
  -2*Log[s23] + 2*Log[s12 - s4], Log[s12] + Log[s23] - Log[s12 - s4] - 
   Log[s23 - s4]}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2*Log[s12], 0, 0, 0, 
  0, 0}, {0, 0, Log[s12] - Log[s4], 0, -2*Log[s12] + 2*Log[s4], 
  (2*Log[s12])/3 - (2*Log[s4])/3, 4*Log[s12] - 4*Log[s4], 0, 0, 
  -2*Log[s12] + 2*Log[s4], 0, -2*Log[s12] + 2*Log[s4], 
  10*Log[s12] - 10*Log[s4], -2*Log[s12] + 2*Log[s12 - s4] - 2*Log[s4], 
  -2*Log[s12] + 2*Log[s4], 2*Log[s12] - 2*Log[s4], -3*Log[s12] + 3*Log[s4], 
  2*Log[s12] - 2*Log[s4]}, {0, 0, 0, Log[s12 + s23 - s4]/2 - Log[s4]/2, 
  -2*Log[s12] + 2*Log[s23] - Log[s12 + s23 - s4]/2 + Log[s4]/2, 
  (2*Log[s12])/3 - (2*Log[s23])/3 - Log[s12 + s23 - s4]/3 + Log[s4]/3, 
  2*Log[s12] - 2*Log[s23], 0, 0, 0, Log[s12 + s23 - s4]/2 - Log[s4]/2, 
  -2*Log[s12] + 2*Log[s23] + Log[s12 + s23 - s4] - Log[s4], 
  4*Log[s12] - 4*Log[s23] - Log[s12 + s23 - s4]/2 + Log[s4]/2, 0, 
  -4*Log[s12] + 4*Log[s12 + s23] - Log[s12 + s23 - s4] - Log[s4], 
  Log[s12 + s23 - s4] - Log[s4], -2*Log[s12] + 2*Log[s23], 
  Log[s12] - Log[s23]}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  Log[s23 - s4] - Log[s4], 0, 0, 0, 0, -Log[s23 - s4] - Log[s4], 0, 
  -1/2*Log[s23 - s4] + Log[s4]/2}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, -2*Log[s12], 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  -2*Log[s4]}}
