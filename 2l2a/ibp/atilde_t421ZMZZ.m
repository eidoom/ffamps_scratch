{{2*Log[s12] - 2*Log[s23] - 2*Log[s12 + s23 - s4], 0, 
  6*Log[s12] - 6*Log[s23], -2*Log[s12] + 2*Log[s4], 
  (-2*Log[s12])/3 + Log[s23 - s4] - Log[s4]/3, 2*Log[s12] - 3*Log[s23 - s4] + 
   Log[s4], 2*Log[s12] - 3*Log[s23 - s4] + Log[s4], 
  2*Log[s12] - 2*Log[s12 + s23 - s4], 4*Log[s23] - 6*Log[s23 - s4] + 
   2*Log[s12 + s23 - s4], 2*Log[s12 - s4] - 3*Log[s23 - s4] + Log[s4]}, 
 {0, Log[s12] - 2*Log[s23] + Log[s12 + s23] - 2*Log[s12 + s23 - s4], 
  2*Log[s12] - 2*Log[s23], 2*Log[s23] - 4*Log[s12 + s23] + 2*Log[s4], 
  (2*Log[s23])/3 - (4*Log[s12 + s23])/3 + (2*Log[s12 + s23 - s4])/3, 
  -2*Log[s23] + 4*Log[s12 + s23] - 2*Log[s12 + s23 - s4], 
  -2*Log[s23] + 4*Log[s12 + s23] - 2*Log[s12 + s23 - s4], 
  -2*Log[s23] + 4*Log[s12 + s23] - 2*Log[s4], 
  -2*Log[s12] + 4*Log[s12 + s23] - 2*Log[s4], 
  -2*Log[s23] + 4*Log[s12 + s23] - 2*Log[s12 + s23 - s4]}, 
 {0, 0, -2*Log[s12 + s23 - s4], 0, 0, 0, 0, 0, 0, 0}, 
 {0, 0, 0, -2*Log[s12 + s23 - s4], 0, 0, 0, 0, 0, 0}, 
 {0, 0, 0, 6*Log[s23] - 6*Log[s12 + s23], Log[s12] - 2*Log[s12 + s23] - 
   Log[s23 - s4], -6*Log[s23] + 6*Log[s12 + s23] + 6*Log[s23 - s4] - 
   6*Log[s12 + s23 - s4], 3*Log[s12] - 6*Log[s23] + 6*Log[s12 + s23] + 
   3*Log[s23 - s4] - 6*Log[s12 + s23 - s4], -6*Log[s23] + 6*Log[s12 + s23], 
  -6*Log[s23] + 6*Log[s12 + s23] + 6*Log[s23 - s4] - 6*Log[s12 + s23 - s4], 
  -6*Log[s23] + 6*Log[s12 + s23] + 6*Log[s23 - s4] - 6*Log[s12 + s23 - s4]}, 
 {-Log[s12] + Log[s4], 0, -3*Log[s12] + 3*Log[s4], 
  Log[s12] + 2*Log[s23] - 3*Log[s4], (2*Log[s12])/3 - (2*Log[s23 - s4])/3, 
  -Log[s12] - 2*Log[s23] + 3*Log[s23 - s4] - 2*Log[s12 + s23 - s4], 
  2*Log[s23 - s4] - 2*Log[s12 + s23 - s4], -Log[s12] + Log[s4], 
  -4*Log[s23] + 4*Log[s23 - s4], -4*Log[s12 - s4] + 3*Log[s23 - s4] + 
   Log[s4]}, {0, 0, 0, 0, 0, 0, -2*Log[s23], 0, 0, 0}, 
 {0, 0, 0, 0, 0, 0, 0, -2*Log[s23], 0, 0}, 
 {0, 0, 0, -Log[s12 + s23] + Log[s4], -1/3*Log[s12 + s23] + Log[s23 - s4]/3, 
  Log[s12 + s23] - Log[s23 - s4], Log[s12 + s23] - Log[s23 - s4], 
  Log[s12 + s23] - Log[s4], Log[s12 + s23] - 2*Log[s23 - s4] - Log[s4], 
  Log[s12 + s23] - Log[s23 - s4]}, {Log[s12] - Log[s4], 0, 
  3*Log[s12] - 3*Log[s4], -Log[s12] + Log[s4], -1/3*Log[s12] + Log[s4]/3, 
  Log[s12] - Log[s4], Log[s12] - Log[s4], Log[s12] - Log[s4], 
  2*Log[s23] - 2*Log[s12 + s23 - s4], -2*Log[s23] + 4*Log[s12 - s4] - 
   2*Log[s12 + s23 - s4] - 2*Log[s4]}}
