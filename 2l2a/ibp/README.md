# 2l2a/ibp

| Family     | Canonical permutation | Planar | Independent |
| ---------- | --------------------- | ------ | ----------- |
| `t331ZZZM` | 1234                  | Y      | Y           |
| `t421ZZZM` | 1234                  | Y      | Y           |
| `t421ZZMZ` | 1243                  | Y      | N           |
| `t421MZZZ` | 4123                  | Y      | N           |
| `t322ZZZM` | 1234                  | N      | Y           |
| `t322MZZZ` | 4123                  | N      | Y           |

| Files                                               | Description                                         | Source                                                                                                         |
| --------------------------------------------------- | --------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- |
| `<family>_utmis.m`                                  | UT master integral definitions                      | Unknown                                                                                                        |
| `mappings.m`                                        | MI mappings                                         | [file](https://gitlab.com/sbadger/myfiniteflowexamples/-/tree/master/ibpsv2/2L_2l2a/mappings/ibp_reduction.wl) |
| `1L/`                                               | 1L IBPs                                             | [file](https://gitlab.com/sbadger/myfiniteflowexamples/-/tree/master/ibpsv2/1L_2l2a/generate_ibps.wl)          |
| `*.tar.xz`                                          | 2L IBPs                                             | [file](https://gitlab.com/sbadger/myfiniteflowexamples/-/tree/master/ibpsv2/2L_2l2a/generate_ibps.wl)          |
| `de_<family>_an.m`                                  | DE $A_x(s_{ij},\epsilon)$ matrix                    | [dir](https://gitlab.com/sbadger/myfiniteflowexamples/-/tree/master/ibpsv2/2L_2l2a)/`deqs_<family>.wl`         |
| `atilde_<family>.m`                                 | DE $\tilde{A}(s_{ij})$ matrix                       | [file](https://bitbucket.org/sbadger/ffamps_tools/src/master/2l2a/de/de.wl)                                    |
| `<family>_sol_<PSname>_<precision>.m`               | DE numerical evaluation in js (canonical ordering)  | [file](https://bitbucket.org/sbadger/ffamps_tools/src/master/2l2a/de/run.wl)                                   |
| `<family>_mis_<PSname>_<precision>.m`               | DE numerical evaluation in MIs (canonical ordering) | [file](https://bitbucket.org/sbadger/ffamps_tools/src/master/2l2a/de/map.wl)                                   |
| `<family>p<permutation>_mis_<PSname>_<precision>.m` | DE numerical evaluation in MIs                      | [file](https://bitbucket.org/sbadger/ffamps_tools/src/master/2l2a/de/boundary_values.wl)                       |
| `<PSname>.m`                                        | Phase space point definition                        | Itself                                                                                                         |
