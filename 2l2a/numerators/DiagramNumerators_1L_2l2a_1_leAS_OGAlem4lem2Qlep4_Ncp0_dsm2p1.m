DiagramTopologies = {topo[{{4}, {1, 2, 3}}]};

DiagramNumerator["-+3+",topo[{{4}, {1, 2, 3}}]] = 0;
DiagramNumerator["-+3-",topo[{{4}, {1, 2, 3}}]] = 0;
DiagramNumerator["-++1",topo[{{4}, {1, 2, 3}}]] = 0;
DiagramNumerator["-++2",topo[{{4}, {1, 2, 3}}]] = 0;
DiagramNumerator["-++3",topo[{{4}, {1, 2, 3}}]] = 0;
DiagramNumerator["-++X",topo[{{4}, {1, 2, 3}}]] = 0;
DiagramNumerator["-+-1",topo[{{4}, {1, 2, 3}}]] = 0;
DiagramNumerator["-+-2",topo[{{4}, {1, 2, 3}}]] = 0;
DiagramNumerator["-+-3",topo[{{4}, {1, 2, 3}}]] = 0;
DiagramNumerator["-+-X",topo[{{4}, {1, 2, 3}}]] = 0;
