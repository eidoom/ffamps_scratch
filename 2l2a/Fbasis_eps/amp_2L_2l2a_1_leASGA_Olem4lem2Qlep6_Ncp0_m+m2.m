(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{f[1] -> (-30/(ex[2] - ex[3])^2 + 3/(ex[2]*(-1 + ex[3])) + 
     (6*ex[2]^2*(5 + ex[3]))/(-1 + ex[3])^3 - (3*(11 + 6*ex[3]))/
      (ex[2] - ex[3]) + (-9 + 5*ex[3] - 2*ex[3]^2)/(1 + ex[2] - ex[3])^2 - 
     (3*ex[2]*(13 - 7*ex[3] + 2*ex[3]^2))/(-1 + ex[3])^2 + 
     (43 - 51*ex[3] - 12*ex[3]^2 + 2*ex[3]^3)/(-1 + ex[3])^2 + 
     (64 - 63*ex[3] + 12*ex[3]^2 + 2*ex[3]^3)/((1 + ex[2] - ex[3])*
       (-1 + ex[3])))/9, 
  f[2] -> (-4*(15 - 6*ex[3] - 9*ex[3]^2 + ex[2]^2*(-34 + 4*ex[3]) + 
      ex[2]*(-24 + 58*ex[3] - 4*ex[3]^2)))/(3*(1 + ex[2] - ex[3])*
     (-1 + ex[3])^2), 
  f[3] -> (2*(30/(ex[2] - ex[3])^2 + (33 + 6*ex[3])/(ex[2] - ex[3]) + 
      3/(ex[2] - ex[2]*ex[3]) + (6 - 8*ex[3] - 8*ex[3]^2)/
       ((1 + ex[2] - ex[3])*(-1 + ex[3])) - 
      (4*ex[2]*(9 + 4*ex[3] + 2*ex[3]^2))/(-1 + ex[3])^3 + 
      (15 - ex[3] + 6*ex[3]^2)/(-1 + ex[3])^2))/3, 
  f[4] -> (2*((300*ex[3])/(-ex[2] + ex[3])^3 - (30*(9 + 10*ex[3]))/
       (ex[2] - ex[3]) - (30*(5 + 11*ex[3]))/(ex[2] - ex[3])^2 - 
      (2*ex[2]*(-23 - 39*ex[3] - 15*ex[3]^2 + 5*ex[3]^3))/(-1 + ex[3])^3 + 
      (326 - 423*ex[3] + 8*ex[3]^3)/(1 + ex[2] - ex[3])^2 + 
      (3*(27 - 131*ex[3] + 92*ex[3]^2 + 8*ex[3]^3))/((1 + ex[2] - ex[3])*
        (-1 + ex[3])) + (2*(-35 + 330*ex[3] - 93*ex[3]^2 + 44*ex[3]^3))/
       (-1 + ex[3])^2))/27, 
  f[5] -> (4*((5 - 11*ex[3])/(1 + ex[2] - ex[3]) + 
      (15*ex[3])/(ex[2] - ex[3]) + (2*ex[2]*(-17 + 2*ex[3]))/(-1 + ex[3])^2 + 
      (2*(-5 + 2*ex[3]))/(-1 + ex[3])))/3, 
  f[6] -> (-4*ex[2]*(6*ex[2]^3*(5 + ex[3]) + 
      ex[2]^2*(75 - 128*ex[3] + 11*ex[3]^2 - 6*ex[3]^3) - 
      (-1 + ex[3])^2*(27 + 40*ex[3] - 15*ex[3]^2 + 8*ex[3]^3) + 
      ex[2]*(27 - 176*ex[3] + 183*ex[3]^2 - 48*ex[3]^3 + 14*ex[3]^4)))/
    (3*(1 + ex[2] - ex[3])^2*(-1 + ex[3])^3), 
  f[7] -> (20*ex[3])/(ex[2] - ex[3])^2 - (8*ex[2]*(1 + 2*ex[3]))/
     (-1 + ex[3])^3 + (22 + 4*ex[3])/(1 - ex[3]) + 
    (10 + 8*ex[3])/(ex[2] - ex[3]) + (24 + 34*ex[3] - 40*ex[3]^2)/
     (3*(1 + ex[2] - ex[3])*(-1 + ex[3])) - (2*(9 - 5*ex[3] + 2*ex[3]^2))/
     (3*(1 + ex[2] - ex[3])^2), 
  f[8] -> (2*(-ex[2] + (3*(-3 + 2*ex[3])*(-2 + ex[3] + 2*ex[3]^2))/
       ((1 + ex[2] - ex[3])*(-1 + ex[3])) + (4*(2 - 9*ex[3] + ex[3]^3))/
       (1 + ex[2] - ex[3])^2 + (-7 + 42*ex[3] - 57*ex[3]^2 + 28*ex[3]^3)/
       (-1 + ex[3])^2))/27, f[9] -> (-593*(-1 + ex[2]))/
    (2880*(1 + ex[2] - ex[3])^2), 
  f[10] -> (-2 + 12*ex[2]^2 + ex[2]*(8 - 20*ex[3]) - 4*ex[3] + 6*ex[3]^2)/
    ((1 + ex[2] - ex[3])*(-1 + ex[3])^2), 
  f[11] -> (-12*ex[2])/(-1 + ex[3])^2 - 4/(-1 + ex[3]) + 
    (2*ex[3])/(ex[2] - ex[3]) - (2*(1 + ex[3]))/(1 + ex[2] - ex[3]), 
  f[12] -> (2*ex[2]*((-9 + ex[3])*(-1 + ex[3]) + 2*ex[2]*(5 + ex[3]) + 
      ((-1 + ex[3])*(4 - 4*ex[3] + ex[2]*(-1 + 3*ex[3])))/
       (1 + ex[2] - ex[3])^2))/(-1 + ex[3])^3, 
  f[13] -> -((2*ex[2]^2*(5 + ex[3]) + ex[2]*(5 - 18*ex[3] + ex[3]^2) + 
      ((-1 + ex[3])*(2*ex[2]^2 + (-1 + ex[3])^2 + 
         ex[2]*(8 - 11*ex[3] + 3*ex[3]^2)))/(1 + ex[2] - ex[3])^2)/
     (-1 + ex[3])^3), f[14] -> 2*((ex[2] - ex[3])^(-1) - 
     (6*ex[2])/(-1 + ex[3])^2 + 4/(-1 + ex[3])), 
  f[15] -> -4/(ex[2] - ex[3])^2 - 6/(ex[2] - ex[3]) + 
    2/(ex[2]*(-1 + ex[3])) + (2*(-2 + ex[3]))/((1 + ex[2] - ex[3])*
      (-1 + ex[3])) - (4*ex[3])/(-1 + ex[3])^2, 
  f[16] -> (2*(3*(-1 + ex[3])^2 + ex[2]^2*(4 + 8*ex[3]) - 
      7*ex[2]*(-1 + ex[3]^2)))/((-1 + ex[3])^3*(-1 - ex[2] + ex[3])), 
  f[17] -> (-4*ex[3])/(ex[2] - ex[3])^2 + (2*ex[3])/(1 + ex[2] - ex[3]) + 
    (4*ex[3])/(-1 + ex[3])^2 - (2*(1 + ex[3]))/(ex[2] - ex[3]), 
  f[18] -> (2 - 2*ex[3])/(1 + ex[2] - ex[3]) + (ex[2]*(-9 + ex[3]))/
     (-1 + ex[3])^2 + (2*ex[3])/(ex[2] - ex[3])^2 - 
    (1 + ex[3])/(-1 + ex[3])^2 + (2*ex[2]^2*(5 + ex[3]))/(-1 + ex[3])^3 + 
    (1 + 2*ex[3])/(ex[2] - ex[3]), f[19] -> (-8*(2 + 2*ex[2] - ex[3]))/
    (1 + ex[2] - ex[3])^2, 
  f[20] -> 2*(-(-1 + ex[3])^(-2) + (2*ex[3])/(ex[2] - ex[3])^3 - 
     (2*(-2 + ex[3])*ex[3])/((1 + ex[2] - ex[3])*(-1 + ex[3])) + 
     (1 + 2*ex[3])/(ex[2] - ex[3]) - (4*ex[2]^2*(1 + 2*ex[3]))/
      (-1 + ex[3])^4 + (1 + 3*ex[3])/(ex[2] - ex[3])^2 + 
     (ex[2]*(5 + 3*ex[3]))/(-1 + ex[3])^3), 
  f[21] -> (2*((2*ex[3])/(ex[2] - ex[3])^3 - (2*(-2 + ex[3])*ex[3])/
       ((1 + ex[2] - ex[3])*(-1 + ex[3])) + (1 + 2*ex[3])/(ex[2] - ex[3]) + 
      (1 + 3*ex[3])/(ex[2] - ex[3])^2))/3, 
  f[22] -> (2*(-2 + ex[3]))/((1 + ex[2] - ex[3])*(-1 + ex[3]))}, 
 {{(6*f[1] - 2*f[12] - 2*f[13] + f[15] - f[16])/6, f[2] + 2*f[10], 
   f[3] - 2*f[12] - 4*f[13] - f[15] - f[16], f[5] + 2*f[11], f[6] + 2*f[12], 
   (144*f[1])/5 + (19*f[2])/5 + (24*f[3])/5 + (36*f[5])/5 + (12*f[6])/5 + 
    (1228800*f[9])/593 + (742*f[10])/15 + (488*f[11])/15 + (956*f[12])/5 + 
    (3176*f[13])/15 + 12*f[14] + (1564*f[16])/15 - (256*f[17])/3 - 
    (512*f[18])/3 + (32*f[19])/3 - (1568*f[22])/15, 
   (-144*f[1])/5 - (19*f[2])/5 - (24*f[3])/5 - (36*f[5])/5 - (12*f[6])/5 - 
    (1228800*f[9])/593 - (742*f[10])/15 - (488*f[11])/15 - (956*f[12])/5 - 
    (3176*f[13])/15 - 12*f[14] - (1564*f[16])/15 + (256*f[17])/3 + 
    (512*f[18])/3 - (32*f[19])/3 + (1568*f[22])/15, 
   (12*f[1])/35 - (103*f[2])/35 + (2*f[3])/35 - (67*f[5])/35 - (34*f[6])/35 - 
    (1459200*f[9])/4151 + (58*f[10])/15 + (94*f[11])/105 - (956*f[12])/105 + 
    (516*f[13])/35 + (164*f[16])/35 + (52*f[17])/21 + (608*f[18])/21 - 
    (38*f[19])/21 - (124*f[22])/105, (-144*f[1])/35 + (11*f[2])/35 - 
    (24*f[3])/35 - f[5]/35 - (12*f[6])/35 + (844800*f[9])/4151 + 
    (14*f[10])/15 + (202*f[11])/105 - (96*f[12])/35 - (2336*f[13])/105 - 
    (1144*f[16])/105 + (76*f[17])/21 - (352*f[18])/21 + (22*f[19])/21 - 
    (52*f[22])/105, f[4] + (4480*f[9])/593 - (7*f[19])/6 - (10*f[21])/3 - 
    (16*f[22])/9, (-143*f[1])/7 - (143*f[2])/42 - (143*f[3])/42 - 
    (99*f[4])/16 - (143*f[5])/28 - (143*f[6])/84 + (63*f[8])/8 + 
    (3791560*f[9])/4151 + (1331*f[10])/24 + (16441*f[11])/252 + 
    (81217*f[12])/504 + (66329*f[13])/252 - (21*f[14])/4 + 
    (21913*f[16])/168 - (7885*f[17])/252 - (3722*f[18])/63 + 
    (13865*f[19])/1008 + (39*f[21])/8 - (52757*f[22])/504, 
   (-5584*f[1])/35 - (2792*f[2])/105 - (2792*f[3])/105 - 9*f[4] - 
    (1396*f[5])/35 - (1396*f[6])/105 + 18*f[8] - (27674240*f[9])/4151 - 
    (1216*f[10])/15 - (2284*f[11])/315 - (147884*f[12])/315 - 
    (114488*f[13])/315 - 36*f[14] - (18524*f[16])/105 + (17372*f[17])/63 + 
    (36256*f[18])/63 - (2957*f[19])/126 - 78*f[21] + (49414*f[22])/315, 
   (-5696*f[1])/35 - (2848*f[2])/105 - (2848*f[3])/105 - 6*f[4] - 
    (1424*f[5])/35 - (1424*f[6])/105 + 18*f[8] - (29255680*f[9])/4151 - 
    (458*f[10])/5 - (5336*f[11])/315 - (160246*f[12])/315 - 
    (129412*f[13])/315 - 36*f[14] - (6982*f[16])/35 + (18352*f[17])/63 + 
    (38216*f[18])/63 - (3685*f[19])/126 - 88*f[21] + (55826*f[22])/315, 
   (-66*f[1])/35 - (46*f[2])/35 - (11*f[3])/35 - (34*f[5])/35 - 
    (23*f[6])/35 - (307200*f[9])/4151 + (12*f[10])/5 + (148*f[11])/105 - 
    (622*f[12])/105 - (394*f[13])/105 - (326*f[16])/105 + (64*f[17])/21 + 
    (128*f[18])/21 - (8*f[19])/21 - (88*f[22])/105, 
   (66*f[1])/35 + (11*f[2])/35 - (24*f[3])/35 - f[5]/35 - (12*f[6])/35 - 
    f[7] - (768000*f[9])/4151 - (106*f[10])/15 - (638*f[11])/105 - 
    (586*f[12])/35 - (1916*f[13])/105 + f[15] - (829*f[16])/105 + 
    (160*f[17])/21 + (278*f[18])/21 - (20*f[19])/21 + (1628*f[22])/105, 
   (66*f[1])/35 + (46*f[2])/35 + (11*f[3])/35 + (34*f[5])/35 + (23*f[6])/35 + 
    f[7] + (307200*f[9])/4151 - (12*f[10])/5 - (148*f[11])/105 + 
    (622*f[12])/105 + (604*f[13])/105 + (326*f[16])/105 - (64*f[17])/21 - 
    (86*f[18])/21 + (8*f[19])/21 + (88*f[22])/105, f[7] + 2*(f[13] + f[18]), 
   -f[7] - 2*(f[13] + f[18]), (204*f[1])/35 + (69*f[2])/35 - (36*f[3])/35 + 
    (51*f[5])/35 + (17*f[6])/35 + (460800*f[9])/4151 - (18*f[10])/5 - 
    (74*f[11])/35 + (346*f[12])/35 + (372*f[13])/35 + 2*f[15] + 
    (198*f[16])/35 - (32*f[17])/7 - (64*f[18])/7 + (4*f[19])/7 + 
    (44*f[22])/35, (-132*f[1])/35 - (57*f[2])/35 + (13*f[3])/35 - 
    (33*f[5])/35 - (11*f[6])/35 + (460800*f[9])/4151 + (142*f[10])/15 + 
    (262*f[11])/35 + (1136*f[12])/105 + (1312*f[13])/105 - f[15] + 
    (503*f[16])/105 - (32*f[17])/7 - (64*f[18])/7 + (4*f[19])/7 - 
    (572*f[22])/35, (12*f[1])/7 + (9*f[2])/7 + (2*f[3])/7 + (3*f[5])/7 + 
    f[6]/7 - (1382400*f[9])/4151 - (46*f[10])/3 - (90*f[11])/7 - 
    (662*f[12])/21 - (748*f[13])/21 - (320*f[16])/21 + (96*f[17])/7 + 
    (192*f[18])/7 - (12*f[19])/7 + (220*f[22])/7, 
   (-2592*f[1])/35 - (432*f[2])/35 - (432*f[3])/35 - (9*f[4])/2 - 
    (648*f[5])/35 - (216*f[6])/35 + 9*f[8] - (8616000*f[9])/4151 - 
    (13*f[10])/15 + (3566*f[11])/105 - (3233*f[12])/35 + (722*f[13])/105 - 
    18*f[14] + (583*f[16])/105 + (1886*f[17])/21 + (4024*f[18])/21 - 
    (80*f[19])/21 - 39*f[21] - (481*f[22])/105, 
   (99352*f[1])/35 + (49676*f[2])/105 + (49676*f[3])/105 + 146*f[4] + 
    (24838*f[5])/35 + (24838*f[6])/105 - 368*f[8] + (454943360*f[9])/4151 + 
    (16898*f[10])/15 - (61538*f[11])/315 + (254138*f[12])/35 + 
    (1555304*f[13])/315 + (1948*f[14])/3 + (249472*f[16])/105 - 
    (96094*f[17])/21 - (603836*f[18])/63 + (49163*f[19])/126 + 
    (4384*f[21])/3 - (217484*f[22])/105, (9997*f[1])/140 + (9997*f[2])/840 + 
    (9997*f[3])/840 + (197*f[4])/64 + (9997*f[5])/560 + (9997*f[6])/1680 - 
    (249*f[8])/32 + (12328450*f[9])/4151 + (5773*f[10])/160 + 
    (14773*f[11])/5040 + (2107741*f[12])/10080 + (812981*f[13])/5040 + 
    (259*f[14])/16 + (262261*f[16])/3360 - (124037*f[17])/1008 - 
    (32369*f[18])/126 + (47161*f[19])/4032 + (3677*f[21])/96 - 
    (677281*f[22])/10080, (-3494*f[1])/35 - (1747*f[2])/105 - 
    (1747*f[3])/105 - (41*f[4])/8 - (1747*f[5])/70 - (1747*f[6])/210 + 
    (53*f[8])/4 - (15810960*f[9])/4151 - (2279*f[10])/60 + (5357*f[11])/630 - 
    (314261*f[12])/1260 - (103961*f[13])/630 - (137*f[14])/6 - 
    (33281*f[16])/420 + (20071*f[17])/126 + (7010*f[18])/21 - 
    (6847*f[19])/504 - (617*f[21])/12 + (87701*f[22])/1260, 
   (6264*f[1])/35 + (1044*f[2])/35 + (1044*f[3])/35 + (27*f[4])/2 + 
    (1566*f[5])/35 + (522*f[6])/35 - 27*f[8] + (22031040*f[9])/4151 + 
    (181*f[10])/15 - (7562*f[11])/105 + (9111*f[12])/35 + (3826*f[13])/105 + 
    42*f[14] + (1499*f[16])/105 - (4790*f[17])/21 - (10168*f[18])/21 + 
    (403*f[19])/42 + 81*f[21] - (1553*f[22])/105, 
   (-2104*f[1])/35 - (1052*f[2])/105 - (1052*f[3])/105 - 5*f[4] - 
    (526*f[5])/35 - (526*f[6])/105 + 10*f[8] - (6650240*f[9])/4151 + 
    (82*f[10])/45 + (3212*f[11])/105 - (21094*f[12])/315 + (4892*f[13])/315 - 
    (44*f[14])/3 + (2798*f[16])/315 + (4400*f[17])/63 + (9416*f[18])/63 - 
    (116*f[19])/63 - (82*f[21])/3 - (3166*f[22])/315, 
   (-4049*f[1])/28 - (4049*f[2])/168 - (4049*f[3])/168 - (461*f[4])/64 - 
    (4049*f[5])/112 - (4049*f[6])/336 + (545*f[8])/32 - 
    (23254610*f[9])/4151 - (16601*f[10])/288 + (3245*f[11])/336 - 
    (248779*f[12])/672 - (253529*f[13])/1008 - (1585*f[14])/48 - 
    (243787*f[16])/2016 + (235309*f[17])/1008 + (61601*f[18])/126 - 
    (82049*f[19])/4032 - (7205*f[21])/96 + (213173*f[22])/2016, 
   (7263*f[1])/35 + (2421*f[2])/70 + (2421*f[3])/70 + (351*f[4])/16 + 
    (7263*f[5])/140 + (2421*f[6])/140 - (315*f[8])/8 + (15171480*f[9])/4151 - 
    (3113*f[10])/40 - (24697*f[11])/140 - (1969*f[12])/280 - 
    (54409*f[13])/140 + (201*f[14])/4 - (54907*f[16])/280 - (4775*f[17])/28 - 
    (2622*f[18])/7 - (1277*f[19])/112 + (621*f[21])/8 + (45669*f[22])/280, 
   (-4884*f[1])/35 - (814*f[2])/35 - (814*f[3])/35 - (57*f[4])/4 - 
    (1221*f[5])/35 - (407*f[6])/35 + (57*f[8])/2 - (10814880*f[9])/4151 + 
    (1333*f[10])/30 + (3919*f[11])/35 - (1297*f[12])/70 + (23869*f[13])/105 - 
    35*f[14] + (24167*f[16])/210 + (2531*f[17])/21 + (5552*f[18])/21 + 
    (425*f[19])/84 - (115*f[21])/2 - (21949*f[22])/210, 
   (5472*f[1])/35 + (912*f[2])/35 + (912*f[3])/35 + 9*f[4] + (1368*f[5])/35 + 
    (456*f[6])/35 - 18*f[8] + (24816000*f[9])/4151 + (898*f[10])/15 - 
    (1376*f[11])/105 + (13798*f[12])/35 + (27868*f[13])/105 + 36*f[14] + 
    (13442*f[16])/105 - (5240*f[17])/21 - (10984*f[18])/21 + (410*f[19])/21 + 
    78*f[21] - (11534*f[22])/105, (-4884*f[1])/35 - (814*f[2])/35 - 
    (814*f[3])/35 - (57*f[4])/4 - (1221*f[5])/35 - (407*f[6])/35 + 
    (57*f[8])/2 - (10814880*f[9])/4151 + (1333*f[10])/30 + (3919*f[11])/35 - 
    (1297*f[12])/70 + (23869*f[13])/105 - 35*f[14] + (24167*f[16])/210 + 
    (2531*f[17])/21 + (5552*f[18])/21 + (425*f[19])/84 - (115*f[21])/2 - 
    (21949*f[22])/210, (-1364*f[1])/35 - (682*f[2])/105 - (682*f[3])/105 - 
    (13*f[4])/4 - (341*f[5])/35 - (341*f[6])/105 + (13*f[8])/2 - 
    (4533280*f[9])/4151 - (101*f[10])/90 + (619*f[11])/35 - (3557*f[12])/70 - 
    (173*f[13])/315 - (29*f[14])/3 + (281*f[16])/630 + (2981*f[17])/63 + 
    (6368*f[18])/63 - (409*f[19])/252 - (109*f[21])/6 - (1987*f[22])/630, 
   (-204*f[1])/5 - (34*f[2])/5 - (34*f[3])/5 - (3*f[4])/4 - (51*f[5])/5 - 
    (17*f[6])/5 + (3*f[8])/2 - (1305120*f[9])/593 - (1169*f[10])/30 - 
    (101*f[11])/5 - (5471*f[12])/30 - (2711*f[13])/15 - 9*f[14] - 
    (2653*f[16])/30 + 89*f[17] + 184*f[18] - (127*f[19])/12 - (49*f[21])/2 + 
    (2231*f[22])/30, (-5808*f[1])/35 - (968*f[2])/35 - (968*f[3])/35 - 
    6*f[4] - (1452*f[5])/35 - (484*f[6])/35 + 18*f[8] - 
    (29748480*f[9])/4151 - (1372*f[10])/15 - (1676*f[11])/105 - 
    (53756*f[12])/105 - (14264*f[13])/35 - 36*f[14] - (20708*f[16])/105 + 
    (6220*f[17])/21 + (12944*f[18])/21 - (209*f[19])/7 - 88*f[21] + 
    (19066*f[22])/105, (-204*f[1])/5 - (34*f[2])/5 - (34*f[3])/5 - 
    (3*f[4])/4 - (51*f[5])/5 - (17*f[6])/5 + (3*f[8])/2 - 
    (1305120*f[9])/593 - (1169*f[10])/30 - (101*f[11])/5 - (5471*f[12])/30 - 
    (2711*f[13])/15 - 9*f[14] - (2653*f[16])/30 + 89*f[17] + 184*f[18] - 
    (127*f[19])/12 - (49*f[21])/2 + (2231*f[22])/30, 
   (-4881*f[1])/35 - (1627*f[2])/70 - (1627*f[3])/70 - (105*f[4])/16 - 
    (4881*f[5])/140 - (1627*f[6])/140 + (141*f[8])/8 - (22759080*f[9])/4151 - 
    (7027*f[10])/120 + (879*f[11])/140 - (102297*f[12])/280 - 
    (106291*f[13])/420 - (127*f[14])/4 - (102233*f[16])/840 + 
    (19171*f[17])/84 + (10030*f[18])/21 - (7055*f[19])/336 - (587*f[21])/8 + 
    (90631*f[22])/840, (-5472*f[1])/35 - (912*f[2])/35 - (912*f[3])/35 - 
    9*f[4] - (1368*f[5])/35 - (456*f[6])/35 + 18*f[8] - 
    (24816000*f[9])/4151 - (898*f[10])/15 + (1376*f[11])/105 - 
    (13798*f[12])/35 - (27868*f[13])/105 - 36*f[14] - (13442*f[16])/105 + 
    (5240*f[17])/21 + (10984*f[18])/21 - (410*f[19])/21 - 78*f[21] + 
    (11534*f[22])/105, (3123*f[1])/35 + (1041*f[2])/70 + (1041*f[3])/70 + 
    (99*f[4])/16 + (3123*f[5])/140 + (1041*f[6])/140 - (111*f[8])/8 + 
    (12318520*f[9])/4151 + (6803*f[10])/360 - (29453*f[11])/1260 + 
    (143033*f[12])/840 + (93619*f[13])/1260 + (85*f[14])/4 + 
    (88057*f[16])/2520 - (31763*f[17])/252 - (16774*f[18])/63 + 
    (8255*f[19])/1008 + (345*f[21])/8 - (74119*f[22])/2520, 
   (-675840*f[9])/593 - (4*(32*f[10] + 32*f[11] + 108*f[12] + 152*f[13] + 
       76*f[16] - 32*f[17] - 64*f[18] + 5*f[19] - 64*f[22]))/3, 
   (-8208*f[1])/35 - (1368*f[2])/35 - (1368*f[3])/35 - (27*f[4])/2 - 
    (2052*f[5])/35 - (684*f[6])/35 + 27*f[8] - (36834240*f[9])/4151 - 
    (1297*f[10])/15 + (2414*f[11])/105 - (20347*f[12])/35 - 
    (40402*f[13])/105 - 54*f[14] - (19463*f[16])/105 + (7790*f[17])/21 + 
    (16336*f[18])/21 - (1195*f[19])/42 - 117*f[21] + (16601*f[22])/105, 
   (-1497*f[1])/35 - (499*f[2])/70 - (499*f[3])/70 - (33*f[4])/16 - 
    (1497*f[5])/140 - (499*f[6])/140 + (69*f[8])/8 - (6747240*f[9])/4151 - 
    (1979*f[10])/120 + (1429*f[11])/420 - (90067*f[12])/840 - 
    (9929*f[13])/140 - (39*f[14])/4 - (28561*f[16])/840 + (5755*f[17])/84 + 
    (3014*f[18])/21 - (685*f[19])/112 - (179*f[21])/8 + (8549*f[22])/280, 
   -15*f[1] - (5*f[2])/2 - (5*f[3])/2 + (3*f[4])/16 - (15*f[5])/4 - 
    (5*f[6])/4 - (15*f[8])/8 - (603640*f[9])/593 - (1513*f[10])/72 - 
    (527*f[11])/36 - (719*f[12])/8 - (3503*f[13])/36 - (11*f[14])/4 - 
    (3437*f[16])/72 + (1451*f[17])/36 + (742*f[18])/9 - (791*f[19])/144 - 
    (71*f[21])/8 + (3155*f[22])/72, (381*f[1])/5 + (127*f[2])/10 + 
    (127*f[3])/10 + (51*f[4])/16 + (381*f[5])/20 + (127*f[6])/20 - 
    (63*f[8])/8 + (1816200*f[9])/593 + (4129*f[10])/120 - (19*f[11])/20 + 
    (25231*f[12])/120 + (9271*f[13])/60 + (69*f[14])/4 + (8933*f[16])/120 - 
    (509*f[17])/4 - 266*f[18] + (563*f[19])/48 + (329*f[21])/8 - 
    (2537*f[22])/40, (183*f[1])/7 + (61*f[2])/14 + (61*f[3])/14 + 
    (3*f[4])/16 + (183*f[5])/28 + (61*f[6])/28 - (15*f[8])/8 + 
    (6536760*f[9])/4151 + (733*f[10])/24 + (1597*f[11])/84 + 
    (22661*f[12])/168 + (3903*f[13])/28 + (21*f[14])/4 + (11479*f[16])/168 - 
    (5329*f[17])/84 - (2738*f[18])/21 + (2773*f[19])/336 + (121*f[21])/8 - 
    (3491*f[22])/56, (-792*f[1])/35 - (132*f[2])/35 - (132*f[3])/35 - 
    (9*f[4])/2 - (198*f[5])/35 - (66*f[6])/35 + 9*f[8] + 
    (2784960*f[9])/4151 + (239*f[10])/5 + (2062*f[11])/35 + (4687*f[12])/35 + 
    (8014*f[13])/35 - 6*f[14] + (3981*f[16])/35 - (150*f[17])/7 - 
    (272*f[18])/7 + (139*f[19])/14 - 3*f[21] - (3327*f[22])/35, 
   (14418*f[1])/35 + (2403*f[2])/35 + (2403*f[3])/35 + (81*f[4])/8 + 
    (7209*f[5])/70 + (2403*f[6])/70 - (117*f[8])/4 + (85774800*f[9])/4151 + 
    (20203*f[10])/60 + (30827*f[11])/210 + (229793*f[12])/140 + 
    (323339*f[13])/210 + (183*f[14])/2 + (315617*f[16])/420 - 
    (35323*f[17])/42 - (36604*f[18])/21 + (16279*f[19])/168 + (963*f[21])/4 - 
    (268799*f[22])/420, (8973*f[1])/35 + (2991*f[2])/70 + (2991*f[3])/70 + 
    (261*f[4])/16 + (8973*f[5])/140 + (2991*f[6])/140 - (297*f[8])/8 + 
    (36358920*f[9])/4151 + (2477*f[10])/40 - (8307*f[11])/140 + 
    (144981*f[12])/280 + (35661*f[13])/140 + (243*f[14])/4 + 
    (33703*f[16])/280 - (10365*f[17])/28 - (5466*f[18])/7 + 
    (2897*f[19])/112 + (1023*f[21])/8 - (27561*f[22])/280, 
   (1053*f[1])/7 + (351*f[2])/14 + (351*f[3])/14 + (81*f[4])/16 + 
    (1053*f[5])/28 + (351*f[6])/28 - (117*f[8])/8 + (28890600*f[9])/4151 + 
    (2447*f[10])/24 + (2719*f[11])/84 + (29549*f[12])/56 + (38767*f[13])/84 + 
    (135*f[14])/4 + (37741*f[16])/168 - (23995*f[17])/84 - (12470*f[18])/21 + 
    (10279*f[19])/336 + (675*f[21])/8 - (32371*f[22])/168, 
   (-2412*f[1])/35 - (402*f[2])/35 - (402*f[3])/35 - (9*f[4])/4 - 
    (603*f[5])/35 - (201*f[6])/35 + (13*f[8])/2 - (12961440*f[9])/4151 - 
    (3913*f[10])/90 - (3797*f[11])/315 - (16201*f[12])/70 - 
    (61769*f[13])/315 - 15*f[14] - (59987*f[16])/630 + (8089*f[17])/63 + 
    (16808*f[18])/63 - (3397*f[19])/252 - (75*f[21])/2 + (53749*f[22])/630, 
   (585*f[1])/7 + (195*f[2])/14 + (195*f[3])/14 + (45*f[4])/16 + 
    (585*f[5])/28 + (195*f[6])/28 - (81*f[8])/8 + (15883080*f[9])/4151 + 
    (1331*f[10])/24 + (1411*f[11])/84 + (16217*f[12])/56 + (21139*f[13])/84 + 
    (75*f[14])/4 + (20569*f[16])/168 - (13231*f[17])/84 - (6878*f[18])/21 + 
    (5611*f[19])/336 + (375*f[21])/8 - (17623*f[22])/168, 
   (3141*f[1])/35 + (1047*f[2])/70 + (1047*f[3])/70 + (45*f[4])/16 + 
    (3141*f[5])/140 + (1047*f[6])/140 - (81*f[8])/8 + (17035080*f[9])/4151 + 
    (6847*f[10])/120 + (7103*f[11])/420 + (84477*f[12])/280 + 
    (106271*f[13])/420 + (75*f[14])/4 + (103133*f[16])/840 - 
    (14191*f[17])/84 - (7358*f[18])/21 + (6091*f[19])/336 + (375*f[21])/8 - 
    (99251*f[22])/840, (1632*f[1])/35 + (272*f[2])/35 + (272*f[3])/35 + 
    (3*f[4])/2 + (408*f[5])/35 + (136*f[6])/35 - 3*f[8] + 
    (8661440*f[9])/4151 + (1259*f[10])/45 + (2162*f[11])/315 + 
    (5323*f[12])/35 + (39734*f[13])/315 + 10*f[14] + (19261*f[16])/315 - 
    (5398*f[17])/63 - (11216*f[18])/63 + (1115*f[19])/126 + 25*f[21] - 
    (17587*f[22])/315, (-1632*f[1])/35 - (272*f[2])/35 - (272*f[3])/35 - 
    (3*f[4])/2 - (408*f[5])/35 - (136*f[6])/35 + 3*f[8] - 
    (8661440*f[9])/4151 - (1259*f[10])/45 - (2162*f[11])/315 - 
    (5323*f[12])/35 - (39734*f[13])/315 - 10*f[14] - (19261*f[16])/315 + 
    (5398*f[17])/63 + (11216*f[18])/63 - (1115*f[19])/126 - 25*f[21] + 
    (17587*f[22])/315, f[8] + (2560*f[9])/593 + f[22]/9, 
   (2022*f[1])/35 + (337*f[2])/35 + (337*f[3])/35 + (15*f[4])/8 + 
    (1011*f[5])/70 + (337*f[6])/70 - (27*f[8])/4 + (10972720*f[9])/4151 + 
    (6751*f[10])/180 + (7079*f[11])/630 + (82781*f[12])/420 + 
    (105983*f[13])/630 + (25*f[14])/2 + (102989*f[16])/1260 - 
    (13711*f[17])/126 - (14236*f[18])/63 + (5851*f[19])/504 + (125*f[21])/4 - 
    (93683*f[22])/1260, 28*f[9] + 
    (593*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/2880, (107520*f[9])/593 + 
    (4*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/3, (-13440*f[9])/593 + 
    (-4*f[10] - 4*f[11] - 12*f[12] - 16*f[13] - 8*f[16] + 4*f[17] + 8*f[18] - 
      f[19] + 8*f[22])/6, (63840*f[9])/593 + 
    (19*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/24, (1505280*f[9])/593 + 
    (56*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/3, (26880*f[9])/593 + 
    (4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 8*f[18] + 
      f[19] - 8*f[22])/3, (107520*f[9])/593 + 
    (4*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/3, (-13440*f[9])/593 + 
    (-4*f[10] - 4*f[11] - 12*f[12] - 16*f[13] - 8*f[16] + 4*f[17] + 8*f[18] - 
      f[19] + 8*f[22])/6, (-161280*f[9])/593 - 
    2*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (107520*f[9])/593 + 
    (4*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/3, (-26880*f[9])/593 + 
    (-4*f[10] - 4*f[11] - 12*f[12] - 16*f[13] - 8*f[16] + 4*f[17] + 8*f[18] - 
      f[19] + 8*f[22])/3, 
   (-527*(80640*f[9] + 593*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 
        8*f[16] - 4*f[17] - 8*f[18] + f[19] - 8*f[22])))/3558, 
   (147840*f[9])/593 + (11*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 
       8*f[16] - 4*f[17] - 8*f[18] + f[19] - 8*f[22]))/6, 
   (-134400*f[9])/593 - (5*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 
       8*f[16] - 4*f[17] - 8*f[18] + f[19] - 8*f[22]))/3, 
   (-26880*f[9])/593 + (-4*f[10] - 4*f[11] - 12*f[12] - 16*f[13] - 8*f[16] + 
      4*f[17] + 8*f[18] - f[19] + 8*f[22])/3, (-322560*f[9])/593 - 
    4*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (322560*f[9])/593 + 
    4*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (-107520*f[9])/593 - 
    (4*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/3, (6720*f[9])/593 + 
    (4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 8*f[18] + 
      f[19] - 8*f[22])/12, (-161280*f[9])/593 - 
    2*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (322560*f[9])/593 + 
    4*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (-161280*f[9])/593 - 
    2*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (591360*f[9])/593 + 
    (22*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/3, (-107520*f[9])/593 - 
    (4*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/3, (-107520*f[9])/593 - 
    (4*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/3, (6733440*f[9])/593 + 
    (167*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/2, 
   (-31*(80640*f[9] + 593*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 
        8*f[16] - 4*f[17] - 8*f[18] + f[19] - 8*f[22])))/7116, 
   (90720*f[9])/593 + (9*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 
       4*f[17] - 8*f[18] + f[19] - 8*f[22]))/8, 
   (-104160*f[9])/593 - (31*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 
       8*f[16] - 4*f[17] - 8*f[18] + f[19] - 8*f[22]))/24, 
   (-10080*f[9])/593 - f[10]/2 - f[11]/2 - (3*f[12])/2 - 2*f[13] - f[16] + 
    f[17]/2 + f[18] - f[19]/8 + f[22], (1088640*f[9])/593 + 
    (27*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/2, (-362880*f[9])/593 - 
    (9*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/2, (725760*f[9])/593 + 
    9*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (-161280*f[9])/593 - 
    2*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (1088640*f[9])/593 + 
    (27*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/2, (161280*f[9])/593 + 
    2*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (-1249920*f[9])/593 - 
    (31*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/2, (416640*f[9])/593 + 
    (31*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/6, (-1249920*f[9])/593 - 
    (31*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/2, (-80640*f[9])/593 - 4*f[10] - 4*f[11] - 
    12*f[12] - 16*f[13] - 8*f[16] + 4*f[17] + 8*f[18] - f[19] + 8*f[22], 
   (241920*f[9])/593 + 3*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 
      4*f[17] - 8*f[18] + f[19] - 8*f[22]), (40320*f[9])/593 + 2*f[10] + 
    2*f[11] + 6*f[12] + 8*f[13] + 4*f[16] - 2*f[17] - 4*f[18] + f[19]/2 - 
    4*f[22], (-147840*f[9])/593 - 
    (11*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/6, (564480*f[9])/593 + 
    7*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (483840*f[9])/593 + 
    6*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (362880*f[9])/593 + 
    (9*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
       8*f[18] + f[19] - 8*f[22]))/2, (-2177280*f[9])/593 - 
    27*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (2499840*f[9])/593 + 
    31*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22]), (241920*f[9])/593 + 
    3*(4*f[10] + 4*f[11] + 12*f[12] + 16*f[13] + 8*f[16] - 4*f[17] - 
      8*f[18] + f[19] - 8*f[22])}, {ipi^2, F[1, 1], F[1, 1]^2, F[1, 2], 
   F[1, 1]*F[1, 2], F[1, 3], F[1, 4], F[1, 2]*F[1, 3], F[1, 2]*F[1, 4], 
   F[1, 1]^3, F[1, 1]^2*F[1, 2], F[1, 1]^2*F[1, 3], F[1, 1]^2*F[1, 4], 
   F[1, 2]^2, F[1, 4]^2, F[2, 2], F[1, 3]^2, F[2, 1], F[1, 1]*F[1, 3], 
   F[2, 3], F[1, 1]*F[1, 4], F[3, 11], zeta3, ipi^2*F[1, 1], ipi^2*F[1, 2], 
   F[1, 1]*F[1, 2]^2, F[1, 2]^3, ipi^2*F[1, 3], F[1, 1]*F[1, 2]*F[1, 3], 
   F[1, 2]^2*F[1, 3], F[1, 1]*F[1, 3]^2, F[1, 2]*F[1, 3]^2, F[1, 3]^3, 
   F[1, 2]^2*F[1, 4], F[1, 1]*F[1, 4]^2, F[1, 2]*F[1, 4]^2, F[3, 1], F[3, 2], 
   F[3, 3], F[3, 5], F[3, 6], F[3, 8], F[3, 9], F[3, 12], F[3, 13], F[3, 4], 
   F[3, 7], F[3, 18], F[1, 1]*F[1, 2]*F[1, 4], F[1, 4]^3, F[3, 19], F[3, 20], 
   F[3, 14], F[3, 17], ipi^2*F[1, 4], F[3, 16], ipi^4, zeta3*F[1, 1], 
   ipi^2*F[1, 1]^2, F[1, 1]^4, zeta3*F[1, 2], ipi^2*F[1, 1]*F[1, 2], 
   F[1, 1]^3*F[1, 2], ipi^2*F[1, 2]^2, F[1, 1]^2*F[1, 2]^2, 
   F[1, 1]*F[1, 2]^3, F[1, 2]^4, zeta3*F[1, 3], ipi^2*F[1, 1]*F[1, 3], 
   F[1, 1]^3*F[1, 3], ipi^2*F[1, 2]*F[1, 3], F[1, 1]^2*F[1, 2]*F[1, 3], 
   F[1, 1]*F[1, 2]^2*F[1, 3], F[1, 2]^3*F[1, 3], ipi^2*F[1, 3]^2, 
   F[1, 1]^2*F[1, 3]^2, F[1, 1]*F[1, 2]*F[1, 3]^2, F[1, 2]^2*F[1, 3]^2, 
   F[1, 1]*F[1, 3]^3, F[1, 2]*F[1, 3]^3, F[1, 3]^4, zeta3*F[1, 4], 
   ipi^2*F[1, 4]^2, ipi^2*F[2, 1], ipi^2*F[2, 2], ipi^2*F[2, 3], F[4, 21], 
   F[4, 22], F[4, 25], F[4, 26], F[4, 27], F[4, 28], F[4, 32], F[4, 33], 
   F[4, 38], F[4, 39], F[4, 40], F[4, 41], F[4, 42], F[4, 43], F[4, 44], 
   F[4, 45], F[4, 65], F[4, 66], F[4, 67]}}}
