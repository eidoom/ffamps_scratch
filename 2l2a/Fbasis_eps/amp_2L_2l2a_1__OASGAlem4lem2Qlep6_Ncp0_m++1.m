(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{f[1] -> (2*ex[1]^3*ex[2]*(-1 - 2*ex[2]^2 + 2*ex[2]*(-2 + ex[3]) + ex[3]))/
    ((1 + ex[2])*(-1 + ex[3])), f[2] -> (ex[1]^3*(1 + ex[2] - ex[3]))/3, 
  f[3] -> (ex[1]^3*(8 + 9*ex[2] - 9*ex[3]))/6, 
  f[4] -> (ex[1]^3*(108 + (4 - 15*ex[3])/(1 + ex[2])^2 - 117*ex[3] + 
      (8*ex[2]^4*(5 + ex[3]))/(-1 + ex[3])^3 - (4*ex[2]^3*(11 + 5*ex[3]))/
       (-1 + ex[3])^2 - ex[2]*(-155 + 8*ex[3]) + (4*(3 + 8*ex[3]))/
       (1 + ex[2]) + (2*ex[2]^2*(2 - 27*ex[3] + 10*ex[3]^2))/(-1 + ex[3])^2))/
    12, f[5] -> 2*ex[1]^3*ex[2]*(1 + (2*ex[2]*ex[3])/(-1 + ex[3])^2 - 
     (1 + ex[2] + 2*ex[3] + 3*ex[2]*ex[3])/(1 + ex[2])^2), 
  f[6] -> -((ex[1]^3*ex[2]*(24*ex[2]^4 + ex[2]^3*(92 - 126*ex[3]) + 
       ex[2]^2*(81 - 180*ex[3] - 41*ex[3]^2) + 
       2*(9 + 32*ex[3] - 86*ex[3]^2 + 45*ex[3]^3) + 
       ex[2]*(31 + 63*ex[3] - 319*ex[3]^2 + 143*ex[3]^3)))/
     ((1 + ex[2])^2*(-1 + ex[3])^2)), 
  f[7] -> (ex[1]^3*(-120 + 113*ex[3] - (5*ex[3])/(1 + ex[2])^2 + 
      (16*ex[2]^3*(1 + 2*ex[3]))/(-1 + ex[3])^3 - (4 + 8*ex[3])/(1 + ex[2]) - 
      (2*ex[2]^2*(12 + 25*ex[3]))/(-1 + ex[3])^2 - (ex[2]*(-109 + 93*ex[3]))/
       (-1 + ex[3])))/2, f[8] -> 2*ex[1]^3*ex[2], 
  f[9] -> 2*ex[1]^3*ex[2]*(4 + 4/(1 + ex[2]) + (12*ex[2]^2)/(-1 + ex[3])^2 - 
     (13*ex[2])/(-1 + ex[3])), 
  f[10] -> (-2*ex[1]^3*ex[2]*(2 + 3*ex[2])*(1 + ex[2] - ex[3]))/
    (1 + ex[2])^2, f[11] -> (ex[1]^3*ex[2]*((81 - 89*ex[3])/(-1 + ex[3]) + 
      (10*ex[2]*(2 + ex[3]))/(-1 + ex[3])^2 - (16*ex[2]^2*(1 + 2*ex[3]))/
       (-1 + ex[3])^3 + (24 + 60*ex[3] + ex[2]*(28 + 93*ex[3]))/
       (1 + ex[2])^2))/2, 
  f[12] -> (2*ex[1]^3*ex[2]*(10 + 17*ex[2]^2 + ex[2]*(27 - 17*ex[3]) - 
      11*ex[3]))/(1 + ex[2])^2, 
  f[13] -> (ex[1]^3*((ex[2]^2*(-1 + 25*ex[3]))/(-1 + ex[3]) + 
      2*(7 - 8/(1 + ex[2]) + (4*(-5 - 6*ex[2] + ex[2]^2)*ex[3])/
         (1 + ex[2])^2 + 6*ex[3]^2) - (2*ex[2]*(-27 - 6*ex[3] + 17*ex[3]^2))/
       (-1 + ex[3])))/6, 
  f[14] -> (2*ex[1]^3*ex[2]*(7 + 10*ex[2]^2 + ex[2]*(17 - 10*ex[3]) - 
      8*ex[3]))/(1 + ex[2])^2, 
  f[15] -> ex[1]^3*(-2 + 3*ex[2]^2 - 20*ex[3] + 4*ex[3]^2 - 
     (2*ex[2]*(9 - 16*ex[3] + 3*ex[3]^2))/(-1 + ex[3])), 
  f[16] -> ex[1]^3*(-46 + 9*ex[2]^2 - 2/(1 + ex[2] - ex[3]) + 40*ex[3] + 
     (4*ex[3])/(1 + ex[2])^3 + 12*ex[3]^2 + (-2 + 6*ex[3])/(1 + ex[2])^2 - 
     (2 + 52*ex[3])/(1 + ex[2]) - (2*ex[2]*(-23 + 2*ex[3] + 9*ex[3]^2))/
      (-1 + ex[3])), f[17] -> 4*ex[1]^3*(7 - 9*ex[3] + ex[3]^2 + 
     (4*ex[2]^4*(1 + 2*ex[3]))/(-1 + ex[3])^4 - (ex[2]^3*(3 + 13*ex[3]))/
      (-1 + ex[3])^3 - (2*ex[2]*(3 - 5*ex[3] + ex[3]^2))/(-1 + ex[3]) + 
     (ex[2]^2*(1 + 4*ex[3] + ex[3]^2))/(-1 + ex[3])^2), 
  f[18] -> (-2*ex[1]^3*(1 + 2*ex[2] - 2*ex[3])*(ex[2] - ex[3])^2)/
    (3*(1 + ex[2] - ex[3])), 
  f[19] -> (2*ex[1]^3*(-18 - 2*ex[2]^2 + 19*ex[3] + (-1 - ex[2] + ex[3])^
       (-1) + ex[2]*(-21 + 2*ex[3])))/3, 
  f[20] -> (ex[1]^3*(13 + (8 - 15*ex[3])/(1 + ex[2])^2 + 
      (4*(-1 + ex[3]))/(1 + ex[2] - ex[3]) - 49*ex[3] - 
      (16*ex[2]^3*(1 + 2*ex[3]))/(-1 + ex[3])^3 + (2*ex[2]^2*(1 + 26*ex[3]))/
       (-1 + ex[3])^2 + (ex[2]*(-35 + 33*ex[3]))/(-1 + ex[3]) + 
      (-17 + 64*ex[3])/(1 + ex[2])))/8, 
  f[21] -> (ex[1]^3*(-34 - 9/(1 + ex[2]) + ((34 + 78*ex[2] + 53*ex[2]^2)*
        ex[3])/(1 + ex[2])^2 + (4*ex[2]^4*(5 + ex[3]))/(-1 + ex[3])^3 - 
      (2*ex[2]^2*(7 + 3*ex[3]))/(-1 + ex[3])^2 - (8*ex[2]^3*(-4 + ex[3]^2))/
       (-1 + ex[3])^3 + (ex[2]*(61 - 63*ex[3] + 4*ex[3]^2))/(-1 + ex[3])))/
    4}, {{(-67*f[1])/30 - (979*f[2])/2 + (18*f[2] - 4*f[3])/eps^2 + 48*f[3] + 
    6*f[4] - (287*f[5])/30 + (19*f[6])/30 - f[7]/3 - (97*f[8])/30 + 
    (19*f[9])/30 - (67*f[10])/150 - (209*f[12])/75 - (9*f[13])/4 + 
    (671*f[14])/300 + (9*f[15])/8 + (-220*f[1] + 60300*f[2] - 10200*f[3] + 
      1055*f[5] - 10*f[6] + 100*f[7] + 1080*f[8] - 10*f[9] - 3864*f[10] + 
      200*f[11] - 456*f[12] - 9*f[14] - 400*f[20])/(300*eps) - (10*f[20])/3 - 
    2*f[21], (87*f[1])/2 + 7008*f[2] - 828*f[3] - 72*f[4] + (117*f[5])/2 - 
    5*f[6] + 4*f[7] + 33*f[8] - 5*f[9] - (597*f[10])/10 + (106*f[12])/5 + 
    27*f[13] - (267*f[14])/10 - (27*f[15])/2 + 40*f[20] + 
    (220*f[1] - 47400*f[2] + 7200*f[3] - 1055*f[5] + 10*f[6] - 100*f[7] - 
      1080*f[8] + 10*f[9] + 3864*f[10] - 200*f[11] + 456*f[12] + 9*f[14] + 
      400*f[20])/(25*eps) + 24*f[21], -29*f[1] - 4926*f[2] + 630*f[3] + 
    36*f[4] - (165*f[5])/4 + (5*f[6])/2 - 5*f[7] - 16*f[8] + (5*f[9])/2 + 
    (353*f[10])/10 - (24*f[12])/5 - (27*f[13])/2 + (141*f[14])/20 + 
    (27*f[15])/4 + (-220*f[1] + 47400*f[2] - 7200*f[3] + 1055*f[5] - 
      10*f[6] + 100*f[7] + 1080*f[8] - 10*f[9] - 3864*f[10] + 200*f[11] - 
      456*f[12] - 9*f[14] - 400*f[20])/(50*eps) - 20*f[20] - 12*f[21], 
   (-288*f[1])/5 - 9852*f[2] + 1260*f[3] + 72*f[4] - (881*f[5])/10 + 
    (26*f[6])/5 - 10*f[7] - (153*f[8])/5 + (26*f[9])/5 + (2397*f[10])/25 - 
    (212*f[12])/25 - 27*f[13] + (1039*f[14])/50 + (27*f[15])/2 + 
    (-220*f[1] + 47400*f[2] - 7200*f[3] + 1055*f[5] - 10*f[6] + 100*f[7] + 
      1080*f[8] - 10*f[9] - 3864*f[10] + 200*f[11] - 456*f[12] - 9*f[14] - 
      400*f[20])/(25*eps) - 40*f[20] - 24*f[21], 
   (-780*f[1] - 47400*f[2] + 7200*f[3] + 145*f[5] + 10*f[6] - 100*f[7] + 
      1920*f[8] + 10*f[9] - 2846*f[10] + 300*f[11] - 84*f[12] + 149*f[14] - 
      1000*f[20])/50 + (-220*f[1] + 47400*f[2] - 7200*f[3] + 1055*f[5] - 
      10*f[6] + 100*f[7] + 1080*f[8] - 10*f[9] - 2394*f[10] + 200*f[11] - 
      276*f[12] + 111*f[14] - 400*f[20])/(50*eps), 
   (220*f[1] - 47400*f[2] + 7200*f[3] - 1055*f[5] + 10*f[6] - 100*f[7] - 
      1080*f[8] + 10*f[9] + 3864*f[10] - 200*f[11] + 456*f[12] + 9*f[14] + 
      400*f[20])/(50*eps) + (980*f[1] + 47400*f[2] - 7200*f[3] - 145*f[5] - 
      10*f[6] + 100*f[7] - 1920*f[8] - 10*f[9] + 6546*f[10] - 300*f[11] + 
      484*f[12] + 301*f[14] + 1000*f[20])/50, (19*f[1])/5 - 1989*f[2] + 
    (12*(9*f[2] - 2*f[3]))/eps^2 + 144*f[3] + 36*f[4] - (547*f[5])/10 + 
    (17*f[6])/5 - (296*f[8])/5 + (17*f[9])/5 + (2379*f[10])/25 - 6*f[11] - 
    (284*f[12])/25 - (27*f[13])/2 + (1290*f[2] - 300*f[3] - 49*f[10] - 
      6*f[12] - 4*f[14])/(5*eps) + (424*f[14])/25 + (27*f[15])/4 - 12*f[21], 
   (940*f[1] + 397800*f[2] - 48600*f[3] - 3600*f[4] + 4415*f[5] - 230*f[6] + 
     300*f[7] + 5540*f[8] - 230*f[9] - 16622*f[10] + 600*f[11] - 488*f[12] + 
     1350*f[13] - 1307*f[14] - 675*f[15] + 1200*f[21])/100, 
   (-56*f[1])/5 - 5079*f[2] - (144*(9*f[2] - 2*f[3]))/eps + 612*f[3] + 
    (973*f[5])/10 - (63*f[6])/5 - 20*f[7] - (916*f[8])/5 - (63*f[9])/5 + 
    (18439*f[10])/25 - 40*f[11] + (2656*f[12])/25 + (39*f[13])/2 - 
    (641*f[14])/25 + (33*f[15])/4 - 4*f[16] + 36*f[18] + 42*f[19] + 80*f[20], 
   (-1835*f[1] + 89100*f[2] - 14400*f[3] + 12990*f[5] - 580*f[6] + 
     3390*f[8] - 630*f[9] - 9262*f[10] - 548*f[12] + 750*f[13] - 1297*f[14] - 
     375*f[15] + 300*f[17] + 1500*f[18] + 1500*f[19])/100, 
   (876*(9*f[2] - 2*f[3]))/eps + (297480*f[1] + 9428700*f[2] + 847200*f[3] - 
      1345920*f[5] + 112440*f[6] + 119400*f[7] + 1120480*f[8] + 112440*f[9] - 
      4293554*f[10] + 238800*f[11] - 688716*f[12] - 81450*f[13] + 
      209926*f[14] - 19275*f[15] - 298200*f[18] - 224700*f[19] - 
      477600*f[20])/900, (18*(9*f[2] - 2*f[3]))/eps + 
    (9570*f[1] + 1464300*f[2] - 220800*f[3] - 39480*f[5] + 3860*f[6] + 
      4800*f[7] + 41620*f[8] + 3860*f[9] - 176286*f[10] + 9600*f[11] - 
      27344*f[12] - 2850*f[13] + 7134*f[14] - 75*f[15] - 9450*f[18] - 
      9000*f[19] - 19200*f[20])/1800, (-107*f[1])/30 + (6203*f[2])/2 + 
    (36*(9*f[2] - 2*f[3]))/eps - (1426*f[3])/3 + (511*f[5])/60 + 
    (227*f[6])/90 + (22*f[7])/3 + (3182*f[8])/45 + (227*f[9])/90 - 
    (18271*f[10])/75 + (44*f[11])/3 - (7352*f[12])/225 - (53*f[13])/12 + 
    (133*f[14])/25 + (29*f[15])/24 - 6*f[18] - 4*f[19] - (88*f[20])/3, 
   (-252*f[2] + 56*f[3])/eps + (-6540*f[1] - 742380*f[2] + 75120*f[3] + 
      27750*f[5] - 2780*f[6] - 3600*f[7] - 33120*f[8] - 2780*f[9] + 
      129028*f[10] - 7200*f[11] + 19832*f[12] + 1950*f[13] - 5432*f[14] + 
      345*f[15] + 7080*f[18] + 5880*f[19] + 14400*f[20])/720, 
   (23*f[1])/2 - (13065*f[2])/2 - (84*(9*f[2] - 2*f[3]))/eps + 990*f[3] - 
    (51*f[5])/4 - (13*f[6])/2 - 18*f[7] - 170*f[8] - (13*f[9])/2 + 
    (2903*f[10])/5 - 36*f[11] + (392*f[12])/5 + (57*f[13])/4 - (72*f[14])/5 - 
    (33*f[15])/8 + 21*f[18] + 18*f[19] + 72*f[20], 
   (84*f[1])/5 + 3945*f[2] + (120*(9*f[2] - 2*f[3]))/eps - 468*f[3] - 
    (551*f[5])/5 + (62*f[6])/5 + 18*f[7] + (824*f[8])/5 + (62*f[9])/5 - 
    (14846*f[10])/25 + 36*f[11] - (2234*f[12])/25 - (33*f[13])/2 + 
    (1223*f[14])/50 + (9*f[15])/4 - 36*f[18] - 30*f[19] - 72*f[20], 
   (-432*f[2] + 96*f[3])/eps + (-6330*f[1] - 331500*f[2] + 12000*f[3] + 
      31695*f[5] - 2590*f[6] - 2700*f[7] - 24480*f[8] - 2590*f[9] + 
      90404*f[10] - 5400*f[11] + 14716*f[12] + 2550*f[13] - 4951*f[14] - 
      75*f[15] + 6600*f[18] + 5100*f[19] + 10800*f[20])/450, 
   (-432*f[2] + 96*f[3])/eps + (-10635*f[1] - 1694400*f[2] + 204600*f[3] + 
      46740*f[5] - 5030*f[6] - 6900*f[7] - 63510*f[8] - 5030*f[9] + 
      254943*f[10] - 13800*f[11] + 38472*f[12] + 4050*f[13] - 8992*f[14] + 
      325*f[15] + 13725*f[18] + 10350*f[19] + 27600*f[20])/900, 
   (51*f[1])/10 - 7401*f[2] - (120*(9*f[2] - 2*f[3]))/eps + 1104*f[3] + 
    (18*f[5])/5 - (41*f[6])/5 - 20*f[7] - (942*f[8])/5 - (41*f[9])/5 + 
    (33731*f[10])/50 - 40*f[11] + (2312*f[12])/25 + (27*f[13])/2 - 
    (789*f[14])/50 - (11*f[15])/4 + 24*f[18] + 18*f[19] + 80*f[20], 
   (13*f[1])/5 + 11664*f[2] + (216*(9*f[2] - 2*f[3]))/eps - 1632*f[3] - 
    (517*f[5])/5 + (104*f[6])/5 + 40*f[7] + (1818*f[8])/5 + (104*f[9])/5 - 
    (31772*f[10])/25 + 80*f[11] - (4538*f[12])/25 - 36*f[13] + 
    (1093*f[14])/25 + 8*f[15] - 63*f[18] - 54*f[19] - 160*f[20], 
   -33*f[1] - (6869*f[2])/2 - (132*(9*f[2] - 2*f[3]))/eps + 314*f[3] + 
    (741*f[5])/4 - (101*f[6])/6 - 20*f[7] - 174*f[8] - (101*f[9])/6 + 
    (6393*f[10])/10 - 40*f[11] + (506*f[12])/5 + (81*f[13])/4 - 
    (493*f[14])/15 - (59*f[15])/24 + 42*f[18] + 36*f[19] + 80*f[20], 
   (7*f[1])/5 + 5151*f[2] + (96*(9*f[2] - 2*f[3]))/eps - 732*f[3] - 
    (341*f[5])/10 + (41*f[6])/5 + 16*f[7] + (712*f[8])/5 + (41*f[9])/5 - 
    (13168*f[10])/25 + 32*f[11] - (1872*f[12])/25 - (27*f[13])/2 + 
    (367*f[14])/25 + (11*f[15])/4 - 24*f[18] - 18*f[19] - 64*f[20], 
   (-118*f[1])/5 - (10955*f[2])/2 - (132*(9*f[2] - 2*f[3]))/eps + 638*f[3] + 
    (2943*f[5])/20 - (499*f[6])/30 - 24*f[7] - (1058*f[8])/5 - 
    (499*f[9])/30 + (38479*f[10])/50 - 48*f[11] + (2908*f[12])/25 + 
    (87*f[13])/4 - (2489*f[14])/75 - (77*f[15])/24 + 42*f[18] + 36*f[19] + 
    96*f[20], (-216*f[2] + 48*f[3])/eps + 
    (-528*f[1] - 70044*f[2] + 6096*f[3] + 3222*f[5] - 308*f[6] - 384*f[7] - 
      3120*f[8] - 308*f[9] + 12444*f[10] - 768*f[11] + 1920*f[12] + 
      414*f[13] - 520*f[14] - 71*f[15] + 720*f[18] + 576*f[19] + 1536*f[20])/
     72, (-69*f[1])/5 - 923*f[2] + 100*f[3] + (-216*f[2] + 48*f[3])/eps + 
    (507*f[5])/10 - (61*f[6])/15 - 4*f[7] - (154*f[8])/5 - (61*f[9])/15 + 
    (3726*f[10])/25 - 8*f[11] + (604*f[12])/25 + (3*f[13])/2 - 
    (532*f[14])/75 + (7*f[15])/12 + 9*f[18] + 6*f[19] + 16*f[20], 
   (-16*f[1])/5 - 4710*f[2] - (96*(9*f[2] - 2*f[3]))/eps + 624*f[3] + 
    (129*f[5])/5 - (38*f[6])/5 - 16*f[7] - (796*f[8])/5 - (38*f[9])/5 + 
    (14599*f[10])/25 - 32*f[11] + (2046*f[12])/25 + 9*f[13] - 
    (356*f[14])/25 - f[15]/2 + 24*f[18] + 18*f[19] + 64*f[20], 
   (154*f[1])/15 + 2385*f[2] + (56*(9*f[2] - 2*f[3]))/eps - 268*f[3] - 
    (1297*f[5])/30 + (29*f[6])/5 + (28*f[7])/3 + (1354*f[8])/15 + 
    (29*f[9])/5 - (25856*f[10])/75 + (56*f[11])/3 - (1258*f[12])/25 - 
    (9*f[13])/2 + (263*f[14])/25 - (5*f[15])/12 - 15*f[18] - 10*f[19] - 
    (112*f[20])/3, (-2*f[1])/5 - 2079*f[2] + 276*f[3] + 
    (-216*f[2] + 48*f[3])/eps + (211*f[5])/10 - (21*f[6])/5 - 8*f[7] - 
    (352*f[8])/5 - (21*f[9])/5 + (6043*f[10])/25 - 16*f[11] + 
    (872*f[12])/25 + (15*f[13])/2 - (242*f[14])/25 - (7*f[15])/4 + 12*f[18] + 
    12*f[19] + 32*f[20], (-432*f[2] + 96*f[3])/eps + 
    (2*(220*f[1] - 52200*f[2] + 8400*f[3] - 1005*f[5] + 10*f[6] - 100*f[7] - 
       1180*f[8] + 10*f[9] + 3914*f[10] - 200*f[11] + 456*f[12] + 9*f[14] + 
       400*f[20]))/25, 52*f[1] - 3702*f[2] + (72*(9*f[2] - 2*f[3]))/eps + 
    648*f[3] - 198*f[5] + 8*f[6] - 4*f[7] - 40*f[8] + 8*f[9] + 
    (312*f[10])/5 - 8*f[11] - (32*f[12])/5 + 3*f[13] + (57*f[14])/5 - 
    (11*f[15])/2 - 18*f[18] - 12*f[19] + 16*f[20], 
   (-2*(220*f[1] - 47400*f[2] + 7200*f[3] - 1055*f[5] + 10*f[6] - 100*f[7] - 
      1080*f[8] + 10*f[9] + 3864*f[10] - 200*f[11] + 456*f[12] + 9*f[14] + 
      400*f[20]))/25, (220*f[1] - 47400*f[2] + 7200*f[3] - 1055*f[5] + 
     10*f[6] - 100*f[7] - 1080*f[8] + 10*f[9] + 3864*f[10] - 200*f[11] + 
     456*f[12] + 9*f[14] + 150*f[18] + 400*f[20])/75, 
   (188*f[1])/5 + (8349*f[2])/2 + (132*(9*f[2] - 2*f[3]))/eps - 426*f[3] - 
    (3453*f[5])/20 + (163*f[6])/10 + 20*f[7] + (888*f[8])/5 + (163*f[9])/10 - 
    (35059*f[10])/50 + 40*f[11] - (2718*f[12])/25 - (57*f[13])/4 + 
    (748*f[14])/25 + f[15]/8 - 39*f[18] - 30*f[19] - 80*f[20], 
   (-36*f[2] + 8*f[3])/eps + (-3615*f[1] + 787800*f[2] - 175200*f[3] + 
      16785*f[5] - 820*f[6] - 4240*f[8] - 820*f[9] + 4067*f[10] + 
      1968*f[12] - 1773*f[14] + 1100*f[15] + 2550*f[18] + 1950*f[19])/900, 
   (-41*f[1])/10 + (2205*f[2])/2 - 214*f[3] + (-108*f[2] + 24*f[3])/eps + 
    (273*f[5])/20 - (19*f[6])/30 - (64*f[8])/15 - (19*f[9])/30 + 
    (917*f[10])/25 + (454*f[12])/75 - (5*f[13])/4 - (21*f[14])/50 + 
    (13*f[15])/8 + 6*f[18] + 6*f[19], (24*(9*f[2] - 2*f[3]))/eps + 
    (1110*f[1] - 93900*f[2] + 20400*f[3] - 3740*f[5] + 180*f[6] + 460*f[8] + 
      180*f[9] - 3568*f[10] - 772*f[12] + 150*f[13] + 242*f[14] - 175*f[15] - 
      450*f[18] - 300*f[19])/50, (-24*f[1])/5 + (553*f[2])/2 - 90*f[3] + 
    (-108*f[2] + 24*f[3])/eps + (99*f[5])/20 - (7*f[6])/30 - (54*f[8])/5 - 
    (7*f[9])/30 + (6101*f[10])/150 + (452*f[12])/75 - (13*f[13])/4 + 
    f[14]/25 + (47*f[15])/24 + 2*f[18] - 2*f[19], 
   (-26*f[1])/5 + (2325*f[2])/2 + (12*(9*f[2] - 2*f[3]))/eps - 226*f[3] + 
    (591*f[5])/20 - (43*f[6])/30 + (122*f[8])/15 - (43*f[9])/30 - 
    (1127*f[10])/50 - (62*f[12])/75 + (7*f[13])/4 - (81*f[14])/25 + f[15]/8 + 
    6*f[18] + 6*f[19], (-108*f[2] + 24*f[3])/eps + 
    (-2520*f[1] + 77700*f[2] - 32400*f[3] + 5430*f[5] - 260*f[6] - 
      3120*f[8] - 260*f[9] + 9836*f[10] + 1744*f[12] - 1050*f[13] - 
      384*f[14] + 725*f[15] + 1200*f[18] - 1200*f[19])/600, 
   (12*(9*f[2] - 2*f[3]))/eps + (-2640*f[1] + 575700*f[2] - 127200*f[3] + 
      12810*f[5] - 620*f[6] + 2560*f[8] - 620*f[9] - 15008*f[10] - 
      1032*f[12] + 450*f[13] - 1848*f[14] + 175*f[15] + 2400*f[18] + 
      1200*f[19])/900, (-66*f[1])/5 + 1687*f[2] - 348*f[3] + (673*f[5])/10 - 
    (49*f[6])/15 + (14*f[8])/5 - (49*f[9])/15 - (2548*f[10])/75 + 
    (58*f[12])/75 + (5*f[13])/2 - (196*f[14])/25 + (17*f[15])/12 + 7*f[18] + 
    8*f[19], (2760*f[1] - 318900*f[2] + 73200*f[3] - 11990*f[5] + 580*f[6] - 
     40*f[8] + 580*f[9] + 3952*f[10] - 392*f[12] - 150*f[13] + 1312*f[14] - 
     325*f[15] - 900*f[18] - 600*f[19])/300, (41*f[1])/5 + 81*f[2] - 
    68*f[3] - (403*f[5])/10 + (29*f[6])/15 - (182*f[8])/15 + (29*f[9])/15 + 
    (571*f[10])/25 + (2*f[12])/75 - (5*f[13])/2 + (101*f[14])/25 + 
    (5*f[15])/4 - 3*f[18] - 6*f[19], (-18*f[1])/5 - 353*f[2] + 116*f[3] + 
    (707*f[5])/30 - (17*f[6])/15 + (106*f[8])/15 - (17*f[9])/15 - 
    (1684*f[10])/75 - (86*f[12])/75 + (5*f[13])/2 - (68*f[14])/25 - 
    (5*f[15])/4 + f[18] + 4*f[19], -2*f[18], 
   (35*f[1] + 585*f[5] + 5*f[6] + 210*f[8] - 70*f[9] - 6978*f[10] - 
      987*f[12] - 568*f[14])/25 + (35*f[5] - 344*f[10] - 46*f[12] - 34*f[14])/
     (5*eps) + (2*(5*f[5] - 44*f[10] - 6*f[12] - 4*f[14]))/(5*eps^2), 
   12*f[1] - 9*f[8] + 3*f[9] + (2743*f[10])/10 + (171*f[12])/5 + 
    (109*f[14])/5 + (2*(49*f[10] + 6*f[12] + 4*f[14]))/(5*eps^2) + 
    (20*f[1] + 327*f[10] + 38*f[12] + 32*f[14])/(5*eps), 
   2*f[1] + 9*f[8] - 3*f[9] - (2743*f[10])/10 - (171*f[12])/5 + 
    (-327*f[10] - 38*f[12] - 32*f[14])/(5*eps) - (109*f[14])/5 - 
    (2*(49*f[10] + 6*f[12] + 4*f[14]))/(5*eps^2), 
   (-108*f[2] + 24*f[3])/eps^2 + (-1290*f[2] + 300*f[3] - 5*f[5] + 44*f[10] + 
      6*f[12] + 4*f[14])/(5*eps) + (130*f[1] + 85500*f[2] - 9000*f[3] + 
      3105*f[5] - 110*f[6] + 300*f[7] + 280*f[8] - 110*f[9] + 2636*f[10] + 
      144*f[12] + 591*f[14])/100, (24*(9*f[2] - 2*f[3]))/eps^2 + 
    (615*f[1] + 56700*f[2] - 12600*f[3] - 1835*f[5] + 120*f[6] - 360*f[8] + 
      120*f[9] - 5857*f[10] - 1028*f[12] - 367*f[14])/50 + 
    (4*(645*f[2] - 150*f[3] + 5*f[5] - 44*f[10] - 6*f[12] - 4*f[14]))/
     (5*eps), (-2*(49*f[10] + 6*f[12] + 4*f[14]))/(5*eps) - 
    (2*(43*f[10] + 2*f[12] + 13*f[14]))/5, 
   (705*f[1] + 142200*f[2] - 21600*f[3] + 1480*f[5] - 10*f[6] + 300*f[7] - 
     20*f[8] - 10*f[9] - 2449*f[10] - 796*f[12] + 456*f[14])/50, 
   (-108*f[2] + 24*f[3])/eps^2 + (-258*f[2] + 60*f[3])/eps + 
    (-1320*f[1] - 198900*f[2] + 34200*f[3] + 1055*f[5] - 110*f[6] - 
      300*f[7] + 380*f[8] - 110*f[9] + 1426*f[10] + 904*f[12] - 769*f[14])/
     100, (48*(9*f[2] - 2*f[3]))/eps + (60*f[1] + 11100*f[2] - 2400*f[3] + 
      410*f[5] - 20*f[6] + 560*f[8] - 20*f[9] - 3408*f[10] - 432*f[12] + 
      150*f[13] - 148*f[14] - 75*f[15])/25, 18*f[1] - (3927*f[2])/2 + 
    (12*(9*f[2] - 2*f[3]))/eps + 438*f[3] - (289*f[5])/4 + (7*f[6])/2 + 
    8*f[8] + (7*f[9])/2 - (67*f[10])/10 - (34*f[12])/5 + (3*f[13])/4 + 
    (39*f[14])/5 - (27*f[15])/8 - 9*f[18] - 6*f[19], 
   (-108*f[2] + 24*f[3])/eps + (-480*f[1] - 278100*f[2] + 66000*f[3] + 
      2970*f[5] - 140*f[6] + 1920*f[8] - 140*f[9] - 8836*f[10] - 944*f[12] + 
      450*f[13] - 616*f[14] - 425*f[15] - 600*f[18])/200, 
   -7*f[1] + 9*f[8] + (2*f[8])/eps + 3*f[9] + (442*f[10])/5 + (68*f[12])/5 + 
    (17*f[14])/5, (-72*f[2] + 16*f[3])/eps - (2*(96*f[2] + 4*f[8] + 3*f[19]))/
     3, (-10*f[1] + 140*f[5] - 5*f[6] + 15*f[8] - 5*f[9] - 632*f[10] - 
     28*f[12] - 167*f[14])/25, (60*f[1] - 14700*f[2] + 3600*f[3] + 410*f[5] - 
     20*f[6] + 560*f[8] - 20*f[9] - 2428*f[10] - 312*f[12] + 150*f[13] - 
     68*f[14] - 75*f[15])/50, (-60*f[1] + 14700*f[2] - 3600*f[3] - 410*f[5] + 
     20*f[6] - 560*f[8] + 20*f[9] + 2428*f[10] + 312*f[12] - 150*f[13] + 
     68*f[14] + 75*f[15])/50, (-108*f[2] + 24*f[3])/eps + 
    (-60*f[1] - 11100*f[2] + 2400*f[3] - 410*f[5] + 20*f[6] - 560*f[8] + 
      20*f[9] + 2428*f[10] + 312*f[12] - 150*f[13] + 68*f[14] + 75*f[15])/
     100, (-108*f[2] + 24*f[3])/eps + (-60*f[1] - 11100*f[2] + 2400*f[3] - 
      410*f[5] + 20*f[6] - 560*f[8] + 20*f[9] + 2428*f[10] + 312*f[12] - 
      150*f[13] + 68*f[14] + 75*f[15])/100, (178767*f[2] - 35492*f[3])/480, 
   -20505*f[2] + (12734*f[3])/3, -51*f[2] + (41*f[3])/6, 
   (-7401*f[2])/8 + (2305*f[3])/12, (72273*f[2])/4 - (21211*f[3])/6, 
   240*f[2] - (133*f[3])/3, (9213*f[2])/4 - (2855*f[3])/6, 
   (-27*(177*f[2] - 34*f[3]))/32, (-59391*f[2])/16 + (6167*f[3])/8, 
   (20289*f[2])/8 - (6331*f[3])/12, (-5133*f[2])/8 + (1603*f[3])/12, 
   (1613889*f[2] - 343666*f[3])/48, (-2451*f[2])/8 + (925*f[3])/12, 
   (25455*f[2])/8 - (7837*f[3])/12, (-3285*f[2] + 586*f[3])/16, 
   (-53271*f[2])/8 + (5487*f[3])/4, (116253*f[2] - 24106*f[3])/16, 
   (-120195*f[2] + 24982*f[3])/48, (6147*f[2] - 1798*f[3])/96, 
   (-54981*f[2])/16 + (5461*f[3])/8, (51129*f[2])/8 - (5249*f[3])/4, 
   (-28611*f[2] + 5926*f[3])/8, (31203*f[2] - 4342*f[3])/48, 
   (-96165*f[2] + 19642*f[3])/48, (-3369*f[2])/16 + (799*f[3])/24, 
   (92997*f[2] - 4058*f[3])/48, (-9*(195*f[2] - 38*f[3]))/8, 
   (-2637*f[2] + 538*f[3])/8, (12195*f[2] - 2422*f[3])/48, 
   (9*(171*f[2] - 34*f[3]))/2, (-10053*f[2])/16 + (973*f[3])/8, 
   (441*f[2])/2 - 43*f[3], (2385*f[2] - 754*f[3])/32, 
   (3*(1467*f[2] - 278*f[3]))/16, (-10053*f[2])/16 + (973*f[3])/8, 
   (3*(441*f[2] - 86*f[3]))/4, (-3015*f[2] + 574*f[3])/16, 
   (441*f[2])/2 - 43*f[3], (-25263*f[2] + 5014*f[3])/48, 
   (-739*(9*f[2] - 2*f[3]))/48, (711*f[2] - 254*f[3])/24, 
   18*(9*f[2] - 2*f[3]), (-123*(9*f[2] - 2*f[3]))/2, 
   (383*(9*f[2] - 2*f[3]))/16, (-4503*(9*f[2] - 2*f[3]))/16, 
   (751*(9*f[2] - 2*f[3]))/8, (-1213*(9*f[2] - 2*f[3]))/8, 
   (-1021*(9*f[2] - 2*f[3]))/8, -369*(9*f[2] - 2*f[3]), 51*(9*f[2] - 2*f[3]), 
   (-3073*(9*f[2] - 2*f[3]))/8, (-9*(799*f[2] - 174*f[3]))/4, 
   72*f[2] - 12*f[3], (-675*f[2])/2 + 63*f[3], (-1017*f[2])/16 + (97*f[3])/8, 
   (-9*(99*f[2] + 74*f[3]))/16, (2001*f[2] - 242*f[3])/8, 
   (-7173*f[2])/8 + (685*f[3])/4, (-6597*f[2])/8 + (637*f[3])/4, 
   -783*f[2] + 138*f[3], 171*f[2] - 30*f[3], (-5193*f[2] + 578*f[3])/8, 
   (739*(9*f[2] - 2*f[3]))/2, -711*f[2] + 254*f[3], 
   (185*(9*f[2] - 2*f[3]))/48, (223*(9*f[2] - 2*f[3]))/8, 
   (-281*(9*f[2] - 2*f[3]))/16, (57*(9*f[2] - 2*f[3]))/16, 
   (-119*(9*f[2] - 2*f[3]))/4, (155*(9*f[2] - 2*f[3]))/8, 
   (59*(9*f[2] - 2*f[3]))/16, (-15*(9*f[2] - 2*f[3]))/8, 
   (-45*f[2])/2 + 5*f[3], (59*(9*f[2] - 2*f[3]))/16, 
   (57*(9*f[2] - 2*f[3]))/16, (-29*(9*f[2] - 2*f[3]))/24, 
   (27*f[2])/2 - 3*f[3], (3*(9*f[2] - 2*f[3]))/4, -27*f[2] + 6*f[3], 
   -54*f[2] + 12*f[3], -162*f[2] + 36*f[3], (-33*(9*f[2] - 2*f[3]))/4, 
   (3*(9*f[2] - 2*f[3]))/8, (-243*f[2])/2 + 27*f[3], 
   (201*(9*f[2] - 2*f[3]))/4, (15*(9*f[2] - 2*f[3]))/2, 6*(9*f[2] - 2*f[3]), 
   (15*(9*f[2] - 2*f[3]))/2, (-39*(9*f[2] - 2*f[3]))/4, 
   (-27*f[2])/2 + 3*f[3], (-27*f[2])/2 + 3*f[3], (-81*f[2])/2 + 9*f[3], 
   (-135*f[2])/2 + 15*f[3], -198*f[2] + 44*f[3], 22*(9*f[2] - 2*f[3]), 
   (-589*(9*f[2] - 2*f[3]))/8, (-493*(9*f[2] - 2*f[3]))/8, 
   (-349*(9*f[2] - 2*f[3]))/8, 27*f[2] - 6*f[3], (-297*f[2])/2 + 33*f[3], 
   27*f[2] - 6*f[3], (-337*(9*f[2] - 2*f[3]))/8, (-301*(9*f[2] - 2*f[3]))/8, 
   -78*(9*f[2] - 2*f[3]), 12*(9*f[2] - 2*f[3]), (-459*f[2])/2 + 51*f[3], 
   (-157*(9*f[2] - 2*f[3]))/8, (-109*(9*f[2] - 2*f[3]))/8, -27*f[2] + 6*f[3], 
   (-81*f[2])/2 + 9*f[3], 27*f[2] - 6*f[3], (-193*(9*f[2] - 2*f[3]))/8, 
   (-169*(9*f[2] - 2*f[3]))/8, -108*f[2] + 24*f[3], -108*f[2] + 24*f[3], 
   29*(9*f[2] - 2*f[3]), (45*f[1])/2 + (2*f[1])/eps^2 + (7*f[1])/eps}, 
  {ipi^2, F[1, 1]*F[1, 2], F[1, 2]^2, F[1, 2]*F[1, 3], F[1, 3]^2, F[2, 1], 
   F[1, 4]^2, F[2, 2], F[3, 6], F[3, 11], zeta3, ipi^2*F[1, 1], F[1, 1]^3, 
   ipi^2*F[1, 2], F[1, 1]^2*F[1, 2], F[1, 1]*F[1, 2]^2, F[1, 2]^3, 
   ipi^2*F[1, 3], F[1, 1]^2*F[1, 3], F[1, 1]*F[1, 2]*F[1, 3], 
   F[1, 2]^2*F[1, 3], F[1, 1]*F[1, 3]^2, F[1, 2]*F[1, 3]^2, F[1, 3]^3, 
   F[3, 1], F[3, 2], F[3, 3], F[3, 4], F[3, 5], F[3, 7], F[3, 15], F[3, 17], 
   F[3, 18], ipi^2*F[1, 4], F[1, 1]^2*F[1, 4], F[1, 1]*F[1, 2]*F[1, 4], 
   F[1, 2]^2*F[1, 4], F[1, 1]*F[1, 4]^2, F[1, 2]*F[1, 4]^2, F[1, 4]^3, 
   F[3, 8], F[3, 9], F[3, 12], F[3, 13], F[3, 14], F[1, 1], F[1, 3], F[1, 4], 
   F[1, 1]^2, F[1, 1]*F[1, 3], F[1, 3]*F[1, 4], F[1, 1]*F[1, 4], F[2, 3], 
   F[3, 10], F[3, 19], F[3, 20], F[1, 2], F[3, 16], F[1, 2]*F[1, 4], 
   F[1, 1]*F[1, 3]*F[1, 4], F[1, 2]*F[1, 3]*F[1, 4], F[1, 3]^2*F[1, 4], 
   F[1, 3]*F[1, 4]^2, ipi^4, zeta3*F[1, 1], ipi^2*F[1, 1]^2, F[1, 1]^4, 
   zeta3*F[1, 2], ipi^2*F[1, 1]*F[1, 2], F[1, 1]^3*F[1, 2], ipi^2*F[1, 2]^2, 
   F[1, 1]^2*F[1, 2]^2, F[1, 1]*F[1, 2]^3, F[1, 2]^4, zeta3*F[1, 3], 
   ipi^2*F[1, 1]*F[1, 3], F[1, 1]^3*F[1, 3], ipi^2*F[1, 2]*F[1, 3], 
   F[1, 1]^2*F[1, 2]*F[1, 3], F[1, 1]*F[1, 2]^2*F[1, 3], F[1, 2]^3*F[1, 3], 
   ipi^2*F[1, 3]^2, F[1, 1]^2*F[1, 3]^2, F[1, 1]*F[1, 2]*F[1, 3]^2, 
   F[1, 2]^2*F[1, 3]^2, F[1, 1]*F[1, 3]^3, F[1, 2]*F[1, 3]^3, F[1, 3]^4, 
   zeta3*F[1, 4], ipi^2*F[1, 1]*F[1, 4], F[1, 1]^3*F[1, 4], 
   ipi^2*F[1, 3]*F[1, 4], F[1, 1]^2*F[1, 3]*F[1, 4], 
   F[1, 1]*F[1, 3]^2*F[1, 4], F[1, 3]^3*F[1, 4], ipi^2*F[1, 4]^2, 
   F[1, 1]^2*F[1, 4]^2, F[1, 1]*F[1, 3]*F[1, 4]^2, F[1, 3]^2*F[1, 4]^2, 
   F[1, 1]*F[1, 4]^3, F[1, 3]*F[1, 4]^3, F[1, 4]^4, ipi^2*F[2, 1], 
   ipi^2*F[2, 2], F[4, 18], F[4, 19], F[4, 20], F[4, 21], F[4, 22], F[4, 23], 
   F[4, 24], F[4, 25], F[4, 26], F[4, 27], F[4, 28], F[4, 29], F[4, 30], 
   F[4, 31], F[4, 32], F[4, 33], F[4, 34], F[4, 35], F[4, 36], F[4, 37], 
   F[4, 38], F[4, 65], F[4, 66], ipi^2*F[1, 2]*F[1, 4], 
   F[1, 1]^2*F[1, 2]*F[1, 4], F[1, 1]*F[1, 2]^2*F[1, 4], F[1, 2]^3*F[1, 4], 
   F[1, 1]*F[1, 2]*F[1, 3]*F[1, 4], F[1, 2]^2*F[1, 3]*F[1, 4], 
   F[1, 2]*F[1, 3]^2*F[1, 4], F[1, 1]*F[1, 2]*F[1, 4]^2, F[1, 2]^2*F[1, 4]^2, 
   F[1, 2]*F[1, 3]*F[1, 4]^2, F[1, 2]*F[1, 4]^3, ipi^2*F[2, 3], F[4, 1], 
   F[4, 2], F[4, 3], F[4, 4], F[4, 5], F[4, 6], F[4, 7], F[4, 8], F[4, 9], 
   F[4, 10], F[4, 12], F[4, 13], F[4, 14], F[4, 15], F[4, 16], F[4, 17], 
   F[4, 40], F[4, 41], F[4, 42], F[4, 43], F[4, 44], F[4, 45], F[4, 46], 
   F[4, 47], F[4, 48], F[4, 49], F[4, 50], F[4, 51], F[4, 53], F[4, 54], 
   F[4, 56], F[4, 57], F[4, 58], F[4, 59], F[4, 60], F[4, 61], F[4, 62], 
   F[4, 63], F[4, 64], F[4, 67], 1}}}
