(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{f[1] -> (1 + ex[2])/(1 + ex[2] - ex[3]), 
  f[2] -> (7 + 3*ex[2])/(2 + 2*ex[2] - 2*ex[3]), 
  f[3] -> (ex[2]*(ex[2] - ex[3]))/(3*(1 + ex[2] - ex[3])^2), 
  f[4] -> (-2*(-1 + ex[2]*(-3 + ex[3]) + ex[3]))/((1 + ex[2] - ex[3])*
     (-1 + ex[3])), f[5] -> 6 + (4*(-1 + ex[3]))/(1 + ex[2] - ex[3]) + 
    (2*ex[3])/(ex[2] - ex[3]), f[6] -> -3 - ex[3]/(1 + ex[2] - ex[3])}, 
 {{(-9494*f[1])/81 - (4*f[1])/(3*eps^3) - (56*f[1])/(9*eps^2) + (62*f[2])/9 + 
    ((-832*f[1])/27 + (4*f[2])/3)/eps, (-4*(f[1] - f[2] - 3*f[3]))/(9*eps) + 
    (-16*f[1] + 40*f[2] + 300*f[3] - 9*f[4] - 27*f[5] - 12*f[6])/54, 
   (-8*f[1])/(3*eps) + (-106*f[1] + 3*(-4*f[2] + f[4] + 3*f[5] + 4*f[6]))/9, 
   (2*(910*f[1] - 260*f[2] - 99*f[5]))/9 + (4*(28*f[1] - 8*f[2] - 3*f[5]))/
     (3*eps), (8*(5*f[1] - 2*(f[2] + 3*f[3])))/(3*eps) - 
    (2*(53*f[1] - 2*f[2] + 300*f[3] + 3*f[4] - 36*f[5]))/9, 
   (4*(-5*f[1] + 2*f[2] + 6*f[3]))/(3*eps) + 
    (2*(94*f[1] - 16*f[2] + 150*f[3] - 27*f[5]))/9, 
   (16*f[1])/(3*eps) + (2*(79*f[1] + 3*(6*f[2] + f[4] - 4*f[6])))/9, 
   (-8*(5*f[1] - 2*(f[2] + 3*f[3])))/(3*eps) + 
    (2*(80*f[1] - 8*f[2] + 300*f[3] - 3*f[4] - 45*f[5]))/9, 
   (4*(-7*f[1] + 2*f[2] + 6*f[3]))/(3*eps) + 
    (-368*f[1] + 88*f[2] + 300*f[3] - 3*f[4] - 9*f[5])/9, 
   (-4*(7*f[1] - 2*f[2]))/3, -6*f[1] + (4*f[2])/3 + (4*f[4])/3 + 2*f[5], 
   (4*(5*f[1] - 2*(f[2] + 3*f[3])))/(3*eps) + 
    (256*f[1] - 88*f[2] - 300*f[3] + 3*f[4] + 9*f[5])/9, 
   (8*f[1])/(3*eps) + (4*(16*f[1] - 3*f[6]))/9, 
   (-8*f[1])/(3*eps) + (44*f[1] + 3*(-8*f[2] + f[4] - 3*f[5] + 4*f[6]))/9, 
   (2*(36*f[1] - 8*f[2] + f[4] - 3*f[5]))/3, -12*f[1] + (8*f[2])/3 - f[4]/3 + 
    f[5], (4*(8*f[1] + f[4]))/(3*eps) + (2*(260*f[1] + 31*f[4]))/9, 
   (-4*(263*f[1] + 50*(f[2] + 3*f[3])))/9, (4*(5*f[1] + 2*f[2] + 6*f[3]))/9, 
   (-16*(f[1] - f[2] - 3*f[3]))/3, (-8*(f[1] + 2*f[2] + 6*f[3]))/3, 
   (8*(3*f[1] + 2*f[2] + 6*f[3]))/9, (-32*(f[2] + 3*f[3]))/3, 
   (16*(4*f[1] + 3*f[2] + 9*f[3]))/9, (16*(4*f[1] + 3*f[2] + 9*f[3]))/9, 
   (-16*(2*f[1] - 3*(f[2] + 3*f[3])))/27, (-8*(3*f[1] + 2*f[2] + 6*f[3]))/9, 
   (-8*(7*f[1] - 2*(f[2] + 3*f[3])))/3, (-16*(f[1] + f[2] + 3*f[3]))/3, 
   (-1412*f[1])/27 - (8*f[1])/(3*eps^2) - (112*f[1])/(9*eps), (-4*f[1])/3, 
   (-8*f[1])/9, (176*f[1])/27, (16*f[1])/3, (-16*f[1])/3, (52*f[1])/27, 
   (8*f[1])/3, (-16*f[1])/3, (8*f[1])/9, (8*f[1])/3, (8*f[1])/9, 
   (80*f[1])/27, (32*f[1])/9, (32*f[1])/3, (16*f[1])/3, 16*f[1], (16*f[1])/9, 
   (-16*f[1])/9, (16*f[1])/3, (-16*f[1])/9, (-8*f[1])/3, -8*f[1]}, 
  {1, ipi^2, F[1, 1]^2, F[1, 2], F[1, 1]*F[1, 2], F[1, 2]^2, F[1, 1]*F[1, 3], 
   F[1, 2]*F[1, 3], F[1, 3]^2, F[1, 4], F[1, 1]*F[1, 4], F[2, 1], F[1, 4]^2, 
   F[2, 3], F[1, 2]*F[1, 4], F[2, 2], F[1, 1], zeta3, ipi^2*F[1, 2], 
   F[1, 1]^2*F[1, 2], F[1, 1]*F[1, 2]^2, F[1, 2]^3, F[1, 1]*F[1, 2]*F[1, 3], 
   F[1, 2]^2*F[1, 3], F[1, 2]*F[1, 3]^2, F[1, 3]^3, F[3, 3], F[3, 7], 
   F[3, 18], F[1, 3], ipi^2*F[1, 1], F[1, 1]^3, ipi^2*F[1, 3], 
   F[1, 1]^2*F[1, 3], F[1, 1]*F[1, 3]^2, ipi^2*F[1, 4], F[1, 1]^2*F[1, 4], 
   F[1, 1]*F[1, 2]*F[1, 4], F[1, 2]^2*F[1, 4], F[1, 1]*F[1, 4]^2, 
   F[1, 2]*F[1, 4]^2, F[1, 4]^3, F[3, 1], F[3, 2], F[3, 4], F[3, 6], F[3, 8], 
   F[3, 9], F[3, 11], F[3, 13], F[3, 19], F[3, 20]}}}
