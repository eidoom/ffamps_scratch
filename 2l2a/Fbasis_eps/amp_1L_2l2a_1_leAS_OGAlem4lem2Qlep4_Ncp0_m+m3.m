(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{f[1] -> -8/(3*(1 + ex[2] - ex[3]))}, 
 {{(5*f[1])/3 + f[1]/eps + (28*eps*f[1])/9 + (164*eps^2*f[1])/27, 
   -f[1] - (5*eps*f[1])/3 - (28*eps^2*f[1])/9, f[1] + (5*eps*f[1])/3 + 
    (28*eps^2*f[1])/9, (eps*f[1])/12 + (5*eps^2*f[1])/36, 
   (eps*f[1])/2 + (5*eps^2*f[1])/6, -(eps*f[1]) - (5*eps^2*f[1])/3, 
   (eps*f[1])/2 + (5*eps^2*f[1])/6, (-7*eps^2*f[1])/3, -1/12*(eps^2*f[1]), 
   -1/6*(eps^2*f[1]), (eps^2*f[1])/12, (eps^2*f[1])/2, -1/2*(eps^2*f[1]), 
   (eps^2*f[1])/6}, {1, F[1, 1], F[1, 3], ipi^2, F[1, 1]^2, F[1, 1]*F[1, 3], 
   F[1, 3]^2, zeta3, ipi^2*F[1, 1], F[1, 1]^3, ipi^2*F[1, 3], 
   F[1, 1]^2*F[1, 3], F[1, 1]*F[1, 3]^2, F[1, 3]^3}}}
