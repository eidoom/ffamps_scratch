(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{f[1] -> ex[1]^3*ex[2]*(ex[2] - ex[3]), 
  f[2] -> (ex[1]^3*ex[2]*(-4 + 3*ex[2] - 3*ex[3]))/2, 
  f[3] -> (ex[1]^3*(1 + ex[2])*(1 + ex[2] - ex[3]))/3, 
  f[4] -> (2*ex[1]^3*ex[2]^2*(-3 + 2*ex[3]))/(-1 + ex[3]), 
  f[5] -> ex[1]^3*ex[2]*(-3*ex[2] + ex[3]), 
  f[6] -> (2*ex[1]^3*ex[2]*(-2 - 2*ex[2]^2 + 2*ex[2]*(-2 + ex[3]) + ex[3]))/
    (1 + ex[2])}, {{(-9494*f[1])/81 - (4*f[1])/(3*eps^3) - 
    (56*f[1])/(9*eps^2) + (62*f[2])/9 + ((-832*f[1])/27 + (4*f[2])/3)/eps, 
   (4*(21*f[1] - 6*f[2] - 2*f[4] - 4*f[5] + 3*f[6]))/(3*eps) + 
    (2*(735*f[1] - 210*f[2] - 62*f[4] - 124*f[5] + 99*f[6]))/9, 
   (-4*(f[1] - f[2] - 3*f[3]))/(9*eps) + 
    (11*f[1] + 22*f[2] + 3*(100*f[3] + 6*f[4] + 8*f[5] + 9*f[6]))/54, 
   (4*(5*f[1] - 2*(f[2] + 3*f[3])))/(3*eps) + 
    (121*f[1] - 46*f[2] - 300*f[3] + 42*f[4] + 84*f[5] - 27*f[6])/9, 
   (-4*(7*f[1] - 2*f[2]))/3, (-5936*f[1])/27 - (8*f[1])/(3*eps^2) + 
    (544*f[2])/9 - 22*f[6] - (4*(94*f[1] - 24*f[2] + 9*f[6]))/(9*eps), 
   (-8*(5*f[1] - 2*(f[2] + 3*f[3])))/(3*eps) - 
    (4*(29*f[1] - 14*f[2] + 3*(-50*f[3] + 5*f[4] + 10*f[5] - 6*f[6])))/9, 
   (16*f[1])/(3*eps) + (2*(f[1] + 18*f[2] + 6*f[4] - 9*f[6]))/9, 
   (-125*f[1] + 38*f[2] + 2*f[4] - 15*f[6])/3, 
   (4*(22*f[1] - 8*f[2] + 3*f[6]))/(3*eps) + 
    (2*(712*f[1] - 260*f[2] + 99*f[6]))/9, 
   (2*(21*f[1] - 6*f[2] + 4*f[4] + 8*f[5] + 3*f[6]))/3, 
   (2*(21*f[1] - 6*f[2] - 2*f[4] - 4*f[5] + 3*f[6]))/3, 
   (4*(-5*f[1] + 2*f[2] + 6*f[3]))/(3*eps) + 
    (2*(40*f[1] - 16*f[2] + 150*f[3] + 27*f[6]))/9, 
   -1/9*f[1] - (8*f[1])/(3*eps) - 2*f[2] - (2*f[4])/3 + f[6], 
   -7*f[1] + 2*f[2] + (2*f[4])/3 + (4*f[5])/3 - f[6], 
   (4*(5*f[1] - 2*(f[2] + 3*f[3])))/(3*eps) + 
    (247*f[1] - 82*f[2] - 3*(100*f[3] + 2*f[4] + 4*f[5] + 3*f[6]))/9, 
   (-16*f[1])/(3*eps) - (4*(32*f[1] + 9*f[4] + 12*f[5]))/9, 
   (8*f[1])/(3*eps) + (4*(16*f[1] - 3*f[5]))/9, 
   (1348*f[1])/3 - 184*(f[2] + 3*f[3]), (2*(5*f[1] - 2*(f[2] + 3*f[3])))/3, 
   (4*(5*f[1] - 2*(f[2] + 3*f[3])))/9, (-92*f[1] + 40*(f[2] + 3*f[3]))/9, 
   (-44*f[1])/3 + 8*(f[2] + 3*f[3]), (8*(13*f[1] - 6*(f[2] + 3*f[3])))/3, 
   -16*f[1] + (64*(f[2] + 3*f[3]))/9, (-2*(199*f[1] - 82*(f[2] + 3*f[3])))/
    27, (-8*(5*f[1] - 2*(f[2] + 3*f[3])))/3, 
   56*f[1] - (80*(f[2] + 3*f[3]))/3, (-4*(103*f[1] - 46*(f[2] + 3*f[3])))/9, 
   (8*(5*f[1] - 2*(f[2] + 3*f[3])))/3, (-4*(103*f[1] - 46*(f[2] + 3*f[3])))/
    9, (-68*(5*f[1] - 2*(f[2] + 3*f[3])))/27, 
   (-26*(5*f[1] - 2*(f[2] + 3*f[3])))/27, (4*(-5*f[1] + 2*f[2] + 6*f[3]))/3, 
   (8*(5*f[1] - 2*(f[2] + 3*f[3])))/3, (-4*(5*f[1] - 2*(f[2] + 3*f[3])))/9, 
   (4*(-5*f[1] + 2*f[2] + 6*f[3]))/3, (-4*(5*f[1] - 2*(f[2] + 3*f[3])))/9, 
   (-40*(5*f[1] - 2*(f[2] + 3*f[3])))/27, (-16*(5*f[1] - 2*(f[2] + 3*f[3])))/
    9, (-16*(5*f[1] - 2*(f[2] + 3*f[3])))/3, 
   16*f[1] - (64*(f[2] + 3*f[3]))/9, (-8*(5*f[1] - 2*(f[2] + 3*f[3])))/3, 
   8*(-5*f[1] + 2*f[2] + 6*f[3]), (8*(7*f[1] - 2*(f[2] + 3*f[3])))/3, 
   (-8*(5*f[1] - 2*(f[2] + 3*f[3])))/9, (8*(5*f[1] - 2*(f[2] + 3*f[3])))/9, 
   (-8*(5*f[1] - 2*(f[2] + 3*f[3])))/3, (8*(5*f[1] - 2*(f[2] + 3*f[3])))/9, 
   (4*(31*f[1] - 14*(f[2] + 3*f[3])))/3, (4*(5*f[1] - 2*(f[2] + 3*f[3])))/3, 
   20*f[1] - 8*(f[2] + 3*f[3])}, {1, F[1, 1], ipi^2, F[1, 1]^2, F[1, 2], 
   F[1, 3], F[1, 1]*F[1, 3], F[1, 2]*F[1, 3], F[1, 3]^2, F[1, 4], 
   F[1, 1]*F[1, 4], F[1, 2]*F[1, 4], F[1, 4]^2, F[2, 1], F[2, 2], F[2, 3], 
   F[1, 1]*F[1, 2], F[1, 2]^2, zeta3, ipi^2*F[1, 1], F[1, 1]^3, 
   ipi^2*F[1, 2], F[1, 1]^2*F[1, 2], F[1, 1]*F[1, 2]^2, F[1, 2]^3, 
   ipi^2*F[1, 3], F[1, 1]^2*F[1, 3], F[1, 1]*F[1, 2]*F[1, 3], 
   F[1, 2]^2*F[1, 3], F[1, 1]*F[1, 3]^2, F[1, 2]*F[1, 3]^2, F[1, 3]^3, 
   ipi^2*F[1, 4], F[1, 1]^2*F[1, 4], F[1, 1]*F[1, 2]*F[1, 4], 
   F[1, 2]^2*F[1, 4], F[1, 1]*F[1, 4]^2, F[1, 2]*F[1, 4]^2, F[1, 4]^3, 
   F[3, 1], F[3, 2], F[3, 3], F[3, 4], F[3, 6], F[3, 7], F[3, 8], F[3, 9], 
   F[3, 11], F[3, 13], F[3, 18], F[3, 19], F[3, 20]}}}
