(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{f[1] -> (-32*ex[1]^3*ex[2]*(ex[2] - ex[3]))/9}, 
 {{9*f[1] + f[1]/eps^2 + (10*f[1])/(3*eps), (-20*f[1])/3 - (2*f[1])/eps, 
   (20*f[1])/3 + (2*f[1])/eps, f[1]/6, 2*f[1], -4*f[1], 2*f[1]}, 
  {1, F[1, 1], F[1, 3], ipi^2, F[1, 1]^2, F[1, 1]*F[1, 3], F[1, 3]^2}}}
