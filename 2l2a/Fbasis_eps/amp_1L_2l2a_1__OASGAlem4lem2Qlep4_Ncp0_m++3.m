(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{f[1] -> -(ex[1]^3*ex[2]), 
  f[2] -> -((ex[1]^3*ex[2]*(1 + 2*ex[2]))/(1 + ex[2])), 
  f[3] -> -1/6*(ex[1]^3*(1 + ex[2] - ex[3])), 
  f[4] -> -1/12*(ex[1]^3*(ex[2]^2 - 5*ex[2]*(-1 + ex[3]) + 2*(-1 + ex[3])^2))/
     (1 + ex[2] - ex[3]), 
  f[5] -> (ex[1]^3*ex[2]*(1 + ex[2] + 2*ex[3] + 3*ex[2]*ex[3]))/
    (1 + ex[2])^2, f[6] -> (ex[1]^3*ex[2]*(1 - ex[2]^2 + 4*ex[3] + 
      7*ex[2]*ex[3]))/(1 + ex[2])^2, 
  f[7] -> (ex[1]^3*ex[2]^2)/(ex[2] - ex[3])}, 
 {{2*(f[1] - f[2] - 2*f[5] + f[6]) + 4*eps^2*(2*f[1] - 2*f[2] - 7*f[5] + 
      3*f[6]) + eps*(4*f[1] - 4*f[2] - 14*f[5] + 6*f[6]), 
   -6*f[1] - (4*f[1])/eps + 2*f[2] + 4*f[5] + 
    eps*(-8*f[1] + 4*f[2] + 14*f[5] - 6*f[6]) - 2*f[6] - 
    4*eps^2*(4*f[1] - 2*f[2] - 7*f[5] + 3*f[6]), 
   -2*eps*(5*f[1] + f[2] + 7*f[5] - 3*f[6]) - 
    4*eps^2*(5*f[1] + f[2] + 7*f[5] - 3*f[6]) - 2*(f[2] + 2*f[5] - f[6]), 
   eps^2*(2*f[1] + 2*f[2] + 12*f[3] + 7*f[5] - 3*f[6]) + 
    eps*(f[1] + f[2] + 12*f[3] + 2*f[5] - f[6]), 
   -2*eps^2*(2*f[1] + 2*f[2] + 12*f[3] + 7*f[5] - 3*f[6]) - 
    2*eps*(f[1] + f[2] + 12*f[3] + 2*f[5] - f[6]), 
   eps^2*(f[1] + 2*f[2] + 12*f[3] - 12*f[4] + 7*f[5] - 3*f[6]) + 
    eps*(-f[1] + f[2] + 2*f[5] - f[6]), 
   eps*(-2*f[1] - f[2] - 12*f[3] - 2*f[5] + f[6]) + 
    eps^2*(-9*f[1] - f[2] - 12*f[3] - 7*f[5] + 3*f[6]), 
   (eps^2*(-f[1] - f[2] - 12*f[3] - 2*f[5] + f[6]))/6, 
   (eps^2*(-f[1] - f[2] - 12*f[3] - 2*f[5] + f[6]))/3, 
   (eps^2*(-f[1] + f[2] + 2*f[5] - f[6]))/6, 
   eps^2*(f[1] + f[2] + 12*f[3] + 2*f[5] - f[6]), 
   eps^2*(-f[1] - f[2] - 12*f[3] - 2*f[5] + f[6]), 
   (eps^2*(-f[1] + f[2] + 2*f[5] - f[6]))/3, 
   (eps^2*(-2*f[1] - f[2] - 12*f[3] - 2*f[5] + f[6]))/6, 
   (eps^2*(-2*f[1] - f[2] - 12*f[3] - 2*f[5] + f[6]))/3, 
   eps*(-1/2*f[1] - 2*f[3]) + (eps^2*(-8*f[1] + f[2] - 12*f[4]))/6, 
   -4*f[1] - 4*eps*f[1] - 2*eps^2*(5*f[1] + 12*f[3] - 12*f[4] + f[7]), 
   2*f[1] + 2*eps*f[1] + eps^2*(5*f[1] + 12*f[3] - 12*f[4] + f[7]), 
   4*f[1] + 4*eps*f[1] + 2*eps^2*(5*f[1] + 12*f[3] - 12*f[4] + f[7]), 
   -2*f[1] - 2*eps*f[1] + eps^2*(-5*f[1] + 12*(-f[3] + f[4])), 
   14*eps^2*(f[1] + 4*f[3]), (eps*f[1])/3 + (eps^2*f[1])/3, 
   2*eps*f[1] + 2*eps^2*f[1], -2*eps*f[1] - 2*eps^2*f[1], 
   (2*eps*f[1])/3 + (2*eps^2*f[1])/3, -4*eps*f[1] - 4*eps^2*f[1], 
   2*eps*f[1] + 2*eps^2*f[1], 2*eps*f[1] + 2*eps^2*f[1], 
   -2*eps*f[1] - 2*eps^2*f[1], (-4*f[1])/eps^2 - (6*f[1])/eps + 
    2*(-9*f[1] + f[2]) + 4*eps*(-9*f[1] + f[2]) + 8*eps^2*(-9*f[1] + f[2]), 
   4*eps^2*(f[1] + 3*f[3]) + 2*eps*(f[1] + 6*f[3]), (-28*eps^2*f[1])/3, 
   -1/3*(eps^2*f[1]), (-2*eps^2*f[1])/3, (eps^2*f[1])/6, eps^2*f[1], 
   (-2*eps^2*f[1])/3, (eps^2*f[1])/6, (eps^2*f[1])/3, 2*eps^2*f[1], 
   -2*eps^2*f[1], (2*eps^2*f[1])/3, -2*eps^2*f[1], eps^2*f[1], 
   (2*eps^2*f[1])/3, 2*eps^2*(f[1] + 6*f[3]), -2*eps^2*f[1], 0, 0, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 2*eps*f[7] + 4*eps^2*f[7]}, 
  {F[1, 1], F[1, 3], F[1, 4], F[1, 1]^2, F[1, 1]*F[1, 3], F[1, 3]^2, 
   F[1, 4]^2, ipi^2*F[1, 1], F[1, 1]^3, ipi^2*F[1, 3], F[1, 1]^2*F[1, 3], 
   F[1, 1]*F[1, 3]^2, F[1, 3]^3, ipi^2*F[1, 4], F[1, 4]^3, ipi^2, 
   F[1, 1]*F[1, 2], F[1, 2]^2, F[1, 2]*F[1, 3], F[2, 1], zeta3, 
   ipi^2*F[1, 2], F[1, 1]^2*F[1, 2], F[1, 1]*F[1, 2]^2, F[1, 2]^3, 
   F[1, 1]*F[1, 2]*F[1, 3], F[1, 2]^2*F[1, 3], F[1, 2]*F[1, 3]^2, F[3, 18], 
   1, F[2, 3], zeta3*F[1, 2], ipi^2*F[1, 1]*F[1, 2], F[1, 1]^3*F[1, 2], 
   ipi^2*F[1, 2]^2, F[1, 1]^2*F[1, 2]^2, F[1, 1]*F[1, 2]^3, F[1, 2]^4, 
   ipi^2*F[1, 2]*F[1, 3], F[1, 1]^2*F[1, 2]*F[1, 3], 
   F[1, 1]*F[1, 2]^2*F[1, 3], F[1, 2]^3*F[1, 3], F[1, 1]*F[1, 2]*F[1, 3]^2, 
   F[1, 2]^2*F[1, 3]^2, F[1, 2]*F[1, 3]^3, F[3, 20], F[4, 65], ipi^4, 
   zeta3*F[1, 1], ipi^2*F[1, 1]^2, F[1, 1]^4, ipi^2*F[1, 1]*F[1, 3], 
   F[1, 1]^3*F[1, 3], F[1, 1]^2*F[1, 3]^2, F[1, 1]*F[1, 3]^3, zeta3*F[1, 4], 
   ipi^2*F[1, 4]^2, F[1, 4]^4, F[4, 67], F[1, 2]}}}
