(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{f[1] -> (1 + ex[2])/(1 + ex[2] - ex[3]), f[2] -> (1 + ex[2] - ex[3])^(-1), 
  f[3] -> (ex[2]*(ex[2] - ex[3]))/(6*(1 + ex[2] - ex[3])^2), 
  f[4] -> (-1 + ex[2]*(-3 + ex[3]) + ex[3])/((-1 + ex[3])*
     (-1 - ex[2] + ex[3])), 
  f[5] -> 3 + (2*(-1 + ex[3]))/(1 + ex[2] - ex[3]) + ex[3]/(ex[2] - ex[3]), 
  f[6] -> -1/6}, {{2*(8*f[1] - 8*f[2] - 3*f[5]) + 
    8*eps*(5*f[1] - 5*f[2] - 2*f[5]) + 16*eps^2*(5*f[1] - 5*f[2] - 2*f[5]), 
   4*(f[1] - 2*(f[2] + 3*f[3])) - 4*eps^2*(5*f[1] - 5*f[2] + 12*f[3] - 
      4*f[5]) + eps*(-4*f[1] + 4*f[2] + 6*(-4*f[3] + f[5])), 
   -2*f[1] + 4*(f[2] + 3*f[3]) + 2*eps^2*(5*f[1] - 5*f[2] + 12*f[3] - 
      4*f[5]) + eps*(2*f[1] - 2*f[2] + 12*f[3] - 3*f[5]), 
   -4*f[1] + 8*(f[2] + 3*f[3]) + eps*(4*f[1] - 4*f[2] + 24*f[3] - 6*f[5]) + 
    4*eps^2*(5*f[1] - 5*f[2] + 12*f[3] - 4*f[5]), 
   (eps*(-f[1] + 2*f[2] + 6*f[3]))/3 + 
    (eps^2*(2*f[1] - 2*f[2] + 12*f[3] - 3*f[5]))/6, 
   eps*(-2*f[1] + 4*(f[2] + 3*f[3])) + eps^2*(2*f[1] - 2*f[2] + 12*f[3] - 
      3*f[5]), 2*eps*(f[1] - 2*(f[2] + 3*f[3])) + 
    eps^2*(-2*f[1] + 2*f[2] + 3*(-4*f[3] + f[5])), 
   (-2*eps*(f[1] - 2*(f[2] + 3*f[3])))/3 + 
    eps^2*((2*f[1])/3 - (2*f[2])/3 + 4*f[3] - f[5]), 
   4*eps*(f[1] - 2*(f[2] + 3*f[3])) + 
    eps^2*(-4*f[1] + 4*f[2] + 6*(-4*f[3] + f[5])), 
   eps*(-2*f[1] + 4*(f[2] + 3*f[3])) + eps^2*(2*f[1] - 2*f[2] + 12*f[3] - 
      3*f[5]), eps*(-2*f[1] + 4*(f[2] + 3*f[3])) + 
    eps^2*(2*f[1] - 2*f[2] + 12*f[3] - 3*f[5]), (-f[1] + 2*f[2] + 6*f[3])/3 + 
    eps^2*((-19*f[1])/6 + (11*f[2])/6 + 4*f[3] - 6*f[6]) + 
    eps*((-3*f[1])/2 + f[2] + 2*f[3] - 2*f[6]), 
   -2*f[1] - 2*eps^2*(5*f[1] + f[4] - 18*f[6]) + 
    eps*(-4*f[1] - f[4] + 12*f[6]), 
   4*f[1] + 4*eps^2*(5*f[1] + f[4] - 18*f[6]) + 
    2*eps*(4*f[1] + f[4] - 12*f[6]), 
   2*f[1] + eps^2*(-f[1] + f[2] - 36*f[6]) - 12*eps*f[6], 
   -2*f[1] + 12*eps*f[6] + 36*eps^2*f[6], 
   (28*eps*(f[1] - 2*(f[2] + 3*f[3])))/3 + 
    14*eps^2*(3*f[1] - 2*f[2] - 4*f[3] + 4*f[6]), 
   (eps*f[1])/3 + (eps^2*(4*f[1] + f[4] - 12*f[6]))/6, 
   (2*eps*f[1])/3 + (eps^2*(4*f[1] + f[4] - 12*f[6]))/3, 
   -2*eps*f[1] + eps^2*(-4*f[1] - f[4] + 12*f[6]), 
   2*eps*f[1] + eps^2*(4*f[1] + f[4] - 12*f[6]), (eps*f[1])/3 - 2*eps^2*f[6], 
   (2*eps*f[1])/3 - 4*eps^2*f[6], -2*eps*f[1] + 12*eps^2*f[6], 
   4*(-f[1] + f[2] + 3*f[3]) + eps^2*(-18*f[1] + 10*f[2] + 24*f[3]) + 
    eps*(-9*f[1] + 6*(f[2] + 2*f[3])), 2*eps^2*(5*f[1] - 5*f[2] - 12*f[3]) + 
    6*eps*(f[1] - f[2] - 2*f[3]) + 2*(f[1] - 2*(f[2] + 3*f[3])), 
   eps^2*((-3*f[1])/2 + f[2] + 2*f[3]) + (2*eps*(-f[1] + f[2] + 3*f[3]))/3, 
   (-4*eps*(f[1] - f[2] - 3*f[3]))/3 + eps^2*(-3*f[1] + 2*f[2] + 4*f[3]), 
   6*eps^2*(f[1] - f[2] - 2*f[3]) + 2*eps*(f[1] - 2*(f[2] + 3*f[3])), 
   (47*eps^2*(f[1] - 2*(f[2] + 3*f[3])))/360, 
   (28*eps^2*(f[1] - 2*(f[2] + 3*f[3])))/3, 
   (eps^2*(f[1] - 2*(f[2] + 3*f[3])))/3, (2*eps^2*(f[1] - 2*(f[2] + 3*f[3])))/
    3, eps^2*(-1/6*f[1] + f[2]/3 + f[3]), eps^2*(-f[1] + 2*f[2] + 6*f[3]), 
   (2*eps^2*(f[1] - 2*(f[2] + 3*f[3])))/3, eps^2*(-1/6*f[1] + f[2]/3 + f[3]), 
   (56*eps^2*(f[1] - f[2] - 3*f[3]))/3, (eps^2*(-f[1] + 2*f[2] + 6*f[3]))/3, 
   eps^2*(-2*f[1] + 4*(f[2] + 3*f[3])), 2*eps^2*(f[1] - 2*(f[2] + 3*f[3])), 
   (-2*eps^2*(f[1] - 2*(f[2] + 3*f[3])))/3, (eps^2*(-f[1] + f[2] + 3*f[3]))/
    3, 2*eps^2*(f[1] - 2*(f[2] + 3*f[3])), eps^2*(-f[1] + 2*f[2] + 6*f[3]), 
   (-2*eps^2*(f[1] - 2*(f[2] + 3*f[3])))/3, (eps^2*(-f[1] + f[2] + 3*f[3]))/
    3, 2*eps^2*(f[1] - 2*(f[2] + 3*f[3])), (-4*f[1])/eps^2 - (6*f[1])/eps + 
    2*(-9*f[1] + f[2]) + 4*eps*(-9*f[1] + f[2]) + 8*eps^2*(-9*f[1] + f[2]), 
   -6*f[1] - (4*f[1])/eps - 16*eps*f[1] - 32*eps^2*f[1], 
   2*(4*f[1] + f[4]) + 4*eps*(5*f[1] + f[4]) + 8*eps^2*(5*f[1] + f[4]), 
   (-28*eps^2*f[1])/3, -1/6*(eps^2*f[1]), -1/6*(eps^2*f[1]), (eps^2*f[1])/3, 
   (2*eps^2*f[1])/3, -(eps^2*f[1]), (2*eps^2*f[1])/3, (-28*eps^2*f[1])/3, 
   (eps^2*f[1])/6, (eps^2*f[1])/6, -2*eps^2*f[1], 
   eps*(-2*f[1] + 2*f[2]) + eps^2*(-4*f[1] + 4*f[2])}, 
  {F[1, 2], F[1, 1]*F[1, 2], F[1, 2]^2, F[1, 2]*F[1, 3], ipi^2*F[1, 2], 
   F[1, 1]^2*F[1, 2], F[1, 1]*F[1, 2]^2, F[1, 2]^3, F[1, 1]*F[1, 2]*F[1, 3], 
   F[1, 2]^2*F[1, 3], F[1, 2]*F[1, 3]^2, ipi^2, F[1, 1]^2, F[1, 1]*F[1, 3], 
   F[1, 4]^2, F[2, 3], zeta3, ipi^2*F[1, 1], F[1, 1]^3, F[1, 1]^2*F[1, 3], 
   F[1, 1]*F[1, 3]^2, ipi^2*F[1, 4], F[1, 4]^3, F[3, 20], F[1, 3]^2, F[2, 1], 
   ipi^2*F[1, 3], F[1, 3]^3, F[3, 18], ipi^4, zeta3*F[1, 2], 
   ipi^2*F[1, 1]*F[1, 2], F[1, 1]^3*F[1, 2], ipi^2*F[1, 2]^2, 
   F[1, 1]^2*F[1, 2]^2, F[1, 1]*F[1, 2]^3, F[1, 2]^4, zeta3*F[1, 3], 
   ipi^2*F[1, 2]*F[1, 3], F[1, 1]^2*F[1, 2]*F[1, 3], 
   F[1, 1]*F[1, 2]^2*F[1, 3], F[1, 2]^3*F[1, 3], ipi^2*F[1, 3]^2, 
   F[1, 1]*F[1, 2]*F[1, 3]^2, F[1, 2]^2*F[1, 3]^2, F[1, 2]*F[1, 3]^3, 
   F[1, 3]^4, F[4, 65], 1, F[1, 3], F[1, 1], zeta3*F[1, 1], ipi^2*F[1, 1]^2, 
   F[1, 1]^4, ipi^2*F[1, 1]*F[1, 3], F[1, 1]^3*F[1, 3], F[1, 1]^2*F[1, 3]^2, 
   F[1, 1]*F[1, 3]^3, zeta3*F[1, 4], ipi^2*F[1, 4]^2, F[1, 4]^4, F[4, 67], 
   F[1, 4]}}}
