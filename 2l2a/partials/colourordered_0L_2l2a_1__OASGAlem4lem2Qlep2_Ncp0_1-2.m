OrderedDiagrams=Join[OrderedDiagrams,{1,2}];
ColourOrderedDiagram[1]=INT[Kprop[le[1,p[1]+p[3]]]*Kvrtx[LE[-2,p[1]],GA[-6,p[3]],le[1,-p[1]-p[3]]]*Kvrtx[LE[2,p[1]+p[3]],AS[-8,-p[1]-p[2]-p[3]],le[-4,p[2]]]*pol[AS[-8,p[4]]]*pol[GA[-6,p[3]]]*pol[le[-2,p[1]]]*pol[LE[-4,p[2]]],{},topo[]];
ColourOrderedDiagram[2]=INT[Kprop[le[1,-p[2]-p[3]]]*Kvrtx[LE[-2,p[1]],AS[-8,-p[1]-p[2]-p[3]],le[1,p[2]+p[3]]]*Kvrtx[LE[2,-p[2]-p[3]],GA[-6,p[3]],le[-4,p[2]]]*pol[AS[-8,p[4]]]*pol[GA[-6,p[3]]]*pol[le[-2,p[1]]]*pol[LE[-4,p[2]]],{},topo[]];
