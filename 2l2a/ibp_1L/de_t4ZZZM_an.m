(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{{0, 0, -(eps/s12), 0}, {0, 0, 0, 0}, {0, 0, -(eps/s12), 0}, 
  {(-2*eps)/(-s12 + s4), (2*eps)/(-s12 - s23 + s4), 
   (2*eps)/(-s12 - s23 + s4), (eps*s23 - eps*s4)/(-s12^2 - s12*s23 + 
     s12*s4)}}, {{0, 0, 0, 0}, {eps/s23, -(eps/s23), -(eps/s23), 0}, 
  {0, 0, 0, 0}, {0, (2*eps*s12)/(s12*s23 + s23^2 - s12*s4 - 2*s23*s4 + s4^2), 
   (2*eps)/(-s12 - s23 + s4), (eps*s12 - eps*s4)/(-(s12*s23) - s23^2 + 
     s23*s4)}}, {{-(eps/s4), 0, eps/s4, 0}, {-(eps/s4), 0, eps/s4, 0}, 
  {0, 0, 0, 0}, {(2*eps*s12)/(-(s12*s4) + s4^2), 
   (-2*eps*s12)/(s12*s23 + s23^2 - s12*s4 - 2*s23*s4 + s4^2), 
   (-2*eps*s12 - 2*eps*s23)/(-(s12*s4) - s23*s4 + s4^2), 
   eps/(-s12 - s23 + s4)}}}
