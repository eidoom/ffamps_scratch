# 2l2a/ibp_1L

| Family   | Canonical permutation |
| -------- | --------------------- |
| `t4ZZZM` | 1234                  |

| Files                                             | Description                      | Source                                                                                                             |
| ------------------------------------------------- | -------------------------------- | ------------------------------------------------------------------------------------------------------------------ |
| `t4ZZZM_utmis.m`                                  | UT MI definitions                | Simone Zoia                                                                                                        |
| `ibpst4ZZZM/`                                     | 1L IBPs                          | [file](https://gitlab.com/sbadger/myfiniteflowexamples/-/tree/master/ibpsv2/1L_2l2a_new/generate_ibps.wl)          |
| `de_t4ZZZM_an.m`                                  | DE $A_x(s_{ij},\epsilon)$ matrix | [file](https://gitlab.com/sbadger/myfiniteflowexamples/-/tree/master/ibpsv2/1L_2l2a_new/differential_equations.wl) |
| `atilde_t4ZZZM.m`                                 | DE $\tilde{A}(s_{ij})$ matrix    | [file](https://gitlab.com/sbadger/myfiniteflowexamples/-/tree/master/ibpsv2/1L_2l2a_new/getAtilde.wl)              |
| `t4ZZZMp<permutation>_mis_<PSname>_<precision>.m` | MI numerical evaluations         | [file](https://bitbucket.org/sbadger/ffamps_tools/src/master/2l2a/de/boundary_values_1L.wl)                        |
| `<PSname>.m`                                      | Phase space point definition     | Itself                                                                                                             |
