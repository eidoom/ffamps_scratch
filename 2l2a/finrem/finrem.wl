(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


toX = {
  s[1, 2] -> ex[1], 
  s[2, 3] -> ex[1]*ex[2], 
  s[3, 4] -> (ex[1]*(ex[2] + ex[2]*ex[5] - ex[4]*ex[5] + ex[3]*ex[4]*ex[5]))/ex[4], 
  s[4, 5] -> ex[1]*ex[3], 
  s[1, 5] -> ex[1]*(-ex[2] + ex[3] + ex[4])*ex[5], 
  spA[1, 2] -> 1, 
  spA[1, 3] -> 1, 
  spA[1, 4] -> 1, 
  spA[1, 5] -> 1, 
  spA[2, 3] -> -ex[1]^(-1), 
  spA[2, 4] -> -((1 + ex[4])/(ex[1]*ex[4])), 
  spA[2, 5] -> -((1 + ex[5] + ex[4]*ex[5])/(ex[1]*ex[4]*ex[5])), 
  spA[3, 4] -> -(1/(ex[1]*ex[4])), 
  spA[3, 5] -> -((1 + ex[5])/(ex[1]*ex[4]*ex[5])), 
  spA[4, 5] -> -(1/(ex[1]*ex[4]*ex[5])), 
  spB[1, 2] -> -ex[1], 
  spB[1, 3] -> ex[1]*(1 + ex[2] - ex[3]), 
  spB[1, 4] -> ex[1]*(-ex[2] + ex[3] - ex[2]*ex[5] + ex[3]*ex[5] + ex[4]*ex[5]), 
  spB[1, 5] -> -(ex[1]*(-ex[2] + ex[3] + ex[4])*ex[5]), 
  spB[2, 3] -> ex[1]^2*ex[2], 
  spB[2, 4] -> ex[1]^2*(-ex[2] - ex[2]*ex[5] + ex[4]*ex[5]), 
  spB[2, 5] -> -(ex[1]^2*(-ex[2] + ex[4])*ex[5]), 
  spB[3, 4] -> ex[1]^2*(ex[2] + ex[2]*ex[5] - ex[4]*ex[5] + ex[3]*ex[4]*ex[5]), 
  spB[3, 5] -> -(ex[1]^2*(ex[2] - ex[4] + ex[3]*ex[4])*ex[5]),
  spB[4, 5] -> ex[1]^2*ex[3]*ex[4]*ex[5]
};


fromX = {
  ex[1]->s[1,2],
  ex[2]->s[2,3]/s[1,2],
  ex[3]->s[4,5]/s[1,2],
  ex[4]->-trp[1,2,3,4]/s[1,2]/s[3,4],
  ex[5]->-trp[1,3,4,5]/s[1,3]/s[4,5]
};


phase=<|
  "-+--+"->spB[2,5]/spB[1,3]/spB[3,4],
  "-+-+-"->spB[2,4]/spB[1,3]/spB[3,5],
  "-++-+"->spA[1,4]/spA[2,3]/spA[3,5],
  "-+++-"->spA[1,5]/spA[2,3]/spA[3,4]
|>;


helsInd = Keys@phase;
helsCurInd = DeleteDuplicates[StringTake[#,3]&/@helsInd];


structures = <|
  0 -> <|
    "0,0"->"_OASGAlem4lem2Qlep2"
  |>,
  1 -> <|
    "0,0"->"_OASGAlem4lem2Qlep4",
    "1,1"->"leAS_OGAlem4lem2Qlep4"
  |>,
  2 -> <|
    "0,0"->"_OASGAlem4lem2Qlep6",
    "1,0"->"le_OASGAlem4lem2Qlep6",
    "1,1"->"leAS_OGAlem4lem2Qlep6",
    "1,2"->"leASGA_Olem4lem2Qlep6",
    "2,1"->"leleAS_OGAlem4lem2Qlep6"
  |>
|>;


mainCur = <|Table[
  l -> <|Table[
    i -> <|Table[
      h -> Get @ FileNameJoin @ {
        "..","apart",
        "amp_"<>ToString[l]<>"L_2l2a_"<>structures[l][[i]]<>"_"<>StringReplace[h, {"-" -> "m"}]<>"u.m"
      }
      ,{h, helsCurInd}
    ]|>
    ,{i,Keys@structures@l}
  ]|>
  ,{l, Keys@structures}
]|>;


decay=<|
  "-+"->{
    -spA[1, 4]*spB[1, 5],
    -spA[2, 4]*spB[2, 5],
    -spA[3, 4]*spB[3, 5],
    (-spA[1, 3]*spA[2, 4]*spB[1, 5]*spB[2, 3] + spA[1, 4]*spA[2, 3]*spB[1, 3]*spB[2, 5]) / s[1, 2]
   },
   "+-"->{
    -spA[1, 5]*spB[1, 4],
    -spA[2, 5]*spB[2, 4],
    -spA[3, 5]*spB[3, 4],
    (-spA[1, 3]*spA[2, 5]*spB[1, 4]*spB[2, 3] + spA[1, 5]*spA[2, 3]*spB[1, 3]*spB[2, 4]) / s[1, 2]
   }
|> / s[4, 5];


(* functions to numerically evaluate *)
getMomSquared[mom_List]:=mom[[1]]^2-mom[[2]]^2-mom[[3]]^2-mom[[4]]^2;
dot4[q_List,i_Integer,j_Integer]:=
  q[[i,1]]*q[[j,1]]-q[[i,2]]*q[[j,2]]-q[[i,3]]*q[[j,3]]-q[[i,4]]*q[[j,4]];
evalTr[q_List,i1_Integer,i2_Integer,i3_Integer,i4_Integer]:=
  4*(dot4[q,i1,i2]*dot4[q,i3,i4]-dot4[q,i1,i3]*dot4[q,i2,i4]+dot4[q,i1,i4]*dot4[q,i2,i3]);
evalTr5[q_List,i1_Integer,i2_Integer,i3_Integer,i4_Integer]:=4*I*Sum[
  Signature[{mu,nu,ro,si}]*q[[i1]][[mu]]*q[[i2]][[nu]]*q[[i3]][[ro]]*q[[i4]][[si]],
  {mu,1,4},{nu,1,4},{ro,1,4},{si,1,4}
];
replTrp[q_List] := trp[i_,j_,k_,l_]:>(evalTr[q,i,j,k,l]+evalTr5[q,i,j,k,l])/2;
replS[q_List] := s[i__] :> getMomSquared@Total[q[[#]]&/@{i}];
replX[q_List]:= fromX /. replTrp[q] /. replS[q];


evalCur[a_,sfvals_,xvals_] := Transpose[
  (a[[2,1]] /. a[[1,1]] /. (a[[1,2]] /. xvals)) . (a[[2,2]] /. sfvals)
];
flipHelsTreeCur[y_]:=Conjugate[{1,1,1,-1}*y];
evalCurFlip[a_, sfvals_, xvals_] := #*{1,1,1,-1}& /@ 
  evalCur[a, sfvals, xvals /. Rule[x_,n_] :> x -> Conjugate[n]];


TranslateToGinac[expr_] := StringReplace[
  ToString@InputForm@expr,
  {"["->"(", "]"->")", " "->"", "Sqrt"->"sqrt"}
];


ImportNumbers[filename_String]:=Module[
  {list},
  list=ToExpression@StringReplace[ReadString@filename,{"E"->"*^"}];
  If[
    Not@And@@(MatchQ[#,_Complex|_Real|_Integer|_Rational]& /@ list),
    Print["Error in GiNaC output"];
    Abort[];
  ];
  list
];


EvaluateGPLs[glist_List, filein_String, ndigits_Integer] := Module[
  {fileout,streamout,values},
  streamout = OpenWrite[filein];
  Do[WriteLine[streamout,TranslateToGinac[gg]], {gg, glist}];
  Close[streamout];
  fileout = filein<>".vals";
  Run@FileNameJoin@{"..","evaluation","ginac_gpl","geval "<>filein<>" "<>fileout<>" "<>ToString[ndigits]};
  values = ImportNumbers@fileout;
  Thread[glist->values]
];


(* TODO {F[4,11],F[4,55]} are unused *)
{Fs, matrix, monos, indices, neededGPLs, logs, constants} = Get @ FileNameJoin @ {
  "..","evaluation","all_funcbasis_to_GPLs_matrix.m"
};


(* analytic regions of phase space *)
regions = {
  s4<0 && s23>0 && s4-s23<s12<0,
  s12<0 && s23<0 && s12+s23<s4<0, (* s23 channel *)
  s4>0 && s23<0 && s12>-s23+s4 (* s12 channel *)
};


(* rules for the analytic continuation of the logarithms in the regions *)
logrules = {
 {Log[s23/s4]->Log[s23]-Log[-s4]-I*Pi},
 {},
 {Log[s23/s4]->Log[-s23]-Log[s4]+I*Pi, Log[-s4]->Log[-s4]-I*Pi}
};


(* analytic continuation: assign imaginary part to indices between 0 and 1 - hard-coded for available regions *)
regionIms = {
  {-1,  0, +1, -1},
  {-1, -1, -1,  0},
  {+1,  0,  0,  0}
};


evalF[q_List, digits_Integer] := Module[
  {X, region, logsValues, indicesVals, indicesIms, neededGPLsX, neededGPLsXvalues, GPLs2vals, allvals, 
  monosvals, Fvals},
  X = {s12 -> s[1,2], s23 -> s[2,3], s4 -> s[4,5]} /. replS[q];
  
  region=Flatten@Position[regions /. X, True];
  If[Length@region===1,
    region=First@region,
    Print["WARNING: the chosen point does not belong to the known phase-space regions"];
    Abort[];
  ];

  logsValues = Thread[logs -> (logs/.logrules[[region]] /. X)];
  
  indicesVals = indices /. X;
  
  indicesIms = Table[
    Keys@indices[[i]] ->
    If[
      0 < Values@indicesVals[[i]] < 1,
      regionIms[[region,i]],
      0
    ],
    {i, Length@indices}
  ];
  
  neededGPLsX = neededGPLs /. G[a_List,1] :> G[a/.indicesVals,a/.indicesIms,1];
  neededGPLsXvalues = EvaluateGPLs[neededGPLsX, FileNameJoin @ {"gpls.txt"}, digits];
  GPLs2vals = Thread[neededGPLs->(neededGPLsX/.neededGPLsXvalues)];
  allvals = Dispatch@Join[constants, logsValues, GPLs2vals];
  monosvals = monos /. allvals;
  Fvals = Thread[Fs -> matrix . monosvals];
  
  Join[Fvals,constants,{ipi -> I Pi}]
];


init[] := <|# -> <||> & /@ Keys@structures|>;


evalAllCur[q_List, nl_Integer, digits_Integer]:=Module[
  {
    xN = replX[q],
    mainCurN = init[],
    mainCurNFlip = init[],
    sfvals = evalF[q, digits]
  },
  
  Do[
    mainCurN[0,h] = 1/Sqrt[2]*mainCur[0,"0,0",h] /. xN;
    mainCurNFlip[0,h] = flipHelsTreeCur@mainCurN[0,h];
    
    mainCurN[1,h] = -1/Sqrt[2]*(
      evalCur[mainCur[1,"0,0",h], sfvals, xN] +
      nl*evalCur[mainCur[1,"1,1",h], sfvals, xN]
    );
    mainCurNFlip[1,h] = -1/Sqrt[2]*(
      evalCurFlip[mainCur[1,"0,0",h], sfvals, xN] +
      nl*evalCurFlip[mainCur[1,"1,1",h], sfvals, xN]
    );

    mainCurN[2,h] = 1/Sqrt[2]*(
      evalCur[mainCur[2,"0,0",h], sfvals, xN] +
      nl*(
        evalCur[mainCur[2,"1,0",h], sfvals, xN] +
        evalCur[mainCur[2,"1,1",h], sfvals, xN] +
        evalCur[mainCur[2,"1,2",h], sfvals, xN]
      ) +
      nl^2*evalCur[mainCur[2,"2,1",h], sfvals, xN]
    );
    mainCurNFlip[2,h] = 1/Sqrt[2]*(
      evalCurFlip[mainCur[2,"0,0",h], sfvals, xN] +
      nl*(
        evalCurFlip[mainCur[2,"1,0",h], sfvals, xN] +
        evalCurFlip[mainCur[2,"1,1",h], sfvals, xN] +
        evalCurFlip[mainCur[2,"1,2",h], sfvals, xN]
      ) +
      nl^2*evalCurFlip[mainCur[2,"2,1",h], sfvals, xN]
    );
    
    ,{h, helsCurInd}
  ];
  
  {mainCurN, mainCurNFlip, sfvals}
];


(* 0 -> e^+(p1) \mu^+(p2) e^-(p3) \mu^-(p4) \gamma(p5) *)
q1r = {
  {-158.11388300841895, 0, 0, -158.11388300841895},
  {-158.113883008419, 0, 0, 158.113883008419}, 
  {138.79740434074432, -86.67897451317037, 39.35460867506775, 101.00836403538823}, 
  {129.99619677356793, 42.4778619262123, -53.71642074997865, -110.49519701589341}, 
  {47.434164902525694, 44.201112586958075, 14.361812074910901, 9.486832980505136}
};
(* map to: 0 -> e^- e^+ \gamma \mu^- \mu^+ *)
q1 = q1r[[{3,1,5,4,2}]];


(* 0 -> e^+(p1) \mu^+(p2) e^-(p3) \mu^-(p4) \gamma(p5) *)
q2r = {
  {-158.11388300841895, 0, 0, -158.11388300841895},
  {-158.113883008419, 0, 0, 158.113883008419}, 
  {122.06391768249948, 97.65777148603496, -21.512477703060043, -69.99837834876408}, 
  {99.29551852928712, -53.04800735081281, 82.91255053526316, 13.077380465733185}, 
  {94.86832980505139, -44.60976413522215, -61.40007283220312, 56.92099788303084}
};
(* map to: 0 -> e^- e^+ \gamma \mu^- \mu^+ *)
q2 = q2r[[{3,1,5,4,2}]];


Z[sfvals_List,nl_Integer] := Module[
  {logs12,Z1,Z2},
  logs12=-F[1,3]/.sfvals;
  Z1=-2/eps^2 + (-3 + (4*nl)/3 + 2*logs12)/eps;
  Z2=2/eps^4 + (6 - (14*nl)/3 - 4*logs12)/eps^3 + ((81 - 88*nl + 32*nl^2)/18 + (-6 + 4*nl)*logs12 + 
   2*logs12^2)/eps^2 + (-3/4 + Pi^2 + (nl*(119 + 9*Pi^2))/27 - (20*nl*logs12)/9 - 12*Zeta[3])/eps;
  {Z1,Z2}
];


evalFinRem[q_List, nl_Integer, digits_Integer]:= Module[
  {
    ampB,
    ampBFlip,
    ampR = init[],
    ampRFlip = init[],
    beta = <|
      0 -> -4/3 nl,
      1 -> -4 nl
    |>,
    sfvals,
    Z1,
    Z2
  },
  {ampB, ampBFlip, sfvals} = evalAllCur[q, nl, digits];
  {Z1, Z2} = Z[sfvals, nl];
  
  Do[
    ampB[1,h] = SeriesData[eps, 0, #, -2, 3, 1]& /@ Transpose@ampB[1,h];
    ampBFlip[1,h] = SeriesData[eps, 0, #, -2, 3, 1]& /@ Transpose@ampBFlip[1,h];
    
    ampB[2,h] = SeriesData[eps, 0, #, -4, 1, 1]& /@ Transpose@ampB[2,h];
    ampBFlip[2,h] = SeriesData[eps, 0, #, -4, 1, 1]& /@ Transpose@ampBFlip[2,h];

    ampR[0,h] = ampB[0,h];
    ampRFlip[0,h] = ampBFlip[0,h];
    
    ampR[1,h] = Chop[
      (ampB[1,h] - 2*beta[0]/eps*ampB[0,h])
      - Z1 * ampR[0,h]
    ];
    ampRFlip[1,h] = Chop[
      (ampBFlip[1,h] - 2*beta[0]/eps*ampBFlip[0,h])
      - Z1 * ampRFlip[0,h]
    ];

    ampR[2,h] = Chop[
      (ampB[2,h] - 3*beta[0]/eps*ampB[1,h] -(beta[1]/eps - 3*beta[0]^2/eps^2)*ampB[0,h])
      - Z2 * ampR[0,h] - Z1 * ampR[1,h]
    ];
    ampRFlip[2,h] = Chop[
      (ampBFlip[2,h] - 3*beta[0]/eps*ampBFlip[1,h] -(beta[1]/eps - 3*beta[0]^2/eps^2)*ampBFlip[0,h])
      - Z2 * ampRFlip[0,h] - Z1 * ampRFlip[1,h]
    ];
    
    ,
    {h, helsCurInd}
  ];
  
  {ampR, ampRFlip}
];


{amp,ampF}=evalFinRem[q1,1,16];


(* remaining poles are actually zero, it's a numerical error *)
amp
