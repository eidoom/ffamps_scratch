(* ::Package:: *)

$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];

(* inputfuncbasis = "all_funcbasis_to_GPLs_matrix.m"; *)
inputfuncbasis = "reduced_funcbasis_to_GPLs_matrix.m";
{Fs,matrix,monos,indices,neededGPLs,logs,constants} = Get[inputfuncbasis];

pathToGiNaC = "./ginac_gpl/";
pathToScratch = "./";

ndigits=30;


If[!DirectoryQ[pathToScratch<>"scratch/"],
  Print["creating scratch directory"];
  CreateDirectory[pathToScratch<>"scratch/"];
];


(* ::Subsection:: *)
(*list of predefined phase-space points*)


(*X={s12->-3,s23->-5,s4->-7};
ptlabel = "Xeu";*)

(*X = {s12 -> 2, s23 -> -1/2, s4 -> 1};
ptlabel = "Xs12";*)

(*X = {s12 -> 7, s23 -> 11, s4 -> 13};
ptlabel = "rnd1";*)

(*X = {s12 -> 17, s23 -> -19, s4 -> 23};
ptlabel = "rnd2";*)

(*X = {s12 -> -1, s4-> -2, s23->1/2};
ptlabel = "rndRI1";*)

(*X = {s4->-(1/2),s12->-1,s23->2};
ptlabel = "rndRI2";*)

(*X ={s12->-11949.943795683,s23->-12000.000000000002,s4->-6166.757588464314};
X = Thread[Keys[X]->Rationalize[Values[X],10^(-10)]]; (* rationalise to 10 digits *)
ptlabel = "Xcc";*)

(*X = {s12->-(13809657845/227374),s23->-(3298534883327999/274877906944),s4->-(8079833045/227374)};
ptlabel = "r2p2";*)

X = {s12->-(17009502199/1423396),s23->-(6597069766656001/549755813888),s4->-(2426569777/393492)};
ptlabel = "r2p1";


region[1] = s4<0&&s23>0&&s4-s23<s12<0;
region[2] = s12<0&&s23<0&&s12+s23<s4<0;
region[3] = s4>0&&s23<0&&s12>-s23+s4; (* s12 channel *)

regions = region/@{1,2,3};


regionNo=Flatten[Position[regions/.X,True]];
If[Length[regionNo]===1,
  regionNo=regionNo[[1]];
  Print["region no. ", regionNo, " (", region[regionNo], ")"];,
  Print["WARNING: the chosen point does not belong to the known phase-space regions"]; Abort[];
 ];


(* ::Section:: *)
(*GiNaC routines*)


TranslateToGinac[expr_] := StringReplace[ToString[InputForm[expr]], {"["->"(","]"->")"," "->"","Sqrt"->"sqrt"}];


ImportNumbers[filename_]:=Block[{list},list=ToExpression@StringReplace[ReadString[filename],{"E"->"*^"}];
If[Not[And@@Map[MatchQ[#,_Complex|_Real|_Integer|_Rational]&,list]],Print["Error GiNaC!!!"]];list];


Clear[EvaluateGPLs];
EvaluateGPLs[glist_List,filein_String,ndigits_Integer] := Block[{fileout,streamout,out,values},

	streamout = OpenWrite[filein];
	Table[WriteLine[streamout,TranslateToGinac[gg]], {gg, glist}];
	Close[streamout];
	
    fileout = StringJoin[StringSplit[filein,"."][[1;;-2]]]<>"_vals.txt";
	Run[pathToGiNaC<>"geval "<>filein<>" "<>fileout<>" "<>ToString[ndigits]];

	values = ImportNumbers[fileout];
	Return[Thread[glist->values]];
];


(* ::Section:: *)
(*evaluation*)


now0=AbsoluteTime[];


(* ::Subsection:: *)
(*logarithms*)


(* rules for the analytic continuation of the logarithms *)
logrules[1] = {
 Log[s23/s4]->Log[s23]-Log[-s4]-I*Pi
};

logrules[2] = {};

logrules[3] = {Log[s23/s4]->Log[-s23]-Log[s4]+I*Pi,
  Log[-s4]->Log[-s4]-I*Pi};


logsvalues = Thread[logs->(logs/.logrules[regionNo]/.X)];


(* ::Subsection:: *)
(*GPLs*)


indicesVals = indices/.X;

(* analytic continuation: assign imaginary part to indices between 0 and 1 *)
regionIims[1] = {-1, 0, +1, -1};
regionIims[2] = {-1, -1, -1, 0};
regionIims[3] = {+1, 0, 0, 0};

indicesIms = Table[indices[[i,1]]->
  If[
    0<Values[indicesVals[[i]]]<1,
    regionIims[regionNo][[i]],
    0
  ]
,{i,Length[indices]}];

Print["indices: ", indicesVals];
Print["analytic continuation: ", indicesIms];


neededGPLsX = neededGPLs/.G[a_List,1]:>G[a/.indicesVals,a/.indicesIms,1];


label="4pt1mGPLs_"<>ToString[RandomInteger[{1,10^4}]];


Print["running GiNaC..."];
now=AbsoluteTime[];
neededGPLsXvalues = EvaluateGPLs[neededGPLsX, pathToScratch<>"scratch/"<>label<>".txt", ndigits];
Print["-> ", AbsoluteTime[]-now, " s"];


(*neededGPLsXvalues=Get["~/Work/function_basis_builder/4pt_1mass/scratch/needed_GPLs_X0_values.m"];*)


GPLs2vals = Thread[neededGPLs->(neededGPLsX/.neededGPLsXvalues)];


(* ::Subsection:: *)
(*put everything together*)


allvals = Dispatch[Join[constants,logsvalues,GPLs2vals]];


monosvals = monos/.allvals;


Fvals = Thread[Fs->matrix . monosvals];


Print["evaluation took ", AbsoluteTime[]-now0, " s"];


Print["check variables: ", Variables[Values[Fvals]]==={}];


Put[{X,Fvals}, pathToScratch<>"scratch/Fvals_"<>ptlabel<>".m"];
