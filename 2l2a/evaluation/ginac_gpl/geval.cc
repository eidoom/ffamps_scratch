#include <iostream>
#include <ginac/ginac.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;
using namespace GiNaC;


int main (int argc, char* argv[]) {

	if(argc!=4){
		cerr<<"Error! Usage: ./geval <input> <output> <number_of_digits>"<<endl;
		return -1;
	}

	ifstream infile (argv[1]);

	ofstream outfile;
	outfile.open (argv[2]);

	Digits = atoi(argv[3]);

	string line;
	symtab table;
	table["x"] = symbol();
	parser reader(table);

	outfile << "{";

	if (infile.is_open())
	{
		while ( getline (infile,line) )
    		{
			cout << reader(line) << '\n';		
			outfile << evalf(reader(line)) << ','<<'\n';
    		}
		infile.close();
	}
	else cout << "Unable to open file: input.txt";   	
	outfile << "Nothing}";
 	outfile.close();

	return 0;
}
