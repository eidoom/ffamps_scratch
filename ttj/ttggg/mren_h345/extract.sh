#!/bin/bash

for i in `seq 1 4`;
do
  for j in ppp ppm pmp;
  do
	myfile="mren_h345_SdotA"$i"_2L_ttggg_T23451_Nfp0_Ncp2_"$j".m"
	echo $myfile
	tar -xzvf $myfile".tar.gz" 
  done

  tar -xzvf "mren_h345_SdotA"$i"_2L_ttggg_T23451_Nfp1_Ncp1.tar.gz" 

done

tar -xzvf mren_h345_SdotA_2L_ttggg_T23451_Nfp2_Ncp0.tar.gz
tar -xzvf amps_0L_1L.tar.gz


