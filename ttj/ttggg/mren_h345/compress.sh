#!/bin/bash

for i in `seq 1 4`;
do
  for j in ppp ppm pmp;
  do
	myfile="mren_h345_SdotA"$i"_2L_ttggg_T23451_Nfp0_Ncp2_"$j".m"
	echo $myfile
	tar -czvf $myfile".tar.gz" $myfile
  done
  
  myfile="mren_h345_SdotA"$i"_2L_ttggg_T23451_Nfp1_Ncp1_*.m" 
  tar -czvf "mren_h345_SdotA"$i"_2L_ttggg_T23451_Nfp1_Ncp1.tar.gz" $myfile

done

tar -czvf mren_h345_SdotA_2L_ttggg_T23451_Nfp2_Ncp0.tar.gz mren_h345_SdotA*_2L_ttggg_T23451_Nfp2_Ncp0_*.m

