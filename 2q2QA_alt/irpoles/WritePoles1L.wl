(* ::Package:: *)

(* normalisation of the colour generators: Tr[T^a T^b] = delta[a,b]/2 , i.e. TR=1/2 *)


$dir = If[$FrontEnd=!=Null,NotebookDirectory[],DirectoryName[$InputFileName]];
SetDirectory[$dir];


process = "2q2QA";
looporder = "1L";


<<FiniteFlow`;
<<FFUtils`;
<< "InitTwoLoopToolsFF.m";
<< "InitDiagramsFF.m";


PathToAmps0L = Directory[]<>"/../numerators/";
PathToAmps1L = Directory[]<>"/../eps_exp/";


(* EXAMPLE:*)
(*amp_1L_2q2QA_d14d23__Oqlm8qlm6_OGAqkm4qkm2Qqk_Ncp1_+m+m+_PSanalyticX1.m*)
colourfactor="d14d23";
closedloopfactor="_Oqlm8qlm6_OGAqkm4qkm2Qqk";
ncfactor="Ncp1";
helicity="+m+m+";
psmode="PSanalyticX1";
checkpoles=True;


CmdOptions = {
  "-helicity",
  "-colourfactor",
  "-ncfactor",
  "-closedloopfactor",
  "-psmode",
  "-checkpoles",
  "-checkpolesonly"
};

psmode = "PSanalytic";
checkpoles = False;
checkpolesonly = False;

Print[$CommandLine];

Do[
pos = Flatten[Position[$CommandLine,CmdOptions[[oo]]]];
If[Length[pos]==1,

  Switch[oo,
    1,  
    helicity = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    2,
    colourfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    3,
    ncfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    4,
    closedloopfactor = MakeFileName[$CommandLine[[pos[[1]]+1]]];,
    5,
    psmode = $CommandLine[[pos[[1]]+1]];,
    6,
    checkpoles = True;,
    7,
    checkpolesonly = True;,
    _,
    Print["unknown argument ",oo,CmdOptions[[oo]]];
  ];

];
,{oo,1,Length[CmdOptions]}];

If[checkpolesonly, checkpoles=True];


closedloopfactortype=StringRiffle[StringSplit[closedloopfactor,"_"][[-2;;-1]],"_"];


(* ::Title:: *)
(*Prepare IR subtraction*)


(* ::Subtitle:: *)
(*from arXiv:0903.1126*)


id = IdentityMatrix[2];


(* ::Section:: *)
(*prepare symbolic form of subtraction terms*)


(* ::Subsection::Closed:: *)
(*UV renormalization & finite remainders (just for illustration purposes, not needed)*)


(*(*a = alphaS/(4 Pi) *)
couplingrenormalisation = a0->a*(1-a*beta[0]/eps+a^2*(beta[0]^2/eps^2-beta[1]/2/eps)+ALARM a^3);

renormalisation = Thread[{Aren[0],Aren[1],Aren[2]}->CoefficientList[Normal[Series[g0^2*(A[0] + g0^2*A[1]+ g0^4*A[2]) /. g0->Sqrt[a0] /. couplingrenormalisation,{a,0,3}]],a][[2;;-1]]];*)


(*Fdef = (Aren[0]+a*Aren[1]+a^2*Aren[2]+ALARM*a^3) - (1+z1*a+z2*a^2+ALARM*a^3)*(F[0]+a*F[1]+a^2*F[2]+ALARM*a^3);

finiteremainders=Simplify[Flatten[Solve[Table[Coefficient[Fdef/.renormalisation,a,k],{k,0,2}]==0,{F[0],F[1],F[2]}]]];*)


(* ::Subsection:: *)
(*pentagon functions*)


log2PF = {
 Log[-s[1,2]] -> F[1,1,1]-tci[1,2],
 Log[-s[1,3]] -> F[1,1,6],
 Log[-s[1,4]] -> F[1,1,9],
 Log[-s[1,5]] -> F[1,1,5],
 Log[-s[2,3]] -> F[1,1,2],
 Log[-s[2,4]] -> F[1,1,7],
 Log[-s[2,5]] -> F[1,1,10],
 Log[-s[3,4]] -> F[1,1,3]-tci[1,2],
 Log[-s[3,5]] -> F[1,1,8]-tci[1,2],
 Log[-s[4,5]] -> F[1,1,4]-tci[1,2],
 Pi->-I*tci[1, 2],
 Zeta[3] -> tcr[3,3]
};

pisqrule = tci[1,2]^a_:>tci[1,1]^2*tci[1,2]^(a-2) /; a>=2;

PF2log=Flatten[Solve[log2PF[[1;;10]]/.Rule->Equal]];


(* ::Subsection:: *)
(*construct general subtraction terms*)


Z1=Get["Z1.m"];


(* values of all anomalous dimensions and flags *)
ExpectedValues = {
  gcusp[0]->4,
  gcusp[1]->(268/9-4*Pi^2/3)*cA-80/9*Tf*nf,
  gcusp[2]->cA^2*(490/3-536/27*Pi^2+44/45*Pi^4+88/3*Zeta[3])+
    cA*Tf*nf*(-1672/27+160/27*Pi^2-224/3*Zeta[3])+
    cF*Tf*nf*(-220/3+64*Zeta[3])-64/27*Tf^2*nf^2,
    
  gquark[0]->-3*cF,
  gquark[1]->cF^2*(-3/2+2*Pi^2-24*Zeta[3])+cF*cA*(-961/54-11/6*Pi^2+26*Zeta[3])+
    cF*Tf*nf*(130/27+2*Pi^2/3),
  gquark[2]->cF^3*gqflag[1]+cF^2*cA*gqflag[2]+cF*cA^2*gqflag[3]+cF^2*Tf*nf*gqflag[4]+
    cF*cA*Tf*nf*gqflag[5]+cF*Tf^2*nf^2*gqflag[6],
    
  ggluon[0]->-beta[0],
  ggluon[1]->cA^2*(-692/27+11/18*Pi^2+2*Zeta[3])+cA*Tf*nf*(256/27-2*Pi^2/9)+4*cF*Tf*nf,
  
  beta[0]->11/3*cA-4/3*Tf*nf,
  beta[1]->2/3*(17*cA^2-10*cA*TR*nf-6*cF*TR*nf),

  cA -> Nc*(2*TR), 
  cF -> (Nc^2-1)/2/Nc*(2*TR),
  TR -> 1/2,
  Tf -> TR
} /.  {Pi->-I*tci[1, 1],Zeta[3] -> tcr[3,3]};


fullamp0Lsymb={A0L["d14d23","Ncp0"]+A0L["d14d23","Ncpm1"]/Nc,
  A0L["d12d34","Ncp0"]+A0L["d12d34","Ncpm1"]/Nc};


sub1L = fullamp0Lsymb . (Z1 + beta[0]/eps*id);
sub1L = Collect[sub1L //. ExpectedValues /. log2PF,eps,Simplify];


(* ::Chapter::Closed:: *)
(*auxiliary functions*)


GetFunctionVector[expr_]:=Module[{tmpexpr,funcs},
  If[MatchQ[expr,0],Return[{{0},{mono[1]}}];];
  tmpexpr = Collect[expr,tci[1,2]] //. tci[1,2]^a_:>tci[1,1]^2*tci[1,2]^(a-2) /; a>=2;
  funcs= Select[Variables[tmpexpr],Or[MatchQ[#,F[__]],MatchQ[#,tci[__]],MatchQ[#,tcr[__]]]&];
  If[Length[funcs]==0,funcs={dummy}];
  Return[Transpose[CoefficientRules[tmpexpr,funcs] /. Rule[a_,b_]:>{b,mono[Times@@(funcs^a)]}]];
];


GetFunctionVectorEps[expr_]:=Module[{tmpexpr,funcs},
  If[MatchQ[expr,0],Return[{{0},{mono[1]}}];];
  tmpexpr = Collect[expr,tci[1,2]] //. tci[1,2]^a_:>tci[1,1]^2*tci[1,2]^(a-2) /; a>=2;
  funcs= Select[Variables[tmpexpr],Or[MatchQ[#,F[__]],MatchQ[#,tci[__]],MatchQ[#,tcr[__]],MatchQ[#,Power[eps,_]],MatchQ[#,eps]]&];
  If[Length[funcs]==0,funcs={dummy}];
  Return[Transpose[CoefficientRules[eps^6*tmpexpr,funcs] /. Rule[a_,b_]:>{b,mono[(Times@@(funcs^a))/eps^6]}]];
];


MyAmpMult[{c1_List,f1_List},{c2_List,f2_List},outmonolist_]:=Module[{cfrules,newc1,newc2,newf1,newf2,fout,cout,cvec,ccc},
  If[DeleteDuplicates[c1]=={0}||DeleteDuplicates[c2]=={0},Return[Table[0,{ii,outmonolist}]]];
  cfrules = Rule@@@Transpose[{(ccc/@Range[Length[c1]]),Factor[c1]}];
  {newc1,newf1} = GetFunctionVector[(ccc/@Range[Length[c1]]) . f1 /. mono[x_]:>x];
  {newc2,newf2} = GetFunctionVector[c2 . f2 /. mono[x_]:>x];
  fout = Outer[mono[Times[#1 /. mono[x_]:>x,#2 /. mono[x_]:>x]]&,newf1,newf2];
  fout = fout //. tci[1,2]^a_:>tci[1,1]^2*tci[1,2]^(a-2) /; a>=2;
  cout = Outer[Times,newc1,newc2];
  cvec = Table[Plus@@Extract[cout,Position[fout,mmm]],{mmm,(mono/@outmonolist)}];
  
  (* add check! *)
  
  Return[cvec /. cfrules];
];


Clear[GetLinearRelations];
Options[GetLinearRelations] = {"PrintDebugInfo"->0};
GetLinearRelations[funcs_List, OptionsPattern[]]:=Module[{vs,lrgraph,in,out,
  fs,degrees,complexity,sortedcs,sortedfs,sorted,zero,fiteq,graphfit,fit,
  fitlearn,fitrec,fitsol,linrels},

  If[Length[funcs]===1, Return[{}]];
  vs=Variables[funcs];
  If[OptionValue["PrintDebugInfo"]>0, Print[Length[funcs], " coefficients"]; Print["variables: ", vs]];
  FFNewGraph[lrgraph,in,vs];
  FFAlgRatExprEval[lrgraph,out,{in},vs,funcs]//Print;
  FFGraphOutput[lrgraph,out];
  
  fs = f/@Range[Length[funcs]];
  
  degrees = FFTotalDegrees[lrgraph];
  complexity = Max@@#&/@degrees;
  
  sortedfs = SortBy[fs,complexity[[#[[1]]]]&];
  sortedcs = c@@#&/@sortedfs;
  
  FFAlgTake[lrgraph,sorted,{out},{fs}->sortedfs];
  FFAlgRatNumEval[lrgraph,zero,{0}];
  FFAlgChain[lrgraph,fiteq,{sorted,zero}];
  FFGraphOutput[lrgraph,fiteq];
  
  FFNewGraph[graphfit];
  FFAlgSubgraphFit[graphfit,fit,{},lrgraph,vs,sortedcs]//Print;
  FFGraphOutput[graphfit,fit];

  fitlearn=FFDenseSolverLearn[graphfit,sortedcs];
  fitrec = FFReconstructNumeric[graphfit];
  fitsol = FFDenseSolverSol[fitrec,fitlearn];
  
  linrels=FFLinearRelationsFromFit[sortedfs,sortedcs,fitsol];
  
  If[OptionValue["PrintDebugInfo"]>0, 
    Print[Length[Variables[Array[f,Length[funcs]]/.linrels]]," linearly independent coefficients"]];

  FFDeleteGraph[graphfit];
  FFDeleteGraph[lrgraph];

  Return[linrels]; 
];


(* ::Title:: *)
(*poles of 1-loop amplitudes*)


(* ::Section:: *)
(*normalisations*)


(* absolute normalisation of the amplitudes *)
Normalisation0L = 1/4;
Normalisation1L = -1/8;
Normalisation2L = 1/16;


(* absolute normalisation of the colour factors *)
Normalisation["d14d23"]=1;
Normalisation["d12d34"]=1;


(* ::Section:: *)
(*symbolic subtraction*)


dictColourFactors = Association[{"d14d23"->1,"d12d34"->2}];
dictNcFactors = Association[Table[StringReplace["Ncp"<>ToString[i],"-"->"m"]->i,{i,-2,2}]];

dictNf2qk["Oqlm8qlm6_OGAqkm4qkm2Qqk"] = Association[{"nfp0"->"","nfp1"->"qb"}];
dictNf2int = Association[{"nfp0"->0,"nfp1"->1,"nfp2"->2}];

dictClosedLoopFactor2nf = Association[{"_Oqlm8qlm6_OGAqkm4qkm2Qqk"->0,
  "qb_Oqlm8qlm6_OGAqkm4qkm2Qqk"->1}];


neededsub1L=Coefficient[Coefficient[sub1L[[dictColourFactors[colourfactor]]],nf,
  dictClosedLoopFactor2nf[closedloopfactor]],Nc,dictNcFactors[ncfactor]];


neededamps0L=Union[Cases[neededsub1L,A0L[__],Infinity]];
Print["needed amplitudes: ", neededamps0L];


missedpieces=Complement[DeleteCases[Variables[neededsub1L],eps|_F|_tci|_tcr],neededamps0L];
If[missedpieces=!={}, Print["careful! something is left unfixed and may cause trouble: ", missedpieces]];


(* ::Section:: *)
(*load lower-loop info*)


(* ::Subsection:: *)
(*tree-level*)


Clear[amp0L];

Do[
  col=a0[[1]];
  ncp=a0[[2]];
  file = "DiagramNumerators_0L_2q2QA_"<>col<>"__"<>closedloopfactortype<>"_"<>ncp<>"_dsm2p0.m";
  If[!FileExistsQ[PathToAmps0L<>file],
    Print[file, " not found - setting to 0"];
    amp0L[col,ncp]=0;,
    
    Print["loading ", PathToAmps0L<>file];
    Get[PathToAmps0L<>file];
    amp0L[col,ncp]=Normalisation0L*Normalisation[col]*DiagramNumerator[StringReplace[helicity,"m"->"-"],topo[]]/.INT[nn_,__]:>nn;
    Clear[DiagramNumerator];
  ];
  Clear[col,ncp,file];
,{a0,neededamps0L}];


(* ::Section:: *)
(*1-loop poles*)


rndX = Which[psmode==="PSanalyticX1",
  Prepend[Thread[Table[ex[i],{i,2,5}]->RandomInteger[{99,999999999},4]],ex[1]->1],
  psmode==="PSanalytic",
  Thread[Table[ex[i],{i,1,5}]->RandomInteger[{99,999999999},5]],
  True,
  Print["unknown psmode"]; Quit[]];


Print["constructing explicit expression of the subtraction term"];
neededsub1Lexpl = Normal@Series[neededsub1L /. A0L->amp0L, {eps,0,2}];
If[psmode==="PSanalyticX1", neededsub1Lexpl = neededsub1Lexpl /. ex[1]->1];


ampname1L = StringJoin["amp_",looporder,"_",process,"_",colourfactor,"_",closedloopfactor,"_",
  ncfactor,"_",helicity,"_",psmode,".m"];

If[checkpoles,
  If[!FileExistsQ[PathToAmps1L<>ampname1L],Print[ampname1L, " not found!"]; Quit[];];
  Print["getting ", ampname1L]; now=AbsoluteTime[];
  ampdata1L = Get[PathToAmps1L<>ampname1L];
  Print["-> ",AbsoluteTime[]-now, " s"];

  coeffrules1L=ampdata1L[[1]];
  amp1L=ampdata1L[[2]];
  Clear[ampdata1L];
  amp1L = {Normalisation[colourfactor]*Normalisation1L*amp1L[[1]],amp1L[[2]]};
  
  (* check if the 1-loop amplitude is finite *)
  If[Union[Flatten[Table[Coefficient[amp1L[[1]],eps,k],{k,-4,-1}]]]==={0},
    Print["1-loop amplitude is finite"]];
  
  checkzero = (Dot@@amp1L /. Dispatch[coeffrules1L/.rndX]) - (neededsub1Lexpl /. rndX);
  checkzero = Table[Coefficient[checkzero,eps,k],{k,-4,-1}];
  checkzero = Expand[checkzero]/.pisqrule;
  If[Union[checkzero]==={0}, Print["the one-loop poles cancel out"], 
    Print["something wrong in the one-loop poles!"];];
  (*Clear[checkzero];*)
  
];


If[checkpolesonly, Quit[]];


{subcoeffs,submonos}=GetFunctionVectorEps[neededsub1Lexpl] /. mono->Identity;

(* rewrite subtraction term in terms of linearly independent f[i]'s *)
rels=GetLinearRelations[subcoeffs, "PrintDebugInfo"->1];
subcoeffsfs=Array[f,Length[subcoeffs]]/.rels;

(* definition of independent f[i]'s *)
indepfs=Variables[subcoeffsfs];
indepfs=Thread[indepfs->subcoeffs[[indepfs/.f->Identity]]];

(* redefine f[i]'s so that i=1,2,3,... *)
subcoeffsfs=subcoeffsfs/.Thread[indepfs[[All,1]]->Array[f,Length[indepfs]]];
indepfs=indepfs/.Thread[indepfs[[All,1]]->Array[f,Length[indepfs]]];

(* common denominator form *)
Print["together... this may take a while"];
indepfs=Thread[indepfs[[All,1]]->Together[indepfs[[All,2]]]];

(* format *)
{subcoeffsfs,submonos}=GetFunctionVector[subcoeffsfs . submonos] /. mono->Identity;
subcoeffsfs = Collect[subcoeffsfs,eps,Factor];

sub1Lfinal={indepfs,{subcoeffsfs,submonos}};

Clear[subcoeffsfs,indepfs,rels,submonos,subcoeffs];


check=Expand[Expand[(Dot@@sub1Lfinal[[2]]/.Dispatch[sub1Lfinal[[1]]/.rndX])-(neededsub1Lexpl /. rndX)] /. pisqrule]===0;
If[!check,Print["something went wrong"]; Quit[]];


subname1L=StringReplace[ampname1L,"amp"->"poles"];
Put[sub1Lfinal,subname1L];


Quit[];
