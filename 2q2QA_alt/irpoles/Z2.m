{{(cF^2*gcusp[0]^2)/(2*eps^4) - (cF*gcusp[1])/(4*eps^2) - 
   (cF*gcusp[0]*((-3*beta[0])/2 - (TR*F[1, 1, 1]*gcusp[0])/Nc - 
      (TR*F[1, 1, 2]*gcusp[0])/Nc + Nc*TR*F[1, 1, 2]*gcusp[0] - 
      (TR*F[1, 1, 3]*gcusp[0])/Nc + (TR*F[1, 1, 6]*gcusp[0])/Nc + 
      (TR*F[1, 1, 7]*gcusp[0])/Nc - (TR*F[1, 1, 9]*gcusp[0])/Nc + 
      Nc*TR*F[1, 1, 9]*gcusp[0] + 4*gquark[0] + (2*TR*gcusp[0]*tci[1, 2])/
       Nc))/(2*eps^3) + (-((TR*F[1, 1, 1]*gcusp[1])/Nc) - 
     (TR*F[1, 1, 2]*gcusp[1])/Nc + Nc*TR*F[1, 1, 2]*gcusp[1] - 
     (TR*F[1, 1, 3]*gcusp[1])/Nc + (TR*F[1, 1, 6]*gcusp[1])/Nc + 
     (TR*F[1, 1, 7]*gcusp[1])/Nc - (TR*F[1, 1, 9]*gcusp[1])/Nc + 
     Nc*TR*F[1, 1, 9]*gcusp[1] + 4*gquark[1] + (2*TR*gcusp[1]*tci[1, 2])/Nc)/
    (4*eps) + ((TR*F[1, 1, 2] - TR*F[1, 1, 6] - TR*F[1, 1, 7] + 
       TR*F[1, 1, 9])*gcusp[0]^2*(-(TR*F[1, 1, 6]) - TR*F[1, 1, 7] + 
       TR*(F[1, 1, 1] - tci[1, 2]) + TR*(F[1, 1, 3] - tci[1, 2])) + 
     (-((TR*F[1, 1, 1]*gcusp[0])/Nc) - (TR*F[1, 1, 2]*gcusp[0])/Nc + 
       Nc*TR*F[1, 1, 2]*gcusp[0] - (TR*F[1, 1, 3]*gcusp[0])/Nc + 
       (TR*F[1, 1, 6]*gcusp[0])/Nc + (TR*F[1, 1, 7]*gcusp[0])/Nc - 
       (TR*F[1, 1, 9]*gcusp[0])/Nc + Nc*TR*F[1, 1, 9]*gcusp[0] + 
       4*gquark[0] + (2*TR*gcusp[0]*tci[1, 2])/Nc)*
      (-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/Nc - (TR*F[1, 1, 2]*gcusp[0])/
        Nc + Nc*TR*F[1, 1, 2]*gcusp[0] - (TR*F[1, 1, 3]*gcusp[0])/Nc + 
       (TR*F[1, 1, 6]*gcusp[0])/Nc + (TR*F[1, 1, 7]*gcusp[0])/Nc - 
       (TR*F[1, 1, 9]*gcusp[0])/Nc + Nc*TR*F[1, 1, 9]*gcusp[0] + 
       4*gquark[0] + (2*TR*gcusp[0]*tci[1, 2])/Nc))/(8*eps^2), 
  -1/2*(cF*gcusp[0]^2*(-(TR*F[1, 1, 6]) - TR*F[1, 1, 7] + 
       TR*(F[1, 1, 1] - tci[1, 2]) + TR*(F[1, 1, 3] - tci[1, 2])))/eps^3 + 
   (gcusp[1]*(-(TR*F[1, 1, 6]) - TR*F[1, 1, 7] + 
      TR*(F[1, 1, 1] - tci[1, 2]) + TR*(F[1, 1, 3] - tci[1, 2])))/(4*eps) + 
   (gcusp[0]*(-(TR*F[1, 1, 6]) - TR*F[1, 1, 7] + 
       TR*(F[1, 1, 1] - tci[1, 2]) + TR*(F[1, 1, 3] - tci[1, 2]))*
      (-((TR*F[1, 1, 1]*gcusp[0])/Nc) - (TR*F[1, 1, 2]*gcusp[0])/Nc + 
       Nc*TR*F[1, 1, 2]*gcusp[0] - (TR*F[1, 1, 3]*gcusp[0])/Nc + 
       (TR*F[1, 1, 6]*gcusp[0])/Nc + (TR*F[1, 1, 7]*gcusp[0])/Nc - 
       (TR*F[1, 1, 9]*gcusp[0])/Nc + Nc*TR*F[1, 1, 9]*gcusp[0] + 
       4*gquark[0] + (2*TR*gcusp[0]*tci[1, 2])/Nc) + 
     gcusp[0]*(-(TR*F[1, 1, 6]) - TR*F[1, 1, 7] + 
       TR*(F[1, 1, 1] - tci[1, 2]) + TR*(F[1, 1, 3] - tci[1, 2]))*
      (-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/Nc + Nc*TR*F[1, 1, 1]*gcusp[0] - 
       (TR*F[1, 1, 2]*gcusp[0])/Nc - (TR*F[1, 1, 3]*gcusp[0])/Nc + 
       Nc*TR*F[1, 1, 3]*gcusp[0] + (TR*F[1, 1, 6]*gcusp[0])/Nc + 
       (TR*F[1, 1, 7]*gcusp[0])/Nc - (TR*F[1, 1, 9]*gcusp[0])/Nc + 
       4*gquark[0] + (2*TR*gcusp[0]*tci[1, 2])/Nc - 2*Nc*TR*gcusp[0]*
        tci[1, 2]))/(8*eps^2)}, 
 {-1/2*(cF*(TR*F[1, 1, 2] - TR*F[1, 1, 6] - TR*F[1, 1, 7] + TR*F[1, 1, 9])*
      gcusp[0]^2)/eps^3 + ((TR*F[1, 1, 2] - TR*F[1, 1, 6] - TR*F[1, 1, 7] + 
      TR*F[1, 1, 9])*gcusp[1])/(4*eps) + 
   ((TR*F[1, 1, 2] - TR*F[1, 1, 6] - TR*F[1, 1, 7] + TR*F[1, 1, 9])*gcusp[0]*
      (-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/Nc - (TR*F[1, 1, 2]*gcusp[0])/
        Nc + Nc*TR*F[1, 1, 2]*gcusp[0] - (TR*F[1, 1, 3]*gcusp[0])/Nc + 
       (TR*F[1, 1, 6]*gcusp[0])/Nc + (TR*F[1, 1, 7]*gcusp[0])/Nc - 
       (TR*F[1, 1, 9]*gcusp[0])/Nc + Nc*TR*F[1, 1, 9]*gcusp[0] + 
       4*gquark[0] + (2*TR*gcusp[0]*tci[1, 2])/Nc) + 
     (TR*F[1, 1, 2] - TR*F[1, 1, 6] - TR*F[1, 1, 7] + TR*F[1, 1, 9])*gcusp[0]*
      (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + Nc*TR*F[1, 1, 1]*gcusp[0] - 
       (TR*F[1, 1, 2]*gcusp[0])/Nc - (TR*F[1, 1, 3]*gcusp[0])/Nc + 
       Nc*TR*F[1, 1, 3]*gcusp[0] + (TR*F[1, 1, 6]*gcusp[0])/Nc + 
       (TR*F[1, 1, 7]*gcusp[0])/Nc - (TR*F[1, 1, 9]*gcusp[0])/Nc + 
       4*gquark[0] + (2*TR*gcusp[0]*tci[1, 2])/Nc - 2*Nc*TR*gcusp[0]*
        tci[1, 2]))/(8*eps^2), (cF^2*gcusp[0]^2)/(2*eps^4) - 
   (cF*gcusp[1])/(4*eps^2) - 
   (cF*gcusp[0]*((-3*beta[0])/2 - (TR*F[1, 1, 1]*gcusp[0])/Nc + 
      Nc*TR*F[1, 1, 1]*gcusp[0] - (TR*F[1, 1, 2]*gcusp[0])/Nc - 
      (TR*F[1, 1, 3]*gcusp[0])/Nc + Nc*TR*F[1, 1, 3]*gcusp[0] + 
      (TR*F[1, 1, 6]*gcusp[0])/Nc + (TR*F[1, 1, 7]*gcusp[0])/Nc - 
      (TR*F[1, 1, 9]*gcusp[0])/Nc + 4*gquark[0] + (2*TR*gcusp[0]*tci[1, 2])/
       Nc - 2*Nc*TR*gcusp[0]*tci[1, 2]))/(2*eps^3) + 
   (-((TR*F[1, 1, 1]*gcusp[1])/Nc) + Nc*TR*F[1, 1, 1]*gcusp[1] - 
     (TR*F[1, 1, 2]*gcusp[1])/Nc - (TR*F[1, 1, 3]*gcusp[1])/Nc + 
     Nc*TR*F[1, 1, 3]*gcusp[1] + (TR*F[1, 1, 6]*gcusp[1])/Nc + 
     (TR*F[1, 1, 7]*gcusp[1])/Nc - (TR*F[1, 1, 9]*gcusp[1])/Nc + 
     4*gquark[1] + (2*TR*gcusp[1]*tci[1, 2])/Nc - 2*Nc*TR*gcusp[1]*tci[1, 2])/
    (4*eps) + ((TR*F[1, 1, 2] - TR*F[1, 1, 6] - TR*F[1, 1, 7] + 
       TR*F[1, 1, 9])*gcusp[0]^2*(-(TR*F[1, 1, 6]) - TR*F[1, 1, 7] + 
       TR*(F[1, 1, 1] - tci[1, 2]) + TR*(F[1, 1, 3] - tci[1, 2])) + 
     (-((TR*F[1, 1, 1]*gcusp[0])/Nc) + Nc*TR*F[1, 1, 1]*gcusp[0] - 
       (TR*F[1, 1, 2]*gcusp[0])/Nc - (TR*F[1, 1, 3]*gcusp[0])/Nc + 
       Nc*TR*F[1, 1, 3]*gcusp[0] + (TR*F[1, 1, 6]*gcusp[0])/Nc + 
       (TR*F[1, 1, 7]*gcusp[0])/Nc - (TR*F[1, 1, 9]*gcusp[0])/Nc + 
       4*gquark[0] + (2*TR*gcusp[0]*tci[1, 2])/Nc - 2*Nc*TR*gcusp[0]*
        tci[1, 2])*(-2*beta[0] - (TR*F[1, 1, 1]*gcusp[0])/Nc + 
       Nc*TR*F[1, 1, 1]*gcusp[0] - (TR*F[1, 1, 2]*gcusp[0])/Nc - 
       (TR*F[1, 1, 3]*gcusp[0])/Nc + Nc*TR*F[1, 1, 3]*gcusp[0] + 
       (TR*F[1, 1, 6]*gcusp[0])/Nc + (TR*F[1, 1, 7]*gcusp[0])/Nc - 
       (TR*F[1, 1, 9]*gcusp[0])/Nc + 4*gquark[0] + (2*TR*gcusp[0]*tci[1, 2])/
        Nc - 2*Nc*TR*gcusp[0]*tci[1, 2]))/(8*eps^2)}}
