F1L["d12d34", "Ncpm2", "nfp0"]*(-Log[mu2]^2 + 
   Log[mu2]*(13/3 + F[1, 1, 1] + F[1, 1, 3] - 2*tci[1, 2])) + 
 A0L["d14d23", "Ncp0"]*(Log[mu2]^3*(F[1, 1, 1] + F[1, 1, 3] - F[1, 1, 6] - 
     F[1, 1, 7] - 2*tci[1, 2]) - Log[mu2]^2*(F[1, 1, 1] + F[1, 1, 3] - 
     F[1, 1, 6] - F[1, 1, 7] - 2*tci[1, 2])*(-3 + F[1, 1, 1] + F[1, 1, 2] + 
     F[1, 1, 3] - F[1, 1, 6] - F[1, 1, 7] + F[1, 1, 9] - 2*tci[1, 2])) + 
 F1L["d14d23", "Ncpm1", "nfp0"]*Log[mu2]*(F[1, 1, 1] + F[1, 1, 3] - 
   F[1, 1, 6] - F[1, 1, 7] - 2*tci[1, 2]) + F1L["d12d34", "Ncp0", "nfp0"]*
  (Log[mu2]^2 + Log[mu2]*(3 - F[1, 1, 1] - F[1, 1, 2] - F[1, 1, 3] + 
     F[1, 1, 6] + F[1, 1, 7] - F[1, 1, 9] + 2*tci[1, 2])) + 
 A0L["d12d34", "Ncpm1"]*(-Log[mu2]^4 + 
   Log[mu2]^3*(-10/9 + 2*F[1, 1, 1] + F[1, 1, 2] + 2*F[1, 1, 3] - 
     F[1, 1, 6] - F[1, 1, 7] + F[1, 1, 9] - 4*tci[1, 2]) + 
   (Log[mu2]^2*(269 - 18*F[1, 1, 1]^2 + 9*F[1, 1, 3] - 18*F[1, 1, 3]^2 + 
      45*F[1, 1, 6] + 9*F[1, 1, 3]*F[1, 1, 6] + 9*F[1, 1, 6]^2 + 
      45*F[1, 1, 7] + 9*F[1, 1, 3]*F[1, 1, 7] + 18*F[1, 1, 6]*F[1, 1, 7] + 
      9*F[1, 1, 7]^2 - 45*F[1, 1, 9] - 9*F[1, 1, 3]*F[1, 1, 9] - 
      9*F[1, 1, 6]*F[1, 1, 9] - 9*F[1, 1, 7]*F[1, 1, 9] + 6*tci[1, 1]^2 - 
      9*F[1, 1, 1]*(-1 + F[1, 1, 2] + 4*F[1, 1, 3] - F[1, 1, 6] - 
        F[1, 1, 7] + F[1, 1, 9] - 8*tci[1, 2]) - 
      9*F[1, 1, 2]*(5 + F[1, 1, 3] + F[1, 1, 6] + F[1, 1, 7] - 2*tci[1, 2]) - 
      18*tci[1, 2] + 72*F[1, 1, 3]*tci[1, 2] - 18*F[1, 1, 6]*tci[1, 2] - 
      18*F[1, 1, 7]*tci[1, 2] + 18*F[1, 1, 9]*tci[1, 2] - 72*tci[1, 2]^2))/
    18 + (Log[mu2]*(1042 - 402*F[1, 1, 3] + 402*F[1, 1, 6] + 402*F[1, 1, 7] - 
      402*F[1, 1, 9] + 9*tci[1, 1]^2 - 18*F[1, 1, 3]*tci[1, 1]^2 + 
      18*F[1, 1, 6]*tci[1, 1]^2 + 18*F[1, 1, 7]*tci[1, 1]^2 - 
      18*F[1, 1, 9]*tci[1, 1]^2 - 6*F[1, 1, 1]*(67 + 3*tci[1, 1]^2) - 
      6*F[1, 1, 2]*(67 + 3*tci[1, 1]^2) + 804*tci[1, 2] + 
      36*tci[1, 1]^2*tci[1, 2] - 108*tcr[3, 3]))/54)
