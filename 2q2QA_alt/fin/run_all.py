#!/usr/bin/env python3

import glob
import subprocess

amp2list = glob.glob('../eps_opt/rcf_1L*');

print(len(amp2list), 'amplitudes found');

amp2list = [a.split('_') for a in amp2list];

inputs = [{"psmode": a[-1].split('.')[0],
  "helicity": a[-2], 
  "ncfactor": a[-3], 
  "closedloopfactor": a[-6]+'_'+a[-5]+'_'+a[-4], 
  "colourfactor": a[-7],
  "looporder": a[-9]} for a in amp2list];

print('=======================================\n',flush=True);

for j in range(len(amp2list)):
  print('running ', j, '/', len(amp2list)-1,flush=True);
  subprocess.run(['wolfram','-script', 'make_fin_rem.wl', 
    '-looporder', inputs[j]["looporder"],
    '-helicity', inputs[j]["helicity"], 
    '-ncfactor', inputs[j]["ncfactor"],
    '-closedloopfactor', inputs[j]["closedloopfactor"],
    '-colourfactor', inputs[j]["colourfactor"],
    '-psmode', inputs[j]["psmode"]]);
  print('=======================================\n',flush=True);

