(* ::Package:: *)

(* example of how to construct finite remainders *)


SetDirectory[NotebookDirectory[]];


(* one-loop *)


(* the special function monomials are defined globally at each loop level *)
sfm1=Get["sfm_1L_2q2QA.m"];


(* rational coefficient functions *)
rcf1=Get["rcf_1L_2q2QA_d12d34__Oqlm8qlm6_OGAqkm4qkm2Qqk_Ncp0_m++m+_PSanalyticX1.m"];


(* rational coefficient sparse matrix *)
rcsm1=Get["rcsm_1L_2q2QA_d12d34__Oqlm8qlm6_OGAqkm4qkm2Qqk_Ncp0_m++m+_PSanalyticX1.m"];


fin = Plus @@ ( (rcf1[[1]] /. rcf1[[2]]) . rcsm1[[#]] . sfm1 * eps^(#-3)& /@ Range[5] )


(* ex[1] restoration: {"-+-++","-++-+"}*ex[1]^2 only *)
