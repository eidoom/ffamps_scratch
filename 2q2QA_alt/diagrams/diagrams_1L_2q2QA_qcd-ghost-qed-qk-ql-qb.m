(*
* file generated by qgraf-3.6.2
*
* output='diagrams_1L_2q2QA_qcd-ghost-qed-qk-ql-qb.m';
* style='../../styles/mathematica.sty';
* model='../../models/qcd-ghost-qed-qk-ql-qb.model';
* in=;
* out=qk,QK,ql,QL,GA;
* loops=1;
* loop_momentum=k;
* options=notadpole,onshell,nosnail;
* true = vsum[gpow,4,4];
*  true = vsum[epow,1,1];
*)
 
 QGRAFdiagram[1]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[GL[3,q3+q4]]*
 prop[qk[5,-k1]]*
 prop[qk[7,-k1-q5]]*
 prop[qk[9,-k1+q1+q2]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[8,k1+q5],GA[-10,-q5],qk[5,-k1]]*
 vrtx[QK[6,k1],GL[2,-q1-q2],qk[9,-k1+q1+q2]]*
 vrtx[QK[10,k1-q1-q2],GL[4,-q3-q4],qk[7,-k1-q5]];
 
 QGRAFdiagram[2]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[GL[3,q3+q4]]*
 prop[qk[5,k1]]*
 prop[qk[7,k1+q5]]*
 prop[qk[9,k1-q1-q2]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[6,-k1],GA[-10,-q5],qk[7,k1+q5]]*
 vrtx[QK[10,-k1+q1+q2],GL[2,-q1-q2],qk[5,k1]]*
 vrtx[QK[8,-k1-q5],GL[4,-q3-q4],qk[9,k1-q1-q2]];
 
 QGRAFdiagram[3]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[GL[3,q3+q4]]*
 prop[ql[5,-k1]]*
 prop[ql[7,-k1-q5]]*
 prop[ql[9,-k1+q1+q2]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QL[8,k1+q5],GA[-10,-q5],ql[5,-k1]]*
 vrtx[QL[6,k1],GL[2,-q1-q2],ql[9,-k1+q1+q2]]*
 vrtx[QL[10,k1-q1-q2],GL[4,-q3-q4],ql[7,-k1-q5]];
 
 QGRAFdiagram[4]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[GL[3,q3+q4]]*
 prop[ql[5,k1]]*
 prop[ql[7,k1+q5]]*
 prop[ql[9,k1-q1-q2]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QL[6,-k1],GA[-10,-q5],ql[7,k1+q5]]*
 vrtx[QL[10,-k1+q1+q2],GL[2,-q1-q2],ql[5,k1]]*
 vrtx[QL[8,-k1-q5],GL[4,-q3-q4],ql[9,k1-q1-q2]];
 
 QGRAFdiagram[5]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[GL[3,q3+q4]]*
 prop[qb[5,-k1]]*
 prop[qb[7,-k1-q5]]*
 prop[qb[9,-k1+q1+q2]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QB[8,k1+q5],GA[-10,-q5],qb[5,-k1]]*
 vrtx[QB[6,k1],GL[2,-q1-q2],qb[9,-k1+q1+q2]]*
 vrtx[QB[10,k1-q1-q2],GL[4,-q3-q4],qb[7,-k1-q5]];
 
 QGRAFdiagram[6]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[GL[3,q3+q4]]*
 prop[qb[5,k1]]*
 prop[qb[7,k1+q5]]*
 prop[qb[9,k1-q1-q2]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QB[6,-k1],GA[-10,-q5],qb[7,k1+q5]]*
 vrtx[QB[10,-k1+q1+q2],GL[2,-q1-q2],qb[5,k1]]*
 vrtx[QB[8,-k1-q5],GL[4,-q3-q4],qb[9,k1-q1-q2]];
 
 QGRAFdiagram[7]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,q3+q5]]*
 prop[ql[5,k1]]*
 prop[GL[7,k1+q4]]*
 prop[ql[9,k1-q1-q2]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[3,q3+q5]]*
 vrtx[QL[6,-k1],GL[7,k1+q4],ql[-8,-q4]]*
 vrtx[QL[10,-k1+q1+q2],GL[2,-q1-q2],ql[5,k1]]*
 vrtx[QL[4,-q3-q5],GL[8,-k1-q4],ql[9,k1-q1-q2]];
 
 QGRAFdiagram[8]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,q3+q5]]*
 prop[GL[5,-k1]]*
 prop[ql[7,-k1-q4]]*
 prop[GL[9,-k1+q1+q2]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[3,q3+q5]]*
 vrtx[QL[8,k1+q4],GL[5,-k1],ql[-8,-q4]]*
 vrtx[GL[2,-q1-q2],GL[6,k1],GL[9,-k1+q1+q2]]*
 vrtx[QL[4,-q3-q5],GL[10,k1-q1-q2],ql[7,-k1-q4]];
 
 QGRAFdiagram[9]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,-q4-q5]]*
 prop[ql[5,-k1]]*
 prop[GL[7,k1+q3]]*
 prop[ql[9,-k1+q1+q2]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[4,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QL[-6,-q3],GL[7,k1+q3],ql[5,-k1]]*
 vrtx[QL[6,k1],GL[2,-q1-q2],ql[9,-k1+q1+q2]]*
 vrtx[QL[10,k1-q1-q2],GL[8,-k1-q3],ql[3,-q4-q5]];
 
 QGRAFdiagram[10]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,-q4-q5]]*
 prop[GL[5,-k1]]*
 prop[ql[7,k1+q3]]*
 prop[GL[9,-k1+q1+q2]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[4,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QL[-6,-q3],GL[5,-k1],ql[7,k1+q3]]*
 vrtx[GL[2,-q1-q2],GL[6,k1],GL[9,-k1+q1+q2]]*
 vrtx[QL[8,-k1-q3],GL[10,k1-q1-q2],ql[3,-q4-q5]];
 
 QGRAFdiagram[11]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,q3+q4]]*
 prop[qk[5,k1]]*
 prop[GL[7,k1+q2]]*
 prop[GL[9,-k1+q1+q5]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[6,-k1],GL[7,k1+q2],qk[-4,-q2]]*
 vrtx[QK[2,-q1-q5],GL[9,-k1+q1+q5],qk[5,k1]]*
 vrtx[GL[4,-q3-q4],GL[8,-k1-q2],GL[10,k1-q1-q5]];
 
 QGRAFdiagram[12]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-k1]]*
 prop[qk[7,-k1-q2]]*
 prop[qk[9,-k1+q1+q5]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[8,k1+q2],GL[5,-k1],qk[-4,-q2]]*
 vrtx[QK[2,-q1-q5],GL[6,k1],qk[9,-k1+q1+q5]]*
 vrtx[QK[10,k1-q1-q5],GL[4,-q3-q4],qk[7,-k1-q2]];
 
 QGRAFdiagram[13]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,q3+q4]]*
 prop[qk[5,-k1]]*
 prop[GL[7,k1+q1]]*
 prop[GL[9,-k1+q2+q5]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[7,k1+q1],qk[5,-k1]]*
 vrtx[QK[6,k1],GL[9,-k1+q2+q5],qk[1,-q2-q5]]*
 vrtx[GL[4,-q3-q4],GL[8,-k1-q1],GL[10,k1-q2-q5]];
 
 QGRAFdiagram[14]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-k1]]*
 prop[qk[7,k1+q1]]*
 prop[qk[9,k1-q2-q5]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[5,-k1],qk[7,k1+q1]]*
 vrtx[QK[10,-k1+q2+q5],GL[6,k1],qk[1,-q2-q5]]*
 vrtx[QK[8,-k1-q1],GL[4,-q3-q4],qk[9,k1-q2-q5]];
 
 QGRAFdiagram[15]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,q3+q5]]*
 prop[ql[5,q3+q5]]*
 prop[ql[7,k1+q3+q5]]*
 prop[GL[9,-k1]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[3,q3+q5]]*
 vrtx[QL[6,-q3-q5],GL[2,-q1-q2],ql[-8,-q4]]*
 vrtx[QL[4,-q3-q5],GL[9,-k1],ql[7,k1+q3+q5]]*
 vrtx[QL[8,-k1-q3-q5],GL[10,k1],ql[5,q3+q5]];
 
 QGRAFdiagram[16]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,-q4-q5]]*
 prop[ql[5,-q4-q5]]*
 prop[ql[7,-k1-q4-q5]]*
 prop[GL[9,-k1]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[4,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QL[-6,-q3],GL[2,-q1-q2],ql[5,-q4-q5]]*
 vrtx[QL[8,k1+q4+q5],GL[9,-k1],ql[3,-q4-q5]]*
 vrtx[QL[6,q4+q5],GL[10,k1],ql[7,-k1-q4-q5]];
 
 QGRAFdiagram[17]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-q3-q4]]*
 prop[qk[7,k1+q3+q4]]*
 prop[qk[9,k1]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[2,-q1-q5],GL[5,-q3-q4],qk[-4,-q2]]*
 vrtx[QK[10,-k1],GL[4,-q3-q4],qk[7,k1+q3+q4]]*
 vrtx[QK[8,-k1-q3-q4],GL[6,q3+q4],qk[9,k1]];
 
 QGRAFdiagram[18]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-q3-q4]]*
 prop[ql[7,k1+q3+q4]]*
 prop[ql[9,k1]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[2,-q1-q5],GL[5,-q3-q4],qk[-4,-q2]]*
 vrtx[QL[10,-k1],GL[4,-q3-q4],ql[7,k1+q3+q4]]*
 vrtx[QL[8,-k1-q3-q4],GL[6,q3+q4],ql[9,k1]];
 
 QGRAFdiagram[19]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-q3-q4]]*
 prop[qb[7,k1+q3+q4]]*
 prop[qb[9,k1]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[2,-q1-q5],GL[5,-q3-q4],qk[-4,-q2]]*
 vrtx[QB[10,-k1],GL[4,-q3-q4],qb[7,k1+q3+q4]]*
 vrtx[QB[8,-k1-q3-q4],GL[6,q3+q4],qb[9,k1]];
 
 QGRAFdiagram[20]:= (+1/2)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-q3-q4]]*
 prop[GL[7,k1+q3+q4]]*
 prop[GL[9,-k1]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[2,-q1-q5],GL[5,-q3-q4],qk[-4,-q2]]*
 vrtx[GL[4,-q3-q4],GL[7,k1+q3+q4],GL[9,-k1]]*
 vrtx[GL[6,q3+q4],GL[8,-k1-q3-q4],GL[10,k1]];
 
 QGRAFdiagram[21]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-q3-q4]]*
 prop[gh[7,k1+q3+q4]]*
 prop[gh[9,k1]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[2,-q1-q5],GL[5,-q3-q4],qk[-4,-q2]]*
 vrtx[GH[10,-k1],GL[4,-q3-q4],gh[7,k1+q3+q4]]*
 vrtx[GH[8,-k1-q3-q4],GL[6,q3+q4],gh[9,k1]];
 
 QGRAFdiagram[22]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-q3-q4]]*
 prop[qk[7,k1+q3+q4]]*
 prop[qk[9,k1]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[5,-q3-q4],qk[1,-q2-q5]]*
 vrtx[QK[10,-k1],GL[4,-q3-q4],qk[7,k1+q3+q4]]*
 vrtx[QK[8,-k1-q3-q4],GL[6,q3+q4],qk[9,k1]];
 
 QGRAFdiagram[23]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-q3-q4]]*
 prop[ql[7,k1+q3+q4]]*
 prop[ql[9,k1]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[5,-q3-q4],qk[1,-q2-q5]]*
 vrtx[QL[10,-k1],GL[4,-q3-q4],ql[7,k1+q3+q4]]*
 vrtx[QL[8,-k1-q3-q4],GL[6,q3+q4],ql[9,k1]];
 
 QGRAFdiagram[24]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-q3-q4]]*
 prop[qb[7,k1+q3+q4]]*
 prop[qb[9,k1]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[5,-q3-q4],qk[1,-q2-q5]]*
 vrtx[QB[10,-k1],GL[4,-q3-q4],qb[7,k1+q3+q4]]*
 vrtx[QB[8,-k1-q3-q4],GL[6,q3+q4],qb[9,k1]];
 
 QGRAFdiagram[25]:= (+1/2)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-q3-q4]]*
 prop[GL[7,k1+q3+q4]]*
 prop[GL[9,-k1]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[5,-q3-q4],qk[1,-q2-q5]]*
 vrtx[GL[4,-q3-q4],GL[7,k1+q3+q4],GL[9,-k1]]*
 vrtx[GL[6,q3+q4],GL[8,-k1-q3-q4],GL[10,k1]];
 
 QGRAFdiagram[26]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,q3+q4]]*
 prop[GL[5,-q3-q4]]*
 prop[gh[7,k1+q3+q4]]*
 prop[gh[9,k1]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[5,-q3-q4],qk[1,-q2-q5]]*
 vrtx[GH[10,-k1],GL[4,-q3-q4],gh[7,k1+q3+q4]]*
 vrtx[GH[8,-k1-q3-q4],GL[6,q3+q4],gh[9,k1]];
 
 QGRAFdiagram[27]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q3+q4]]*
 prop[qk[3,q1+q5]]*
 prop[qk[5,q1+q5]]*
 prop[qk[7,k1+q1+q5]]*
 prop[GL[9,-k1]]*
 vrtx[QL[-6,-q3],GL[1,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[3,q1+q5]]*
 vrtx[QK[6,-q1-q5],GL[2,-q3-q4],qk[-4,-q2]]*
 vrtx[QK[4,-q1-q5],GL[9,-k1],qk[7,k1+q1+q5]]*
 vrtx[QK[8,-k1-q1-q5],GL[10,k1],qk[5,q1+q5]];
 
 QGRAFdiagram[28]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q3+q4]]*
 prop[qk[3,-q2-q5]]*
 prop[qk[5,-q2-q5]]*
 prop[qk[7,-k1-q2-q5]]*
 prop[GL[9,-k1]]*
 vrtx[QL[-6,-q3],GL[1,q3+q4],ql[-8,-q4]]*
 vrtx[QK[4,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QK[-2,-q1],GL[2,-q3-q4],qk[5,-q2-q5]]*
 vrtx[QK[8,k1+q2+q5],GL[9,-k1],qk[3,-q2-q5]]*
 vrtx[QK[6,q2+q5],GL[10,k1],qk[7,-k1-q2-q5]];
 
 QGRAFdiagram[29]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,q3+q5]]*
 prop[GL[3,q1+q2]]*
 prop[GL[5,-q1-q2]]*
 prop[qk[7,k1+q1+q2]]*
 prop[qk[9,k1]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[1,q3+q5]]*
 vrtx[QK[-2,-q1],GL[3,q1+q2],qk[-4,-q2]]*
 vrtx[QL[2,-q3-q5],GL[5,-q1-q2],ql[-8,-q4]]*
 vrtx[QK[10,-k1],GL[4,-q1-q2],qk[7,k1+q1+q2]]*
 vrtx[QK[8,-k1-q1-q2],GL[6,q1+q2],qk[9,k1]];
 
 QGRAFdiagram[30]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,q3+q5]]*
 prop[GL[3,q1+q2]]*
 prop[GL[5,-q1-q2]]*
 prop[ql[7,k1+q1+q2]]*
 prop[ql[9,k1]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[1,q3+q5]]*
 vrtx[QK[-2,-q1],GL[3,q1+q2],qk[-4,-q2]]*
 vrtx[QL[2,-q3-q5],GL[5,-q1-q2],ql[-8,-q4]]*
 vrtx[QL[10,-k1],GL[4,-q1-q2],ql[7,k1+q1+q2]]*
 vrtx[QL[8,-k1-q1-q2],GL[6,q1+q2],ql[9,k1]];
 
 QGRAFdiagram[31]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,q3+q5]]*
 prop[GL[3,q1+q2]]*
 prop[GL[5,-q1-q2]]*
 prop[qb[7,k1+q1+q2]]*
 prop[qb[9,k1]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[1,q3+q5]]*
 vrtx[QK[-2,-q1],GL[3,q1+q2],qk[-4,-q2]]*
 vrtx[QL[2,-q3-q5],GL[5,-q1-q2],ql[-8,-q4]]*
 vrtx[QB[10,-k1],GL[4,-q1-q2],qb[7,k1+q1+q2]]*
 vrtx[QB[8,-k1-q1-q2],GL[6,q1+q2],qb[9,k1]];
 
 QGRAFdiagram[32]:= (+1/2)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,q3+q5]]*
 prop[GL[3,q1+q2]]*
 prop[GL[5,-q1-q2]]*
 prop[GL[7,k1+q1+q2]]*
 prop[GL[9,-k1]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[1,q3+q5]]*
 vrtx[QK[-2,-q1],GL[3,q1+q2],qk[-4,-q2]]*
 vrtx[QL[2,-q3-q5],GL[5,-q1-q2],ql[-8,-q4]]*
 vrtx[GL[4,-q1-q2],GL[7,k1+q1+q2],GL[9,-k1]]*
 vrtx[GL[6,q1+q2],GL[8,-k1-q1-q2],GL[10,k1]];
 
 QGRAFdiagram[33]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,q3+q5]]*
 prop[GL[3,q1+q2]]*
 prop[GL[5,-q1-q2]]*
 prop[gh[7,k1+q1+q2]]*
 prop[gh[9,k1]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[1,q3+q5]]*
 vrtx[QK[-2,-q1],GL[3,q1+q2],qk[-4,-q2]]*
 vrtx[QL[2,-q3-q5],GL[5,-q1-q2],ql[-8,-q4]]*
 vrtx[GH[10,-k1],GL[4,-q1-q2],gh[7,k1+q1+q2]]*
 vrtx[GH[8,-k1-q1-q2],GL[6,q1+q2],gh[9,k1]];
 
 QGRAFdiagram[34]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,-q4-q5]]*
 prop[GL[3,q1+q2]]*
 prop[GL[5,-q1-q2]]*
 prop[qk[7,k1+q1+q2]]*
 prop[qk[9,k1]]*
 vrtx[QL[2,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[3,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[5,-q1-q2],ql[1,-q4-q5]]*
 vrtx[QK[10,-k1],GL[4,-q1-q2],qk[7,k1+q1+q2]]*
 vrtx[QK[8,-k1-q1-q2],GL[6,q1+q2],qk[9,k1]];
 
 QGRAFdiagram[35]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,-q4-q5]]*
 prop[GL[3,q1+q2]]*
 prop[GL[5,-q1-q2]]*
 prop[ql[7,k1+q1+q2]]*
 prop[ql[9,k1]]*
 vrtx[QL[2,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[3,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[5,-q1-q2],ql[1,-q4-q5]]*
 vrtx[QL[10,-k1],GL[4,-q1-q2],ql[7,k1+q1+q2]]*
 vrtx[QL[8,-k1-q1-q2],GL[6,q1+q2],ql[9,k1]];
 
 QGRAFdiagram[36]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,-q4-q5]]*
 prop[GL[3,q1+q2]]*
 prop[GL[5,-q1-q2]]*
 prop[qb[7,k1+q1+q2]]*
 prop[qb[9,k1]]*
 vrtx[QL[2,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[3,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[5,-q1-q2],ql[1,-q4-q5]]*
 vrtx[QB[10,-k1],GL[4,-q1-q2],qb[7,k1+q1+q2]]*
 vrtx[QB[8,-k1-q1-q2],GL[6,q1+q2],qb[9,k1]];
 
 QGRAFdiagram[37]:= (+1/2)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,-q4-q5]]*
 prop[GL[3,q1+q2]]*
 prop[GL[5,-q1-q2]]*
 prop[GL[7,k1+q1+q2]]*
 prop[GL[9,-k1]]*
 vrtx[QL[2,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[3,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[5,-q1-q2],ql[1,-q4-q5]]*
 vrtx[GL[4,-q1-q2],GL[7,k1+q1+q2],GL[9,-k1]]*
 vrtx[GL[6,q1+q2],GL[8,-k1-q1-q2],GL[10,k1]];
 
 QGRAFdiagram[38]:= (-1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,-q4-q5]]*
 prop[GL[3,q1+q2]]*
 prop[GL[5,-q1-q2]]*
 prop[gh[7,k1+q1+q2]]*
 prop[gh[9,k1]]*
 vrtx[QL[2,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[3,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[5,-q1-q2],ql[1,-q4-q5]]*
 vrtx[GH[10,-k1],GL[4,-q1-q2],gh[7,k1+q1+q2]]*
 vrtx[GH[8,-k1-q1-q2],GL[6,q1+q2],gh[9,k1]];
 
 QGRAFdiagram[39]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[GL[3,-k1]]*
 prop[ql[5,k1+q3]]*
 prop[ql[7,k1-q4]]*
 prop[ql[9,k1+q3+q5]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,-k1],ql[5,k1+q3]]*
 vrtx[QL[8,-k1+q4],GL[4,k1],ql[-8,-q4]]*
 vrtx[QL[6,-k1-q3],GA[-10,-q5],ql[9,k1+q3+q5]]*
 vrtx[QL[10,-k1-q3-q5],GL[2,-q1-q2],ql[7,k1-q4]];
 
 QGRAFdiagram[40]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[GL[3,-k1]]*
 prop[ql[5,-k1-q4]]*
 prop[ql[7,-k1+q3]]*
 prop[ql[9,-k1-q4-q5]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[6,k1+q4],GL[3,-k1],ql[-8,-q4]]*
 vrtx[QL[-6,-q3],GL[4,k1],ql[7,-k1+q3]]*
 vrtx[QL[10,k1+q4+q5],GA[-10,-q5],ql[5,-k1-q4]]*
 vrtx[QL[8,k1-q3],GL[2,-q1-q2],ql[9,-k1-q4-q5]];
 
 QGRAFdiagram[41]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,k1]]*
 prop[ql[5,k1+q5]]*
 prop[GL[7,-k1+q3]]*
 prop[GL[9,k1+q4+q5]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[4,-k1],GA[-10,-q5],ql[5,k1+q5]]*
 vrtx[QL[-6,-q3],GL[7,-k1+q3],ql[3,k1]]*
 vrtx[QL[6,-k1-q5],GL[9,k1+q4+q5],ql[-8,-q4]]*
 vrtx[GL[2,-q1-q2],GL[8,k1-q3],GL[10,-k1-q4-q5]];
 
 QGRAFdiagram[42]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,-k1]]*
 prop[ql[5,k1+q3]]*
 prop[qk[7,k1-q2]]*
 prop[GL[9,k1+q3+q4]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QL[-6,-q3],GL[3,-k1],ql[5,k1+q3]]*
 vrtx[QK[8,-k1+q2],GL[4,k1],qk[-4,-q2]]*
 vrtx[QL[6,-k1-q3],GL[9,k1+q3+q4],ql[-8,-q4]]*
 vrtx[QK[2,-q1-q5],GL[10,-k1-q3-q4],qk[7,k1-q2]];
 
 QGRAFdiagram[43]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,-k1]]*
 prop[ql[5,-k1-q4]]*
 prop[qk[7,k1-q2]]*
 prop[GL[9,k1+q3+q4]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QL[6,k1+q4],GL[3,-k1],ql[-8,-q4]]*
 vrtx[QK[8,-k1+q2],GL[4,k1],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[9,k1+q3+q4],ql[5,-k1-q4]]*
 vrtx[QK[2,-q1-q5],GL[10,-k1-q3-q4],qk[7,k1-q2]];
 
 QGRAFdiagram[44]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,-k1]]*
 prop[ql[5,k1+q3]]*
 prop[qk[7,-k1+q1]]*
 prop[GL[9,k1+q3+q4]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,-k1],ql[5,k1+q3]]*
 vrtx[QK[-2,-q1],GL[4,k1],qk[7,-k1+q1]]*
 vrtx[QL[6,-k1-q3],GL[9,k1+q3+q4],ql[-8,-q4]]*
 vrtx[QK[8,k1-q1],GL[10,-k1-q3-q4],qk[1,-q2-q5]];
 
 QGRAFdiagram[45]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,-k1]]*
 prop[ql[5,-k1-q4]]*
 prop[qk[7,-k1+q1]]*
 prop[GL[9,k1+q3+q4]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QL[6,k1+q4],GL[3,-k1],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[4,k1],qk[7,-k1+q1]]*
 vrtx[QL[-6,-q3],GL[9,k1+q3+q4],ql[5,-k1-q4]]*
 vrtx[QK[8,k1-q1],GL[10,-k1-q3-q4],qk[1,-q2-q5]];
 
 QGRAFdiagram[46]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q3+q4]]*
 prop[GL[3,-k1]]*
 prop[qk[5,k1+q1]]*
 prop[qk[7,k1-q2]]*
 prop[qk[9,k1+q1+q5]]*
 vrtx[QL[-6,-q3],GL[1,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[3,-k1],qk[5,k1+q1]]*
 vrtx[QK[8,-k1+q2],GL[4,k1],qk[-4,-q2]]*
 vrtx[QK[6,-k1-q1],GA[-10,-q5],qk[9,k1+q1+q5]]*
 vrtx[QK[10,-k1-q1-q5],GL[2,-q3-q4],qk[7,k1-q2]];
 
 QGRAFdiagram[47]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q3+q4]]*
 prop[GL[3,-k1]]*
 prop[qk[5,-k1-q2]]*
 prop[qk[7,-k1+q1]]*
 prop[qk[9,-k1-q2-q5]]*
 vrtx[QL[-6,-q3],GL[1,q3+q4],ql[-8,-q4]]*
 vrtx[QK[6,k1+q2],GL[3,-k1],qk[-4,-q2]]*
 vrtx[QK[-2,-q1],GL[4,k1],qk[7,-k1+q1]]*
 vrtx[QK[10,k1+q2+q5],GA[-10,-q5],qk[5,-k1-q2]]*
 vrtx[QK[8,k1-q1],GL[2,-q3-q4],qk[9,-k1-q2-q5]];
 
 QGRAFdiagram[48]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q3+q4]]*
 prop[qk[3,k1]]*
 prop[qk[5,k1+q5]]*
 prop[GL[7,-k1+q1]]*
 prop[GL[9,k1+q2+q5]]*
 vrtx[QL[-6,-q3],GL[1,q3+q4],ql[-8,-q4]]*
 vrtx[QK[4,-k1],GA[-10,-q5],qk[5,k1+q5]]*
 vrtx[QK[-2,-q1],GL[7,-k1+q1],qk[3,k1]]*
 vrtx[QK[6,-k1-q5],GL[9,k1+q2+q5],qk[-4,-q2]]*
 vrtx[GL[2,-q3-q4],GL[8,k1-q1],GL[10,-k1-q2-q5]];
 
 QGRAFdiagram[49]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,q3+q5]]*
 prop[qk[3,-k1]]*
 prop[GL[5,k1+q1]]*
 prop[GL[7,-k1+q2]]*
 prop[ql[9,-k1-q1-q4]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[1,q3+q5]]*
 vrtx[QK[-2,-q1],GL[5,k1+q1],qk[3,-k1]]*
 vrtx[QK[4,k1],GL[7,-k1+q2],qk[-4,-q2]]*
 vrtx[QL[10,k1+q1+q4],GL[6,-k1-q1],ql[-8,-q4]]*
 vrtx[QL[2,-q3-q5],GL[8,k1-q2],ql[9,-k1-q1-q4]];
 
 QGRAFdiagram[50]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,q3+q5]]*
 prop[qk[3,k1]]*
 prop[GL[5,k1+q2]]*
 prop[GL[7,-k1+q1]]*
 prop[ql[9,-k1-q2-q4]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[1,q3+q5]]*
 vrtx[QK[4,-k1],GL[5,k1+q2],qk[-4,-q2]]*
 vrtx[QK[-2,-q1],GL[7,-k1+q1],qk[3,k1]]*
 vrtx[QL[10,k1+q2+q4],GL[6,-k1-q2],ql[-8,-q4]]*
 vrtx[QL[2,-q3-q5],GL[8,k1-q1],ql[9,-k1-q2-q4]];
 
 QGRAFdiagram[51]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,-q4-q5]]*
 prop[qk[3,-k1]]*
 prop[GL[5,k1+q1]]*
 prop[GL[7,-k1+q2]]*
 prop[ql[9,k1+q1+q3]]*
 vrtx[QL[2,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[5,k1+q1],qk[3,-k1]]*
 vrtx[QK[4,k1],GL[7,-k1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[6,-k1-q1],ql[9,k1+q1+q3]]*
 vrtx[QL[10,-k1-q1-q3],GL[8,k1-q2],ql[1,-q4-q5]];
 
 QGRAFdiagram[52]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,-q4-q5]]*
 prop[qk[3,k1]]*
 prop[GL[5,k1+q2]]*
 prop[GL[7,-k1+q1]]*
 prop[ql[9,k1+q2+q3]]*
 vrtx[QL[2,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QK[4,-k1],GL[5,k1+q2],qk[-4,-q2]]*
 vrtx[QK[-2,-q1],GL[7,-k1+q1],qk[3,k1]]*
 vrtx[QL[-6,-q3],GL[6,-k1-q2],ql[9,k1+q2+q3]]*
 vrtx[QL[10,-k1-q2-q3],GL[8,k1-q1],ql[1,-q4-q5]];
 
 QGRAFdiagram[53]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,-q4-q5]]*
 prop[ql[5,k1]]*
 prop[GL[7,k1+q4]]*
 prop[ql[9,k1-q5]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[2,-q1-q2],ql[3,-q4-q5]]*
 vrtx[QL[6,-k1],GL[7,k1+q4],ql[-8,-q4]]*
 vrtx[QL[10,-k1+q5],GA[-10,-q5],ql[5,k1]]*
 vrtx[QL[4,q4+q5],GL[8,-k1-q4],ql[9,k1-q5]];
 
 QGRAFdiagram[54]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,q3+q5]]*
 prop[ql[5,-k1]]*
 prop[GL[7,k1+q3]]*
 prop[ql[9,-k1+q5]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[4,-q3-q5],GL[2,-q1-q2],ql[-8,-q4]]*
 vrtx[QL[-6,-q3],GL[7,k1+q3],ql[5,-k1]]*
 vrtx[QL[6,k1],GA[-10,-q5],ql[9,-k1+q5]]*
 vrtx[QL[10,k1-q5],GL[8,-k1-q3],ql[3,q3+q5]];
 
 QGRAFdiagram[55]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,-q3-q4]]*
 prop[ql[5,-k1]]*
 prop[GL[7,k1+q3]]*
 prop[GL[9,-k1+q4]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QK[2,-q1-q5],GL[3,-q3-q4],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[7,k1+q3],ql[5,-k1]]*
 vrtx[QL[6,k1],GL[9,-k1+q4],ql[-8,-q4]]*
 vrtx[GL[4,q3+q4],GL[8,-k1-q3],GL[10,k1-q4]];
 
 QGRAFdiagram[56]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,-q3-q4]]*
 prop[GL[5,-k1]]*
 prop[ql[7,k1+q3]]*
 prop[ql[9,k1-q4]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QK[2,-q1-q5],GL[3,-q3-q4],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[5,-k1],ql[7,k1+q3]]*
 vrtx[QL[10,-k1+q4],GL[6,k1],ql[-8,-q4]]*
 vrtx[QL[8,-k1-q3],GL[4,q3+q4],ql[9,k1-q4]];
 
 QGRAFdiagram[57]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,-q3-q4]]*
 prop[ql[5,-k1]]*
 prop[GL[7,k1+q3]]*
 prop[GL[9,-k1+q4]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QK[-2,-q1],GL[3,-q3-q4],qk[1,-q2-q5]]*
 vrtx[QL[-6,-q3],GL[7,k1+q3],ql[5,-k1]]*
 vrtx[QL[6,k1],GL[9,-k1+q4],ql[-8,-q4]]*
 vrtx[GL[4,q3+q4],GL[8,-k1-q3],GL[10,k1-q4]];
 
 QGRAFdiagram[58]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,-q3-q4]]*
 prop[GL[5,-k1]]*
 prop[ql[7,k1+q3]]*
 prop[ql[9,k1-q4]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QK[-2,-q1],GL[3,-q3-q4],qk[1,-q2-q5]]*
 vrtx[QL[-6,-q3],GL[5,-k1],ql[7,k1+q3]]*
 vrtx[QL[10,-k1+q4],GL[6,k1],ql[-8,-q4]]*
 vrtx[QL[8,-k1-q3],GL[4,q3+q4],ql[9,k1-q4]];
 
 QGRAFdiagram[59]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q3+q4]]*
 prop[qk[3,-q2-q5]]*
 prop[qk[5,k1]]*
 prop[GL[7,k1+q2]]*
 prop[qk[9,k1-q5]]*
 vrtx[QL[-6,-q3],GL[1,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[2,-q3-q4],qk[3,-q2-q5]]*
 vrtx[QK[6,-k1],GL[7,k1+q2],qk[-4,-q2]]*
 vrtx[QK[10,-k1+q5],GA[-10,-q5],qk[5,k1]]*
 vrtx[QK[4,q2+q5],GL[8,-k1-q2],qk[9,k1-q5]];
 
 QGRAFdiagram[60]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q3+q4]]*
 prop[qk[3,q1+q5]]*
 prop[qk[5,-k1]]*
 prop[GL[7,k1+q1]]*
 prop[qk[9,-k1+q5]]*
 vrtx[QL[-6,-q3],GL[1,q3+q4],ql[-8,-q4]]*
 vrtx[QK[4,-q1-q5],GL[2,-q3-q4],qk[-4,-q2]]*
 vrtx[QK[-2,-q1],GL[7,k1+q1],qk[5,-k1]]*
 vrtx[QK[6,k1],GA[-10,-q5],qk[9,-k1+q5]]*
 vrtx[QK[10,k1-q5],GL[8,-k1-q1],qk[3,q1+q5]];
 
 QGRAFdiagram[61]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,q3+q5]]*
 prop[GL[3,-q1-q2]]*
 prop[qk[5,-k1]]*
 prop[GL[7,k1+q1]]*
 prop[GL[9,-k1+q2]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[1,q3+q5]]*
 vrtx[QL[2,-q3-q5],GL[3,-q1-q2],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[7,k1+q1],qk[5,-k1]]*
 vrtx[QK[6,k1],GL[9,-k1+q2],qk[-4,-q2]]*
 vrtx[GL[4,q1+q2],GL[8,-k1-q1],GL[10,k1-q2]];
 
 QGRAFdiagram[62]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,q3+q5]]*
 prop[GL[3,-q1-q2]]*
 prop[GL[5,-k1]]*
 prop[qk[7,k1+q1]]*
 prop[qk[9,k1-q2]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[1,q3+q5]]*
 vrtx[QL[2,-q3-q5],GL[3,-q1-q2],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[5,-k1],qk[7,k1+q1]]*
 vrtx[QK[10,-k1+q2],GL[6,k1],qk[-4,-q2]]*
 vrtx[QK[8,-k1-q1],GL[4,q1+q2],qk[9,k1-q2]];
 
 QGRAFdiagram[63]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,-q4-q5]]*
 prop[GL[3,-q1-q2]]*
 prop[qk[5,-k1]]*
 prop[GL[7,k1+q1]]*
 prop[GL[9,-k1+q2]]*
 vrtx[QL[2,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QL[-6,-q3],GL[3,-q1-q2],ql[1,-q4-q5]]*
 vrtx[QK[-2,-q1],GL[7,k1+q1],qk[5,-k1]]*
 vrtx[QK[6,k1],GL[9,-k1+q2],qk[-4,-q2]]*
 vrtx[GL[4,q1+q2],GL[8,-k1-q1],GL[10,k1-q2]];
 
 QGRAFdiagram[64]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[ql[1,-q4-q5]]*
 prop[GL[3,-q1-q2]]*
 prop[GL[5,-k1]]*
 prop[qk[7,k1+q1]]*
 prop[qk[9,k1-q2]]*
 vrtx[QL[2,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QL[-6,-q3],GL[3,-q1-q2],ql[1,-q4-q5]]*
 vrtx[QK[-2,-q1],GL[5,-k1],qk[7,k1+q1]]*
 vrtx[QK[10,-k1+q2],GL[6,k1],qk[-4,-q2]]*
 vrtx[QK[8,-k1-q1],GL[4,q1+q2],qk[9,k1-q2]];
 
 QGRAFdiagram[65]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-k1]]*
 prop[GL[3,k1+q1]]*
 prop[GL[5,-k1+q2]]*
 prop[ql[7,k1+q1+q3]]*
 prop[ql[9,k1-q2-q4]]*
 vrtx[QK[-2,-q1],GL[3,k1+q1],qk[1,-k1]]*
 vrtx[QK[2,k1],GL[5,-k1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[4,-k1-q1],ql[7,k1+q1+q3]]*
 vrtx[QL[10,-k1+q2+q4],GL[6,k1-q2],ql[-8,-q4]]*
 vrtx[QL[8,-k1-q1-q3],GA[-10,-q5],ql[9,k1-q2-q4]];
 
 QGRAFdiagram[66]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-k1]]*
 prop[GL[3,k1+q1]]*
 prop[GL[5,-k1+q2]]*
 prop[ql[7,-k1-q1-q4]]*
 prop[ql[9,-k1+q2+q3]]*
 vrtx[QK[-2,-q1],GL[3,k1+q1],qk[1,-k1]]*
 vrtx[QK[2,k1],GL[5,-k1+q2],qk[-4,-q2]]*
 vrtx[QL[8,k1+q1+q4],GL[4,-k1-q1],ql[-8,-q4]]*
 vrtx[QL[-6,-q3],GL[6,k1-q2],ql[9,-k1+q2+q3]]*
 vrtx[QL[10,k1-q2-q3],GA[-10,-q5],ql[7,-k1-q1-q4]];
 
 QGRAFdiagram[67]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,-k1]]*
 prop[qk[3,k1+q1]]*
 prop[ql[5,-k1+q3]]*
 prop[qk[7,k1+q1+q5]]*
 prop[GL[9,-k1+q3+q4]]*
 vrtx[QK[-2,-q1],GL[1,-k1],qk[3,k1+q1]]*
 vrtx[QL[-6,-q3],GL[2,k1],ql[5,-k1+q3]]*
 vrtx[QK[4,-k1-q1],GA[-10,-q5],qk[7,k1+q1+q5]]*
 vrtx[QL[6,k1-q3],GL[9,-k1+q3+q4],ql[-8,-q4]]*
 vrtx[QK[8,-k1-q1-q5],GL[10,k1-q3-q4],qk[-4,-q2]];
 
 QGRAFdiagram[68]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,-k1]]*
 prop[qk[3,k1+q1]]*
 prop[ql[5,k1-q4]]*
 prop[qk[7,k1+q1+q5]]*
 prop[GL[9,-k1+q3+q4]]*
 vrtx[QK[-2,-q1],GL[1,-k1],qk[3,k1+q1]]*
 vrtx[QL[6,-k1+q4],GL[2,k1],ql[-8,-q4]]*
 vrtx[QK[4,-k1-q1],GA[-10,-q5],qk[7,k1+q1+q5]]*
 vrtx[QL[-6,-q3],GL[9,-k1+q3+q4],ql[5,k1-q4]]*
 vrtx[QK[8,-k1-q1-q5],GL[10,k1-q3-q4],qk[-4,-q2]];

(* end *)

