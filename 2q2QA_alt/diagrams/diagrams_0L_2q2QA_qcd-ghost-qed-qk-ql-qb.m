(*
* file generated by qgraf-3.6.2
*
* output='diagrams_0L_2q2QA_qcd-ghost-qed-qk-ql-qb.m';
* style='../../styles/mathematica.sty';
* model='../../models/qcd-ghost-qed-qk-ql-qb.model';
* in=;
* out=qk,QK,ql,QL,GA;
* loops=0;
* loop_momentum=k;
* options=notadpole,onshell,nosnail;
* true = vsum[gpow,2,2];
*  true = vsum[epow,1,1];
*)
 
 QGRAFdiagram[1]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,q3+q5]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GA[-10,-q5],ql[3,q3+q5]]*
 vrtx[QL[4,-q3-q5],GL[2,-q1-q2],ql[-8,-q4]];
 
 QGRAFdiagram[2]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[GL[1,q1+q2]]*
 prop[ql[3,-q4-q5]]*
 vrtx[QK[-2,-q1],GL[1,q1+q2],qk[-4,-q2]]*
 vrtx[QL[4,q4+q5],GA[-10,-q5],ql[-8,-q4]]*
 vrtx[QL[-6,-q3],GL[2,-q1-q2],ql[3,-q4-q5]];
 
 QGRAFdiagram[3]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,q1+q5]]*
 prop[GL[3,q3+q4]]*
 vrtx[QK[-2,-q1],GA[-10,-q5],qk[1,q1+q5]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[2,-q1-q5],GL[4,-q3-q4],qk[-4,-q2]];
 
 QGRAFdiagram[4]:= (+1)*
 pol[qk[-2,q1]]*
 pol[QK[-4,q2]]*
 pol[ql[-6,q3]]*
 pol[QL[-8,q4]]*
 pol[GA[-10,q5]]*
 prop[qk[1,-q2-q5]]*
 prop[GL[3,q3+q4]]*
 vrtx[QK[2,q2+q5],GA[-10,-q5],qk[-4,-q2]]*
 vrtx[QL[-6,-q3],GL[3,q3+q4],ql[-8,-q4]]*
 vrtx[QK[-2,-q1],GL[4,-q3-q4],qk[1,-q2-q5]];

(* end *)

