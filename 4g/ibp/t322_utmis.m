(* Created with the Wolfram Language : www.wolfram.com *)
{mi[t322, 1] -> -(s23*j[t322, 0, 1, 0, 1, 1, 1, 1, 0, 0]) - 
  s23*j[t322, 0, 1, 1, 0, 1, 1, 1, 0, 0] + 
  s23*j[t322, 0, 1, 1, 1, 0, 1, 1, 0, 0] + 
  s12*j[t322, 0, 1, 1, 1, 1, 1, 0, 0, 0] + 
  s23*j[t322, 0, 1, 1, 1, 1, 1, 1, -1, 0] - 
  s12*(-j[t322, -1, 1, 1, 1, 1, 1, 1, 0, 0] + 
    j[t322, 0, 0, 1, 1, 1, 1, 1, 0, 0] + 
    j[t322, 0, 1, 1, 0, 1, 1, 1, 0, 0] + 
    j[t322, 0, 1, 1, 1, 1, 0, 1, 0, 0] - 
    j[t322, 0, 1, 1, 1, 1, 1, 1, 0, -1]) + 
  s12*s23*j[t322, 1, 1, 1, 0, 1, 1, 1, 0, 0] + 
  s12^2*(j[t322, 0, 1, 1, 1, 1, 1, 1, 0, 0] - 
    j[t322, 1, 1, 1, 1, 1, 0, 1, 0, 0] + 
    j[t322, 1, 1, 1, 1, 1, 1, 0, 0, 0] - 
    j[t322, 1, 1, 1, 1, 1, 1, 1, -1, 0] + 
    j[t322, 1, 1, 1, 1, 1, 1, 1, 0, -1] + 
    s23*j[t322, 1, 1, 1, 1, 1, 1, 1, 0, 0]), 
mi[t322, 2] -> -(s23*j[t322, 0, 1, 0, 1, 1, 1, 1, 0, 0]) - 
  s23*j[t322, 0, 1, 1, 0, 1, 1, 1, 0, 0] + 
  s23*j[t322, 0, 1, 1, 1, 1, 1, 1, -1, 0] + 
  s12*s23*j[t322, 1, 1, 1, 0, 1, 1, 1, 0, 0] - 
  s12*(j[t322, 0, 1, 1, 1, 1, 1, 1, -1, 0] - 
    j[t322, 1, 1, 1, 1, 1, 0, 1, -1, 0] + 
    j[t322, 1, 1, 1, 1, 1, 1, 0, -1, 0] - 
    j[t322, 1, 1, 1, 1, 1, 1, 1, -2, 0] + 
    j[t322, 1, 1, 1, 1, 1, 1, 1, -1, -1] + 
    s23*j[t322, 1, 1, 1, 1, 1, 1, 1, -1, 0]), 
mi[t322, 3] -> (s12^2 + s12*s23)*j[t322, 1, 1, 1, 0, 1, 1, 1, 0, 0], 
mi[t322, 4] -> -(s12*j[t322, 0, 1, 1, 1, 0, 1, 1, 0, 0]) + 
  s12*j[t322, 1, 1, 1, 1, 0, 1, 1, -1, 0], 
mi[t322, 5] -> -(s12*j[t322, 1, 0, 1, 1, 1, 1, 1, -1, 0]), 
mi[t322, 6] -> -(s12*j[t322, 1, 0, 1, 0, 1, 1, 1, 0, 0]), 
mi[t322, 7] -> -(s23*j[t322, 0, 1, 1, 1, 1, 1, 0, 0, 0]), 
mi[t322, 8] -> (-s12 - s23)*j[t322, 0, 1, 1, 0, 1, 1, 1, 0, 0], 
mi[t322, 9] -> (-s12 - s23)*j[t322, 0, 1, 1, 1, 0, 1, 1, 0, 0], 
mi[t322, 10] -> -(s23*j[t322, 0, 1, 1, 1, 1, 0, 1, 0, 0]), 
mi[t322, 11] -> -(s12*j[t322, 0, 1, 0, 1, 1, 1, 1, 0, 0]), 
mi[t322, 12] -> -(s12*j[t322, 0, 0, 1, 1, 1, 1, 1, 0, 0])}
